# Winprint Hylafax for Windows #

_(Brought to you by: [Michal Havranek](https://michalhavranek.bitbucket.io/))_

## Description ##
Simple Windows client for HylaFax server.
This is rewrited version of WinPrint HylaFax (winprinthylafax.sourceforge.net by Michael Stowe).  
Works on Windows 7, Windows 8, 10, Windows Server 2008,2012. And later

***
## Download latest version (rel. date 06/2021, size 1.97 MB): ##
[wphfxwin_inst-update_1.8.0.1.exe](https://bitbucket.org/michalhavranek/wphfxwin/downloads/wphfxwin_inst-update_1.8.0.1.exe)

```
#!c++
CRC32: 8165CAD6
MD5: BA6E5E7DE74658C063723BEF647064DC
SHA-1: 9B8181844DBB3D0F85CF7592A9AEC1069804A15E

```

***

## Project web (instructions): ##
https://michalhavranek.bitbucket.io/wphfx/

## Operating system: ##
Windows 32/64 bit

## Programming language: ##
C/C++, Win32API

***

![image009.png](readme_img/image009.png "Send FAX dialog") ![image14.png](readme_img/image14.png "Status dialog")
![sheet1.png](readme_img/sheet1.png "Configuration dialog-1")![sheet2.png](readme_img/sheet2.png "Configuration dialog-2")
![ldap_settings.png](readme_img/ldap_settings.png ""Configuration dialog-LDAP")![ldap_autocomplete.png](readme_img/ldap_autocomplete.png "using LDAP")


### readme ###
```
#!c++
v.1.8.0.1
------------------
* add IPv6 support
* clear source code

v.1.7.4.1
------------------
* insert debug information (not in release)
* delete critical section at end of monitor

v.1.7.3.1
------------------
* possibility to insert a fax number from the clipboard
* cleanup source code

v.1.7.2.1
------------------
* improve autocomplete search
* some changes in port monitor internal management

v.1.7.0.0
------------------
* only for Windows 7 SP1 and Windows Server 2008 and later
* add Identifier string [-i] to Send Fax dialog, JOBINFO parameter
* add logged in user as FROMUSER parameter


SW version:
Major release number.Minor release number.Maintenance release number.Build number
```