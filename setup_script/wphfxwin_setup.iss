; wphfxwin - setup for  32/64 bit

; version - "major.minor.release.build"
#define MyAppVersion "1.8.0.1"
#define MyAppName "WinPrint Hylafax for Windows"
#define RegPortSubkey "SYSTEM\CurrentControlSet\Control\Print\Monitors\Winprint Hylafax\Ports\HFAX1:"
#define WinRelease "WinPrintHylafax-for-Windows-Release"
#define Rel64 "Only-For-Windows-64bit"
#define Rel32 "Only-For-Windows-32bit"
#define PortMonitor "HylaPrintMon.dll"
#define PortGUI "HylaPrintUI.exe"
#define PortReg "HylaPrintMonReg.exe"
#define PortName "HFAX1:"
#define PrinterName "FAX-srv"

[Setup]
AppName={#MyAppName}
AppId={#MyAppName}
VersionInfoCopyright="2011-2021 Michal Havranek"
OutputDir=..\install\setup
OutputBaseFilename=wphfxwin_inst-update_{#MyAppVersion}
AppVersion={#MyAppVersion}
CreateAppDir=False
VersionInfoVersion={#MyAppVersion}
VersionInfoDescription=WinPrint HylaFax for 32/64 bit OS
VersionInfoTextVersion={#MyAppVersion}
VersionInfoProductName={#MyAppName}
VersionInfoProductVersion={#MyAppVersion}
VersionInfoProductTextVersion={#MyAppVersion}
ArchitecturesInstallIn64BitMode=x64
UsePreviousGroup=False
DisableProgramGroupPage=yes
DisableStartupPrompt=yes
AppPublisher=Michal Havranek
UninstallFilesDir={sys}
; min Win 7 SP1
MinVersion=6.1sp1
MergeDuplicateFiles=No
InfoBeforeFile=info_before_setup.txt
UninstallDisplayIcon={sys}\{#PortGUI}
SetupIconFile=..\HylaPrintUI\fax32-16.ico
;LicenseFile=..\LICENSE.GPLv2.txt
Compression=lzma2/max
SolidCompression=yes
PrivilegesRequired=admin
WizardStyle=modern

[Files]
;64 bit BRA
Source: "..\install\{#WinRelease}_BRA\{#Rel64}\{#PortMonitor}"; DestDir: "{sys}"; Flags: replacesameversion; Check: IsWin64; Languages: brazilianPortuguese
Source: "..\install\{#WinRelease}_BRA\{#Rel64}\{#PortGUI}"; DestDir: "{sys}"; Flags: replacesameversion; Check: IsWin64; Languages: brazilianPortuguese
;32 bit BRA
Source: "..\install\{#WinRelease}_BRA\{#Rel32}\{#PortMonitor}"; DestDir: "{sys}"; Flags: replacesameversion; Check: not IsWin64; Languages: brazilianPortuguese
Source: "..\install\{#WinRelease}_BRA\{#Rel32}\{#PortGUI}"; DestDir: "{sys}"; Flags: replacesameversion; Check: not IsWin64; Languages: brazilianPortuguese

;64 bit CZE
Source: "..\install\{#WinRelease}_CZE\{#Rel64}\{#PortMonitor}"; DestDir: "{sys}"; Flags: replacesameversion; Check: IsWin64; Languages: cesky
Source: "..\install\{#WinRelease}_CZE\{#Rel64}\{#PortGUI}"; DestDir: "{sys}"; Flags: replacesameversion; Check: IsWin64; Languages: cesky
;32 bit CZE
Source: "..\install\{#WinRelease}_CZE\{#Rel32}\{#PortMonitor}"; DestDir: "{sys}"; Flags: replacesameversion; Check: not IsWin64; Languages: cesky
Source: "..\install\{#WinRelease}_CZE\{#Rel32}\{#PortGUI}"; DestDir: "{sys}"; Flags: replacesameversion; Check: not IsWin64; Languages: cesky

;64 bit DUTCH
Source: "..\install\{#WinRelease}_DUTCH\{#Rel64}\{#PortMonitor}"; DestDir: "{sys}"; Flags: replacesameversion; Check: IsWin64; Languages: dutch
Source: "..\install\{#WinRelease}_DUTCH\{#Rel64}\{#PortGUI}"; DestDir: "{sys}"; Flags: replacesameversion; Check: IsWin64; Languages: dutch
;32 bit DUTCH
Source: "..\install\{#WinRelease}_DUTCH\{#Rel32}\{#PortMonitor}"; DestDir: "{sys}"; Flags: replacesameversion; Check: not IsWin64; Languages: dutch
Source: "..\install\{#WinRelease}_DUTCH\{#Rel32}\{#PortGUI}"; DestDir: "{sys}"; Flags: replacesameversion; Check: not IsWin64; Languages: dutch

;64 bit ENG
Source: "..\install\{#WinRelease}_ENG\{#Rel64}\{#PortMonitor}"; DestDir: "{sys}"; Flags: replacesameversion; Check: IsWin64; Languages: english
Source: "..\install\{#WinRelease}_ENG\{#Rel64}\{#PortGUI}"; DestDir: "{sys}"; Flags: replacesameversion; Check: IsWin64; Languages: english
;32 bit ENG
Source: "..\install\{#WinRelease}_ENG\{#Rel32}\{#PortMonitor}"; DestDir: "{sys}"; Flags: replacesameversion; Check: not IsWin64; Languages: english
Source: "..\install\{#WinRelease}_ENG\{#Rel32}\{#PortGUI}"; DestDir: "{sys}"; Flags: replacesameversion; Check: not IsWin64; Languages: english

;64 bit FRA
Source: "..\install\{#WinRelease}_FRA\{#Rel64}\{#PortMonitor}"; DestDir: "{sys}"; Flags: replacesameversion; Check: IsWin64; Languages: french
Source: "..\install\{#WinRelease}_FRA\{#Rel64}\{#PortGUI}"; DestDir: "{sys}"; Flags: replacesameversion; Check: IsWin64; Languages: french
;32 bit FRA
Source: "..\install\{#WinRelease}_FRA\{#Rel32}\{#PortMonitor}"; DestDir: "{sys}"; Flags: replacesameversion; Check: not IsWin64; Languages: french
Source: "..\install\{#WinRelease}_FRA\{#Rel32}\{#PortGUI}"; DestDir: "{sys}"; Flags: replacesameversion; Check: not IsWin64; Languages: french

;64 bit GER
Source: "..\install\{#WinRelease}_GER\{#Rel64}\{#PortMonitor}"; DestDir: "{sys}"; Flags: replacesameversion; Check: IsWin64; Languages: german
Source: "..\install\{#WinRelease}_GER\{#Rel64}\{#PortGUI}"; DestDir: "{sys}"; Flags: replacesameversion; Check: IsWin64; Languages: german
;32 bit GER
Source: "..\install\{#WinRelease}_GER\{#Rel32}\{#PortMonitor}"; DestDir: "{sys}"; Flags: replacesameversion; Check: not IsWin64; Languages: german
Source: "..\install\{#WinRelease}_GER\{#Rel32}\{#PortGUI}"; DestDir: "{sys}"; Flags: replacesameversion; Check: not IsWin64; Languages: german

;64 bit RUS
Source: "..\install\{#WinRelease}_RUS\{#Rel64}\{#PortMonitor}"; DestDir: "{sys}"; Flags: replacesameversion; Check: IsWin64; Languages: russian
Source: "..\install\{#WinRelease}_RUS\{#Rel64}\{#PortGUI}"; DestDir: "{sys}"; Flags: replacesameversion; Check: IsWin64; Languages: russian
;32 bit RUS
Source: "..\install\{#WinRelease}_RUS\{#Rel32}\{#PortMonitor}"; DestDir: "{sys}"; Flags: replacesameversion; Check: not IsWin64; Languages: russian
Source: "..\install\{#WinRelease}_RUS\{#Rel32}\{#PortGUI}"; DestDir: "{sys}"; Flags: replacesameversion; Check: not IsWin64; Languages: russian

;64 bit SPA
Source: "..\install\{#WinRelease}_SPA\{#Rel64}\{#PortMonitor}"; DestDir: "{sys}"; Flags: replacesameversion; Check: IsWin64; Languages: spanish
Source: "..\install\{#WinRelease}_SPA\{#Rel64}\{#PortGUI}"; DestDir: "{sys}"; Flags: replacesameversion; Check: IsWin64; Languages: spanish
;32 bit SPA
Source: "..\install\{#WinRelease}_SPA\{#Rel32}\{#PortMonitor}"; DestDir: "{sys}"; Flags: replacesameversion; Check: not IsWin64; Languages: spanish
Source: "..\install\{#WinRelease}_SPA\{#Rel32}\{#PortGUI}"; DestDir: "{sys}"; Flags: replacesameversion; Check: not IsWin64; Languages: spanish

;64 bit ITA
Source: "..\install\{#WinRelease}_ITA\{#Rel64}\{#PortMonitor}"; DestDir: "{sys}"; Flags: replacesameversion; Check: IsWin64; Languages: italian
Source: "..\install\{#WinRelease}_ITA\{#Rel64}\{#PortGUI}"; DestDir: "{sys}"; Flags: replacesameversion; Check: IsWin64; Languages: italian
;32 bit ITA
Source: "..\install\{#WinRelease}_ITA\{#Rel32}\{#PortMonitor}"; DestDir: "{sys}"; Flags: replacesameversion; Check: not IsWin64; Languages: italian
Source: "..\install\{#WinRelease}_ITA\{#Rel32}\{#PortGUI}"; DestDir: "{sys}"; Flags: replacesameversion; Check: not IsWin64; Languages: italian

; registration utility
;32 bit
Source: "..\install\portmon-registration\32bit\{#PortReg}"; DestDir: "{sys}"; Flags: replacesameversion; Check: not IsWin64;
;64 bit
Source: "..\install\portmon-registration\64bit\{#PortReg}"; DestDir: "{sys}"; Flags: replacesameversion; Check: IsWin64;

; configure printer port
; 64 bit
Source: "..\install\setup-configport\64bit\SetupConfigPrinterPort.exe"; DestDir: "{tmp}"; Check: IsWin64;
; 32 bit
Source: "..\install\setup-configport\32bit\SetupConfigPrinterPort.exe"; DestDir: "{tmp}"; Check: not IsWin64;

; Win 8 driver
Source: "..\install\Win8drvXeroxPhaser6120PS\amd64\*.*"; DestDir: "{tmp}\amd64_Win8_6120PS"; Flags: recursesubdirs createallsubdirs; Check: IsWin64;
Source: "..\install\Win8drvXeroxPhaser6120PS\x86\*.*"; DestDir: "{tmp}\x86_Win8_6120PS"; Flags: recursesubdirs createallsubdirs; Check: not IsWin64;

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"
Name: "german"; MessagesFile: "compiler:Languages\German.isl"
Name: "cesky"; MessagesFile: "compiler:Languages\Czech.isl"
Name: "dutch"; MessagesFile: "compiler:Languages\Dutch.isl"
Name: "french"; MessagesFile: "compiler:Languages\French.isl"
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"
Name: "brazilianPortuguese"; MessagesFile: "compiler:Languages\BrazilianPortuguese.isl"
Name: "spanish"; MessagesFile: "compiler:Languages\Spanish.isl"
Name: "italian"; MessagesFile: "compiler:Languages\Italian.isl"

; registry key for printer port
[Registry]
Root: "HKLM"; Subkey: "SYSTEM\CurrentControlSet\Control\Print\Monitors\Winprint Hylafax"; ValueType: none; Flags: createvalueifdoesntexist
Root: "HKLM"; Subkey: "SYSTEM\CurrentControlSet\Control\Print\Monitors\Winprint Hylafax"; ValueType: string; ValueName: "Driver"; ValueData: "{#PortMonitor}"; Flags: createvalueifdoesntexist
Root: "HKLM"; Subkey: "SYSTEM\CurrentControlSet\Control\Print\Monitors\Winprint Hylafax\Ports"; ValueType: none; Flags: createvalueifdoesntexist
; port HFAX1:
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: none; Flags: createvalueifdoesntexist
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: string; ValueName: "Description"; ValueData: "WinPrint Hylafax Port"; Flags: createvalueifdoesntexist
;1
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: string; ValueName: "Server"; ValueData: "{code:GetRegServer}"; Flags: createvalueifdoesntexist
;2
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: string; ValueName: "Username"; ValueData: "{code:GetRegUsername}"; Flags: createvalueifdoesntexist
;3
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: string; ValueName: "Password"; ValueData: "{code:GetRegPassword}"; Flags: createvalueifdoesntexist
;4
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: string; ValueName: "DefaultEmail"; ValueData: "{code:GetRegDefaultEmail}"; Flags: createvalueifdoesntexist
;5
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: string; ValueName: "Modem"; ValueData: "{code:GetRegModem}"; Flags: createvalueifdoesntexist
;6
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: string; ValueName: "AddressBookPath"; ValueData: "{code:GetRegAddressBookPath}"; Flags: createvalueifdoesntexist
;7
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: string; ValueName: "NotificationType"; ValueData: "{code:GetRegNotificationType}"; Flags: createvalueifdoesntexist
;8
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: string; ValueName: "PageSize"; ValueData: "{code:GetRegPageSize}"; Flags: createvalueifdoesntexist
;9
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: dword; ValueName: "IgnorePassiveIP"; ValueData: "{code:GetRegIgnorePassiveIP}"; Flags: createvalueifdoesntexist
;10
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: dword; ValueName: "UseActiveMode"; ValueData: "{code:GetRegUseActiveMode}"; Flags: createvalueifdoesntexist
;11
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: string; ValueName: "ActivePortNumber"; ValueData: "{code:GetRegActivePortNumber}"; Flags: createvalueifdoesntexist
;12
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: string; ValueName: "HylaFaxPortNumber"; ValueData: "{code:GetRegHylaFaxPortNumber}"; Flags: createvalueifdoesntexist
;13
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: string; ValueName: "Resolution"; ValueData: "{code:GetRegResolution}"; Flags: createvalueifdoesntexist
;18
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: dword; ValueName: "UseLDAP"; ValueData: "{code:GetRegUseLDAP}"; Flags: createvalueifdoesntexist
;19
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: string; ValueName: "LDAPServer"; ValueData: "{code:GetRegLDAPServer}"; Flags: createvalueifdoesntexist
;20
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: string; ValueName: "LDAPPort"; ValueData: "{code:GetRegLDAPPort}"; Flags: createvalueifdoesntexist
;21
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: dword; ValueName: "LDAPSSL"; ValueData: "{code:GetRegLDAPSSL}"; Flags: createvalueifdoesntexist
;22
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: string; ValueName: "LDAPBindDN"; ValueData: "{code:GetRegLDAPBindDN}"; Flags: createvalueifdoesntexist
;23
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: string; ValueName: "LDAPPassword"; ValueData: "{code:GetRegLDAPPassword}"; Flags: createvalueifdoesntexist
;24
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: string; ValueName: "LDAPBaseDN"; ValueData: "{code:GetRegLDAPBaseDN}"; Flags: createvalueifdoesntexist
;25
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: string; ValueName: "LDAPFilter"; ValueData: "{code:GetRegLDAPFilter}"; Flags: createvalueifdoesntexist
;26
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: string; ValueName: "LDAPAutocompleteAttribute"; ValueData: "{code:GetRegLDAPAutocompleteAttribute}"; Flags: createvalueifdoesntexist
;27
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: dword; ValueName: "LDAPStartTLS"; ValueData: "{code:GetRegLDAPStartTLS}"; Flags: createvalueifdoesntexist
;28
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: string; ValueName: "MaxDials"; ValueData: "{code:GetRegMaxDials}"; Flags: createvalueifdoesntexist
;29
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: string; ValueName: "MaxTries"; ValueData: "{code:GetRegMaxTries}"; Flags: createvalueifdoesntexist
;30
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: string; ValueName: "InfoDialogTimer"; ValueData: "{code:GetRegInfoDialogTimer}"; Flags: createvalueifdoesntexist
;31
Root: "HKLM"; Subkey: "{#RegPortSubkey}"; ValueType: dword; ValueName: "FaxNumberFromClipboard"; ValueData: "{code:GetRegFaxNumberFromClipboard}"; Flags: createvalueifdoesntexist

[Run]
; register printer port
Filename: "{sys}\{#PortReg}"; Parameters: "install"; AfterInstall: Sleep(1000);
; restart spooler
Filename: "net"; Parameters: "stop spooler"; AfterInstall: Sleep(500);
Filename: "net"; Parameters: "start spooler"; AfterInstall: Sleep(500);
; install printer Win 7
Filename: "rundll32"; Parameters: "printui.dll,PrintUIEntry /if /w /b ""{code:GetPrinterName}"" /f {win}\inf\ntprint.inf /r ""{#PortName}"" /m ""Xerox Phaser 6120 PS"""; StatusMsg: "Installing FAX Printer for HylaFax..."; MinVersion: 6.1; OnlyBelowVersion: 6.2; Check: not IsServer; Flags: waituntilterminated shellexec;
; install printer Win Server 2008
Filename: "rundll32"; Parameters: "printui.dll,PrintUIEntry /if /w /b ""{code:GetPrinterName}"" /f {win}\inf\ntprint.inf /r ""{#PortName}"" /m ""Xerox Phaser 6250B PS"""; StatusMsg: "Installing FAX Printer for HylaFax..."; MinVersion: 6.1; OnlyBelowVersion: 6.2; Check: IsServer; Flags: waituntilterminated shellexec;
; install driver and printer Win 8, Win Server 2012
Filename: "rundll32"; Parameters: "printui.dll,PrintUIEntry /ia /m ""Xerox Phaser 6120 PS"" /b ""Xerox Phaser 6120 PS"" /f {tmp}\amd64_Win8_6120PS\prnxx002.inf /u"; StatusMsg: "Installing driver for FAX printer..."; MinVersion: 6.2; Check: IsWin64; Flags: waituntilterminated shellexec;
Filename: "rundll32"; Parameters: "printui.dll,PrintUIEntry /ia /m ""Xerox Phaser 6120 PS"" /b ""Xerox Phaser 6120 PS"" /f {tmp}\x86_Win8_6120PS\prnxx002.inf /u"; StatusMsg: "Installing driver for FAX printer..."; MinVersion: 6.2; Check: not IsWin64; Flags: waituntilterminated shellexec;
Filename: "rundll32"; Parameters: "printui.dll,PrintUIEntry /if /b ""{code:GetPrinterName}"" /f {win}\inf\ntprint.inf /r ""{#PortName}"" /m ""Xerox Phaser 6120 PS"""; StatusMsg: "Installing FAX Printer for HylaFax..."; MinVersion: 6.2; Flags: waituntilterminated shellexec;

;configure port dialog
Filename: "{tmp}\SetupConfigPrinterPort.exe"; StatusMsg: "Configure - HylaFax settings"; Check: not WizardSilent;

[UninstallRun]
; delete printer
; printer was uninstalled by InitializeUninstall

; unregister printer port
Filename: "{sys}\{#PortReg}"; Parameters: "uninstall"; AfterInstall: Sleep(1500); RunOnceId: "delete_printer"
; pause
Filename: "{sys}\ping.exe"; Parameters: "-n 2 127.0.0.1" ; RunOnceId: "delete_printer"

; script section
[Code]
// global variables
var 
  // user page
  UserPage: TInputQueryWizardPage;
  // settings data
  RegServer: string;                 // 1
  RegUsername: string;               // 2
  RegPassword: string;               // 3
  RegDefaultEmail: string;           // 4
  RegModem: string;                  // 5
  RegAddressBookPath: string;        // 6
  RegNotificationType: string;       // 7
  RegPageSize: string;               // 8
  RegIgnorePassiveIP: Cardinal;      // 9
  RegUseActiveMode: Cardinal;        // 10
  RegActivePortNumber: string;       // 11
  RegHylaFaxPortNumber: string;      // 12
  RegResolution: string;             // 13
  RegUseLDAP: Cardinal;              // 18
  RegLDAPServer: string;             // 19
  RegLDAPPort: string;               // 20
  RegLDAPSSL: Cardinal;              // 21
  RegLDAPBindDN: string;             // 22
  RegLDAPPassword: string;           // 23
  RegLDAPBaseDN: string;             // 24
  RegLDAPFilter: string;             // 25
  RegLDAPAutocompleteAttribute: string; // 26
  RegLDAPStartTLS: Cardinal;         // 27
  RegMaxDials: string;               // 28
  RegMaxTries: string;               // 29
  RegInfoDialogTimer: string;        // 30
  RegFaxNumberFromClipboard : Cardinal;  // 31

// initial default values
procedure InitialDefaultData(); 
begin 
  RegServer := '192.168.1.1';     // 1
  RegUsername := 'fax';           // 2
  RegPassword := '';              // 3
  RegDefaultEmail := '';          // 4
  RegModem := '';                 // 5
  RegAddressBookPath := '';       // 6
  RegNotificationType := 'Failure and Success'; // 7
  RegPageSize := 'A4';            // 8
  RegIgnorePassiveIP := 0;        // 9
  RegUseActiveMode := 0;          // 10
  RegActivePortNumber := '32001'; // 11
  RegHylaFaxPortNumber := '4559'; // 12
  RegResolution := 'Fine';        // 13
  RegUseLDAP := 0                 // 18
  RegLDAPServer := '';            // 19
  RegLDAPPort := '389';           // 20
  RegLDAPSSL := 0;                // 21
  RegLDAPBindDN := '';            // 22
  RegLDAPPassword := '';          // 23
  RegLDAPBaseDN := 'dc=example,dc=com';            // 24
  RegLDAPFilter := '(&(objectClass=person)(cn=%s))';// 25
  RegLDAPAutocompleteAttribute := 'cn' // 26
  RegLDAPStartTLS := 0;           // 27
  RegMaxDials := '-';             // 28
  RegMaxTries := '-';             // 29
  RegInfoDialogTimer := '3'       // 30
  RegFaxNumberFromClipboard := 0;   // 31
end;

// get registry values
// 1
function GetRegServer(Value: string): string;
begin
  Result := RegServer;
end;
// 2
function GetRegUsername(Value: string): string;
begin
  Result := RegUsername;
end;
// 3
function GetRegPassword(Value: string): string;
begin
  Result := RegPassword;
end;
// 4
function GetRegDefaultEmail(Value: string): string;
begin
  Result := RegDefaultEmail;
end;
// 5
function GetRegModem(Value: string): string;
begin
  Result := RegModem;
end;
// 6
function GetRegAddressBookPath(Value: string): string;
begin
  Result := RegAddressBookPath;
end;
// 7
function GetRegNotificationType(Value: string): string;
begin
  Result := RegNotificationType;
end;
// 8
function GetRegPageSize(Value: string): string;
begin
  Result := RegPageSize;
end;
// 9
function GetRegIgnorePassiveIP(Value: string): string;
begin
  Result := IntToStr(RegIgnorePassiveIP);
end;
// 10
function GetRegUseActiveMode(Value: string): string;
begin
  Result := IntToStr(RegUseActiveMode);
end;
// 11
function GetRegActivePortNumber(Value: string): string;
begin
  Result := RegActivePortNumber;
end;
// 12
function GetRegHylaFaxPortNumber(Value: string): string;
begin
  Result := RegHylaFaxPortNumber;
end;
// 13
function GetRegResolution(Value: string): string;
begin
  Result := RegResolution;
end;
// 18
function GetRegUseLDAP(Value: string): string;
begin
  Result := IntToStr(RegUseLDAP);
end;
// 19
function GetRegLDAPServer(Value: string): string;
begin
  Result := RegLDAPServer;
end;
// 20
function GetRegLDAPPort(Value: string): string;
begin
  Result := RegLDAPPort;
end;
// 21
function GetRegLDAPSSL(Value: string): string;
begin
  Result := IntToStr(RegLDAPSSL);
end;
// 22
function GetRegLDAPBindDN(Value: string): string;
begin
  Result := RegLDAPBindDN;
end;
// 23
function GetRegLDAPPassword(Value: string): string;
begin
  Result := RegLDAPPassword;
end;
// 24
function GetRegLDAPBaseDN(Value: string): string;
begin
  Result := RegLDAPBaseDN;
end;
// 25
function GetRegLDAPFilter(Value: string): string;
begin
  Result := RegLDAPFilter;
end;
// 26
function GetRegLDAPAutocompleteAttribute(Value: string): string;
begin
  Result := RegLDAPAutocompleteAttribute;
end;
// 27
function GetRegLDAPStartTLS(Value: string): string;
begin
  Result := IntToStr(RegLDAPStartTLS);
end;
// 28
function GetRegMaxDials(Value: string): string;
begin
  Result := RegMaxDials;
end;
// 29
function GetRegMaxTries(Value: string): string;
begin
  Result := RegMaxTries;
end;
// 30
function GetRegInfoDialogTimer(Value: string): string;
begin
  Result := RegInfoDialogTimer;
end;
// 31
function GetRegFaxNumberFromClipboard(Value: string): string;
begin
  Result := IntToStr(RegFaxNumberFromClipboard);
end;

// save data for upgrade
procedure SetOldData();
begin
RegQueryStringValue(HKLM,'{#RegPortSubkey}','Server', RegServer);                             // 1
RegQueryStringValue(HKLM,'{#RegPortSubkey}','Username', RegUsername);                         // 2
RegQueryStringValue(HKLM,'{#RegPortSubkey}','Password', RegPassword);                         // 3
RegQueryStringValue(HKLM,'{#RegPortSubkey}','DefaultEmail', RegDefaultEmail);                 // 4
RegQueryStringValue(HKLM,'{#RegPortSubkey}','Modem', RegModem);                               // 5
RegQueryStringValue(HKLM,'{#RegPortSubkey}','AddressBookPath', RegAddressBookPath);           // 6
RegQueryStringValue(HKLM,'{#RegPortSubkey}','NotificationType', RegNotificationType);         // 7
RegQueryStringValue(HKLM,'{#RegPortSubkey}','PageSize', RegPageSize);                         // 8
RegQueryDWordValue(HKLM,'{#RegPortSubkey}','IgnorePassiveIP', RegIgnorePassiveIP);            // 9
RegQueryDWordValue(HKLM,'{#RegPortSubkey}','UseActiveMode', RegUseActiveMode);                // 10
RegQueryStringValue(HKLM,'{#RegPortSubkey}','ActivePortNumber', RegActivePortNumber);         // 11
RegQueryStringValue(HKLM,'{#RegPortSubkey}','HylaFaxPortNumber', RegHylaFaxPortNumber);       // 12
RegQueryStringValue(HKLM,'{#RegPortSubkey}','Resolution', RegResolution);                     // 13
RegQueryDWordValue(HKLM,'{#RegPortSubkey}','UseLDAP', RegUseLDAP);                            // 18
RegQueryStringValue(HKLM,'{#RegPortSubkey}','LDAPServer', RegLDAPServer);                     // 19
RegQueryStringValue(HKLM,'{#RegPortSubkey}','LDAPPort', RegLDAPPort);                         // 20
RegQueryDWordValue(HKLM,'{#RegPortSubkey}','LDAPSSL', RegLDAPSSL);                            // 21
RegQueryStringValue(HKLM,'{#RegPortSubkey}','LDAPBindDN', RegLDAPBindDN);                     // 22
RegQueryStringValue(HKLM,'{#RegPortSubkey}','LDAPPassword', RegLDAPPassword);                 // 23
RegQueryStringValue(HKLM,'{#RegPortSubkey}','LDAPBaseDN', RegLDAPBaseDN);                     // 24
RegQueryStringValue(HKLM,'{#RegPortSubkey}','LDAPFilter', RegLDAPFilter);                     // 25
RegQueryStringValue(HKLM,'{#RegPortSubkey}','LDAPAutocompleteAttribute', RegLDAPAutocompleteAttribute); // 26
RegQueryDWordValue(HKLM,'{#RegPortSubkey}','LDAPStartTLS', RegLDAPStartTLS);                  // 27
RegQueryStringValue(HKLM,'{#RegPortSubkey}','MaxDials', RegMaxDials);                         // 28
RegQueryStringValue(HKLM,'{#RegPortSubkey}','MaxTries', RegMaxTries);                         // 29
RegQueryStringValue(HKLM,'{#RegPortSubkey}','InfoDialogTimer', RegInfoDialogTimer);           // 30
RegQueryDWordValue(HKLM,'{#RegPortSubkey}','FaxNumberFromClipboard', RegFaxNumberFromClipboard);   // 31
end;

// detect server/workstation
function IsServer: Boolean;
var
  Version: TWindowsVersion;
begin
  GetWindowsVersionEx(Version);
  
  // Allow installation on domain controllers or servers
  if (Version.ProductType = VER_NT_DOMAIN_CONTROLLER) or
     (Version.ProductType = VER_NT_SERVER) then
  begin
    Result := True;
    Exit;
  end;
  Result := False;
end;

// detect prev install
// *****************************************
function GetNumber(var temp: String): Integer;
var
  part: String;
  pos1: Integer;
begin
  if Length(temp) = 0 then
  begin
    Result := -1;
    Exit;
  end;
    pos1 := Pos('.', temp);
    if (pos1 = 0) then
    begin
      Result := StrToInt(temp);
    temp := '';
    end
    else
    begin
    part := Copy(temp, 1, pos1 - 1);
      temp := Copy(temp, pos1 + 1, Length(temp));
      Result := StrToInt(part);
    end;
end;

// *****************************************
function CompareInner(var temp1, temp2: String): Integer;
var
  num1, num2: Integer;
begin
  num1 := GetNumber(temp1);
  num2 := GetNumber(temp2);
  if (num1 = -1) or (num2 = -1) then
  begin
    Result := 0;
    Exit;
  end;
      if (num1 > num2) then
      begin
        Result := 1;
      end
      else if (num1 < num2) then
      begin
        Result := -1;
      end
      else
      begin
        Result := CompareInner(temp1, temp2);
      end;
end;

// ***************************************** 
function CompareVersion(str1, str2: String): Integer;
var
  temp1, temp2: String;
begin
  temp1 := str1;
  temp2 := str2;
  Result := CompareInner(temp1, temp2);
end;

// ***************************************** 
function InitializeSetup(): Boolean;
var
  oldVersion: String;
  uninstaller: String;
  ErrorCode: Integer;
begin
  InitialDefaultData();
  if RegKeyExists(HKEY_LOCAL_MACHINE,
    'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{#MyAppName}_is1') then
  begin
    RegQueryStringValue(HKEY_LOCAL_MACHINE,
      'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{#MyAppName}_is1',
      'DisplayVersion', oldVersion);
    
    if (CompareVersion(oldVersion, '{#MyAppVersion}') < 0) then
    begin
        if WizardSilent() = false then
        begin
            if MsgBox('Version ' + oldVersion + ' of {#MyAppName} is already installed. Continue to upgrade to version {#MyAppVersion}?',
               mbConfirmation, MB_YESNO) = IDNO then
            begin
               Result := False;
            end
            else  // if MsgBox (...
            begin
               RegQueryStringValue(HKEY_LOCAL_MACHINE,
               'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{#MyAppName}_is1',
               'UninstallString', uninstaller);
               SetOldData();
               ShellExec('runas', uninstaller, '/SILENT', '', SW_HIDE, ewWaitUntilTerminated, ErrorCode);
               Result := True;
            end; // if MsgBox (...
        end
       
        else  // if WizardSilent()...
        begin
          RegQueryStringValue(HKEY_LOCAL_MACHINE,
          'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{#MyAppName}_is1',
          'UninstallString', uninstaller);
          SetOldData();
          ShellExec('runas', uninstaller, '/SILENT', '', SW_HIDE, ewWaitUntilTerminated, ErrorCode);
        Result := True;
        end; // if WizardSilent()...
    end
    
    else  // if (CompareVersion(...
    begin
      MsgBox('A newer or the same version of {#MyAppName} is already installed (' + oldVersion + '). This installer will exit.',
        mbInformation, MB_OK);
      Result := False;
    end;  // if (CompareVersion(...

  end  
  else  // if RegKeyExists (...
  begin
    Result := True;
  end; // if RegKeyExists (...
end;
// *****************************************
// port name from multistring
function GetPortName(MultiSzStr :String):String;
var
  I,StartPos:Integer;
  Element:String;
begin
  StartPos := 1;
  for I:= 1 to Length(MultiSzStr) do
  begin
    if MultiSzStr[I] = #0 then
    begin
      Element := Copy(MultiSzStr, StartPos, I - StartPos);
      Result := Element;
      exit;
    end;
  end
end;
// *****************************************
// get real printer name for uninstall
function InitializeUninstall(): Boolean;
var
  Names: TArrayOfString;
  I,cmp: Integer;
  S: String;
  subKey: String;
  portname: String;
  ErrorCode: Integer;
begin
  subKey := 'SOFTWARE\Microsoft\Windows NT\CurrentVersion\Print\Printers';
  if RegGetSubkeyNames(HKEY_LOCAL_MACHINE, subKey, Names) then
  begin
    S := '';
    for I := 0 to GetArrayLength(Names)-1 do
    begin
      S := subKey + '\' + Names[I] + '\DsSpooler';
      if(RegQueryMultiStringValue(HKEY_LOCAL_MACHINE, S,'portName',portname)) then
      begin
        portname := GetPortName(portname); 
        cmp := CompareText('{#PortName}',portname);
        if(cmp = 0) then
        begin
          ShellExec('','rundll32', 'printui.dll,PrintUIEntry /dl /n "' + Names[I] + '"', '',SW_HIDE, ewWaitUntilTerminated,ErrorCode);
          Result := True;
          exit;
        end;
      end
    end;
  end;
  ShellExec('','rundll32', 'printui.dll,PrintUIEntry /dl /n "' + '{#PrinterName}' + '"', '',SW_HIDE, ewWaitUntilTerminated,ErrorCode);
  Result := True;
end;
// *****************************************
// set printer name
procedure InitializeWizard;
begin
 UserPage := CreateInputQueryPage(wpReady,
  'Printer Information', 'Printer name',
  'Specify the printer name, then click Next.');
UserPage.Add('Name:', False);
UserPage.Values[0] := '{#PrinterName}';
end;
// *****************************************
// validate func
function NextButtonClick(CurPageID: Integer): Boolean;
begin
  if CurPageID = UserPage.ID then begin
    if UserPage.Values[0] = '' then begin
      MsgBox('You must enter a valid printer name.', mbError, MB_OK);
      Result := False;
      exit;
    end
  end;
  Result := True;
end;
// *****************************************
// get printer name
function GetPrinterName(Param: String): String;
begin
  Result := UserPage.Values[0];
end;
