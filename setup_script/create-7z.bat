@ echo off
:: copy 32bit portmon registration utility 
echo(
echo Copying 32 bit files regmon utility
echo(
for /D %%a in ("..\install\WinPrintHylafax-for-Windows-Release_*") do xcopy /F /R /Y "..\install\portmon-registration\32bit\HylaPrintMonReg.exe" "%%a\Only-For-Windows-32bit\"
:: copy 64bit portmon registration utility
echo(
echo Copying 64 bit files regmon utility
echo(
for /D %%a in ("..\install\WinPrintHylafax-for-Windows-Release_*") do xcopy /F /R /Y "..\install\portmon-registration\64bit\HylaPrintMonReg.exe" "%%a\Only-For-Windows-64bit\"
echo(
:: create 7z package
echo Create 7z package
7z.exe a  ../install/WPHylafax-for-Win_1.8.0.1.7z ../install/WinPrintHylafax-for-Windows-Release_* readme.txt ../LICENSE.GPLv2.txt
pause
