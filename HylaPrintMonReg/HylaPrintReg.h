#pragma once

#define ID_INSTALL       2
#define ID_UNINSTALL     3
#define ID_RESTART       4
#define ID_CLEAR         5

#include "resource.h"

#define TextUninstall L"FAX printer must be deleted\nbefore uninstall print monitor"
#define TextRestartButton L"Restart Spooler"
#define TextClearQueue L"Clear Printer Queue"
#define TextWorking L"Working..."

#ifdef WIN64
#define TextInstall   L"Register/Unregister print monitor"
#else
#define TextInstall   L"Register/Unregister print monitor"
#endif

void GetVersionReg(HWND hDlg);
void RestartSpooler(PVOID pvoid);
void StartProcess(LPTSTR str);
void ClearPrinterQueue(PVOID pvoid);
BOOL Is64BitWindows();
void ErrorMessage(DWORD dw);

class CForm1
{
public:
	CForm1();
	~CForm1();
	void InstallClick(HWND hwnd, BOOL interactive);
	void DeinstallClick(HWND hwnd, BOOL interactive);
private:	// User declarations
	BOOL Interactive;
};
CForm1* Class1;
//---------------------------------------------------------------------------
