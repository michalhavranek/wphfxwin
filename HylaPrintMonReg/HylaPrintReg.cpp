// HylaPrintReg.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "HylaPrintReg.h"

#define MAX_LOADSTRING 100
#define WINDOWS_WIDTH  370
#define WINDOWS_HEIGHT 210
#define BTN_HEIGHT     30
#define BTN_WIDTH      120
// Global Variables:
HINSTANCE hInst;								// current instance
WCHAR szTitle[MAX_LOADSTRING];					// The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

MONITOR_INFO_2 mi2;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
	_In_opt_  HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

#ifndef WIN64 // 32-bit application
	if (Is64BitWindows()) // running on 64-bit OS
	{
		MessageBox(NULL, L"Only for 32-bit Operating System\nThe application will be closed.", L"Info", MB_OK | MB_ICONERROR);
		return FALSE;
	}
#endif

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_PORT_REGISTRATION, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PORT_REGISTRATION));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	INITCOMMONCONTROLSEX icc;
	icc.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icc.dwICC = ICC_WIN95_CLASSES;
	if (!InitCommonControlsEx(&icc))
		return FALSE;

	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PORT_REGISTRATION));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCE(IDC_PORT_REGISTRATION);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;

	hInst = hInstance; // Store instance handle in our global variable

	hWnd = CreateWindow(szWindowClass, szTitle, WS_BORDER | WS_MINIMIZEBOX | WS_SYSMENU,
		CW_USEDEFAULT, 0, WINDOWS_WIDTH, WINDOWS_HEIGHT, NULL, NULL, hInst, NULL);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	HWND hButInstall, hButUninstall, hButRestart;
	HWND hClearQueue;
	static RECT rect1, rect2, rect3;
	int argCount;
	LPWSTR* szArgList;

	switch (message)
	{
	case WM_CREATE:
	{
		Class1 = new CForm1();
		szArgList = CommandLineToArgvW(GetCommandLine(), &argCount);
		if (argCount == 2 && _tcscmp(L"install", szArgList[1]) == 0)
			Class1->InstallClick(hWnd, FALSE);
		if (argCount == 2 && _tcscmp(L"uninstall", szArgList[1]) == 0)
			Class1->DeinstallClick(hWnd, FALSE);

		hButInstall = CreateWindow(L"BUTTON", L"INSTALL", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
			((WINDOWS_WIDTH / 2) - BTN_WIDTH) / 2, 30, BTN_WIDTH, BTN_HEIGHT,
			hWnd, (HMENU)ID_INSTALL, hInst, NULL);
		hButUninstall = CreateWindow(L"BUTTON", L"UNINSTALL", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
			((WINDOWS_WIDTH / 2) - BTN_WIDTH) / 2, 100, BTN_WIDTH, BTN_HEIGHT,
			hWnd, (HMENU)ID_UNINSTALL, hInst, NULL);
		hButRestart = CreateWindow(L"BUTTON", TextRestartButton, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
			((3 * WINDOWS_WIDTH) - (2 * BTN_WIDTH)) / 4, 30, BTN_WIDTH, BTN_HEIGHT,
			hWnd, (HMENU)ID_RESTART, hInst, NULL);
		hClearQueue = CreateWindow(L"BUTTON", TextClearQueue, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
			((3 * WINDOWS_WIDTH) - (2 * BTN_WIDTH)) / 4, 100, BTN_WIDTH, BTN_HEIGHT,
			hWnd, (HMENU)ID_CLEAR, hInst, NULL);
		HFONT hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
		SendMessage(hButInstall, WM_SETFONT, (WPARAM)hFont, (LPARAM)TRUE);
		SendMessage(hButUninstall, WM_SETFONT, (WPARAM)hFont, (LPARAM)TRUE);
		SendMessage(hButRestart, WM_SETFONT, (WPARAM)hFont, (LPARAM)TRUE);
		SendMessage(hClearQueue, WM_SETFONT, (WPARAM)hFont, (LPARAM)TRUE);

		rect1.bottom = 30;
		rect1.left = 2;
		rect1.right = WINDOWS_WIDTH / 2;
		rect1.top = 5;

		rect2.bottom = 80;
		rect2.left = 2;
		rect2.right = WINDOWS_WIDTH / 2;
		rect2.top = 65;

		rect3.bottom = 30;
		rect3.left = WINDOWS_WIDTH / 2;
		rect3.right = WINDOWS_WIDTH - 2;
		rect3.top = 5;
	}
	break;

	case WM_CLOSE:
		DestroyWindow(hWnd);
		break;

	case WM_COMMAND:
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case ID_INSTALL:
			Class1->InstallClick(hWnd, TRUE);
			break;
		case ID_UNINSTALL:
			Class1->DeinstallClick(hWnd, TRUE);
			break;
		case ID_RESTART:
			EnableWindow(GetDlgItem(hWnd, ID_RESTART), FALSE);
			EnableWindow(GetDlgItem(hWnd, ID_CLEAR), FALSE);
			SetWindowText(GetDlgItem(hWnd, ID_RESTART), TextWorking);
			_beginthread(RestartSpooler, 0, (PVOID)(hWnd));
			break;
		case ID_CLEAR:
			EnableWindow(GetDlgItem(hWnd, ID_RESTART), FALSE);
			EnableWindow(GetDlgItem(hWnd, ID_CLEAR), FALSE);
			SetWindowText(GetDlgItem(hWnd, ID_CLEAR), TextWorking);
			_beginthread(ClearPrinterQueue, 0, (PVOID)(hWnd));
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;

	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		SetBkMode(hdc, TRANSPARENT); // text transparent
		SelectObject(hdc, GetStockObject(DEFAULT_GUI_FONT));  // syst. font
		DrawText(hdc, TextInstall, -1, &rect1, DT_NOCLIP | DT_CENTER);
		DrawText(hdc, TextUninstall, -1, &rect2, DT_NOCLIP | DT_CENTER);
		DrawText(hdc, L"Special functions", -1, &rect3, DT_NOCLIP | DT_CENTER);
		MoveToEx(hdc, WINDOWS_WIDTH / 2, 0, NULL);
		LineTo(hdc, WINDOWS_WIDTH / 2, WINDOWS_HEIGHT);
		EndPaint(hWnd, &ps);
		break;
		// free memory
	case WM_DESTROY:
		delete Class1; // set pointer to null no needed
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}
// **********************************************************************
// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		GetVersionReg(hDlg);
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
// **********************************************************************
void CForm1::InstallClick(HWND hwnd, BOOL interactive)
{
	DWORD dwError;

	mi2.pName = L"Winprint Hylafax";
#ifdef WIN64
	mi2.pEnvironment = L"Windows x64";
#else
	mi2.pEnvironment = L"Windows NT x86";
#endif
	mi2.pDLLName = L"HylaPrintMon.dll";

	Interactive = interactive;

	if (GetFileAttributes(L"C:\\Windows\\System32\\HylaPrintMon.dll") == INVALID_FILE_ATTRIBUTES)
	{
		MessageBox(HWND_DESKTOP, L"Missing file HylaPrintMon.dll in SYSTEM32 directory", L"Error", MB_OK);
		return;
	}
	if (GetFileAttributes(L"C:\\Windows\\System32\\HylaPrintUI.exe") == INVALID_FILE_ATTRIBUTES)
	{
		MessageBox(HWND_DESKTOP, L"Missing file HylaPrintUI.exe in SYSTEM32 directory", L"Error", MB_OK);
		return;
	}

	if (!AddMonitor(NULL, 2, (LPBYTE)&mi2))
	{
		dwError = GetLastError();
		ErrorMessage(dwError);
		if (!Interactive) // install by installer
			PostMessage(hwnd, WM_CLOSE, 0, 0);
	}
	else
	{
		if (Interactive) // button click
			MessageBox(HWND_DESKTOP, L"INSTALL - OK", L"SUCCESS", MB_OK);
		else // install by installer
			PostMessage(hwnd, WM_CLOSE, 0, 0);
	}
}
//---------------------------------------------------------------------------
void CForm1::DeinstallClick(HWND hwnd, BOOL interactive)
{
	DWORD dwError;

	mi2.pName = L"Winprint Hylafax";
#ifdef WIN64
	mi2.pEnvironment = L"Windows x64";
#else
	mi2.pEnvironment = L"Windows NT x86";
#endif
	mi2.pDLLName = L"HylaPrintMon.dll";

	Interactive = interactive;
	if (!DeleteMonitor(NULL, mi2.pEnvironment, mi2.pName))
	{
		dwError = GetLastError();
		ErrorMessage(dwError);
		if (!Interactive)
			PostMessage(hwnd, WM_CLOSE, 0, 0);
	}
	else
	{
		if (Interactive)
			MessageBox(HWND_DESKTOP, L"UNINSTALL - OK", L"SUCCESS", MB_OK);
		else
			PostMessage(hwnd, WM_CLOSE, 0, 0);
	}
}
//---------------------------------------------------------------------------
CForm1::CForm1()
{
	Interactive = FALSE;
}
CForm1::~CForm1()
{
}
//*********************************************************
void GetVersionReg(HWND hDlg)
{
	DWORD dwVerInfoSize;
	DWORD dwHandle = 0;
	LPWSTR szDir;
	int iBufSize = 512;
	WCHAR szVerFile[64];
	WCHAR* pTemp;
	VS_FIXEDFILEINFO* pBuff;
	UINT nInfoLen;

	szDir = (LPWSTR)HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS,
		iBufSize * sizeof(WCHAR));
	if (szDir)
	{
		szDir[0] = 0;
		if (GetModuleFileName(hInst, szDir, iBufSize - 1) > 0)
		{
			dwVerInfoSize = GetFileVersionInfoSize(szDir, &dwHandle);
			pTemp = (WCHAR*)malloc(dwVerInfoSize);
			if (pTemp)
			{
				GetFileVersionInfo(szDir, (DWORD)NULL, dwVerInfoSize, pTemp);
				VerQueryValue(pTemp, L"\\", (LPVOID*)&pBuff, &nInfoLen);

				StringCbPrintf(szVerFile, sizeof(szVerFile), L"v.%d.%d.%d.%d - %s",
					HIWORD(pBuff->dwFileVersionMS), // major
					LOWORD(pBuff->dwFileVersionMS), // minor
					HIWORD(pBuff->dwFileVersionLS), // release
					LOWORD(pBuff->dwFileVersionLS), // build
					_T(__DATE__));                  // release date
				SetWindowText(GetDlgItem(hDlg, IDC_STATIC_VERSION), szVerFile);
				free(pTemp);
			}
		}

		// free memory
		HeapFree(GetProcessHeap(), 0, szDir);
	}
}
// *********************************************************
// restart printer spooler
void RestartSpooler(PVOID pvoid)
{
	HWND hwnd;
	TCHAR cmdStop[] = L"net stop spooler";
	TCHAR cmdStart[] = L"net start spooler";

	StartProcess(cmdStop); // stop spooler
	StartProcess(cmdStart); // start spooler

	hwnd = (HWND)(PVOID)pvoid;
	EnableWindow(GetDlgItem(hwnd, ID_RESTART), TRUE);
	EnableWindow(GetDlgItem(hwnd, ID_CLEAR), TRUE);
	SetWindowText(GetDlgItem(hwnd, ID_RESTART), TextRestartButton);
}
// *********************************************************
// Clear printer queue
void ClearPrinterQueue(PVOID pvoid)
{
	HWND hwnd;
	TCHAR cmdStop[] = L"net stop spooler";
	TCHAR cmdStart[] = L"net start spooler";
	TCHAR cmdDel1[] = L"cmd /c del %systemroot%\\system32\\spool\\printers\\*.shd";
	TCHAR cmdDel2[] = L"cmd /c del %systemroot%\\system32\\spool\\printers\\*.spl";

	StartProcess(cmdStop); // stop spooler
	StartProcess(cmdDel1); // delete print job files
	StartProcess(cmdDel2); // delete print job files
	StartProcess(cmdStart); // start spooler

	hwnd = (HWND)(PVOID)pvoid;
	EnableWindow(GetDlgItem(hwnd, ID_RESTART), TRUE);
	EnableWindow(GetDlgItem(hwnd, ID_CLEAR), TRUE);
	SetWindowText(GetDlgItem(hwnd, ID_CLEAR), TextClearQueue);
}
// *********************************************************
// start process
void StartProcess(LPTSTR str)
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));
	// Start the child process. 
	if (!CreateProcess(NULL, // No module name (use command line). 
		str,             // Command line. 
		NULL,                // Process handle not inheritable. 
		NULL,                // Thread handle not inheritable. 
		FALSE,               // handle inheritance 
		CREATE_NO_WINDOW,    // creation flags. 
		NULL,                // Use parent's environment block. 
		NULL,                // starting directory. 
		&si,                 // Pointer to STARTUPINFO structure.
		&pi)                 // Pointer to PROCESS_INFORMATION structure.
		)
		return;
	// Wait until child process exits.
	WaitForSingleObject(pi.hProcess, 20000);

	// Close process and thread handles. 
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
}
// *********************************************************
// detect 64-bit OS
typedef BOOL(WINAPI* LPFN_ISWOW64PROCESS) (HANDLE, PBOOL);
BOOL Is64BitWindows()
{
#if defined(_WIN64)
	return TRUE;  // 64-bit programs run only on Win64
#elif defined(_WIN32)
	// 32-bit programs run on both 32-bit and 64-bit Windows
	// so must sniff
	BOOL f64 = FALSE;
	LPFN_ISWOW64PROCESS fnIsWow64Process;

	fnIsWow64Process = (LPFN_ISWOW64PROCESS)GetProcAddress(GetModuleHandle(_T("kernel32")), "IsWow64Process");
	if (NULL != fnIsWow64Process)
	{
		return fnIsWow64Process(GetCurrentProcess(), &f64) && f64;
	}
	return FALSE;
#else
	return FALSE; // Win64 does not support Win16
#endif
}
// *********************************************************
// error messages
void ErrorMessage(DWORD dwError)
{
	WCHAR temp[128];
	LPTSTR Error = NULL;

	StringCbPrintf(temp, 128 * sizeof(WCHAR), L"Error number: %lu", dwError);

	if (FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, dwError, 0, (LPTSTR)&Error, 0, NULL) == 0)
	{
		MessageBox(HWND_DESKTOP, temp, L"ERROR", MB_OK);
	}
	else
		MessageBox(HWND_DESKTOP, Error, temp, MB_OK);

	if (Error)
		LocalFree(Error);
}
// *********************************************************
