#ifndef HFAXInterfaceH
#define HFAXInterfaceH

#include "constants.h"
#include "ipc.h"
#include "system_message.h"
#include "../Language/language.h"
#include "config.h"
#include "SendFAX.h"
#include "PrintMon.h"
#include "config_class.h"
#include "debug_output.h"

// **************************************************************
BOOL hfax_open_port(HANDLE hMonitor, LPWSTR pName, PHANDLE pHandle);
BOOL hfax_start_doc_port(h_portdata* prd, LPWSTR pPrinterName,
	DWORD JobId, DWORD Level, LPBYTE pDocInfo);

BOOL hfax_write_port(h_portdata* prd, LPBYTE pBuffer,
	DWORD cbBuf, LPDWORD pcbWritten);
// **************************************************************
BOOL hfax_end_doc_port(h_portdata* prd);
BOOL hfax_close_port(HANDLE hPort);
extern CRITICAL_SECTION LcmSpoolerSection;
typedef VOID(*MYPROC)(HANDLE, DWORD, BOOL*);

BOOL Is_Win2000();
void TransferSendData(struct TFAXSend* fs, h_portdata* prd);

// **************************************************************
#endif
