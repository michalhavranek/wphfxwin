#include "stdafx.h"
#include "ipc.h"
#define PIPE_TIMEOUT_CONNECT 5000

BOOL StartProcess(PROCESS_INFORMATION* pi, STARTUPINFO* si, PHANDLE hPipe)
{
	BOOL bResult = FALSE;
	DWORD dwSessionId = 0;
	HANDLE hToken = INVALID_HANDLE_VALUE, hTokenDup = INVALID_HANDLE_VALUE;
	DWORD dwCreationFlags;
	LPVOID pEnv = NULL;
	DWORD dwRetLen = 0;
	WCHAR lpszPipename[128];

	// primary token
	bResult = OpenThreadToken(GetCurrentThread(), TOKEN_QUERY | TOKEN_ASSIGN_PRIMARY | TOKEN_DUPLICATE | TOKEN_IMPERSONATE, TRUE, &hToken);
	if (!bResult) // fail
	{
		if (!hToken) // if NULL (bug in ATL)
			hToken = INVALID_HANDLE_VALUE;
		goto Cleanup;
	}
	// duplikate token
	bResult = DuplicateTokenEx(hToken, MAXIMUM_ALLOWED, NULL, SecurityImpersonation, TokenPrimary, &hTokenDup);
	if (!bResult) // fail
	{
		if (!hTokenDup) // if NULL (bug in ATL)
			hTokenDup = INVALID_HANDLE_VALUE;
		goto Cleanup;
	}
	// console ID
	bResult = GetTokenInformation(hTokenDup, TokenSessionId, &dwSessionId, sizeof(dwSessionId), &dwRetLen);
	if (!bResult)
		dwSessionId = WTSGetActiveConsoleSessionId();

	StringCchPrintf(lpszPipename, _countof(lpszPipename),
		L"\\\\.\\pipe\\HylaPrintMonUI_%lu", dwSessionId);

	// named pipe
	*hPipe = CreateNamedPipe(
		lpszPipename,             // pipe name
		PIPE_ACCESS_DUPLEX,       // read/write access
		PIPE_TYPE_BYTE |          // byte type pipe
		PIPE_READMODE_BYTE |      // stream of bytes mode
		PIPE_WAIT,                // blocking mode
		PIPE_UNLIMITED_INSTANCES, // max. instances
		STRVALUE_TEXT_512,           // output buffer size
		STRVALUE_TEXT_512,           // input buffer size
		PIPE_TIMEOUT_CONNECT,     // client time-out
		NULL);

	//Server: Main thread awaiting client connection
	if (*hPipe == INVALID_HANDLE_VALUE) // fail
	{
		bResult = FALSE;
		goto Cleanup;
	}

	// create environment
	dwCreationFlags = NORMAL_PRIORITY_CLASS | CREATE_NEW_CONSOLE;
	si->cb = sizeof(STARTUPINFO);
	si->lpDesktop = L"winsta0\\default";
	si->wShowWindow = SW_SHOW;
	si->dwFlags = STARTF_USESHOWWINDOW;

	if (CreateEnvironmentBlock(&pEnv, hTokenDup, TRUE))
		dwCreationFlags |= CREATE_UNICODE_ENVIRONMENT;
	else
		pEnv = NULL;

	bResult = CreateProcessAsUser(hTokenDup, L"HylaPrintUI.exe", NULL,
		NULL, NULL, FALSE, dwCreationFlags,
		pEnv, NULL, si, pi);
	if (!bResult) // fail
		CloseHandle(*hPipe);

	if (pEnv)
		DestroyEnvironmentBlock(pEnv);

Cleanup:
	RevertToSelf();
	if (hToken != INVALID_HANDLE_VALUE)
		CloseHandle(hToken);
	if (hTokenDup != INVALID_HANDLE_VALUE)
		CloseHandle(hTokenDup);

	return bResult;
}

//***********************************************************
// start pipe names
BOOL PipeServer()
{
	BOOL   fConnected = FALSE;
	DWORD  dwThreadId = 0;
	HANDLE hPipe = INVALID_HANDLE_VALUE, hThread = NULL;
	PROCESS_INFORMATION pi;
	STARTUPINFO si;

	// start dialog FAX send as normal user
	ZeroMemory(&si, sizeof(STARTUPINFO));
	ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));
	pi.hThread = INVALID_HANDLE_VALUE;
	pi.hProcess = INVALID_HANDLE_VALUE;

	// create named pipe, get consolesession ID ...
	if (!StartProcess(&pi, &si, &hPipe))
		return FALSE;

	// Wait for the client to connect, if it succeeds, 
	// the function returns a nonzero value. If the function
	// returns zero, GetLastError returns ERROR_PIPE_CONNECTED. 
	fConnected = ConnectNamedPipe(hPipe, NULL) ? TRUE : (GetLastError() == ERROR_PIPE_CONNECTED);

	// pipe connected (hPipe is deleted by "InstanceThread" func)
	if (fConnected)
	{
		// Create a thread for this client.
		hThread = CreateThread(
			NULL,              // no security attribute
			0,                 // default stack size
			InstanceThread,    // thread proc
			(LPVOID)&hPipe,    // thread parameter
			0,                 // not suspended
			&dwThreadId);      // returns thread ID

		if (hThread == NULL) // fails
		{
			if (pi.hThread != INVALID_HANDLE_VALUE)
				CloseHandle(pi.hThread);
			if (pi.hProcess != INVALID_HANDLE_VALUE)
				CloseHandle(pi.hProcess);
			CloseHandle(hPipe);
			return FALSE;
		} // if (hThread == NULL)
		else
		{
			if (WaitForSingleObject(hThread, 10000) == WAIT_TIMEOUT)
				CloseHandle(hPipe);
			CloseHandle(hThread);
		}

	} //if (fConnected)
	else
		CloseHandle(hPipe);

	// no waiting to GUI
	if (pi.hThread != INVALID_HANDLE_VALUE)
		CloseHandle(pi.hThread);
	if (pi.hProcess != INVALID_HANDLE_VALUE)
		CloseHandle(pi.hProcess);

	return fConnected;
}

DWORD WINAPI InstanceThread(LPVOID lpvParam)
{
	HANDLE hHeap = GetProcessHeap();
	WCHAR* pchRequest = (WCHAR*)HeapAlloc(hHeap, 0, STRVALUE_TEXT_512 * sizeof(WCHAR));
	DWORD cbBytesRead = 0, cbReplyBytes = 0, cbWritten = 0;
	BOOL fSuccess = FALSE;
	HANDLE hPipe = NULL;

	// The thread's parameter is a handle to a pipe object instance.
	hPipe = *static_cast<HANDLE*>(lpvParam);
	// Do some extra error checking since the app will keep running even if this
	// thread fails.
	if (lpvParam == NULL)
	{
		if (pchRequest != NULL)
			HeapFree(hHeap, 0, pchRequest);
		CloseHandle(hPipe);
		return (DWORD)-1;
	}
	if (pchRequest == NULL)
	{
		CloseHandle(hPipe);
		return (DWORD)-1;
	}
	// Loop until done reading
	for (;;)
	{
		// Read client requests from the pipe. This simplistic code only allows messages
		// up to BUFSIZE characters in length.
		fSuccess = ReadFile(
			hPipe,         // handle to pipe
			pchRequest,    // buffer to receive data
			STRVALUE_TEXT_512 * sizeof(WCHAR), // size of buffer
			&cbBytesRead,   // number of bytes read
			NULL);          // not overlapped I/O

		if (!fSuccess || cbBytesRead == 0)
			break;

		if (wcscmp(pchRequest, L"CONNECT") == 0)
		{
			WCHAR* sf_bytes = reinterpret_cast<WCHAR*>(FAXSend);
			cbReplyBytes = sizeof(struct TFAXSend);
			// reply struct as WCHAR*
			// Write the reply to the pipe.
			fSuccess = WriteFile(
				hPipe,        // handle to pipe
				sf_bytes,     // buffer to write from
				cbReplyBytes, // number of bytes to write
				&cbWritten,   // number of bytes written
				NULL);        // not overlapped I/O
			if (!fSuccess || cbReplyBytes != cbWritten)
				break;
		}
		break;
	}
	// Flush the pipe to allow the client to read the pipe's contents
	// before disconnecting. Then disconnect the pipe, and close the
	// handle to this pipe instance.

	FlushFileBuffers(hPipe);
	DisconnectNamedPipe(hPipe);
	CloseHandle(hPipe);

	HeapFree(hHeap, 0, pchRequest);
	return 1;
}
// **********************************************************************
// **********************************************************************
