#ifndef SharedFunctionsH
#define SharedFunctionsH

// included function body into cpp file
// *********************************************************
template <typename T>
void SaveUserData(HWND hwnd, const T& dataConfigClass)
{
	LRESULT bCheck;
	// combobox
	// pagesize
	GetTextItemCB(GetDlgItem(hwnd, IDC_COMBO_CONFPAGESIZE),
		dataConfigClass->GetPageSizeText(), STRVALUE_PAGESIZE);
	// notif
	GetTextItemCB(GetDlgItem(hwnd, IDC_COMBO_CONFNOTIF_TYP),
		dataConfigClass->GetNotificationText(), STRVALUE_NOTIFICATION);
	// resolution
	GetTextItemCB(GetDlgItem(hwnd, IDC_COMBO_CONFRESOLUTION),
		dataConfigClass->GetResolutionText(), STRVALUE_RESOLUTION);
	// maxdials
	GetTextItemCB(GetDlgItem(hwnd, IDC_COMBO_MAXDIALS),
		dataConfigClass->GetMaxDialsText(), STRVALUE_MAXTIMES_NUMBER);
	// maxtries
	GetTextItemCB(GetDlgItem(hwnd, IDC_COMBO_MAXTRIES),
		dataConfigClass->GetMaxTriesText(), STRVALUE_MAXTIMES_NUMBER);
	// infodialog timer
	GetTextItemCB(GetDlgItem(hwnd, IDC_COMBO_CONFINFODIALOG_TIMER),
		dataConfigClass->GetInfoDialogTimerText(), STRVALUE_INFODIALOG_TIMER_NUMBER);
	// editbox
	// username
	GetTextEBConfigDialog(GetDlgItem(hwnd, IDC_EDIT_CONFUSERNAME),
		dataConfigClass->GetUsernameText(), STRVALUE_USER);
	// password
	GetTextEBConfigDialog(GetDlgItem(hwnd, IDC_EDIT_CONFPASSWORD),
		dataConfigClass->GetPasswordText(), STRVALUE_PASSWORD);
	// email
	GetTextEBConfigDialog(GetDlgItem(hwnd, IDC_EDIT_CONFDEF_NOTIFEMAIL),
		dataConfigClass->GetEmailText(), STRVALUE_EMAIL);
	// modem
	GetTextEBConfigDialog(GetDlgItem(hwnd, IDC_EDIT_CONFMODEM),
		dataConfigClass->GetModemText(), STRVALUE_MODEM);
	// address book dir
	GetTextEBConfigDialog(GetDlgItem(hwnd, IDC_EDIT_CONFBOOK_DIR),
		dataConfigClass->GetAddressBookText(), MAX_PATH);
	// checkbox faxnum from the clipboard
	bCheck = SendDlgItemMessage(hwnd, IDC_CHECK_FAXNUM_CLIPBOARD, BM_GETCHECK, 0, 0);
	(bCheck == BST_CHECKED) ? dataConfigClass->SetUseFaxnumberFromClipboard(1) :
		dataConfigClass->SetUseFaxnumberFromClipboard(0);
}
// *********************************************************
// get text from combobox
LRESULT GetTextItemCB(HWND hCombo, LPWSTR str, size_t iBufLen)
{
	LPWSTR lpText;
	LRESULT select;
	select = SendMessage(hCombo, CB_GETCURSEL, 0, 0);

	if (select == CB_ERR)
	{
		str[0] = L'\0';
		return select;
	}

	LRESULT textLength = SendMessage(hCombo, CB_GETLBTEXTLEN, select, 0);
	if (textLength <= 0)
	{
		str[0] = L'\0';
		return select;
	}
	lpText = (LPWSTR)HeapAlloc(GetProcessHeap(), 0,
		(textLength + 1) * sizeof(WCHAR));
	if (lpText)
	{
		SendMessage(hCombo, CB_GETLBTEXT, select, (LPARAM)lpText);
		wcsncpy_s(str, iBufLen, lpText, _TRUNCATE);

		HeapFree(GetProcessHeap(), 0, lpText);
	}

	return select;
}
//*********************************************************
// get text from editbox
void GetTextEBConfigDialog(HWND hEdit, LPWSTR str, size_t iBufLen)
{
	LPWSTR lpText;
	lpText = (LPWSTR)HeapAlloc(GetProcessHeap(), 0,
		(STRVALUE_TEXT_512 + 1) * sizeof(WCHAR));
	if (lpText)
	{
		GetWindowText(hEdit, lpText, STRVALUE_TEXT_512);

		wcsncpy_s(str, iBufLen, lpText, _TRUNCATE);
		HeapFree(GetProcessHeap(), 0, lpText);
	}
}
//*********************************************************
// add comboboxes strings - only CB which is the same for single user/system wide
void AddSharedStringsToCB(HWND hDlg)
{
	// pagesize
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFPAGESIZE, CB_ADDSTRING, 0, (LPARAM)_T(US_LETTER)); // index 0
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFPAGESIZE, CB_ADDSTRING, 0, (LPARAM)_T(A4_SIZE));   // index 1
	// notification
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFNOTIF_TYP, CB_ADDSTRING, 0, (LPARAM)_T(FAILURE_SUCCESS)); // index 0
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFNOTIF_TYP, CB_ADDSTRING, 0, (LPARAM)_T(SUCCESS_ONLY));    // index 1
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFNOTIF_TYP, CB_ADDSTRING, 0, (LPARAM)_T(FAILURE_ONLY));    // index 2
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFNOTIF_TYP, CB_ADDSTRING, 0, (LPARAM)_T(NONE_NOTIF));      // index 3
	// resolution
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFRESOLUTION, CB_ADDSTRING, 0, (LPARAM)_T(STAND_RES)); // index 0
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFRESOLUTION, CB_ADDSTRING, 0, (LPARAM)_T(FINE_RES));  // index 1
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFRESOLUTION, CB_ADDSTRING, 0, (LPARAM)_T(SUPER_RES)); // index 2
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFRESOLUTION, CB_ADDSTRING, 0, (LPARAM)_T(ULTRA_RES)); // index 3
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFRESOLUTION, CB_ADDSTRING, 0, (LPARAM)_T(HYPER_RES)); // index 4
	// maxdials
	SendDlgItemMessage(hDlg, IDC_COMBO_MAXDIALS, CB_ADDSTRING, 0, (LPARAM)_T(MAXDIALS_NONE)); // index 0
	SendDlgItemMessage(hDlg, IDC_COMBO_MAXDIALS, CB_ADDSTRING, 0, (LPARAM)_T(MAX_TIMES_1));   // index 1
	SendDlgItemMessage(hDlg, IDC_COMBO_MAXDIALS, CB_ADDSTRING, 0, (LPARAM)_T(MAX_TIMES_3));   // index 2
	SendDlgItemMessage(hDlg, IDC_COMBO_MAXDIALS, CB_ADDSTRING, 0, (LPARAM)_T(MAX_TIMES_6));   // index 3
	SendDlgItemMessage(hDlg, IDC_COMBO_MAXDIALS, CB_ADDSTRING, 0, (LPARAM)_T(MAX_TIMES_9));   // index 4
	SendDlgItemMessage(hDlg, IDC_COMBO_MAXDIALS, CB_ADDSTRING, 0, (LPARAM)_T(MAX_TIMES_12));  // index 5
	// maxtries
	SendDlgItemMessage(hDlg, IDC_COMBO_MAXTRIES, CB_ADDSTRING, 0, (LPARAM)_T(MAXTRIES_NONE)); // index 0
	SendDlgItemMessage(hDlg, IDC_COMBO_MAXTRIES, CB_ADDSTRING, 0, (LPARAM)_T(MAX_TIMES_1));   // index 1
	SendDlgItemMessage(hDlg, IDC_COMBO_MAXTRIES, CB_ADDSTRING, 0, (LPARAM)_T(MAX_TIMES_3));   // index 2
	SendDlgItemMessage(hDlg, IDC_COMBO_MAXTRIES, CB_ADDSTRING, 0, (LPARAM)_T(MAX_TIMES_6));   // index 3
	// infodialog timer
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFINFODIALOG_TIMER, CB_ADDSTRING, 0, (LPARAM)_T(INFODIALOG_TIMER_0)); // index 0
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFINFODIALOG_TIMER, CB_ADDSTRING, 0, (LPARAM)_T(INFODIALOG_TIMER_3)); // index 1
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFINFODIALOG_TIMER, CB_ADDSTRING, 0, (LPARAM)_T(INFODIALOG_TIMER_5)); // index 2
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFINFODIALOG_TIMER, CB_ADDSTRING, 0, (LPARAM)_T(INFODIALOG_TIMER_10)); // index 3
}
// *********************************************************
// *********************************************************
#endif
