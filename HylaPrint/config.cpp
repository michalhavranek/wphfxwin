//---------------------------------------------------------------------------
#include "stdafx.h"
#include "config.h"

CConfigure_sheet3* ConfigureLDAP_sheet3;
// ******************************************************************************************
// set dialog items - HDialog, flag UseUserConfig, config class
template <typename T>
void SetData(HWND hSheet, BOOL flag, const T& dataConfigClass)
{
	SetWindowText(GetDlgItem(hSheet, IDC_EDIT_CONFUSERNAME), dataConfigClass->GetUsernameText());
	SetWindowText(GetDlgItem(hSheet, IDC_EDIT_CONFPASSWORD), dataConfigClass->GetPasswordText());
	SetWindowText(GetDlgItem(hSheet, IDC_EDIT_CONFDEF_NOTIFEMAIL), dataConfigClass->GetEmailText());
	SetWindowText(GetDlgItem(hSheet, IDC_EDIT_CONFMODEM), dataConfigClass->GetModemText());
	SetWindowText(GetDlgItem(hSheet, IDC_EDIT_CONFBOOK_DIR), dataConfigClass->GetAddressBookText());
	// combobox settings
	SendDlgItemMessage(hSheet, IDC_COMBO_CONFNOTIF_TYP, CB_SELECTSTRING,
		(WPARAM)(-1), (LPARAM)dataConfigClass->GetNotificationText());
	SendDlgItemMessage(hSheet, IDC_COMBO_CONFPAGESIZE, CB_SELECTSTRING,
		(WPARAM)(-1), (LPARAM)dataConfigClass->GetPageSizeText());
	SendDlgItemMessage(hSheet, IDC_COMBO_CONFRESOLUTION, CB_SELECTSTRING,
		(WPARAM)(-1), (LPARAM)dataConfigClass->GetResolutionText());
	SendDlgItemMessage(hSheet, IDC_COMBO_MAXDIALS, CB_SELECTSTRING,
		(WPARAM)(-1), (LPARAM)dataConfigClass->GetMaxDialsText());
	SendDlgItemMessage(hSheet, IDC_COMBO_MAXTRIES, CB_SELECTSTRING,
		(WPARAM)(-1), (LPARAM)dataConfigClass->GetMaxTriesText());
	SendDlgItemMessage(hSheet, IDC_COMBO_CONFINFODIALOG_TIMER, CB_SELECTSTRING,
		(WPARAM)(-1), (LPARAM)dataConfigClass->GetInfoDialogTimerText());
	SendDlgItemMessage(hSheet, IDC_COMBO_SELECT_USER, CB_SETCURSEL, flag, 0);
	// faxnumber from clipboard
	SendDlgItemMessage(hSheet, IDC_CHECK_FAXNUM_CLIPBOARD, BM_SETCHECK,
		(LPARAM)dataConfigClass->GetUseFaxnumberFromClipboard(), 0);
	// flag
	UserConfigure->SetUseUserConfig(flag);
}
//*********************************************************
// SHARED TEMPLATE FUNCTION
// save user data 
#include "shared_savedata.h"
//*********************************************************
//---------------------------------------------------------------------------
void ConfigCallUpDialog(HINSTANCE hInst, HWND hwnd)
{
	DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG_CONFIG_MAIN), hwnd, ConfigProc);
}
// ***********************************************
// Message handler for port.
INT_PTR CALLBACK ConfigProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);

	switch (message)
	{
	case WM_INITDIALOG:
		CenterDialogWindow(hDlg);
		CreateSheet(hDlg);
		EndDialog(hDlg, LOWORD(wParam));
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

//*********************************************************
// version of application
void GetVersionHyla(HWND hDlg)
{
	DWORD dwVerInfoSize;
	DWORD dwHandle = 0;
	LPWSTR szDirectory;
	int iBufLen = MAX_PATH;
	WCHAR szFileVersion[64];
	WCHAR* pTemp;
	VS_FIXEDFILEINFO* pBuff = nullptr;
	UINT nInfoLen;

	szDirectory = (LPWSTR)HeapAlloc(GetProcessHeap(), 0,
		iBufLen * sizeof(WCHAR));
	if (!szDirectory) // fail
		return;
	szDirectory[0] = L'\0';
	if (GetModuleFileName(Configure_sheet1->GetHinstance(), szDirectory, iBufLen) > 0)
	{
		dwVerInfoSize = GetFileVersionInfoSize(szDirectory, &dwHandle);
		pTemp = (WCHAR*)malloc(dwVerInfoSize);
		if (pTemp)
		{
			GetFileVersionInfo(szDirectory, (DWORD)NULL, dwVerInfoSize, pTemp);
			VerQueryValue(pTemp, L"\\", (LPVOID*)&pBuff, &nInfoLen);

			StringCbPrintf(szFileVersion, sizeof(szFileVersion), L"v.%d.%d.%d.%d - %s",
				HIWORD(pBuff->dwFileVersionMS), // major
				LOWORD(pBuff->dwFileVersionMS), // minor
				HIWORD(pBuff->dwFileVersionLS), // release
				LOWORD(pBuff->dwFileVersionLS), // build
				_T(__DATE__));                  // release date
			SetWindowText(GetDlgItem(hDlg, IDC_STATIC_VERSION), szFileVersion);
			free(pTemp);
		}
	}

	// free mem
	HeapFree(GetProcessHeap(), 0, szDirectory);
}

//*********************************************************
// prop sheet 1 (settings)
LRESULT CALLBACK Sheet1(HWND hSheet, UINT message, WPARAM wParam, LPARAM lParam)
{
	LRESULT lr;
	static HFONT hfDialog;
	static LRESULT lrCurrentIndex;
	LOGFONT logFont;

	switch (message)
	{
	case WM_INITDIALOG:
		// hylafax server IP and port
		SetWindowText(GetDlgItem(hSheet, IDC_EDIT_CONFSERVER_ADDRESS), Configure_sheet1->GetServerText());
		SetWindowText(GetDlgItem(hSheet, IDC_EDIT_CONFSERVER_HFPORT), Configure_sheet1->GetHFPortNumber());

		// combobox shared values
		// combobox start values - add strings to comboboxes whis are the same for single user/system wide
		AddSharedStringsToCB(hSheet);

		// add CB strings - show users (only in system wide dialog)
		SendDlgItemMessage(hSheet, IDC_COMBO_SELECT_USER, CB_ADDSTRING, 0, (LPARAM)_T(DEFAULT_USER));
		SendDlgItemMessage(hSheet, IDC_COMBO_SELECT_USER, CB_ADDSTRING, 0, (LPARAM)UserConfigure->GetLoggedInUser());

		// all users is set
		lrCurrentIndex = UserConfigure->GetUseUserConfig();
		if (!lrCurrentIndex) // system wide 
			SetData<CConfigure_sheet1*>(hSheet, FALSE, Configure_sheet1);
		else // single user is set
		{
			SetData<CUserConfigure*>(hSheet, TRUE, UserConfigure);
			DisableCommonSettings(hSheet);
		}

		// pasv ignore IP
		SendMessage(GetDlgItem(hSheet, IDC_CHECK_PASVIP), BM_SETCHECK, Configure_sheet1->GetPasvIPIgnoreMode(), 0);

		GetVersionHyla(hSheet);
		// tooltip
		CreateTooltip(GetDlgItem(hSheet, IDC_EDIT_CONFSERVER_HFPORT), Configure_sheet1->GetHinstance(),
			L"Port of the Hylafax server. Default: 4559");
		CreateTooltip(GetDlgItem(hSheet, IDC_EDIT_CONFSERVER_ADDRESS), Configure_sheet1->GetHinstance(),
			L"Insert IP address or name of the HylaFax server.");
		CreateTooltip(GetDlgItem(hSheet, IDC_CHECK_PASVIP), Configure_sheet1->GetHinstance(),
			L"If using passive mode. Client ignores an IP address for a data connection.");
		CreateTooltip(GetDlgItem(hSheet, IDC_EDIT_CONFMODEM), Configure_sheet1->GetHinstance(),
			L"Defines a logical name for a set of modem devices on a server.");
		CreateTooltip(GetDlgItem(hSheet, IDC_BUTTON_CONFIG_BROWSEDIR), Configure_sheet1->GetHinstance(),
			L"Select your address book directory. There will be stored the file addrbook.csv which contains addressbook data.");
		CreateTooltip(GetDlgItem(hSheet, IDC_COMBO_MAXDIALS), Configure_sheet1->GetHinstance(),
			L"Optional - max phone calls to make to transmit a job.");
		CreateTooltip(GetDlgItem(hSheet, IDC_COMBO_MAXTRIES), Configure_sheet1->GetHinstance(),
			L"Optional - max attempts to transmit a job.");
		CreateTooltip(GetDlgItem(hSheet, IDC_COMBO_CONFINFODIALOG_TIMER), Configure_sheet1->GetHinstance(),
			L"Timer of the successful information window after a FAX sending");
		// bold font
		GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(logFont), &logFont);
		logFont.lfWeight = FW_BOLD;
		hfDialog = CreateFontIndirect(&logFont);
		SendDlgItemMessage(hSheet, IDC_STATIC_WPHFXWIN, WM_SETFONT, (WPARAM)hfDialog, (LPARAM)TRUE);
		LocalDialogTab1(hSheet);
		LocalDialog_UserSettingsItems(hSheet);
		return (INT_PTR)TRUE;

		// free memory
	case WM_DESTROY:
		DeleteObject(hfDialog);
		return 0;

	case WM_COMMAND:
		switch LOWORD(wParam)
		{
		case IDC_BUTTON_CONFIG_BROWSEDIR:
			// all users
			if (!UserConfigure->GetUseUserConfig())
			{
				if (BrowseForFolder(Configure_sheet1->GetAddressBookText(), MAX_PATH))
					SetDlgItemText(hSheet, IDC_EDIT_CONFBOOK_DIR, Configure_sheet1->GetAddressBookText());
			}
			else // single user
			{
				if (BrowseForFolder(UserConfigure->GetAddressBookText(), MAX_PATH))
					SetDlgItemText(hSheet, IDC_EDIT_CONFBOOK_DIR, UserConfigure->GetAddressBookText());
			}
			break;

		case IDC_COMBO_SELECT_USER: // system wide-single user
			if (HIWORD(wParam) == CBN_SELCHANGE)
			{
				// current index
				lr = SendDlgItemMessage(hSheet, IDC_COMBO_SELECT_USER, CB_GETCURSEL, 0, 0);

				if (lr == lrCurrentIndex) // ComboBox has changed, but select the same item as before (not switch) -> do nothing
					break;
				else lrCurrentIndex = lr; // set new item index as selected index

				if (lr == 0) // switch to all users data (wide - default)
				{
					SaveUserData<CUserConfigure*>(hSheet, UserConfigure);
					SetData<CConfigure_sheet1*>(hSheet, FALSE, Configure_sheet1);
					EnableDefaultSettings(hSheet);
				}
				else // switch to user data (single)
				{
					SaveDefaultUserData(hSheet);
					SaveUserData<CConfigure_sheet1*>(hSheet, Configure_sheet1);
					SetData<CUserConfigure*>(hSheet, TRUE, UserConfigure);
					EnableDefaultSettings(hSheet);
					DisableCommonSettings(hSheet);
				}
			}
			break;
			/*
			case IDC_BUTTON_UPDATE:
				EnableWindow(GetDlgItem(hSheet, IDC_BUTTON_UPDATE), FALSE);
				CheckUpdateVersion(hSheet);
				EnableWindow(GetDlgItem(hSheet, IDC_BUTTON_UPDATE), TRUE);
				break;
			*/

		}
		break;

	case WM_NOTIFY:
		switch (((LPNMHDR)lParam)->code)
		{
		case PSN_APPLY: //OK button
			// combobox
			lr = SendDlgItemMessage(hSheet, IDC_COMBO_SELECT_USER, CB_GETCURSEL, 0, 0);
			if (lr == 0) // wide settings
			{
				SaveDefaultUserData(hSheet);
				SaveUserData<CConfigure_sheet1*>(hSheet, Configure_sheet1);
			}
			else // only user settings
				SaveUserData<CUserConfigure*>(hSheet, UserConfigure);
			// set dialog OK click flag
			Configure_sheet1->OKClick();
			break;

		case PSN_RESET: // Close Button
			// set dialog cancel click flag
			Configure_sheet1->CancelClick();
			break;

		case NM_CLICK: // open hyperlink
			PNMLINK pNMLink = (PNMLINK)lParam;
			LITEM   item = pNMLink->item;
			if (((LPNMHDR)lParam)->hwndFrom == GetDlgItem(hSheet, IDC_SYSLINK1) && (item.iLink == 0))
			{
				ShellExecute(NULL, L"open", item.szUrl, NULL, NULL, SW_SHOW);
			}
			break;

		} // switch (((LPNMHDR)lParam)->code)
		break; // case WM_NOTIFY:

	} // switch (message)

	return FALSE;
}
// property sheet 2 (active connection)
LRESULT CALLBACK Sheet2(HWND hSheet, UINT message, WPARAM wParam, LPARAM lParam)
{
	LRESULT lr;

	switch (message)
	{
	case WM_INITDIALOG:
		// active port
		SendMessage(GetDlgItem(hSheet, IDC_CHECK_USEACTIVE), BM_SETCHECK, ConfigureActiveMode_sheet2->GetFTPActiveMode(), 0);
		SetWindowText(GetDlgItem(hSheet, IDC_EDIT_ACTIVEPORT), ConfigureActiveMode_sheet2->GetActivePortNumber());

		EnableDisableActiveMode(hSheet, ConfigureActiveMode_sheet2->GetFTPActiveMode());
		// tool tips
		CreateTooltip(GetDlgItem(hSheet, IDC_CHECK_USEACTIVE), Configure_sheet1->GetHinstance(),
			TEXT("Enable active mode connection. Same as active mode FTP."));
		CreateTooltip(GetDlgItem(hSheet, IDC_EDIT_ACTIVEPORT), Configure_sheet1->GetHinstance(),
			TEXT("Set the active port number for incoming data connection from HylaFax server."));
		LocalDialogTab2(hSheet);
		return (INT_PTR)TRUE;
		// free memory
	case WM_DESTROY:
		return 0;

	case WM_COMMAND:
		switch LOWORD(wParam)
		{
			// enable/disable items active mode
		case IDC_CHECK_USEACTIVE:
			// use active
			if (SendDlgItemMessage(hSheet, IDC_CHECK_USEACTIVE, BM_GETCHECK, 0, 0) == BST_CHECKED)
			{
				ConfigureActiveMode_sheet2->SetFTPActiveMode(1);
				EnableDisableActiveMode(hSheet, 1);
			}
			else // use passive
			{
				ConfigureActiveMode_sheet2->SetFTPActiveMode(0);
				EnableDisableActiveMode(hSheet, 0);
			}
			break;
		}
		break;

	case WM_NOTIFY:
		switch (((LPNMHDR)lParam)->code)
		{
		case PSN_APPLY: //OK button
			// active FTP
			lr = SendDlgItemMessage(hSheet, IDC_CHECK_USEACTIVE, BM_GETCHECK, 0, 0);
			ConfigureActiveMode_sheet2->SetFTPActiveMode(lr);
			// active port number
			GetTextEBConfigDialog(GetDlgItem(hSheet, IDC_EDIT_ACTIVEPORT),
				ConfigureActiveMode_sheet2->GetActivePortNumber(), STRVALUE_PORTNUMBER);
			break;

		case PSN_SETACTIVE:
		{
			// disable this sheet when using user config or not an admin user
			if (UserConfigure->GetUseUserConfig())
				SetWindowLongPtr(hSheet, DWLP_MSGRESULT, IDD_DIALOG_CONFIG1);
			return TRUE;
		}

		} // switch (((LPNMHDR)lParam)->code)
		break; // case WM_NOTIFY:

	} // switch (message)

	return FALSE;
}
// property sheet 3 (LDAP)
LRESULT CALLBACK Sheet3(HWND hSheet, UINT message, WPARAM wParam, LPARAM lParam)
{
	LRESULT lr;

	switch (message)
	{
	case WM_INITDIALOG:
		// checkbox
		// use LDAP
		SendMessage(GetDlgItem(hSheet, IDC_CHECK_USE_LDAP), BM_SETCHECK, ConfigureLDAP_sheet3->GetUseLDAP(), 0);
		if (!ConfigureLDAP_sheet3->GetUseLDAP())
			DisableLDAPSettings(hSheet);
		// SSL
		SendMessage(GetDlgItem(hSheet, IDC_CHECK_LDAP_SSL), BM_SETCHECK, ConfigureLDAP_sheet3->GetUseLDAPSsl(), 0);
		// StartTLS
		SendMessage(GetDlgItem(hSheet, IDC_CHECK_LDAP_STARTTLS), BM_SETCHECK, ConfigureLDAP_sheet3->GetUseLDAPStartTLS(), 0);
		// edit box
		// LDAP server
		SetWindowText(GetDlgItem(hSheet, IDC_EDIT_LDAP_SERVER), ConfigureLDAP_sheet3->GetLDAPServer());
		// LDAP port
		SetWindowText(GetDlgItem(hSheet, IDC_EDIT_LDAP_PORT), ConfigureLDAP_sheet3->GetLDAPPort());
		// Bind DN
		SetWindowText(GetDlgItem(hSheet, IDC_EDIT_LDAP_BIND_DN), ConfigureLDAP_sheet3->GetLDAPBindDN());
		// password
		SetWindowText(GetDlgItem(hSheet, IDC_EDIT_LDAP_PASSWORD), ConfigureLDAP_sheet3->GetLDAPPassword());
		// Base DN
		SetWindowText(GetDlgItem(hSheet, IDC_EDIT_LDAP_BASE_DN), ConfigureLDAP_sheet3->GetLDAPBaseDN());
		// filter
		SetWindowText(GetDlgItem(hSheet, IDC_EDIT_LDAP_FILTER), ConfigureLDAP_sheet3->GetLDAPFilter());
		// combo box
		SendDlgItemMessage(hSheet, IDC_COMBO_LDAP_AUTOCOMPLETE_ATTRIBUTE, CB_ADDSTRING, 0, (LPARAM)_T(LDAP_AUTOCOMPLETE_ATT_CN));
		SendDlgItemMessage(hSheet, IDC_COMBO_LDAP_AUTOCOMPLETE_ATTRIBUTE, CB_ADDSTRING, 0, (LPARAM)_T(LDAP_AUTOCOMPLETE_ATT_DISPLAYNAME));
		SendDlgItemMessage(hSheet, IDC_COMBO_LDAP_AUTOCOMPLETE_ATTRIBUTE, CB_SETCURSEL, ConfigureLDAP_sheet3->GetLDAPAutocompleteAttIndex(), 0);
		// tooltip
		// bind dn
		CreateTooltip(GetDlgItem(hSheet, IDC_EDIT_LDAP_BIND_DN), Configure_sheet1->GetHinstance(),
			L"String that contains the distinguished name of the entry used to bind (identifier).");
		// combobox
		CreateTooltip(GetDlgItem(hSheet, IDC_COMBO_LDAP_AUTOCOMPLETE_ATTRIBUTE), Configure_sheet1->GetHinstance(),
			L"Set search attribute to \"cn <facsimileTelephoneNumber>\" or \" displayName <facsimileTelephoneNumber>\"");
		// base dn
		CreateTooltip(GetDlgItem(hSheet, IDC_EDIT_LDAP_BASE_DN), Configure_sheet1->GetHinstance(),
			L"String that contains the distinguished name of the entry at which to start the search.");
		LocalDialogTab3(hSheet);
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		switch LOWORD(wParam)
		{
			// enable/disable items
		case IDC_CHECK_USE_LDAP:
			lr = SendDlgItemMessage(hSheet, IDC_CHECK_USE_LDAP, BM_GETCHECK, 0, 0);
			// use active
			if (lr == BST_CHECKED)
				EnableLDAPSettings(hSheet);
			else
				DisableLDAPSettings(hSheet);
			break;
		case IDC_CHECK_LDAP_SSL:
			lr = SendDlgItemMessage(hSheet, IDC_CHECK_LDAP_SSL, BM_GETCHECK, 0, 0);
			if (lr == BST_CHECKED) // if checked - disable StartTLS
			{
				SendDlgItemMessage(hSheet, IDC_CHECK_LDAP_STARTTLS, BM_SETCHECK, BST_UNCHECKED, 0);
				SetDlgItemText(hSheet, IDC_EDIT_LDAP_PORT, L"636");
			}
			else
				SetDlgItemText(hSheet, IDC_EDIT_LDAP_PORT, L"389");
			break;
		case IDC_CHECK_LDAP_STARTTLS:
			lr = SendDlgItemMessage(hSheet, IDC_CHECK_LDAP_STARTTLS, BM_GETCHECK, 0, 0);
			if (lr == BST_CHECKED) // if checked - disable SSL
			{
				SendDlgItemMessage(hSheet, IDC_CHECK_LDAP_SSL, BM_SETCHECK, BST_UNCHECKED, 0);
				SetDlgItemText(hSheet, IDC_EDIT_LDAP_PORT, L"389");
			}
			break;
		case IDC_COMBO_LDAP_AUTOCOMPLETE_ATTRIBUTE:
			if (HIWORD(wParam) == CBN_SELCHANGE)
			{
				lr = SendDlgItemMessage(hSheet, IDC_COMBO_LDAP_AUTOCOMPLETE_ATTRIBUTE, CB_GETCURSEL, 0, 0);
				if (lr == 0) // cd
					SetDlgItemText(hSheet, IDC_EDIT_LDAP_FILTER, _T(LDAP_FILTER_CN));
				else
					SetDlgItemText(hSheet, IDC_EDIT_LDAP_FILTER, _T(LDAP_FILTER_DISPLAYNAME));
			}
			break;
		}
		break;

	case WM_NOTIFY:
		switch (((LPNMHDR)lParam)->code)
		{
		case PSN_APPLY: //OK button
			// use LDAP
			lr = SendDlgItemMessage(hSheet, IDC_CHECK_USE_LDAP, BM_GETCHECK, 0, 0);
			ConfigureLDAP_sheet3->SetUseLDAP(lr);
			// SSL
			lr = SendDlgItemMessage(hSheet, IDC_CHECK_LDAP_SSL, BM_GETCHECK, 0, 0);
			ConfigureLDAP_sheet3->SetUseLDAPSsl(lr);
			// StartTLS
			lr = SendDlgItemMessage(hSheet, IDC_CHECK_LDAP_STARTTLS, BM_GETCHECK, 0, 0);
			ConfigureLDAP_sheet3->SetUseLDAPStartTLS(lr);
			// server
			GetTextEBConfigDialog(GetDlgItem(hSheet, IDC_EDIT_LDAP_SERVER),
				ConfigureLDAP_sheet3->GetLDAPServer(), STRVALUE_SERVER);
			// port
			GetTextEBConfigDialog(GetDlgItem(hSheet, IDC_EDIT_LDAP_PORT),
				ConfigureLDAP_sheet3->GetLDAPPort(), STRVALUE_PORTNUMBER);
			// bind DN
			GetTextEBConfigDialog(GetDlgItem(hSheet, IDC_EDIT_LDAP_BIND_DN),
				ConfigureLDAP_sheet3->GetLDAPBindDN(), STRVALUE_LDAP_BIND_DN);
			// password
			GetTextEBConfigDialog(GetDlgItem(hSheet, IDC_EDIT_LDAP_PASSWORD),
				ConfigureLDAP_sheet3->GetLDAPPassword(), STRVALUE_PASSWORD);
			// base DN
			GetTextEBConfigDialog(GetDlgItem(hSheet, IDC_EDIT_LDAP_BASE_DN),
				ConfigureLDAP_sheet3->GetLDAPBaseDN(), STRVALUE_LDAP_BASE_DN);
			// filter
			GetTextEBConfigDialog(GetDlgItem(hSheet, IDC_EDIT_LDAP_FILTER),
				ConfigureLDAP_sheet3->GetLDAPFilter(), STRVALUE_LDAP_FILTER);
			// search attribute
			lr = GetTextItemCB(GetDlgItem(hSheet, IDC_COMBO_LDAP_AUTOCOMPLETE_ATTRIBUTE),
				ConfigureLDAP_sheet3->GetLDAPAutocompleteAttribute(), STRVALUE_LDAP_AUTOCOMPLETE_ATT);
			ConfigureLDAP_sheet3->SetLDAPAutocompleteAttIndex(lr);
			break;

		case PSN_SETACTIVE:
		{
			// disable this sheet when using user config or not an admin user
			if (UserConfigure->GetUseUserConfig())
				SetWindowLongPtr(hSheet, DWLP_MSGRESULT, IDD_DIALOG_CONFIG1);
			return TRUE;
		}

		} // switch (((LPNMHDR)lParam)->code)
		break; // case WM_NOTIFY:

	} // switch (message)

	return FALSE;
}
// create property sheet
INT_PTR CreateSheet(HWND hSettings)
{
	PROPSHEETPAGE psp[3];
	PROPSHEETHEADER psh;
	INT_PTR iReturn;

	// sheet 1
	psp[0].dwSize = sizeof(PROPSHEETPAGE);
	psp[0].dwFlags = PSP_USETITLE;
	psp[0].hInstance = Configure_sheet1->GetHinstance();
	psp[0].pszTemplate = MAKEINTRESOURCE(IDD_DIALOG_CONFIG1);
	psp[0].pfnDlgProc = (DLGPROC)Sheet1;
	psp[0].pszTitle = _T(LNG_SETTINGS);

	// sheet 2
	psp[1].dwSize = sizeof(PROPSHEETPAGE);
	psp[1].dwFlags = PSP_USETITLE;
	psp[1].hInstance = Configure_sheet1->GetHinstance();
	psp[1].pszTemplate = MAKEINTRESOURCE(IDD_DIALOG_CONFIG2);
	psp[1].pfnDlgProc = (DLGPROC)Sheet2;
	psp[1].pszTitle = _T(LNG_ACTIVEMODE);

	// sheet 3
	psp[2].dwSize = sizeof(PROPSHEETPAGE);
	psp[2].dwFlags = PSP_USETITLE;
	psp[2].hInstance = Configure_sheet1->GetHinstance();
	psp[2].pszTemplate = MAKEINTRESOURCE(IDD_DIALOG_CONFIG3);
	psp[2].pfnDlgProc = (DLGPROC)Sheet3;
	psp[2].pszTitle = L"LDAP";

	// insert sheets into dialog
	memset(&psh, 0, sizeof(PROPSHEETHEADER));
	psh.dwSize = sizeof(PROPSHEETHEADER);
	psh.dwFlags = PSH_PROPSHEETPAGE | PSH_NOAPPLYNOW | PSH_NOCONTEXTHELP;
	psh.hwndParent = hSettings;
	psh.pszCaption = _T(LNG_PRINTERPORT_SETTINGS);
	psh.nPages = sizeof(psp) / sizeof(PROPSHEETPAGE);
	psh.ppsp = (LPCPROPSHEETPAGE)&psp;

	iReturn = PropertySheet(&psh);
	return iReturn;
}

//*********************************************************
// enable/disable window use active connection
void EnableDisableActiveMode(HWND hwnd, LRESULT lrActiveMode)
{
	if (lrActiveMode)
		EnableWindow(GetDlgItem(hwnd, IDC_EDIT_ACTIVEPORT), TRUE);
	else
		EnableWindow(GetDlgItem(hwnd, IDC_EDIT_ACTIVEPORT), FALSE);
}

//*********************************************************
// tool tip
void CreateTooltip(HWND hwnd, HINSTANCE hInst, LPWSTR szToolTipText)
{
	HWND hwndTT;   // handle to the ToolTip control

	// struct specifying info about tool in ToolTip control
	TOOLINFO ti = { 0 };
	unsigned int uid = 0;       // for ti initialization
	RECT rect;    // for client area coordinates

	/* CREATE A TOOLTIP WINDOW */
	hwndTT = CreateWindowEx(WS_EX_TOPMOST,
		TOOLTIPS_CLASS,
		NULL,
		WS_POPUP | TTS_NOPREFIX | TTS_ALWAYSTIP,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		hwnd,
		NULL,
		hInst,
		NULL
	);

	SetWindowPos(hwndTT,
		HWND_TOPMOST,
		0,
		0,
		0,
		0,
		SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);

	/* GET COORDINATES OF THE MAIN CLIENT AREA */
	GetClientRect(hwnd, &rect);

	/* INITIALIZE MEMBERS OF THE TOOLINFO STRUCTURE */
	ti.cbSize = sizeof(TOOLINFO);
	ti.uFlags = TTF_SUBCLASS;
	ti.hwnd = hwnd;
	ti.hinst = hInst;
	ti.uId = uid;
	ti.lpszText = szToolTipText;
	// ToolTip control will cover the whole window
	ti.rect.left = rect.left;
	ti.rect.top = rect.top;
	ti.rect.right = rect.right;
	ti.rect.bottom = rect.bottom;

	/* SEND AN ADDTOOL MESSAGE TO THE TOOLTIP CONTROL WINDOW */
	SendMessage(hwndTT, TTM_ADDTOOL, 0, (LPARAM)(LPTOOLINFO)&ti);
}

//*********************************************************
// user settings-config
BOOL Get_user_from_sessionid(CUserConfigure* user_configure)
{
	BOOL fRet = TRUE;
	HANDLE htoken = NULL, hduptoken = NULL;
	DWORD dwSessionId = 0;

	fRet = ImpersonateSelf(SecurityImpersonation);
	if (!fRet) // fail
	{
		return FALSE;
	}
	/* get impersonation token */
	fRet = OpenThreadToken(GetCurrentThread(), TOKEN_DUPLICATE | TOKEN_IMPERSONATE, TRUE, &htoken);

	if (fRet) // OK
	{
		/* Duplicate it to create a primary token */
		fRet = DuplicateTokenEx(htoken, TOKEN_ALL_ACCESS, NULL, SecurityImpersonation, TokenPrimary, &hduptoken);
		CloseHandle(htoken);
	}

	if (fRet) // OK
	{
		DWORD dwRetLen = 0;
		LPTSTR szUsername = NULL;
		/* query session-id from token */
		fRet = GetTokenInformation(hduptoken, TokenSessionId, &dwSessionId, sizeof(dwSessionId), &dwRetLen);
		if (fRet)
		{
			WTSQuerySessionInformation(WTS_CURRENT_SERVER_HANDLE, dwSessionId, WTSUserName, &szUsername, &dwRetLen);
			user_configure->SetLoggedInUser(szUsername);
			user_configure->SetIsAdmin(IsUserAdmin(hduptoken));
			WTSFreeMemory(szUsername);
		}
		CloseHandle(hduptoken);
	}

	RevertToSelf();
	return(fRet);
}
//*********************************************************
// save default data 
void SaveDefaultUserData(HWND hSheet)
{
	LRESULT lr;
	// editbox
	// server
	GetTextEBConfigDialog(GetDlgItem(hSheet, IDC_EDIT_CONFSERVER_ADDRESS),
		Configure_sheet1->GetServerText(), STRVALUE_SERVER);
	// check box PASV IP
	lr = SendDlgItemMessage(hSheet, IDC_CHECK_PASVIP, BM_GETCHECK, 0, 0);
	Configure_sheet1->SetPasvIPIgnoreMode(lr);

	// hylafax port number
	GetTextEBConfigDialog(GetDlgItem(hSheet, IDC_EDIT_CONFSERVER_HFPORT),
		Configure_sheet1->GetHFPortNumber(), STRVALUE_PORTNUMBER);
}

//*********************************************************
//*********************************************************
// enable settings - all users - default
void EnableDefaultSettings(HWND hSheet)
{
	EnumChildWindows(hSheet, EnumChildProcSettings, 1);
}
//*********************************************************
// disable common settings (server. port ..) user
void DisableCommonSettings(HWND hSheet)
{
	EnableWindow(GetDlgItem(hSheet, IDC_EDIT_CONFSERVER_ADDRESS), FALSE);
	EnableWindow(GetDlgItem(hSheet, IDC_EDIT_CONFSERVER_HFPORT), FALSE);
	EnableWindow(GetDlgItem(hSheet, IDC_CHECK_PASVIP), FALSE);
}
//*********************************************************
// disable common settings (server. port ..) - all users - default
void DisableDefaultSettings(HWND hSheet)
{
	EnumChildWindows(hSheet, EnumChildProcSettings, 0);
}
/*
Return Value:
TRUE - Caller has Administrators local group.
FALSE - Caller does not have Administrators local group. --
*/
BOOL IsUserAdmin(const HANDLE TokenHandle)
{
	PSID            pAdminSid = NULL;
	BYTE            buffer[1024];
	PTOKEN_GROUPS   pGroups = (PTOKEN_GROUPS)buffer;
	DWORD           dwSize;    // buffer size
	DWORD           i;
	BOOL            bSuccess;
	SID_IDENTIFIER_AUTHORITY siaNtAuth = SECURITY_NT_AUTHORITY;

	bSuccess = GetTokenInformation(TokenHandle, TokenGroups, (LPVOID)pGroups, 1024,
		&dwSize);
	if (!bSuccess)
		return FALSE;
	if (!AllocateAndInitializeSid(&siaNtAuth, 2,
		SECURITY_BUILTIN_DOMAIN_RID,
		DOMAIN_ALIAS_RID_ADMINS,
		0, 0, 0, 0, 0, 0, &pAdminSid))
		return FALSE;

	bSuccess = FALSE;
	for (i = 0; (i < pGroups->GroupCount) && !bSuccess; i++)
	{
		if (EqualSid(pAdminSid, pGroups->Groups[i].Sid))
			bSuccess = TRUE;
	}
	FreeSid(pAdminSid);

	return bSuccess;
}
//*********************************************************
// enable-disable all child windows 
BOOL CALLBACK EnumChildProcSettings(HWND hwnd, LPARAM lParam)
{
	LPARAM localID;
	localID = GetWindowLong(hwnd, GWL_ID);
	if (localID != IDC_COMBO_SELECT_USER)
		EnableWindow(hwnd, (BOOL)lParam);
	return TRUE;
}
void DisableLDAPSettings(HWND hSheet)
{
	EnumChildWindows(hSheet, EnumChildProcLDAP, 0);
}
void EnableLDAPSettings(HWND hSheet)
{
	EnumChildWindows(hSheet, EnumChildProcLDAP, 1);
}
// enable-disable all child windows 
BOOL CALLBACK EnumChildProcLDAP(HWND hwnd, LPARAM lParam)
{
	LPARAM localID;
	localID = GetWindowLong(hwnd, GWL_ID);
	if (localID != IDC_CHECK_USE_LDAP)
		EnableWindow(hwnd, (BOOL)lParam);
	return TRUE;
}

//*********************************************************
// check new version
/*
void CheckUpdateVersion(HWND hDlg)
{
	DWORD dwSize = 0;
	DWORD dwDownloaded = 0;
	LPSTR  pszOutBuffer;
	CHAR szUpVer[32];

	BOOL bResults = FALSE, bTimers = FALSE;
	HINTERNET  hSession = NULL, hConnect = NULL, hRequest = NULL;
	// Use WinHttpOpen to obtain a session handle.
	hSession = WinHttpOpen(L"WinHTTP /1.0", WINHTTP_ACCESS_TYPE_DEFAULT_PROXY, WINHTTP_NO_PROXY_NAME, WINHTTP_NO_PROXY_BYPASS, 0);
	// Specify an HTTP server.
	if (hSession)
	{
		// Use WinHttpSetTimeouts to set a new time-out values - in milliseconds
		bTimers = WinHttpSetTimeouts(hSession, 8000, 8000, 8000, 8000);
		if (bTimers)
			hConnect = WinHttpConnect(hSession, L"michalhavranek.bitbucket.org", INTERNET_DEFAULT_HTTP_PORT, 0);
	}
	// Create an HTTP request handle.
	if (hConnect)
		hRequest = WinHttpOpenRequest(hConnect, L"GET", L"/wphfx/Library/version.lbi", NULL, WINHTTP_NO_REFERER, NULL, NULL);
	// Send a request.
	if (hRequest)
		bResults = WinHttpSendRequest(hRequest, WINHTTP_NO_ADDITIONAL_HEADERS, 0, WINHTTP_NO_REQUEST_DATA, 0, 0, 0);
	// End the request.
	if (bResults)
		bResults = WinHttpReceiveResponse(hRequest, NULL);

	// Keep checking for data until there is nothing left.
	if (bResults)
	{
		do
		{
			// Check for available data.
			dwSize = 0;
			if (!WinHttpQueryDataAvailable(hRequest, &dwSize))
			{
				bResults = FALSE;
				break;
			}
			// Allocate space for the buffer.
			if (dwSize == 0)
				break;
			if (dwSize > 16)
			{
				bResults = FALSE;
				break;
			}

			pszOutBuffer = new CHAR[dwSize + 1];
			if (!pszOutBuffer)
			{
				bResults = FALSE;
				dwSize = 0;
			}
			else
			{
				// Read the Data.
				ZeroMemory(pszOutBuffer, dwSize + 1);
				if (!WinHttpReadData(hRequest, (LPVOID)pszOutBuffer, dwSize, &dwDownloaded))
				{
					bResults = FALSE;
					dwSize = 0;
				}
				else
				{
					StringCbCopyA(szUpVer, 32 * sizeof(CHAR), "[ ");
					StringCchCatA(szUpVer, 32, pszOutBuffer);
					StringCchCatA(szUpVer, 32, " ]");
					SendDlgItemMessageA(hDlg, IDC_STATIC_UPDATE, WM_SETTEXT, 0, (LPARAM)szUpVer);
				}
				// Free the memory allocated to the buffer.
				delete[] pszOutBuffer;
			}
		} while (dwSize > 0);

	}

	// Report any errors.
	if (!bResults)
		SendDlgItemMessage(hDlg, IDC_STATIC_UPDATE, WM_SETTEXT, 0, (LPARAM)L"[----]");
	// Close any open handles.
	if (hRequest) WinHttpCloseHandle(hRequest);
	if (hConnect) WinHttpCloseHandle(hConnect);
	if (hSession) WinHttpCloseHandle(hSession);
}
*/
//*********************************************************
//*********************************************************
