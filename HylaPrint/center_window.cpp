//***********************************************************
#include "center_window.h"

//********************************************************
void CenterDialogWindow(HWND hwndDlg)
{
	HWND hwndOwner;
	RECT rc, rcDlg, rcOwner;

	// Get the owner window and dialog box rectangles.
	if ((hwndOwner = GetParent(hwndDlg)) == NULL)
	{
		hwndOwner = GetDesktopWindow();
	}

	GetWindowRect(hwndOwner, &rcOwner);
	GetWindowRect(hwndDlg, &rcDlg);
	CopyRect(&rc, &rcOwner);

	// Offset the owner and dialog box rectangles so that right and bottom
	// values represent the width and height, and then offset the owner again
	// to discard space taken up by the dialog box.

	OffsetRect(&rcDlg, -rcDlg.left, -rcDlg.top);
	OffsetRect(&rc, -rc.left, -rc.top);
	OffsetRect(&rc, -rcDlg.right, -rcDlg.bottom);

	// The new position is the sum of half the remaining space and the owner's
	// original position.

	SetWindowPos(hwndDlg,
		HWND_TOP,
		rcOwner.left + (rc.right / 2),
		rcOwner.top + (rc.bottom / 2),
		0, 0,          // Ignores size arguments.
		SWP_NOSIZE);
}
//********************************************************
