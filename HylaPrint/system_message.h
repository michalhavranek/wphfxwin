#ifndef SysMessageH
#define SysMessageH
// system 
#define FAILURE_SUCCESS "Failure and Success" // index 0
#define SUCCESS_ONLY    "Success Only"        // index 1
#define FAILURE_ONLY    "Failure Only"        // index 2
#define NONE_NOTIF      "None"                // index 3

#define US_LETTER       "US Letter" // index 0
#define A4_SIZE         "A4"        // index 1

#define STAND_RES       "Standard"   // index 0
#define FINE_RES        "Fine"       // index 1
#define SUPER_RES       "Superfine"  // index 2
#define ULTRA_RES       "Ultrafine"  // index 3
#define HYPER_RES       "Hyperfine"  // index 4

#define DEFAULT_USER    "Default (system wide)" // index 0

#define SETTINGS        "Settings"
#define APP_NAME        "Wphfxwin"

#define MAXDIALS_NONE   "-"  // index 0
#define MAXTRIES_NONE   "-"  // index 0
#define MAX_TIMES_1     "1"  // index 1
#define MAX_TIMES_3     "3"  // index 2
#define MAX_TIMES_6     "6"  // index 3
#define MAX_TIMES_9     "9"  // index 4
#define MAX_TIMES_12    "12" // index 5

#define INFODIALOG_TIMER_0 "0"   // index 0
#define INFODIALOG_TIMER_3 "3"   // index 1
#define INFODIALOG_TIMER_5 "5"   // index 2
#define INFODIALOG_TIMER_10 "10" // index 3

// XML
#define HYLAFAX_USE_USERCONFIG           "UseUserConfig"
#define HYLAFAX_WINDOW_POSITION          "WindowPosition"
#define HYLAFAX_WINDOW_CENTER            "Menu_Center"
#define HYLAFAX_WINDOW_REMEMBERPOSITION  "Menu_RememberPosition"
#define HYLAFAX_WINDOW_CURSORPOSITION    "Menu_CursorPosition"
#define HYLAFAX_WINDOW_XPOS              "x-win_position"
#define HYLAFAX_WINDOW_YPOS              "y-win_position"

// registry settings
#define HYLAFAX_SERVER               "Server"
#define HYLAFAX_HF_PORTNUM           "HylaFaxPortNumber"
#define HYLAFAX_USER                 "Username"
#define HYLAFAX_PASSWORD             "Password"
#define HYLAFAX_DEFAULTNOTIFYEMAIL   "DefaultEmail"
#define HYLAFAX_MODEM                "Modem"
#define HYLAFAX_ADDRESSBOOK          "AddressBookPath"
#define HYLAFAX_NOTIFICATION         "NotificationType"
#define HYLAFAX_PAGESIZE             "PageSize"
#define HYLAFAX_RESOLUTION           "Resolution"
#define HYLAFAX_PASVIPIGNORE         "IgnorePassiveIP"
#define HYLAFAX_MAXDIALS             "MaxDials"
#define HYLAFAX_MAXTRIES             "MaxTries"
#define HYLAFAX_INFODIALOG_TIMER     "InfoDialogTimer"
#define HYLAFAX_FAXNUMBER_FROM_CLIPBOARD "FaxNumberFromClipboard"
// active mode
#define HYLAFAX_USEACTIVEMODE        "UseActiveMode"
#define HYLAFAX_ACTIVEPORTNUM        "ActivePortNumber"
// LDAP
#define LDAP_USE_LDAP                "UseLDAP"
#define LDAP_SERVER                  "LDAPServer"
#define LDAP_MYPORT                  "LDAPPort"
#define LDAP_SSL                     "LDAPSSL"
#define LDAP_STARTTLS                "LDAPStartTLS"
#define LDAP_BIND_DN                 "LDAPBindDN"
#define LDAP_PASSWORD                "LDAPPassword"
#define LDAP_BASE_DN                 "LDAPBaseDN"
#define LDAP_FILTER                  "LDAPFilter"
#define LDAP_AUTOCOMPLETE_ATT        "LDAPAutocompleteAttribute"
#define LDAP_AUTOCOMPLETE_ATT_CN     "cn"
#define LDAP_AUTOCOMPLETE_ATT_DISPLAYNAME "displayName"
#define LDAP_FILTER_CN               "(&(objectClass=person)(cn=%s))"
#define LDAP_FILTER_DISPLAYNAME      "(&(objectClass=person)(displayName=%s))"

#endif
