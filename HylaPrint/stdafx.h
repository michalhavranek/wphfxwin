// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once
#include "targetver.h"

// #define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <winsock2.h>
#include <cstdio>
#include <stdlib.h>
#include <iphlpapi.h>
#include <Winhttp.h>
#include <windows.h>
#include <ws2tcpip.h>
#include <winsplp.h>
#include <string>
#include <tchar.h>
#include <CommCtrl.h>
#include <Userenv.h>
#include <Shlobj.h>
#include <Wtsapi32.h>
#include <Strsafe.h>
#include <VersionHelpers.h>

// disable warning
#pragma warning(disable:4996)

// additional lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")
#pragma comment (lib, "ComCtl32.lib")
#pragma comment (lib, "Userenv.lib")
#pragma comment (lib, "Version.lib")
#pragma comment(lib, "IPHLPAPI.lib")
#pragma comment(lib, "Wtsapi32.lib")
#pragma comment(lib, "Winhttp.lib")

#ifndef WIN64 // 32bit
#pragma comment (linker, "/EXPORT:InitializePrintMonitor2=_InitializePrintMonitor2@8")
#pragma comment (linker, "/EXPORT:InitializePrintMonitorUI=_InitializePrintMonitorUI@0")
//#pragma comment (linker, "/EXPORT:DllMain=_DllMain@12")
#else // 64 bit
#pragma comment (linker, "/EXPORT:InitializePrintMonitor2")
#pragma comment (linker, "/EXPORT:InitializePrintMonitorUI")
//#pragma comment (linker, "/EXPORT:DllMain")
#endif
