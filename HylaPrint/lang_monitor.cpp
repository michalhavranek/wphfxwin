#include "stdafx.h"
#include "lang_monitor.h"
#include "../Language/language.h"
#include "resource.h"

// TAB 1
void LocalDialogTab1(HWND hdlg)
{
	// HF server
	SetWindowText(GetDlgItem(hdlg, IDC_STATIC_HF_SERVER), _T(LNG_HYLAFAX_SERVER));
	// port
	SetWindowText(GetDlgItem(hdlg, IDC_STATIC_PORT_NUM), _T(LNG_PORT));
	// Ignore passive IP
	SetWindowText(GetDlgItem(hdlg, IDC_CHECK_PASVIP), _T(LNG_IGNORE_PASVIP));
	// system wide/single
	SetWindowText(GetDlgItem(hdlg, IDC_STATIC_SYSTEMWIDE), _T(LNG_SYSTEMWIDE_SINGLE));
	// user, email, paassword, pagesize .... = user items == lang_usersettings.h
	// .....
}
// TAB 2
void LocalDialogTab2(HWND hdlg)
{
	// active mode
	SetWindowText(GetDlgItem(hdlg, IDC_STATIC_ACTIVE_MODE), _T(LNG_ACTIVEMODE));
	// use active mode
	SetWindowText(GetDlgItem(hdlg, IDC_CHECK_USEACTIVE), _T(LNG_USE_ACTIVEMODE));
	// incoming port
	SetWindowText(GetDlgItem(hdlg, IDC_STATIC_INCOMING_PORT), _T(LNG_INCOMING_PORT));
	
}
// TAB 3
void LocalDialogTab3(HWND hdlg)
{
	// use LDAP
	SetWindowText(GetDlgItem(hdlg, IDC_CHECK_USE_LDAP), _T(LNG_USE_LDAP));
	// LDAP settings
	SetWindowText(GetDlgItem(hdlg, IDC_STATIC_LDAP_SETTINGS), _T(LNG_LDAP_SETTINGS));
	// LDAP server
	SetWindowText(GetDlgItem(hdlg, IDC_STATIC_LDAP_SERVER), _T(LNG_LDAP_SERVER));
	// port
	SetWindowText(GetDlgItem(hdlg, IDC_STATIC_LDAP_PORT), _T(LNG_PORT));
	// StartTLS
	SetWindowText(GetDlgItem(hdlg, IDC_CHECK_LDAP_STARTTLS), _T(LNG_START_TLS));
	// SSL
	SetWindowText(GetDlgItem(hdlg, IDC_CHECK_LDAP_SSL), _T(LNG_SSL));
	// bind DN
	SetWindowText(GetDlgItem(hdlg, IDC_STATIC_LDAP_BIND_DN), _T(LNG_BIND_DN));
	// password
	SetWindowText(GetDlgItem(hdlg, IDC_STATIC_LDAP_PASSWORD), _T(LNG_PASSWORD));
	// advanced
	SetWindowText(GetDlgItem(hdlg, IDC_STATIC_LDAP_ADVANCED), _T(LNG_ADVANCED));
	// base DN
	SetWindowText(GetDlgItem(hdlg, IDC_STATIC_LDAP_BASE_DN), _T(LNG_BASE_DN));
	// search filter
	SetWindowText(GetDlgItem(hdlg, IDC_STATIC_LDAP_FILTER), _T(LNG_SEARCH_FILTER));
	// autocomplete
	SetWindowText(GetDlgItem(hdlg, IDC_STATIC_LDAP_SEARCH_ATTRIBUTE), _T(LNG_AUTOCOMPLETE));
}
// users items
#include "../HylaPrintUI/lang_usersettings.h"
// **********************************************************************
