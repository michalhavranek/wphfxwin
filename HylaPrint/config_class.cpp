//---------------------------------------------------------------------------
#include "config_class.h"

CConfigure_sheet1* Configure_sheet1;
CUserConfigure* UserConfigure;
CConfigure_sheet2* ConfigureActiveMode_sheet2;

//******************************************************************
// sheet 1 - basic config system wide/single user
//******************************************************************
CConfigure_sheet1::CConfigure_sheet1()
{
	// system wide variables
	returnValue = FALSE;             // 1
	szServer[0] = L'\0';             // 2
	szHylafaxPort[0] = L'\0';        // 3
	pasv_ignoreIP = 0;               // 4
	hInst = NULL;                    // 5

	// initialise protected variables, system wide/single user - inherits to userconfigure
	szUser[0] = L'\0';             // 1              
	szPasswd[0] = L'\0';           // 2
	szEmail[0] = L'\0';            // 3
	szModem[0] = L'\0';            // 4
	szAddrBook[0] = L'\0';         // 5
	szPageSize[0] = L'\0';         // 6
	szResolution[0] = L'\0';       // 7
	szNotification[0] = L'\0';     // 8
	szMaxDials[0] = L'\0';         // 9
	szMaxTries[0] = L'\0';         // 10
	szInfoDialogTimer[0] = L'\0';  // 11
	dwFaxNumberFromClipboard = 0;  // 12

}
CConfigure_sheet1::~CConfigure_sheet1()
{
}
//******************************************************************

void CConfigure_sheet1::OKClick()
{
	returnValue = TRUE;
}
void CConfigure_sheet1::CancelClick()
{
	returnValue = FALSE;
}

//******************************************************************
// SET - sheet 1
//******************************************************************
void CConfigure_sheet1::SetServerText(LPCWSTR text)
{
	wcsncpy_s(szServer, _countof(szServer), text, _TRUNCATE);
}
void CConfigure_sheet1::SetUsernameText(LPCWSTR text)
{
	wcsncpy_s(szUser, _countof(szUser), text, _TRUNCATE);
}
void CConfigure_sheet1::SetPasswordText(LPCWSTR text)
{
	wcsncpy_s(szPasswd, _countof(szPasswd), text, _TRUNCATE);
}
void CConfigure_sheet1::SetEmailText(LPCWSTR text)
{
	wcsncpy_s(szEmail, _countof(szEmail), text, _TRUNCATE);
}
void CConfigure_sheet1::SetModemText(LPCWSTR text)
{
	wcsncpy_s(szModem, _countof(szModem), text, _TRUNCATE);
}
void CConfigure_sheet1::SetAddressBookText(LPCWSTR text)
{
	wcsncpy_s(szAddrBook, _countof(szAddrBook), text, _TRUNCATE);
}
void CConfigure_sheet1::SetHFPortNumber(LPCWSTR text)
{
	int x;
	x = _wtoi(text);
	if (x > 65535 || x < 1024)
		wcscpy_s(szHylafaxPort, _countof(szHylafaxPort), L"4559");
	else
		wcsncpy_s(szHylafaxPort, _countof(szHylafaxPort), text, _TRUNCATE);
}
LPWSTR CConfigure_sheet1::GetHFPortNumber()
{
	return szHylafaxPort;
}
LPWSTR CConfigure_sheet1::GetServerText()
{
	return szServer;
}
LPWSTR CConfigure_sheet1::GetUsernameText()
{
	return szUser;
}
LPWSTR CConfigure_sheet1::GetPasswordText()
{
	return szPasswd;
}
LPWSTR CConfigure_sheet1::GetEmailText()
{
	return szEmail;
}
LPWSTR CConfigure_sheet1::GetModemText()
{
	return szModem;
}
LPWSTR CConfigure_sheet1::GetAddressBookText()
{
	return szAddrBook;
}
void CConfigure_sheet1::SetPasvIPIgnoreMode(LRESULT mode)
{
	pasv_ignoreIP = mode;
}
LRESULT CConfigure_sheet1::GetPasvIPIgnoreMode()
{
	return pasv_ignoreIP;
}

void CConfigure_sheet1::SetPageSizeText(LPWSTR str)
{
	wcsncpy_s(szPageSize, _countof(szPageSize), str, _TRUNCATE);
}
void CConfigure_sheet1::SetResolutionText(LPWSTR str)
{
	wcsncpy_s(szResolution, _countof(szResolution), str, _TRUNCATE);
}
void CConfigure_sheet1::SetNotificationText(LPWSTR str)
{
	wcsncpy_s(szNotification, _countof(szNotification), str, _TRUNCATE);
}
void CConfigure_sheet1::SetMaxDialsText(LPWSTR str)
{
	wcsncpy_s(szMaxDials, _countof(szMaxDials), str, _TRUNCATE);
}
void CConfigure_sheet1::SetMaxTriesText(LPWSTR str)
{
	wcsncpy_s(szMaxTries, _countof(szMaxTries), str, _TRUNCATE);
}
void CConfigure_sheet1::SetInfoDialogTimerText(LPWSTR str)
{
	wcsncpy_s(szInfoDialogTimer, _countof(szInfoDialogTimer), str, _TRUNCATE);
}
void CConfigure_sheet1::SetHinstance(HINSTANCE hinst)
{
	hInst = hinst;
}
void CConfigure_sheet1::SetUseFaxnumberFromClipboard(DWORD dw)
{
	dwFaxNumberFromClipboard = dw;
}

//******************************************************************
// GET - sheet 1
//******************************************************************
BOOL CConfigure_sheet1::GetReturnValue()
{
	return returnValue;
}
HINSTANCE CConfigure_sheet1::GetHinstance()
{
	return hInst;
}
LPWSTR CConfigure_sheet1::GetPageSizeText()
{
	return szPageSize;
}
LPWSTR CConfigure_sheet1::GetResolutionText()
{
	return szResolution;
}
LPWSTR CConfigure_sheet1::GetNotificationText()
{
	return szNotification;
}
LPWSTR CConfigure_sheet1::GetMaxDialsText()
{
	return szMaxDials;
}
LPWSTR CConfigure_sheet1::GetMaxTriesText()
{
	return szMaxTries;
}
LPWSTR CConfigure_sheet1::GetInfoDialogTimerText()
{
	return szInfoDialogTimer;
}
DWORD CConfigure_sheet1::GetUseFaxnumberFromClipboard()
{
	return dwFaxNumberFromClipboard;
}

//******************************************************************
// class - single user configure (derived from Configure class sheet1
//******************************************************************
CUserConfigure::CUserConfigure()
{
	// default - if not exists node in LoadXML()
	bUseUserConfig = FALSE;
	// default variables - later set in function Get_user_from_sessionid()
	bUserIsAdmin = FALSE;
	szLoggedInUser[0] = L'\0';
	// default comboboxes - if not exists node in LoadXML()
	wcscpy_s(szNotification, _countof(szNotification), _T(FAILURE_SUCCESS));
	wcscpy_s(szPageSize, _countof(szPageSize), _T(A4_SIZE));
	wcscpy_s(szResolution, _countof(szResolution), _T(FINE_RES));
	wcscpy_s(szMaxDials, _countof(szMaxDials), _T(MAXDIALS_NONE));
	wcscpy_s(szMaxTries, _countof(szMaxTries), _T(MAXTRIES_NONE));
	wcscpy_s(szInfoDialogTimer, _countof(szInfoDialogTimer), _T(INFODIALOG_TIMER_3));
	// default window position - if not exists node in LoadXML()
	x_win_pos = 0;
	y_win_pos = 0;
	iMenuWindowCenter = 1;
	iMenuWindowRememberPos = 0;
	iMenuWindowCursorPos = 0;
}
CUserConfigure::~CUserConfigure()
{
}
LPWSTR CUserConfigure::GetLoggedInUser()
{
	return szLoggedInUser;
}
void CUserConfigure::SetLoggedInUser(LPWSTR str)
{
	wcsncpy_s(szLoggedInUser, _countof(szLoggedInUser), str, _TRUNCATE);
}
void CUserConfigure::SetUseUserConfig(BOOL bUse)
{
	bUseUserConfig = bUse;
}
BOOL CUserConfigure::GetUseUserConfig()const
{
	return bUseUserConfig;
}
void CUserConfigure::SetIsAdmin(BOOL bUse)
{
	bUserIsAdmin = bUse;
}
BOOL CUserConfigure::GetIsAdmin()const
{
	return bUserIsAdmin;
}
int CUserConfigure::GetXWinPos()const
{
	return x_win_pos;
}
int CUserConfigure::GetYWinPos()const
{
	return y_win_pos;
}
int CUserConfigure::GetIntWinCenter()const
{
	return iMenuWindowCenter;
}
int CUserConfigure::GetIntWinRememberPos()const
{
	return iMenuWindowRememberPos;
}
int CUserConfigure::GetIntWinCursorPos()const
{
	return iMenuWindowCursorPos;
}

void CUserConfigure::LoadXML()
{
	tinyxml2::XMLDocument doc;
	const tinyxml2::XMLElement* node, * exists;
	tinyxml2::XMLError xmlResult;
	WCHAR szWidecharString[MAX_PATH];
	CHAR szMultibyteString[MAX_PATH];
	const CHAR* title = NULL;
	int iUseClipboard;

	// user folder
	GetUserFolder(szWidecharString);
	UnicodeToAnsi(szWidecharString, szMultibyteString, _countof(szMultibyteString));

	if (doc.LoadFile(szMultibyteString) != tinyxml2::XML_SUCCESS)
		return;

	node = doc.FirstChildElement()->FirstChildElement(SETTINGS);
	// user config
	if (node)
	{
		exists = node->FirstChildElement(HYLAFAX_USE_USERCONFIG);
		if (exists)
		{
			title = node->FirstChildElement(HYLAFAX_USE_USERCONFIG)->GetText();
			// prevent null pointer
			if (title)
				if (title[0] == '0')
					bUseUserConfig = FALSE;
				else
					bUseUserConfig = TRUE;
			else
				bUseUserConfig = FALSE;
		}
		// username
		exists = node->FirstChildElement(HYLAFAX_USER);
		if (exists)
		{
			title = node->FirstChildElement(HYLAFAX_USER)->GetText();
			AnsiToUnicode(title, szWidecharString, _countof(szWidecharString));
			SetUsernameText(szWidecharString);
		}
		// password
		exists = node->FirstChildElement(HYLAFAX_PASSWORD);
		if (exists)
		{
			title = node->FirstChildElement(HYLAFAX_PASSWORD)->GetText();
			AnsiToUnicode(title, szWidecharString, _countof(szWidecharString));
			SetPasswordText(szWidecharString);
		}
		// notify email
		exists = node->FirstChildElement(HYLAFAX_DEFAULTNOTIFYEMAIL);
		if (exists)
		{
			title = node->FirstChildElement(HYLAFAX_DEFAULTNOTIFYEMAIL)->GetText();
			AnsiToUnicode(title, szWidecharString, _countof(szWidecharString));
			SetEmailText(szWidecharString);
		}
		// modem
		exists = node->FirstChildElement(HYLAFAX_MODEM);
		if (exists)
		{
			title = node->FirstChildElement(HYLAFAX_MODEM)->GetText();
			AnsiToUnicode(title, szWidecharString, _countof(szWidecharString));
			SetModemText(szWidecharString);
		}
		// addrbook
		exists = node->FirstChildElement(HYLAFAX_ADDRESSBOOK);
		if (exists)
		{
			title = node->FirstChildElement(HYLAFAX_ADDRESSBOOK)->GetText();
			AnsiToUnicode(title, szWidecharString, _countof(szWidecharString));
			SetAddressBookText(szWidecharString);
		}
		// notification
		exists = node->FirstChildElement(HYLAFAX_NOTIFICATION);
		if (exists)
		{
			title = node->FirstChildElement(HYLAFAX_NOTIFICATION)->GetText();
			AnsiToUnicode(title, szWidecharString, _countof(szWidecharString));
			SetNotificationText(szWidecharString);
		}
		// pagesize
		exists = node->FirstChildElement(HYLAFAX_PAGESIZE);
		if (exists)
		{
			title = node->FirstChildElement(HYLAFAX_PAGESIZE)->GetText();
			AnsiToUnicode(title, szWidecharString, _countof(szWidecharString));
			SetPageSizeText(szWidecharString);
		}
		// resolution
		exists = node->FirstChildElement(HYLAFAX_RESOLUTION);
		if (exists)
		{
			title = node->FirstChildElement(HYLAFAX_RESOLUTION)->GetText();
			AnsiToUnicode(title, szWidecharString, _countof(szWidecharString));
			SetResolutionText(szWidecharString);
		}
		// maxtries
		exists = node->FirstChildElement(HYLAFAX_MAXTRIES);
		if (exists)
		{
			title = node->FirstChildElement(HYLAFAX_MAXTRIES)->GetText();
			AnsiToUnicode(title, szWidecharString, _countof(szWidecharString));
			SetMaxTriesText(szWidecharString);
		}
		// maxdials
		exists = node->FirstChildElement(HYLAFAX_MAXDIALS);
		if (exists)
		{
			title = node->FirstChildElement(HYLAFAX_MAXDIALS)->GetText();
			AnsiToUnicode(title, szWidecharString, _countof(szWidecharString));
			SetMaxDialsText(szWidecharString);
		}
		// infodialog timer
		exists = node->FirstChildElement(HYLAFAX_INFODIALOG_TIMER);
		if (exists)
		{
			title = node->FirstChildElement(HYLAFAX_INFODIALOG_TIMER)->GetText();
			AnsiToUnicode(title, szWidecharString, _countof(szWidecharString));
			SetInfoDialogTimerText(szWidecharString);
		}
		// faxnumber from the clipboard
		exists = node->FirstChildElement(HYLAFAX_FAXNUMBER_FROM_CLIPBOARD);
		if (exists)
		{
			xmlResult = exists->QueryIntText(&iUseClipboard);
			if (xmlResult == tinyxml2::XML_SUCCESS)
				SetUseFaxnumberFromClipboard(iUseClipboard);
			else // error
				SetUseFaxnumberFromClipboard(0);
		}
	} // end user config 

	// window position
	node = doc.FirstChildElement()->FirstChildElement(HYLAFAX_WINDOW_POSITION);
	if (!node) return;
	// center window
	exists = node->FirstChildElement(HYLAFAX_WINDOW_CENTER);
	if (exists)
		node->FirstChildElement(HYLAFAX_WINDOW_CENTER)->QueryIntText(&iMenuWindowCenter);
	// remember position
	exists = node->FirstChildElement(HYLAFAX_WINDOW_REMEMBERPOSITION);
	if (exists)
		node->FirstChildElement(HYLAFAX_WINDOW_REMEMBERPOSITION)->QueryIntText(&iMenuWindowRememberPos);
	// cursor position
	exists = node->FirstChildElement(HYLAFAX_WINDOW_CURSORPOSITION);
	if (exists)
		node->FirstChildElement(HYLAFAX_WINDOW_CURSORPOSITION)->QueryIntText(&iMenuWindowCursorPos);
	// x window coordinate
	exists = node->FirstChildElement(HYLAFAX_WINDOW_XPOS);
	if (exists)
		node->FirstChildElement(HYLAFAX_WINDOW_XPOS)->QueryIntText(&x_win_pos);
	// y window coordinate
	exists = node->FirstChildElement(HYLAFAX_WINDOW_YPOS);
	if (exists)
		node->FirstChildElement(HYLAFAX_WINDOW_YPOS)->QueryIntText(&y_win_pos);
}
// save XML variable 
// *************************************************************************
BOOL CUserConfigure::SaveXML()
{
	tinyxml2::XMLDeclaration* decl;
	tinyxml2::XMLElement* root, * settings, * elem;
	tinyxml2::XMLDocument doc;
	WCHAR szWidecharString[MAX_PATH];
	CHAR szMultibyteString[STRVALUE_TEXT_512];

	// user folder
	GetUserFolder(szWidecharString);
	UnicodeToAnsi(szWidecharString, szMultibyteString, _countof(szMultibyteString));
	if (doc.LoadFile(szMultibyteString) != tinyxml2::XML_SUCCESS)
	{
		decl = doc.NewDeclaration();
		doc.InsertFirstChild(decl);
		root = doc.NewElement(APP_NAME);
	}
	else
		root = doc.FirstChildElement(APP_NAME);

	if (!root) return FALSE;
	settings = root->FirstChildElement(SETTINGS);
	if (!settings)
		settings = doc.NewElement(SETTINGS);
	if (!settings) return FALSE;

	// user config
	elem = settings->FirstChildElement(HYLAFAX_USE_USERCONFIG);
	if (!elem)
		elem = doc.NewElement(HYLAFAX_USE_USERCONFIG);
	if (!elem) return FALSE;
	if (bUseUserConfig)
		elem->SetText("1");
	else
		elem->SetText("0");
	settings->InsertEndChild(elem);
	// username
	UnicodeToAnsi(GetUsernameText(), szMultibyteString, _countof(szMultibyteString));
	elem = settings->FirstChildElement(HYLAFAX_USER);
	if (!elem)
		elem = doc.NewElement(HYLAFAX_USER);
	if (!elem) return FALSE;
	elem->SetText(szMultibyteString);
	settings->InsertEndChild(elem);
	// password
	UnicodeToAnsi(GetPasswordText(), szMultibyteString, _countof(szMultibyteString));
	elem = settings->FirstChildElement(HYLAFAX_PASSWORD);
	if (!elem)
		elem = doc.NewElement(HYLAFAX_PASSWORD);
	if (!elem) return FALSE;
	elem->SetText(szMultibyteString);
	settings->InsertEndChild(elem);
	// notify email
	UnicodeToAnsi(GetEmailText(), szMultibyteString, _countof(szMultibyteString));
	elem = settings->FirstChildElement(HYLAFAX_DEFAULTNOTIFYEMAIL);
	if (!elem)
		elem = doc.NewElement(HYLAFAX_DEFAULTNOTIFYEMAIL);
	if (!elem) return FALSE;
	elem->SetText(szMultibyteString);
	settings->InsertEndChild(elem);
	// notify type
	UnicodeToAnsi(GetNotificationText(), szMultibyteString, _countof(szMultibyteString));
	elem = settings->FirstChildElement(HYLAFAX_NOTIFICATION);
	if (!elem)
		elem = doc.NewElement(HYLAFAX_NOTIFICATION);
	if (!elem) return FALSE;
	elem->SetText(szMultibyteString);
	settings->InsertEndChild(elem);
	// modem
	UnicodeToAnsi(GetModemText(), szMultibyteString, _countof(szMultibyteString));
	elem = settings->FirstChildElement(HYLAFAX_MODEM);
	if (!elem)
		elem = doc.NewElement(HYLAFAX_MODEM);
	if (!elem) return FALSE;
	elem->SetText(szMultibyteString);
	settings->InsertEndChild(elem);
	// addrbook
	UnicodeToAnsi(GetAddressBookText(), szMultibyteString, _countof(szMultibyteString));
	elem = settings->FirstChildElement(HYLAFAX_ADDRESSBOOK);
	if (!elem)
		elem = doc.NewElement(HYLAFAX_ADDRESSBOOK);
	if (!elem) return FALSE;
	elem->SetText(szMultibyteString);
	settings->InsertEndChild(elem);
	// page size
	UnicodeToAnsi(GetPageSizeText(), szMultibyteString, _countof(szMultibyteString));
	elem = settings->FirstChildElement(HYLAFAX_PAGESIZE);
	if (!elem)
		elem = doc.NewElement(HYLAFAX_PAGESIZE);
	if (!elem) return FALSE;
	elem->SetText(szMultibyteString);
	settings->InsertEndChild(elem);
	// resolution
	UnicodeToAnsi(GetResolutionText(), szMultibyteString, _countof(szMultibyteString));
	elem = settings->FirstChildElement(HYLAFAX_RESOLUTION);
	if (!elem)
		elem = doc.NewElement(HYLAFAX_RESOLUTION);
	if (!elem) return FALSE;
	elem->SetText(szMultibyteString);
	settings->InsertEndChild(elem);
	// maxdials
	UnicodeToAnsi(GetMaxDialsText(), szMultibyteString, _countof(szMultibyteString));
	elem = settings->FirstChildElement(HYLAFAX_MAXDIALS);
	if (!elem)
		elem = doc.NewElement(HYLAFAX_MAXDIALS);
	if (!elem) return FALSE;
	elem->SetText(szMultibyteString);
	settings->InsertEndChild(elem);
	// maxtries
	UnicodeToAnsi(GetMaxTriesText(), szMultibyteString, _countof(szMultibyteString));
	elem = settings->FirstChildElement(HYLAFAX_MAXTRIES);
	if (!elem)
		elem = doc.NewElement(HYLAFAX_MAXTRIES);
	if (!elem) return FALSE;
	elem->SetText(szMultibyteString);
	settings->InsertEndChild(elem);
	// infodialog timer
	UnicodeToAnsi(GetInfoDialogTimerText(), szMultibyteString, _countof(szMultibyteString));
	elem = settings->FirstChildElement(HYLAFAX_INFODIALOG_TIMER);
	if (!elem)
		elem = doc.NewElement(HYLAFAX_INFODIALOG_TIMER);
	if (!elem) return FALSE;
	elem->SetText(szMultibyteString);
	settings->InsertEndChild(elem);
	// faxnumber from the clipboard
	elem = settings->FirstChildElement(HYLAFAX_FAXNUMBER_FROM_CLIPBOARD);
	if (!elem)
		elem = doc.NewElement(HYLAFAX_FAXNUMBER_FROM_CLIPBOARD);
	if (!elem) return FALSE;
	(GetUseFaxnumberFromClipboard() == 0) ? elem->SetText("0") : elem->SetText("1");
	settings->InsertEndChild(elem);

	root->InsertEndChild(settings);
	doc.InsertEndChild(root);
	doc.SetBOM(true);
	// user folder
	GetUserFolder(szWidecharString);
	UnicodeToAnsi(szWidecharString, szMultibyteString, _countof(szMultibyteString));
	doc.SaveFile(szMultibyteString);

	return TRUE;
}
// ***********************************************
// user directory
void GetUserFolder(LPWSTR szPath)
{
	SHGetFolderPath(NULL, CSIDL_APPDATA,
		NULL, 0, szPath);
	StringCchCat(szPath, MAX_PATH, L"\\");
	StringCchCat(szPath, MAX_PATH, _T(APP_NAME));
	CreateDirectory(szPath, NULL);
	StringCchCat(szPath, MAX_PATH, L"\\settings.xml");
}
//*********************************************************
// address book directory
BOOL BrowseForFolder(LPWSTR pszPath, int iMaxLength)
{
	HRESULT hr;

	// Allocate memory for our root folder...
	LPMALLOC pMalloc;
	hr = SHGetMalloc(&pMalloc);

	if (hr != S_OK)
		return FALSE;

	// Setup our BROWSEINFO structure...
	BROWSEINFO bi = { 0 };
	bi.lpszTitle = L"Select a folder:";
	bi.ulFlags = BIF_NEWDIALOGSTYLE;

	// Ask the user to select a folder...
	LPITEMIDLIST pItemIDList = SHBrowseForFolder(&bi);

	// Get the path of the selected folder...
	WCHAR szPath[MAX_PATH] = { 0 };
	SHGetPathFromIDList(pItemIDList, szPath);

	// Free the memory that we previously allocated...
	pMalloc->Free(pItemIDList);
	pMalloc->Release();

	if (lstrlen(szPath) == 0)
	{
		// Invalid selection...
		return FALSE;
	}

	StringCchCopy(pszPath, iMaxLength, szPath);
	return TRUE;
}

//******************************************************************
// sheet 2 - Active mode class
//******************************************************************
CConfigure_sheet2::CConfigure_sheet2()
{
	active_mode = FALSE;               // 1
	szActivePortNumber[0] = L'\0';     // 2
}
CConfigure_sheet2::~CConfigure_sheet2()
{

}

//******************************************************************
// SET - sheet 2
//******************************************************************
void CConfigure_sheet2::SetActivePortNumber(LPCWSTR text)
{
	int x;
	x = _wtoi(text);
	if (x > 65535 || x < 1024)
		wcscpy_s(szActivePortNumber, _countof(szActivePortNumber), L"32001");
	else
		wcsncpy_s(szActivePortNumber, _countof(szActivePortNumber), text, _TRUNCATE);
}
void CConfigure_sheet2::SetFTPActiveMode(LRESULT mode)
{
	active_mode = mode;
}
//******************************************************************
// GET- sheet 2
//******************************************************************
LPWSTR CConfigure_sheet2::GetActivePortNumber()
{
	return szActivePortNumber;
}
LRESULT CConfigure_sheet2::GetFTPActiveMode()
{
	return active_mode;
}

//******************************************************************
// sheet 3 -LDAP Class
//******************************************************************
CConfigure_sheet3::CConfigure_sheet3()
{
	lrUseLDAP = 0;                           // 1
	lrLDAPSsl = 0;                           // 2
	lrLDAPStartTLS = 0;                      // 3
	lrLDAPAutocompleteAttIndex = 0;          // 4
	szLDAPServer[0] = L'\0';                 // 5
	szLDAPPort[0] = L'\0';                   // 6
	szLDAPBindDN[0] = L'\0';                 // 7
	szLDAPPassword[0] = L'\0';               // 8
	szLDAPBaseDN[0] = L'\0';                 // 9
	szLDAPFilter[0] = L'\0';                 // 10
	szLDAPAutocompleteAttribute[0] = L'\0';  // 11
}
CConfigure_sheet3::~CConfigure_sheet3()
{

}
//******************************************************************
// SET - sheet 3
//******************************************************************
void CConfigure_sheet3::SetLDAPServer(LPCWSTR text)
{
	wcsncpy_s(szLDAPServer, _countof(szLDAPServer), text, _TRUNCATE);
}
void CConfigure_sheet3::SetLDAPPort(LPCWSTR text)
{
	wcsncpy_s(szLDAPPort, _countof(szLDAPPort), text, _TRUNCATE);
}
void CConfigure_sheet3::SetLDAPBindDN(LPCWSTR text)
{
	wcsncpy_s(szLDAPBindDN, _countof(szLDAPBindDN), text, _TRUNCATE);
}
void CConfigure_sheet3::SetLDAPPassword(LPCWSTR text)
{
	wcsncpy_s(szLDAPPassword, _countof(szLDAPPassword), text, _TRUNCATE);
}
void CConfigure_sheet3::SetLDAPBaseDN(LPCWSTR text)
{
	wcsncpy_s(szLDAPBaseDN, _countof(szLDAPBaseDN), text, _TRUNCATE);
}
void CConfigure_sheet3::SetLDAPFilter(LPCWSTR text)
{
	wcsncpy_s(szLDAPFilter, _countof(szLDAPFilter), text, _TRUNCATE);
}
void CConfigure_sheet3::SetUseLDAP(LRESULT lr)
{
	lrUseLDAP = lr;
}
void CConfigure_sheet3::SetUseLDAPSsl(LRESULT lr)
{
	lrLDAPSsl = lr;
}
void CConfigure_sheet3::SetUseLDAPStartTLS(LRESULT lr)
{
	lrLDAPStartTLS = lr;
}
void CConfigure_sheet3::SetLDAPAutocompleteAttIndex(LRESULT lr)
{
	lrLDAPAutocompleteAttIndex = lr;
}
void CConfigure_sheet3::SetLDAPAutocompleteAttribute(LPCWSTR text)
{
	wcsncpy_s(szLDAPAutocompleteAttribute, _countof(szLDAPAutocompleteAttribute), text, _TRUNCATE);
}
//******************************************************************
// GET - sheet 3
//******************************************************************
LPWSTR CConfigure_sheet3::GetLDAPServer()
{
	return szLDAPServer;
}
LPWSTR CConfigure_sheet3::GetLDAPPort()
{
	return szLDAPPort;
}
LPWSTR CConfigure_sheet3::GetLDAPBindDN()
{
	return szLDAPBindDN;
}
LPWSTR CConfigure_sheet3::GetLDAPPassword()
{
	return szLDAPPassword;
}
LPWSTR CConfigure_sheet3::GetLDAPBaseDN()
{
	return szLDAPBaseDN;
}
LPWSTR CConfigure_sheet3::GetLDAPFilter()
{
	return szLDAPFilter;
}
LPWSTR CConfigure_sheet3::GetLDAPAutocompleteAttribute()
{
	return szLDAPAutocompleteAttribute;
}
LRESULT CConfigure_sheet3::GetUseLDAPSsl()
{
	return lrLDAPSsl;
}
LRESULT CConfigure_sheet3::GetUseLDAPStartTLS()
{
	return lrLDAPStartTLS;
}
LRESULT CConfigure_sheet3::GetUseLDAP()
{
	return lrUseLDAP;
}
LRESULT CConfigure_sheet3::GetLDAPAutocompleteAttIndex()
{
	return lrLDAPAutocompleteAttIndex;
}
//******************************************************************
//******************************************************************
