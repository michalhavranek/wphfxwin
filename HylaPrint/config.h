//---------------------------------------------------------------------------

#ifndef ConfigH
#define ConfigH
//---------------------------------------------------------------------------

#include "resource.h"
#include "constants.h"
#include "system_message.h"
#include "../Language/language.h"
#include "convert_string.h"
#include "config_class.h"
#include "center_window.h"
#include "lang_monitor.h"

void ConfigCallUpDialog(HINSTANCE hInst, HWND hwnd);
void GetTextEBConfigDialog(HWND hEdit, LPWSTR str, size_t iBufLen);
LRESULT GetTextItemCB(HWND hCombo, LPWSTR str, size_t iBufLen);
INT_PTR CALLBACK ConfigProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
void GetVersionHyla(HWND hDlg);
INT_PTR CreateSheet(HWND hSettings);
LRESULT CALLBACK Sheet1(HWND hSheet, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK Sheet2(HWND hSheet, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK Sheet3(HWND hSheet, UINT message, WPARAM wParam, LPARAM lParam);
void EnableDisableActiveMode(HWND hwnd, LRESULT lrActiveMode);
//void CheckUpdateVersion(HWND hDlg);
void CreateTooltip(HWND hwnd, HINSTANCE hInst, LPWSTR szToolTipText);
void SaveDefaultUserData(HWND hSheet);
void EnableDefaultSettings(HWND hSheet);
void DisableDefaultSettings(HWND hSheet);
void EnableLDAPSettings(HWND hSheet);
void DisableLDAPSettings(HWND hSheet);
void DisableCommonSettings(HWND hSheet);
BOOL CALLBACK EnumChildProcSettings(HWND hwnd, LPARAM lParam);
BOOL CALLBACK EnumChildProcLDAP(HWND hwnd, LPARAM lParam);
BOOL IsUserAdmin(const HANDLE TokenHandle);

//---------------------------------------------------------------------------
#endif
