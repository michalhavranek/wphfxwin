#ifndef UserConfigH
#define UserConfigH

#include "windows.h"
#include <tchar.h>
#include <Strsafe.h>
#include <Shlobj.h>
#include "system_message.h"
#include "constants.h"
#include "convert_string.h"
#include "tinyxml2.h"

//******************************************************************************
void GetUserFolder(LPWSTR szPath);
BOOL BrowseForFolder(LPWSTR pszPath, int iMaxLength);

//******************************************************************************
// sheet1 - class basic configuration, system wide/single user
//******************************************************************************
class CConfigure_sheet1
{
private:	// system wide data
	BOOL returnValue;                                        // 1
	WCHAR szServer[STRVALUE_SERVER];                         // 2
	WCHAR szHylafaxPort[STRVALUE_PORTNUMBER];                // 3
	LRESULT pasv_ignoreIP;                                   // 4
	HINSTANCE hInst;                                         // 5

protected: // data system wide/single user - inherits to userconfigure
	WCHAR szUser[STRVALUE_USER];                               // 1
	WCHAR szPasswd[STRVALUE_PASSWORD];                         // 2
	WCHAR szEmail[STRVALUE_EMAIL];                             // 3
	WCHAR szModem[STRVALUE_MODEM];                             // 4
	WCHAR szAddrBook[MAX_PATH];                                // 5
	WCHAR szPageSize[STRVALUE_PAGESIZE];                       // 6
	WCHAR szResolution[STRVALUE_RESOLUTION];                   // 7
	WCHAR szNotification[STRVALUE_NOTIFICATION];               // 8
	WCHAR szMaxDials[STRVALUE_MAXTIMES_NUMBER];                // 9
	WCHAR szMaxTries[STRVALUE_MAXTIMES_NUMBER];                // 10
	WCHAR szInfoDialogTimer[STRVALUE_INFODIALOG_TIMER_NUMBER]; // 11
	DWORD dwFaxNumberFromClipboard;                            // 12

public:
	CConfigure_sheet1();
	virtual ~CConfigure_sheet1();
	// system wide/single user
	void SetUsernameText(LPCWSTR text);
	void SetPasswordText(LPCWSTR text);
	void SetEmailText(LPCWSTR text);
	void SetModemText(LPCWSTR text);
	void SetAddressBookText(LPCWSTR text);
	void SetPageSizeText(LPWSTR str);
	void SetResolutionText(LPWSTR str);
	void SetNotificationText(LPWSTR str);
	void SetMaxDialsText(LPWSTR str);
	void SetMaxTriesText(LPWSTR str);
	void SetInfoDialogTimerText(LPWSTR str);
	void SetUseFaxnumberFromClipboard(DWORD dw);

	// system wide data
	void OKClick();
	void CancelClick();
	void SetServerText(LPCWSTR text);
	void SetHFPortNumber(LPCWSTR text);
	void SetPasvIPIgnoreMode(LRESULT mode);
	void SetHinstance(HINSTANCE hinst);

	// system wide data
	BOOL GetReturnValue();
	LPWSTR GetServerText();
	LPWSTR GetHFPortNumber();
	HINSTANCE GetHinstance();
	LRESULT GetPasvIPIgnoreMode();

	// system wide/single user
	LPWSTR GetUsernameText();
	LPWSTR GetPasswordText();
	LPWSTR GetEmailText();
	LPWSTR GetModemText();
	LPWSTR GetAddressBookText();
	LPWSTR GetPageSizeText();
	LPWSTR GetResolutionText();
	LPWSTR GetNotificationText();
	LPWSTR GetMaxDialsText();
	LPWSTR GetMaxTriesText();
	LPWSTR GetInfoDialogTimerText();
	DWORD GetUseFaxnumberFromClipboard();
};
extern CConfigure_sheet1* Configure_sheet1;

//******************************************************************************
// class for single user config
//******************************************************************************
class CUserConfigure : public CConfigure_sheet1
{
public:
	CUserConfigure();
	~CUserConfigure();
	void SetUseUserConfig(BOOL bUse);
	void SetIsAdmin(BOOL bUse);
	BOOL GetUseUserConfig()const;
	BOOL GetIsAdmin()const;
	LPWSTR GetLoggedInUser();
	void SetLoggedInUser(LPWSTR str);
	BOOL SaveXML();
	void LoadXML();
	int GetXWinPos()const;
	int GetYWinPos()const;
	int GetIntWinCenter()const;
	int GetIntWinRememberPos()const;
	int GetIntWinCursorPos()const;
private:
	BOOL bUseUserConfig;
	WCHAR szLoggedInUser[STRVALUE_USER];
	BOOL bUserIsAdmin;
	int x_win_pos, y_win_pos;
	int iMenuWindowCenter, iMenuWindowRememberPos, iMenuWindowCursorPos;
};
extern CUserConfigure* UserConfigure;

//******************************************************************************
// sheet 2 - class active mode
//******************************************************************************
class CConfigure_sheet2
{
public:
	CConfigure_sheet2();
	~CConfigure_sheet2();

	// set
	void SetActivePortNumber(LPCWSTR text);
	void SetFTPActiveMode(LRESULT mode);
	
	// get
	LRESULT GetFTPActiveMode();
	LPWSTR GetActivePortNumber();

private:
	LRESULT active_mode;   // 1
	WCHAR szActivePortNumber[STRVALUE_PORTNUMBER]; // 2
};
extern CConfigure_sheet2* ConfigureActiveMode_sheet2;

//******************************************************************************
// sheet 3 - LDAP class
//******************************************************************************
class CConfigure_sheet3
{
public:
	CConfigure_sheet3();
	~CConfigure_sheet3();

	// set
	void SetLDAPServer(LPCWSTR text);
	void SetLDAPPort(LPCWSTR text);
	void SetLDAPBindDN(LPCWSTR text);
	void SetLDAPPassword(LPCWSTR text);
	void SetLDAPBaseDN(LPCWSTR text);
	void SetLDAPFilter(LPCWSTR text);
	void SetLDAPAutocompleteAttribute(LPCWSTR text);
	void SetUseLDAP(LRESULT lr);
	void SetUseLDAPSsl(LRESULT lr);
	void SetUseLDAPStartTLS(LRESULT lr);
	void SetLDAPAutocompleteAttIndex(LRESULT lr);

	// get
	LPWSTR GetLDAPServer();
	LPWSTR GetLDAPPort();
	LPWSTR GetLDAPBindDN();
	LPWSTR GetLDAPPassword();
	LPWSTR GetLDAPBaseDN();
	LPWSTR GetLDAPFilter();
	LPWSTR GetLDAPAutocompleteAttribute();
	LRESULT GetUseLDAP();
	LRESULT GetUseLDAPSsl();
	LRESULT GetUseLDAPStartTLS();
	LRESULT GetLDAPAutocompleteAttIndex();

private:
	LRESULT lrUseLDAP;                                                 // 1
	LRESULT lrLDAPSsl;                                                 // 2
	LRESULT lrLDAPStartTLS;                                            // 3
	LRESULT lrLDAPAutocompleteAttIndex;                                // 4
	WCHAR szLDAPServer[STRVALUE_SERVER];                               // 5
	WCHAR szLDAPPort[STRVALUE_PORTNUMBER];                             // 6
	WCHAR szLDAPBindDN[STRVALUE_LDAP_BIND_DN];                         // 7
	WCHAR szLDAPPassword[STRVALUE_PASSWORD];                           // 8
	WCHAR szLDAPBaseDN[STRVALUE_LDAP_BASE_DN];                         // 9
	WCHAR szLDAPFilter[STRVALUE_LDAP_FILTER];                          // 10
	WCHAR szLDAPAutocompleteAttribute[STRVALUE_LDAP_AUTOCOMPLETE_ATT]; // 11
};
extern CConfigure_sheet3* ConfigureLDAP_sheet3;

//******************************************************************************
BOOL Get_user_from_sessionid(CUserConfigure* user_configure);

#endif
