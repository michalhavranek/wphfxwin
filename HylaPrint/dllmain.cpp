// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"
#include "PrintMon.h"
#include "HFAXInterface.h"

#define PORT_DESC L"WinPrint Hylafax Port"
#define PORT_DESC_LEN  sizeof(L"WinPrint Hylafax Port")
#define PORTSNAME L"Ports"
#define BACKSLASH L"\\"

#define DESCKEY L"Description"

// **************************************************
HANDLE hHylaMonitor;
HANDLE hHylaInst;
CRITICAL_SECTION LcmSpoolerSection;

// **************************************************
// **************************************************
BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	UNREFERENCED_PARAMETER(lpReserved);

	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		InitCommonControls();
		HylaMonInit(hModule);
		// Initialize CriticalSection ...
		InitializeCriticalSection(&LcmSpoolerSection);
		DisableThreadLibraryCalls(hModule);
		break;

	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		DeleteCriticalSection(&LcmSpoolerSection);
		break;
	}
	return TRUE;
}
// **************************************************


// **************************************************
// **************************************************
BOOL IsUACEnabled()
{
	BOOL bRet = FALSE;

	if (IsWindowsVistaOrGreater())
	{
		HKEY hKey;
		LONG rc;
		rc = RegOpenKeyExW(HKEY_LOCAL_MACHINE,
			L"Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System",
			0, KEY_QUERY_VALUE, &hKey);
		if (rc == ERROR_SUCCESS)
		{
			DWORD dwType;
			DWORD data = 0;
			DWORD cbData = sizeof(data);
			rc = RegQueryValueExW(hKey, L"EnableLUA", NULL, &dwType, (LPBYTE)&data, &cbData);
			if (rc == ERROR_SUCCESS)
				bRet = (data == 0x00000001);
			RegCloseKey(hKey);
		}
	}

	return bRet;
}
//***********************************************************
void HylaMonInit(HANDLE hModule)
{
	hHylaInst = hModule;
}
//***********************************************************
void ShowError(DWORD derr, WCHAR* module)
{
	LPWSTR lpMsgBuf = nullptr;
	WCHAR szMessage[256];
	if (!FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		derr,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
		(LPWSTR)&lpMsgBuf,
		0,
		NULL))
	{
		StringCbPrintf(szMessage, sizeof(szMessage), L"%lu", derr);
		SendInfoMessage(szMessage, module, MB_OK | MB_ICONINFORMATION);
		return;
	}
	SendInfoMessage((LPWSTR)lpMsgBuf, module, MB_OK | MB_ICONINFORMATION);

	LocalFree(lpMsgBuf);
}
//***********************************************************
LONG hmOpenKey(HANDLE hMonitor, LPWSTR pszSubKey, REGSAM samDesired,
	HKEY* phkResult)
{
	LONG rc = ERROR_SUCCESS;

	MONITORINIT* pMonitorInit = (MONITORINIT*)hMonitor;
	rc = pMonitorInit->pMonitorReg->fpOpenKey(
		pMonitorInit->hckRegistryRoot,
		pszSubKey,
		samDesired,
		phkResult,
		pMonitorInit->hSpooler);

	return rc;
}
//***********************************************************
LONG hmEnumKey(HANDLE hMonitor, HKEYMONITOR hcKey, DWORD dwIndex, LPWSTR pszName,
	PDWORD pcchName)
{
	LONG rc = ERROR_SUCCESS;
	MONITORINIT* pMonitorInit = (MONITORINIT*)hMonitor;
	FILETIME ft;
	rc = pMonitorInit->pMonitorReg->fpEnumKey(
		hcKey, dwIndex, (WCHAR*)pszName, pcchName,
		&ft, pMonitorInit->hSpooler);
	return rc;
}
//***********************************************************
LONG hmQueryValue(HANDLE hMonitor, HKEYMONITOR hcKey, LPWSTR pszValue,
	PDWORD pType, PBYTE pData, PDWORD pcbData)
{
	LONG rc = ERROR_SUCCESS;
	MONITORINIT* pMonitorInit = (MONITORINIT*)hMonitor;
	rc = pMonitorInit->pMonitorReg->fpQueryValue(
		hcKey, (WCHAR*)pszValue, pType, pData, pcbData,
		pMonitorInit->hSpooler);
	return rc;
}
//***********************************************************
LONG hmCloseKey(HANDLE hMonitor, HKEYMONITOR hcKey)
{
	LONG rc = ERROR_SUCCESS;
	MONITORINIT* pMonitorInit = (MONITORINIT*)hMonitor;
	rc = pMonitorInit->pMonitorReg->fpCloseKey(
		hcKey, pMonitorInit->hSpooler);

	return rc;
}
//***********************************************************
LONG hmCreateKey(HANDLE hMonitor, HKEYMONITOR hcKey, LPWSTR pszSubKey,
	DWORD dwOptions, REGSAM samDesired,
	PSECURITY_ATTRIBUTES pSecurityAttributes,
	HKEYMONITOR* phckResult, PDWORD pdwDisposition)
{
	LONG rc = ERROR_SUCCESS;
	MONITORINIT* pMonitorInit = (MONITORINIT*)hMonitor;
	rc = pMonitorInit->pMonitorReg->fpCreateKey(
		hcKey, (WCHAR*)pszSubKey, dwOptions,
		samDesired, pSecurityAttributes, phckResult,
		pdwDisposition, pMonitorInit->hSpooler);
	return rc;
}
//***********************************************************
LONG hmDeleteKey(HANDLE hMonitor, HKEYMONITOR hcKey, LPWSTR pszSubKey)
{
	LONG rc = ERROR_SUCCESS;
	MONITORINIT* pMonitorInit = (MONITORINIT*)hMonitor;
	rc = pMonitorInit->pMonitorReg->fpDeleteKey(
		hcKey, (WCHAR*)pszSubKey, pMonitorInit->hSpooler);
	return rc;
}
//***********************************************************
LONG hmSetValue(HANDLE hMonitor, HKEYMONITOR hcKey, LPWSTR pszValue, DWORD dwType,
	LPVOID pData, DWORD cbData)
{
	LONG rc = ERROR_SUCCESS;
	MONITORINIT* pMonitorInit = (MONITORINIT*)hMonitor;
	rc = pMonitorInit->pMonitorReg->fpSetValue(hcKey, pszValue,
		dwType, (const BYTE*)pData, cbData, pMonitorInit->hSpooler);
	return rc;
}
//***********************************************************
void reset_portdata(h_portdata* prd)
{
	/* do not touch prd->portname, prd->hPort or prd->hMonitor */
	memset(&(prd->config), 0, sizeof(prd->config));
	prd->hPrinter = INVALID_HANDLE_VALUE;
	prd->hFile = INVALID_HANDLE_VALUE;
	prd->pFileName[0] = L'\0';
	prd->pPrinterName[0] = L'\0';
	prd->pDocName[0] = L'\0';
	prd->JobId = 0;
	prd->pUserName[0] = L'\0';
	prd->pMachineName[0] = L'\0';
}
//***********************************************************
/* Suggest a port name and store it in portname.
* len is the size in characters of portname.
* nAttempt is the number of times we have attempted
* to generate a unique port name and can be used
* to add a numeric suffix.  On first call this will be 1.
*
* If we can't generate a unique name then return FALSE.
* If we only support one possible name then return FALSE when nAttempt > 1.
*/
BOOL hm_suggest_portname(WCHAR* portname, int len, int nAttempt)
{
	WCHAR rname[] = L"HFAX%d:";
	if (len < sizeof(rname) / sizeof(WCHAR) + 12)
		return FALSE;
	wsprintf(portname, rname, nAttempt);
	return TRUE;
}
//***********************************************************
DWORD hm_sizeof_config(void)
{
	return sizeof(h_config);
}
// default values
//***********************************************************
LPWSTR hm_init_config(h_config* config)
{
	memset(config, 0, sizeof(h_config));
	config->dwSize = sizeof(h_config);
	config->dwVersion = static_cast<DWORD>(VERSION_NUMBER);
	wcscpy_s(config->szDescription, STRVALUE_TEXT_256, PORT_DESC);
	// defaults
	config->dwFaxNumberFromClipboard = 0;
	config->dwPassiveIPIgnore = 0;
	wcscpy_s(config->szServer, STRVALUE_SERVER, L"192.168.1.1");
	wcscpy_s(config->szUsername, STRVALUE_USER, L"fax");
	wcscpy_s(config->szNotificationType, STRVALUE_NOTIFICATION, _T(FAILURE_SUCCESS));
	wcscpy_s(config->szPageSize, STRVALUE_PAGESIZE, _T(A4_SIZE));
	wcscpy_s(config->szResolution, STRVALUE_RESOLUTION, _T(FINE_RES));
	wcscpy_s(config->szMaxDials, STRVALUE_MAXTIMES_NUMBER, _T(MAXDIALS_NONE));
	wcscpy_s(config->szMaxTries, STRVALUE_MAXTIMES_NUMBER, _T(MAXTRIES_NONE));
	wcscpy_s(config->szHFPortNumber, STRVALUE_PORTNUMBER, L"4559");
	wcscpy_s(config->szInfoDialogTimer, STRVALUE_INFODIALOG_TIMER_NUMBER, _T(INFODIALOG_TIMER_3));
	// tab2
	wcscpy_s(config->szActivePortNumber, STRVALUE_PORTNUMBER, L"32001");
	config->dwUseActiveMode = 0;
	// tab3
	config->dwUseLDAP = 0;
	config->dwLDAP_STARTTLS = 0;
	wcscpy_s(config->szLDAPPort, STRVALUE_PORTNUMBER, L"389");
	wcscpy_s(config->szLDAPBaseDN, STRVALUE_LDAP_BASE_DN, L"dc=example,dc=com");
	wcscpy_s(config->szLDAPFilter, STRVALUE_LDAP_FILTER, _T(LDAP_FILTER_CN));
	wcscpy_s(config->szLDAPAutocompleteAttribute, STRVALUE_LDAP_AUTOCOMPLETE_ATT, _T(LDAP_AUTOCOMPLETE_ATT_CN));
	config->dwLDAP_SSL = 0;

	return config->szPortName;
}
//***********************************************************
/* read the configuration from the registry */
BOOL hm_get_config(HANDLE hMonitor, LPCWSTR portname, h_config* config)
{
	LONG rc = ERROR_SUCCESS;
	HKEY hkey;
	WCHAR buf[STRVALUE_TEXT_256];
	DWORD cbData;
	DWORD dwType;

	hm_init_config(config);

	wcsncpy_s(config->szPortName, STRVALUE_TEXT_256, (LPWSTR)portname, _TRUNCATE);

	wcsncpy_s(buf, _countof(buf), PORTSNAME, _TRUNCATE);
	wcsncat_s(buf, _countof(buf), BACKSLASH, _TRUNCATE);
	wcsncat_s(buf, _countof(buf), config->szPortName, _TRUNCATE);

	rc = hmOpenKey(hMonitor, (WCHAR*)buf, KEY_READ, &hkey);
	if (rc != ERROR_SUCCESS)
		return FALSE;

	// settings - TAB 1
	cbData = sizeof(config->szDescription) - sizeof(WCHAR);
	rc = hmQueryValue(hMonitor, hkey, DESCKEY, &dwType,
		(PBYTE)(config->szDescription), &cbData);


	cbData = sizeof(config->szServer) - sizeof(WCHAR);
	rc = hmQueryValue(hMonitor, hkey, _T(HYLAFAX_SERVER), &dwType,
		(PBYTE)(config->szServer), &cbData);

	cbData = sizeof(config->szHFPortNumber) - sizeof(WCHAR);
	rc = hmQueryValue(hMonitor, hkey, _T(HYLAFAX_HF_PORTNUM), &dwType,
		(PBYTE)(config->szHFPortNumber), &cbData);

	cbData = sizeof(config->szUsername) - sizeof(WCHAR);
	rc = hmQueryValue(hMonitor, hkey, _T(HYLAFAX_USER), &dwType,
		(PBYTE)(config->szUsername), &cbData);

	cbData = sizeof(config->szPassword) - sizeof(WCHAR);
	rc = hmQueryValue(hMonitor, hkey, _T(HYLAFAX_PASSWORD), &dwType,
		(PBYTE)(config->szPassword), &cbData);

	cbData = sizeof(config->szDefaultNotifyEmail) - sizeof(WCHAR);
	rc = hmQueryValue(hMonitor, hkey, _T(HYLAFAX_DEFAULTNOTIFYEMAIL), &dwType,
		(PBYTE)(config->szDefaultNotifyEmail), &cbData);

	cbData = sizeof(config->szModem) - sizeof(WCHAR);
	rc = hmQueryValue(hMonitor, hkey, _T(HYLAFAX_MODEM), &dwType,
		(PBYTE)(config->szModem), &cbData);

	cbData = sizeof(config->szAddressBook) - sizeof(WCHAR);
	rc = hmQueryValue(hMonitor, hkey, _T(HYLAFAX_ADDRESSBOOK), &dwType,
		(PBYTE)(config->szAddressBook), &cbData);

	cbData = sizeof(config->szPageSize) - sizeof(WCHAR);
	rc = hmQueryValue(hMonitor, hkey, _T(HYLAFAX_PAGESIZE), &dwType,
		(PBYTE)(config->szPageSize), &cbData);

	cbData = sizeof(config->szNotificationType) - sizeof(WCHAR);
	rc = hmQueryValue(hMonitor, hkey, _T(HYLAFAX_NOTIFICATION), &dwType,
		(PBYTE)(config->szNotificationType), &cbData);

	cbData = sizeof(config->szMaxDials) - sizeof(WCHAR);
	rc = hmQueryValue(hMonitor, hkey, _T(HYLAFAX_MAXDIALS), &dwType,
		(PBYTE)(config->szMaxDials), &cbData);

	cbData = sizeof(config->szMaxTries) - sizeof(WCHAR);
	rc = hmQueryValue(hMonitor, hkey, _T(HYLAFAX_MAXTRIES), &dwType,
		(PBYTE)(config->szMaxTries), &cbData);

	cbData = sizeof(DWORD);
	rc = hmQueryValue(hMonitor, hkey, _T(HYLAFAX_PASVIPIGNORE), &dwType,
		(PBYTE)(&config->dwPassiveIPIgnore), &cbData);

	cbData = sizeof(config->szResolution) - sizeof(WCHAR);
	rc = hmQueryValue(hMonitor, hkey, _T(HYLAFAX_RESOLUTION), &dwType,
		(PBYTE)(config->szResolution), &cbData);

	cbData = sizeof(config->szInfoDialogTimer) - sizeof(WCHAR);
	rc = hmQueryValue(hMonitor, hkey, _T(HYLAFAX_INFODIALOG_TIMER), &dwType,
		(PBYTE)(config->szInfoDialogTimer), &cbData);

	cbData = sizeof(DWORD);
	rc = hmQueryValue(hMonitor, hkey, _T(HYLAFAX_FAXNUMBER_FROM_CLIPBOARD), &dwType,
		(PBYTE)(&config->dwFaxNumberFromClipboard), &cbData);

	// active mode - TAB2
	cbData = sizeof(DWORD);
	rc = hmQueryValue(hMonitor, hkey, _T(HYLAFAX_USEACTIVEMODE), &dwType,
		(PBYTE)(&config->dwUseActiveMode), &cbData);

	cbData = sizeof(config->szActivePortNumber) - sizeof(WCHAR);
	rc = hmQueryValue(hMonitor, hkey, _T(HYLAFAX_ACTIVEPORTNUM), &dwType,
		(PBYTE)(config->szActivePortNumber), &cbData);

	// LDAP - TAB 3
	cbData = sizeof(DWORD);
	rc = hmQueryValue(hMonitor, hkey, _T(LDAP_USE_LDAP), &dwType,
		(PBYTE)(&config->dwUseLDAP), &cbData);

	cbData = sizeof(config->szLDAPServer) - sizeof(WCHAR);
	rc = hmQueryValue(hMonitor, hkey, _T(LDAP_SERVER), &dwType,
		(PBYTE)(config->szLDAPServer), &cbData);

	cbData = sizeof(config->szLDAPPort) - sizeof(WCHAR);
	rc = hmQueryValue(hMonitor, hkey, _T(LDAP_MYPORT), &dwType,
		(PBYTE)(config->szLDAPPort), &cbData);

	cbData = sizeof(DWORD);
	rc = hmQueryValue(hMonitor, hkey, _T(LDAP_SSL), &dwType,
		(PBYTE)(&config->dwLDAP_SSL), &cbData);

	cbData = sizeof(DWORD);
	rc = hmQueryValue(hMonitor, hkey, _T(LDAP_STARTTLS), &dwType,
		(PBYTE)(&config->dwLDAP_STARTTLS), &cbData);

	cbData = sizeof(config->szLDAPBindDN) - sizeof(WCHAR);
	rc = hmQueryValue(hMonitor, hkey, _T(LDAP_BIND_DN), &dwType,
		(PBYTE)(config->szLDAPBindDN), &cbData);

	cbData = sizeof(config->szLDAPPassword) - sizeof(WCHAR);
	rc = hmQueryValue(hMonitor, hkey, _T(LDAP_PASSWORD), &dwType,
		(PBYTE)(config->szLDAPPassword), &cbData);

	cbData = sizeof(config->szLDAPBaseDN) - sizeof(WCHAR);
	rc = hmQueryValue(hMonitor, hkey, _T(LDAP_BASE_DN), &dwType,
		(PBYTE)(config->szLDAPBaseDN), &cbData);

	cbData = sizeof(config->szLDAPFilter) - sizeof(WCHAR);
	rc = hmQueryValue(hMonitor, hkey, _T(LDAP_FILTER), &dwType,
		(PBYTE)(config->szLDAPFilter), &cbData);

	cbData = sizeof(config->szLDAPAutocompleteAttribute) - sizeof(WCHAR);
	rc = hmQueryValue(hMonitor, hkey, _T(LDAP_AUTOCOMPLETE_ATT), &dwType,
		(PBYTE)(config->szLDAPAutocompleteAttribute), &cbData);

	hmCloseKey(hMonitor, hkey);
	return TRUE;
}
//***********************************************************
BOOL hm_validate_config(h_config* config)
{
	if (config == NULL)
		return FALSE;
	if (config->dwSize != sizeof(h_config))
		return FALSE;
	if (config->dwVersion != VERSION_NUMBER)
		return FALSE;
	return TRUE;
}
//***********************************************************
/* write the configuration to the registry */
BOOL hm_set_config(HANDLE hMonitor, h_config* config)
{
	LONG rc = ERROR_SUCCESS;
	HKEY hkey;
	WCHAR buf[STRVALUE_TEXT_256];

	if (!hm_validate_config(config))
		return FALSE;

	HANDLE hToken = NULL;
	if (IsUACEnabled())
	{
		OpenThreadToken(GetCurrentThread(), TOKEN_IMPERSONATE, TRUE, &hToken);
		RevertToSelf();
	}

	wcsncpy_s(buf, _countof(buf), PORTSNAME, _TRUNCATE);
	wcsncat_s(buf, _countof(buf), BACKSLASH, _TRUNCATE);
	wcsncat_s(buf, _countof(buf), config->szPortName, _TRUNCATE);

	rc = hmOpenKey(hMonitor, buf, KEY_WRITE, &hkey);
	if (rc == ERROR_SUCCESS)
	{
		rc = hmSetValue(hMonitor, hkey, DESCKEY, REG_SZ,
			config->szDescription,
			static_cast<DWORD>(sizeof(WCHAR) * (wcslen(config->szDescription) + 1)));
		// settings - TAB 1
		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(HYLAFAX_SERVER), REG_SZ,
				config->szServer,
				static_cast<DWORD>(sizeof(WCHAR) * (wcslen(config->szServer) + 1)));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(HYLAFAX_HF_PORTNUM), REG_SZ,
				config->szHFPortNumber,
				static_cast<DWORD>(sizeof(WCHAR) * (wcslen(config->szHFPortNumber) + 1)));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(HYLAFAX_USER), REG_SZ,
				config->szUsername,
				static_cast<DWORD>(sizeof(WCHAR) * (wcslen(config->szUsername) + 1)));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(HYLAFAX_PASSWORD), REG_SZ,
				config->szPassword,
				static_cast<DWORD>(sizeof(WCHAR) * (wcslen(config->szPassword) + 1)));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(HYLAFAX_DEFAULTNOTIFYEMAIL), REG_SZ,
				config->szDefaultNotifyEmail,
				static_cast<DWORD>(sizeof(WCHAR) * (wcslen(config->szDefaultNotifyEmail) + 1)));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(HYLAFAX_MODEM), REG_SZ,
				config->szModem,
				static_cast<DWORD>(sizeof(WCHAR) * (wcslen(config->szModem) + 1)));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(HYLAFAX_ADDRESSBOOK), REG_SZ,
				config->szAddressBook,
				static_cast<DWORD>(sizeof(WCHAR) * (wcslen(config->szAddressBook) + 1)));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(HYLAFAX_NOTIFICATION), REG_SZ,
				config->szNotificationType,
				static_cast<DWORD>(sizeof(WCHAR) * (wcslen(config->szNotificationType) + 1)));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(HYLAFAX_PAGESIZE), REG_SZ,
				config->szPageSize,
				static_cast<DWORD>(sizeof(WCHAR) * (wcslen(config->szPageSize) + 1)));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(HYLAFAX_MAXDIALS), REG_SZ,
				config->szMaxDials,
				static_cast<DWORD>(sizeof(WCHAR) * (wcslen(config->szMaxDials) + 1)));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(HYLAFAX_MAXTRIES), REG_SZ,
				config->szMaxTries,
				static_cast<DWORD>(sizeof(WCHAR) * (wcslen(config->szMaxTries) + 1)));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(HYLAFAX_PASVIPIGNORE), REG_DWORD,
				&config->dwPassiveIPIgnore,
				sizeof(DWORD));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(HYLAFAX_RESOLUTION), REG_SZ,
				config->szResolution,
				static_cast<DWORD>(sizeof(WCHAR) * (wcslen(config->szResolution) + 1)));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(HYLAFAX_INFODIALOG_TIMER), REG_SZ,
				config->szInfoDialogTimer,
				static_cast<DWORD>(sizeof(WCHAR) * (wcslen(config->szInfoDialogTimer) + 1)));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(HYLAFAX_FAXNUMBER_FROM_CLIPBOARD), REG_DWORD,
				&config->dwFaxNumberFromClipboard,
				sizeof(DWORD));

		// active mode - TAB 2
		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(HYLAFAX_USEACTIVEMODE), REG_DWORD,
				&config->dwUseActiveMode,
				sizeof(DWORD));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(HYLAFAX_ACTIVEPORTNUM), REG_SZ,
				config->szActivePortNumber,
				static_cast<DWORD>(sizeof(WCHAR) * (wcslen(config->szActivePortNumber) + 1)));

		// LDAP - TAB 3
		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(LDAP_USE_LDAP), REG_DWORD,
				&config->dwUseLDAP,
				sizeof(DWORD));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(LDAP_SERVER), REG_SZ,
				config->szLDAPServer,
				static_cast<DWORD>(sizeof(WCHAR) * (wcslen(config->szLDAPServer) + 1)));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(LDAP_MYPORT), REG_SZ,
				config->szLDAPPort,
				static_cast<DWORD>(sizeof(WCHAR) * (wcslen(config->szLDAPPort) + 1)));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(LDAP_SSL), REG_DWORD,
				&config->dwLDAP_SSL,
				sizeof(DWORD));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(LDAP_STARTTLS), REG_DWORD,
				&config->dwLDAP_STARTTLS,
				sizeof(DWORD));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(LDAP_BIND_DN), REG_SZ,
				config->szLDAPBindDN,
				static_cast<DWORD>(sizeof(WCHAR) * (wcslen(config->szLDAPBindDN) + 1)));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(LDAP_PASSWORD), REG_SZ,
				config->szLDAPPassword,
				static_cast<DWORD>(sizeof(WCHAR) * (wcslen(config->szLDAPPassword) + 1)));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(LDAP_BASE_DN), REG_SZ,
				config->szLDAPBaseDN,
				static_cast<DWORD>(sizeof(WCHAR) * (wcslen(config->szLDAPBaseDN) + 1)));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(LDAP_FILTER), REG_SZ,
				config->szLDAPFilter,
				static_cast<DWORD>(sizeof(WCHAR) * (wcslen(config->szLDAPFilter) + 1)));

		if (rc == ERROR_SUCCESS)
			rc = hmSetValue(hMonitor, hkey, _T(LDAP_AUTOCOMPLETE_ATT), REG_SZ,
				config->szLDAPAutocompleteAttribute,
				static_cast<DWORD>(sizeof(WCHAR) * (wcslen(config->szLDAPAutocompleteAttribute) + 1)));

		hmCloseKey(hMonitor, hkey);
	}

	if (hToken)
	{
		SetThreadToken(NULL, hToken);
		CloseHandle(hToken);
	}

	return (rc == ERROR_SUCCESS);
}
//***********************************************************
BOOL WINAPI heStartDocPort(HANDLE hPort, LPWSTR pPrinterName,
	DWORD JobId, DWORD Level, LPBYTE pDocInfo)
{
	h_portdata* prd = (h_portdata*)hPort;
	BOOL flag;

#ifdef _DEBUG
	DebugWriteToFile(L"heStartDocPort");
#endif

	if (prd == (h_portdata*)NULL)
	{
		SetLastError((DWORD)ERROR_INVALID_HANDLE);
		return FALSE;
	}

	reset_portdata(prd);
	flag = hfax_start_doc_port(prd, pPrinterName, JobId, Level, pDocInfo);

	return flag;
}
//***********************************************************
/* WritePort is normally called between StartDocPort and EndDocPort,
* but can be called outside this pair for bidirectional printers.
*/
BOOL WINAPI heWritePort(HANDLE  hPort, LPBYTE  pBuffer,
	DWORD   cbBuf, LPDWORD pcbWritten)
{
	h_portdata* prd = (h_portdata*)hPort;
	BOOL flag;

#ifdef _DEBUG
	DebugWriteToFile(L"heWritePort");
#endif

	if (prd == (h_portdata*)NULL)
	{
		SetLastError((DWORD)ERROR_INVALID_HANDLE);
		return FALSE;
	}

	*pcbWritten = 0;
	flag = hfax_write_port(prd, pBuffer, cbBuf, pcbWritten);

	return flag;	/* returning FALSE crashes Win95 spooler */
}
//***********************************************************
/* ReadPort can be called within a Start/EndDocPort pair,
* and also outside this pair.
*/
BOOL WINAPI heReadPort(HANDLE hPort, LPBYTE pBuffer, DWORD cbBuffer, LPDWORD pcbRead)
{
	UNREFERENCED_PARAMETER(cbBuffer);
	UNREFERENCED_PARAMETER(pBuffer);

	h_portdata* prd = (h_portdata*)hPort;
	BOOL flag;

#ifdef _DEBUG
	DebugWriteToFile(L"heReadPort");
#endif

	/* we don't support reading */
	* pcbRead = 0;

	if (prd == (h_portdata*)NULL)
	{
		SetLastError((unsigned long)ERROR_INVALID_HANDLE);
		return FALSE;
	}
	flag = FALSE;

	return flag;
}
//***********************************************************
BOOL WINAPI heEndDocPort(HANDLE hPort)
{
	h_portdata* prd = (h_portdata*)hPort;
	BOOL flag;

#ifdef _DEBUG
	DebugWriteToFile(L"heEndDocPort");
#endif

	if (prd == (h_portdata*)NULL)
	{
		SetLastError((DWORD)ERROR_INVALID_HANDLE);
		return FALSE;
	}

	flag = hfax_end_doc_port(prd);
	reset_portdata(prd);

	return flag;
}
//***********************************************************
BOOL WINAPI heClosePort(HANDLE hPort)
{
	BOOL flag;

#ifdef _DEBUG
	DebugWriteToFile(L"heClosePort");
#endif

	flag = hfax_close_port(hPort);
	return flag;
}
//***********************************************************
BOOL WINAPI heEnumPorts(HANDLE hMonitor, LPWSTR pName, DWORD Level,
	LPBYTE pPorts, DWORD cbBuf, LPDWORD pcbNeeded, LPDWORD pcReturned)
{
	LONG rc = ERROR_SUCCESS;
	HKEY hkey, hsubkey;
	WCHAR portname[MAXSHORTSTR], portdesc[MAXSHORTSTR] = { 0 };
	WCHAR monitorname[MAXSHORTSTR] = L"WinPrint Hylafax";
	WCHAR buf[STRVALUE_TEXT_256];
	size_t needed;
	DWORD cbData, keytype;
	PORT_INFO_1* pi1;
	PORT_INFO_2* pi2;
	LPWSTR pstr;
	int i;

	*pcbNeeded = 0;
	*pcReturned = 0;

#ifdef _DEBUG
	DebugWriteToFile(L"heEnumPorts");
#endif

	if (hMonitor == NULL)
	{
		if (pName != NULL)
			return FALSE;
	}

	if ((Level < 1) || (Level > 2))
	{
		SetLastError((DWORD)ERROR_INVALID_LEVEL);
		return FALSE;
	}

	rc = hmOpenKey(hMonitor, PORTSNAME, KEY_READ, &hkey);
	if (rc != ERROR_SUCCESS)
	{
		return TRUE;	/* There are no ports */
	}

	/* First pass is to calculate the number of bytes needed */
	needed = 0;
	i = 0;
	cbData = sizeof(portname);
	rc = hmEnumKey(hMonitor, hkey, 0, portname, &cbData);
	while (rc == ERROR_SUCCESS) {
		needed += (wcslen(portname) + 1) * sizeof(WCHAR);
		if (Level == 1)
		{
			needed += sizeof(PORT_INFO_1);
		}
		else if (Level == 2)
		{
			needed += sizeof(PORT_INFO_2);
			needed += (wcslen(monitorname) + 1) * sizeof(WCHAR);
			wcsncpy_s(buf, _countof(buf), PORTSNAME, _TRUNCATE);
			wcsncat_s(buf, _countof(buf), BACKSLASH, _TRUNCATE);
			wcsncat_s(buf, _countof(buf), portname, _TRUNCATE);
			rc = hmOpenKey(hMonitor, buf, KEY_READ, &hsubkey);
			if (rc == ERROR_SUCCESS) {
				cbData = sizeof(portdesc);
				keytype = REG_SZ;
				hmQueryValue(hMonitor, hsubkey, DESCKEY, &keytype,
					(LPBYTE)portdesc, &cbData);
				if (rc == ERROR_SUCCESS)
					needed += (wcslen(portdesc) + 1) * sizeof(WCHAR);
				else
					needed += 1 * sizeof(WCHAR);	/* empty string */
				hmCloseKey(hMonitor, hsubkey);
			}
			else {
				needed += 1 * sizeof(WCHAR);	/* empty string */
			}
		}
		i++;
		cbData = sizeof(portname);
		rc = hmEnumKey(hMonitor, hkey, i, portname, &cbData);
	}
	*pcbNeeded = static_cast<DWORD>(needed);

	if ((pPorts == NULL) || ((DWORD)needed > cbBuf))
	{
		hmCloseKey(hMonitor, hkey);
		SetLastError((DWORD)ERROR_INSUFFICIENT_BUFFER);
		return FALSE;
	}

	/* Second pass to copy the data to the buffer */

	/* PORT_INFO_x structures must be placed at the beginning
	* of the buffer, and strings at the end of the buffer.
	* This is important!  It appears that one buffer is
	* allocated, then each port monitor is called in turn to
	* add its entries to the buffer, between previous entries.
	*/

	i = 0;
	pi1 = (PORT_INFO_1*)pPorts;
	pi2 = (PORT_INFO_2*)pPorts;
	pstr = (LPWSTR)(pPorts + cbBuf);
	cbData = sizeof(portname);
	rc = hmEnumKey(hMonitor, hkey, 0, portname, &cbData);
	while (rc == ERROR_SUCCESS)
	{
		if (Level == 1)
		{
			pstr -= wcslen(portname) + 1;
			wcscpy(pstr, portname);
			pi1[i].pName = (WCHAR*)pstr;
		}
		else if (Level == 2)
		{
			pstr -= wcslen(portname) + 1;
			wcscpy(pstr, portname);
			pi2[i].pPortName = (WCHAR*)pstr;

			pstr -= wcslen(monitorname) + 1;
			wcscpy(pstr, monitorname);
			pi2[i].pMonitorName = (WCHAR*)pstr;

			wcsncpy_s(buf, _countof(buf), PORTSNAME, _TRUNCATE);
			wcsncat_s(buf, _countof(buf), BACKSLASH, _TRUNCATE);
			wcsncat_s(buf, _countof(buf), portname, _TRUNCATE);
			rc = hmOpenKey(hMonitor, buf, KEY_READ, &hsubkey);
			if (rc == ERROR_SUCCESS) {
				cbData = sizeof(portdesc);
				keytype = REG_SZ;
				hmQueryValue(hMonitor, hsubkey, DESCKEY, &keytype,
					(LPBYTE)portdesc, &cbData);
				if (rc != ERROR_SUCCESS)
					portdesc[0] = L'\0';
				hmCloseKey(hMonitor, hsubkey);
			}
			else
			{
				portdesc[0] = L'\0';
			}

			pstr -= wcslen(portdesc) + 1;
			wcscpy(pstr, portdesc);
			pi2[i].pDescription = (WCHAR*)pstr;

			/* Say that writing to this port is supported, */
			/* but reading is not. */
			/* Using fPortType = 3 is wrong. */
			/* Options are PORT_TYPE_WRITE=1, PORT_TYPE_READ=2, */
			/*   PORT_TYPE_REDIRECTED=4, PORT_TYPE_NET_ATTACHED=8 */
			pi2[i].fPortType = PORT_TYPE_WRITE;
			pi2[i].Reserved = 0;
		}
		i++;
		cbData = sizeof(portname);
		rc = hmEnumKey(hMonitor, hkey, i, portname, &cbData);
	}
	*pcReturned = i;
	hmCloseKey(hMonitor, hkey);

	return TRUE;
}
//***********************************************************
BOOL WINAPI heSetPortTimeOuts(HANDLE hPort, LPCOMMTIMEOUTS lpCTO, DWORD reserved)
{
	UNREFERENCED_PARAMETER(reserved);
	UNREFERENCED_PARAMETER(lpCTO);

	h_portdata* prd = (h_portdata*)hPort;
	BOOL flag = TRUE;

#ifdef _DEBUG
	DebugWriteToFile(L"heSetPortTimeOuts");
#endif

	if (prd == (h_portdata*)NULL)
	{
		SetLastError((DWORD)ERROR_INVALID_HANDLE);
		return FALSE;
	}

	return flag;
}
//***********************************************************
BOOL sPortExists(HANDLE hMonitor, LPWSTR pszPortName)
{
	UNREFERENCED_PARAMETER(hMonitor);

	BOOL port_exists = FALSE, bStatus;
	LPBYTE lpPorts;
	DWORD dwcbBuf, dwNeeded, dwReturned;
	PORT_INFO_2* pi2;
	int j;

#ifdef _DEBUG
	DebugWriteToFile(L"sPortExists");
#endif

	bStatus = EnumPorts(NULL, 2, NULL, 0, &dwNeeded, &dwReturned);
	if (!bStatus && GetLastError() == ERROR_INSUFFICIENT_BUFFER)
	{
		/* try again, with requested size */
		dwcbBuf = dwNeeded;
		lpPorts = new BYTE[dwNeeded];
		if (!EnumPorts(NULL, 2, lpPorts, dwcbBuf, &dwNeeded, &dwReturned))
		{
			ShowError(GetLastError(), L"EnumPorts");
			delete[] lpPorts;
			return FALSE;
		}
	}
	else
	{
		ShowError(GetLastError(), L"EnumPorts - needed bytes");
		return FALSE;
	}

	pi2 = (PORT_INFO_2*)lpPorts;

	for (j = 0; j < (int)dwReturned; j++)
	{
		if (lstrcmp(pszPortName, pi2[j].pPortName) == 0)
			port_exists = TRUE;
	}

	delete[] lpPorts;
	return port_exists;
}
//***********************************************************
BOOL sAddPort(HANDLE hMonitor, LPWSTR pszPortName)
{
	LONG rc;
	HKEY hkey, hsubkey;
	WCHAR wPortName[STRVALUE_TEXT_256];

#ifdef _DEBUG
	DebugWriteToFile(L"sAddPort - 1");
#endif

	if (sPortExists(hMonitor, pszPortName))
	{
		return FALSE;
	}

#ifdef _DEBUG
	DebugWriteToFile(L"sAddPort - 2");
#endif

	HANDLE hToken = NULL;
	if (IsUACEnabled())
	{
		OpenThreadToken(GetCurrentThread(), TOKEN_IMPERSONATE, TRUE, &hToken);
		RevertToSelf();
	}

	wcsncpy_s(wPortName, _countof(wPortName), pszPortName, _TRUNCATE);

	/* store new port name in registry */
	rc = hmOpenKey(hMonitor, PORTSNAME, KEY_ALL_ACCESS, &hkey);

	if (rc != ERROR_SUCCESS)
	{
		/* try to create the Ports key */
		rc = hmOpenKey(hMonitor, NULL, KEY_ALL_ACCESS, &hkey);
		if (rc == ERROR_SUCCESS)
		{
			rc = hmCreateKey(hMonitor, hkey, PORTSNAME, 0,
				KEY_ALL_ACCESS, NULL, &hsubkey, NULL);
			if (rc == ERROR_SUCCESS)
				hmCloseKey(hMonitor, hsubkey);
			hmCloseKey(hMonitor, hkey);
		}
		else
		{
			ShowError(rc, L"sAddPort");
		}
		rc = hmOpenKey(hMonitor, PORTSNAME, KEY_ALL_ACCESS, &hkey);
	}
	if (rc == ERROR_SUCCESS)
	{
		rc = hmCreateKey(hMonitor, hkey, wPortName, 0, KEY_ALL_ACCESS,
			NULL, &hsubkey, NULL);
		if (rc == ERROR_SUCCESS)
		{
			rc = hmSetValue(hMonitor, hsubkey, DESCKEY, REG_SZ,
				PORT_DESC, PORT_DESC_LEN);
			hmCloseKey(hMonitor, hsubkey);
		}
		hmCloseKey(hMonitor, hkey);
	}

	if (hToken)
	{
		SetThreadToken(NULL, hToken);
		CloseHandle(hToken);
	}
	return (rc == ERROR_SUCCESS);
}
//***********************************************************
BOOL sDeletePort(HANDLE hMonitor, LPCWSTR pPortName)
{
	HKEY hkey;
	LONG rc;
	WCHAR buf[STRVALUE_TEXT_256];
	HANDLE hToken = NULL;

#ifdef _DEBUG
	DebugWriteToFile(L"sDeletePort");
#endif

	if (IsUACEnabled())
	{
		OpenThreadToken(GetCurrentThread(), TOKEN_IMPERSONATE, TRUE, &hToken);
		RevertToSelf();
	}

	wcsncpy_s(buf, _countof(buf), (LPWSTR)pPortName, _TRUNCATE);

	rc = hmOpenKey(hMonitor, PORTSNAME, KEY_ALL_ACCESS, &hkey);
	if (rc == ERROR_SUCCESS) {
		rc = hmDeleteKey(hMonitor, hkey, buf);
		hmCloseKey(hMonitor, hkey);
	}

	if (hToken)
	{
		SetThreadToken(NULL, hToken);
		CloseHandle(hToken);
	}

	return (rc == ERROR_SUCCESS);
}
//***********************************************************
typedef struct tagREXCVPORT
{
	DWORD dwSize;
	HANDLE hMonitor;
	WCHAR szPortName[STRVALUE_TEXT_256];
	ACCESS_MASK GrantedAccess;
} REXCVPORT;
//***********************************************************
BOOL WINAPI heXcvOpenPort(HANDLE hMonitor, LPCWSTR pszObject,
	ACCESS_MASK GrantedAccess, PHANDLE phXcv)
{
	REXCVPORT* xcv;

#ifdef _DEBUG
	DebugWriteToFile(L"heXcvOpenPort - 1");
#endif

	if (phXcv == NULL)
	{
		SetLastError(ERROR_INVALID_PARAMETER);
		return FALSE;
	}
	*phXcv = (HANDLE)NULL;

#ifdef _DEBUG
	DebugWriteToFile(L"heXcvOpenPort - 2");
#endif

	// HeapFree will be called in heXcvClosePort
	xcv = (REXCVPORT*)(HeapAlloc(GetProcessHeap(), 0, static_cast<DWORD>(sizeof(REXCVPORT))));

	if (xcv == NULL)
		return FALSE;

	memset(xcv, 0, sizeof(REXCVPORT));
	xcv->dwSize = sizeof(REXCVPORT);
	xcv->hMonitor = hMonitor;
	wcsncpy_s(xcv->szPortName, STRVALUE_TEXT_256, (WCHAR*)pszObject, _TRUNCATE);
	xcv->GrantedAccess = GrantedAccess;
	*phXcv = (HANDLE)xcv;

	return TRUE;
}
//***********************************************************
DWORD WINAPI heXcvDataPort(HANDLE hXcv, LPCWSTR pszDataName, PBYTE pInputData,
	DWORD cbInputData, PBYTE pOutputData, DWORD cbOutputData,
	PDWORD pcbOutputNeeded)
{
	UNREFERENCED_PARAMETER(cbInputData);
	REXCVPORT* xcv = (REXCVPORT*)hXcv;

#ifdef _DEBUG
	DebugWriteToFile(L"heXcvDataPort - 1");
#endif

	if ((xcv == NULL) || (xcv->dwSize != sizeof(REXCVPORT)))
	{
		return ERROR_INVALID_PARAMETER;
	}

#ifdef _DEBUG
	DebugWriteToFile(L"heXcvDataPort - 2");
#endif

	if (wcscmp(pszDataName, L"AddPort") == 0)
	{
		if (!(xcv->GrantedAccess & SERVER_ACCESS_ADMINISTER))
		{
			return ERROR_ACCESS_DENIED;
		}
		/* pInputData contains the port name */
		if (sAddPort(xcv->hMonitor, (LPWSTR)pInputData))
		{
			return ERROR_SUCCESS;
		}
		else
			SendInfoMessage((LPWSTR)pInputData, L"sAddPort Failed", MB_OK);
	}
	else if (wcscmp(pszDataName, L"DeletePort") == 0)
	{
		if (!(xcv->GrantedAccess & SERVER_ACCESS_ADMINISTER))
			return ERROR_ACCESS_DENIED;
		if (sDeletePort(xcv->hMonitor, (LPWSTR)pInputData))
			return ERROR_SUCCESS;
	}
	else if (wcscmp(pszDataName, L"MonitorUI") == 0)
	{
		/* Our server and UI DLLs are combined */
		WCHAR buf[STRVALUE_TEXT_256];
		DWORD len;
		if (pOutputData == NULL)
		{
			return ERROR_INSUFFICIENT_BUFFER;
		}
		len = GetModuleFileName((HMODULE)hHylaInst, buf, STRVALUE_TEXT_256);
		*pcbOutputNeeded = (len + 1) * sizeof(WCHAR);
		if (*pcbOutputNeeded > cbOutputData)
		{
			return ERROR_INSUFFICIENT_BUFFER;
		}
		wcscpy((PWSTR)pOutputData, buf);
		return ERROR_SUCCESS;
	}
	else if (wcscmp(pszDataName, L"PortExists") == 0)
	{
		/* pInputData contains the port name */
		if (sPortExists(xcv->hMonitor, (LPWSTR)pInputData))
			return ERROR_PRINTER_ALREADY_EXISTS;	/* TRUE */
		else
			return ERROR_SUCCESS;			/* FALSE */
	}
	else if (wcscmp(pszDataName, L"GetConfig") == 0)
	{
		*pcbOutputNeeded = hm_sizeof_config();
		if (*pcbOutputNeeded > cbOutputData)
			return ERROR_INSUFFICIENT_BUFFER;
		if (hm_get_config(xcv->hMonitor, (LPCWSTR)pInputData,
			(h_config*)pOutputData))
			return ERROR_SUCCESS;
	}
	else if (wcscmp(pszDataName, L"SetConfig") == 0)
	{
		if (!(xcv->GrantedAccess & SERVER_ACCESS_ADMINISTER))
			return ERROR_ACCESS_DENIED;
		if (!hm_validate_config((h_config*)pInputData))
			return ERROR_INVALID_PARAMETER;
		if (hm_set_config(xcv->hMonitor, (h_config*)pInputData))
			return ERROR_SUCCESS;
	}
	return ERROR_INVALID_PARAMETER;
}
//***********************************************************
BOOL WINAPI heXcvClosePort(HANDLE hXcv)
{
	REXCVPORT* xcv = (REXCVPORT*)hXcv;

#ifdef _DEBUG
	DebugWriteToFile(L"heXcvClosePort - 1");
#endif

	if ((xcv == NULL) || (xcv->dwSize != sizeof(REXCVPORT)))
		return FALSE;

#ifdef _DEBUG
	DebugWriteToFile(L"heXcvClosePort - 2");
#endif
	// free memory from heXcvOpenPort
	HeapFree(GetProcessHeap(), 0, xcv);

	return TRUE;
}
//***********************************************************
VOID WINAPI heShutdown(HANDLE hMonitor)
{
	UNREFERENCED_PARAMETER(hMonitor);

#ifdef _DEBUG
	DebugWriteToFile(L"heShutdown");
#endif
	/* undo InitializePrintMonitor2 */
	/* nothing to do */
}
//***********************************************************
BOOL WINAPI heOpenPort(HANDLE hMonitor, LPWSTR pName, PHANDLE pHandle)
{
	BOOL flag;
#ifdef _DEBUG
	DebugWriteToFile(L"heOpenPort");
#endif

	flag = hfax_open_port(hMonitor, pName, pHandle);
	return flag;
}
//---------------------------------------------------------------------------
BOOL Is_Win2000()
{
	OSVERSIONINFOEX osvi;
	DWORDLONG dwlConditionMask = 0;

	// Initialize the OSVERSIONINFOEX structure.
	ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	osvi.dwMajorVersion = 5;
	osvi.dwMinorVersion = 0;

	// Initialize the condition mask.
	VER_SET_CONDITION(dwlConditionMask, VER_MAJORVERSION, VER_EQUAL);
	VER_SET_CONDITION(dwlConditionMask, VER_MINORVERSION, VER_EQUAL);

	// Perform the test.
	return VerifyVersionInfo(
		&osvi,
		VER_MAJORVERSION | VER_MINORVERSION,
		dwlConditionMask);
}
// ---------------------------------------------------------------------------
// InitializeMonitorEx
//
// This is the only real entry point that should be exported out of this
// monitor dll. The rest of the entry points are exposed to the caller via
// this routine which initializes the local monitor.
//
// ---------------------------------------------------------------------------

MONITOR2 Monitor2 = {
	sizeof(MONITOR2),
	heEnumPorts,
	heOpenPort,
	NULL, /* OpenPortEx */
	heStartDocPort,
	heWritePort,
	heReadPort,
	heEndDocPort,
	heClosePort,
	NULL, /* AddPort */
	NULL, /* AddPortEx */
	NULL, /* ConfigurePort */
	NULL, /* DeletePort */
	NULL, /* GetPrinterDataFromPort */
	heSetPortTimeOuts,
	heXcvOpenPort,
	heXcvDataPort,
	heXcvClosePort,
	heShutdown
};
// The above needs serious initialization;
//***********************************************************
LPMONITOR2 WINAPI InitializePrintMonitor2(__in PMONITORINIT  pMonitorInit,
	__out PHANDLE  phMonitor)
{
#ifdef _DEBUG
	DebugWriteToFile(L"InitializePrintMonitor2");
#endif
	if (Is_Win2000())
		Monitor2.cbSize = MONITOR2_SIZE_WIN2K;

	*phMonitor = (PHANDLE)pMonitorInit;

	return(&Monitor2);
}
//***********************************************************
/* get servername size first
* the port monitor server */
DWORD GetServerNameSize(PCWSTR pServer, PCWSTR pObjectName, PCWSTR pObjectType)
{
	DWORD   cbOutput;
	// example: ",XcvMonitor Winprint Hylafax"
	cbOutput = static_cast<DWORD>(pServer ? (wcslen(pServer) + 2) * sizeof(WCHAR) : sizeof(WCHAR));   /* "\\Server\" -> +2 = "\\Server" + "\," OR only "," */
	cbOutput += static_cast<DWORD>((wcslen(pObjectType) + 1) * sizeof(WCHAR));                        /* "\\Server\,XcvMonitor " -> +1 = " " */
	cbOutput += pObjectName ? static_cast<DWORD>((wcslen(pObjectName) + 1) * sizeof(WCHAR)) : sizeof(WCHAR); /* "\\Server\,XcvMonitor Object_"  -> +1 = 0 */
	return cbOutput;
}
/* Copy to pServerName the printer name for accessing
* the port monitor server */
void ConstructXcvName(PWSTR pServerName, PCWSTR pServer, PCWSTR pObjectName, PCWSTR pObjectType)
{
	if (pServerName)
	{
		pServerName[0] = L'\0';
		if (pServer)
		{
			wcscpy(pServerName, pServer);
			wcscat(pServerName, L"\\");
		}

		wcscat(pServerName, L",");
		wcscat(pServerName, pObjectType); // XcvMonitor
		wcscat(pServerName, L" ");

		if (pObjectName)
			wcscat(pServerName, pObjectName); // Winprint Hylafax
	}
}
//***********************************************************
LONG cAddPortUI(HANDLE hXcv, HWND hWnd, PCWSTR pszPortNameIn, PWSTR pszPortNameOut)
{
	UNREFERENCED_PARAMETER(pszPortNameIn);

	DWORD dwStatus;
	DWORD dwOutput = 0;
	DWORD dwNeeded;
	WCHAR buf[STRVALUE_TEXT_256];
	BOOL unique;
	int i;
	DWORD config_len = hm_sizeof_config();
	h_config* pconfig;
	LPWSTR portname;	/* pointer to port name in h_config */
	PWSTR  pszPortName = nullptr;

#ifdef _DEBUG
	DebugWriteToFile(L"cAddPortUI");
#endif

	* pszPortNameOut = L'\0';
	pconfig = (h_config*)malloc(config_len);

	if (pconfig == NULL)
		return ERROR_NOT_ENOUGH_MEMORY;

	portname = hm_init_config(pconfig);

	dwStatus = ERROR_SUCCESS;
	unique = TRUE;
	do {
		if (!unique)
		{
			StringCbPrintf(buf, STRVALUE_TEXT_256 * sizeof(WCHAR),
				L"Port \042%s\042 is already used.  Please select a unique port name", portname);
			if (SendInfoMessage(buf, L"Port added", MB_OKCANCEL) == IDCANCEL)
			{
				dwStatus = ERROR_CANCELLED;
				break;
			}
		}
		if (dwStatus == ERROR_CANCELLED)
			break;

		/* Suggest a unique port name */
		for (i = 1; i < 10; i++)
		{
			if (!hm_suggest_portname(portname, MAXSHORTSTR, i))
				break;
			dwStatus = ERROR_SUCCESS;
			if (!XcvData(hXcv, L"PortExists", (PBYTE)portname,
				static_cast<DWORD>(sizeof(WCHAR) * (wcslen(portname) + 1)),
				(PBYTE)(&dwOutput), 0, &dwNeeded, &dwStatus))
			{
				dwStatus = GetLastError();
			}
			if (dwStatus == ERROR_SUCCESS)
				break;	/* unique name */
		}
		AddPortForm = new TAddPortForm();
		AddPortForm->SetPortName(portname);
		AddPortForm->CallUpDialog((HINSTANCE)hHylaInst, hWnd);

		if (!AddPortForm->GetReturnValue())
		{
			dwStatus = ERROR_CANCELLED;
			delete AddPortForm;
			AddPortForm = nullptr; // global variable - set to null
			break; // while (!unique)
		}
		wcscpy(portname, AddPortForm->GetPortName());
		delete AddPortForm;
		AddPortForm = nullptr; // global variable - set to null

		if (lstrlen(portname) && portname[lstrlen(portname) - 1] != ':')
			lstrcat(portname, L":");	/* append ':' if not present */

		pszPortName = (LPWSTR)portname;
		dwStatus = ERROR_SUCCESS;
		if (!XcvData(hXcv, L"PortExists", (BYTE*)pszPortName,
			static_cast<DWORD>(wcslen(pszPortName) + 1) * sizeof(WCHAR),
			(PBYTE)(&dwOutput), 0, &dwNeeded, &dwStatus))
			dwStatus = GetLastError();
		if (dwStatus != ERROR_SUCCESS)
			unique = FALSE;
	} while (!unique);

	pszPortName = (LPWSTR)portname;
	if (dwStatus == ERROR_SUCCESS)
	{
		if (!XcvData(hXcv, L"AddPort", (BYTE*)pszPortName,
			static_cast<DWORD>((wcslen(pszPortName) + 1) * sizeof(WCHAR)),
			(PBYTE)(&dwOutput), 0, &dwNeeded, &dwStatus))
			dwStatus = GetLastError();
		else
		{
			wcscpy(pszPortNameOut, portname);
			wcsncpy_s(pconfig->szPortName, STRVALUE_TEXT_256, pszPortNameOut, _TRUNCATE);
		}
	}

	if (dwStatus == ERROR_SUCCESS) {
		/* Set configuration if supplied */
		dwOutput = 0;
		dwNeeded = 0;
		if (!XcvData(hXcv, L"SetConfig",
			(PBYTE)pconfig, config_len,
			(PBYTE)(&dwOutput), 0, &dwNeeded, &dwStatus))
			dwStatus = GetLastError();
	}
	else
		ShowError(dwStatus, L"cAddPortUI");

	free(pconfig);

	return dwStatus;
}
//***********************************************************
BOOL WINAPI heAddPortUI(PCWSTR pszServer, HWND hWnd,
	PCWSTR pszPortNameIn, PWSTR* ppszPortNameOut)
{
	UNREFERENCED_PARAMETER(ppszPortNameOut);

	PWSTR  pszServerName = NULL;
	WCHAR portname[STRVALUE_TEXT_256] = { 0 };
	PRINTER_DEFAULTS pd = { NULL, NULL, SERVER_ACCESS_ADMINISTER };
	HANDLE hXcv = NULL;
	DWORD dwError, dwServerNameSize;

#ifdef _DEBUG
	DebugWriteToFile(L"heAddPortUI");
#endif

	dwServerNameSize = GetServerNameSize(pszServer, pszPortNameIn, L"XcvMonitor");
	pszServerName = (WCHAR*)malloc(dwServerNameSize);
	if (!pszServerName)
		return FALSE;
	ConstructXcvName(pszServerName, pszServer, pszPortNameIn, L"XcvMonitor");

	if (!OpenPrinter(pszServerName, &hXcv, &pd))
	{
		free(pszServerName);
		return FALSE;
	}

	if (hXcv == NULL)
		SendInfoMessage(L"HANDLE hXcv is NULL", L"heAddPortUI", MB_OK | MB_ICONINFORMATION);

	dwError = cAddPortUI(hXcv, hWnd, pszPortNameIn, (WCHAR*)portname);

	ClosePrinter(hXcv);
	free(pszServerName);
	return (dwError == ERROR_SUCCESS);
}
//***********************************************************
DWORD cConfigurePortUI(HANDLE hXcv, HWND hWnd, PCWSTR pszPortName)
{
	DWORD config_len = sizeof(h_config);
	h_config* pconfig = (h_config*)malloc(config_len);
	DWORD dwOutput = 0;
	DWORD dwNeeded = 0;
	DWORD dwError = ERROR_SUCCESS;

	if (pconfig == NULL)
		return ERROR_NOT_ENOUGH_MEMORY;

#ifdef _DEBUG
	DebugWriteToFile(L"cConfigurePortUI");
#endif

	/* get current configuration */
	if (!XcvData(hXcv, L"GetConfig", (PBYTE)pszPortName, static_cast<DWORD>((wcslen(pszPortName) + 1) * sizeof(WCHAR)),
		(PBYTE)pconfig, config_len, &dwNeeded, &dwError))
		dwError = GetLastError();

	/* update it */
	if (dwError == ERROR_SUCCESS)
	{
		Configure_sheet1 = new CConfigure_sheet1();
		UserConfigure = new CUserConfigure();
		ConfigureActiveMode_sheet2 = new CConfigure_sheet2();
		ConfigureLDAP_sheet3 = new CConfigure_sheet3();
		Get_user_from_sessionid(UserConfigure);
		UserConfigure->LoadXML();

		// ************************************************************************************
		// sheet 1
		// ************************************************************************************
		Configure_sheet1->SetHinstance((HINSTANCE)hHylaInst);
		Configure_sheet1->SetServerText(pconfig->szServer);
		Configure_sheet1->SetUsernameText(pconfig->szUsername);
		Configure_sheet1->SetPasswordText(pconfig->szPassword);
		Configure_sheet1->SetEmailText(pconfig->szDefaultNotifyEmail);
		Configure_sheet1->SetModemText(pconfig->szModem);
		Configure_sheet1->SetAddressBookText(pconfig->szAddressBook);
		Configure_sheet1->SetHFPortNumber(pconfig->szHFPortNumber);
		Configure_sheet1->SetPageSizeText(pconfig->szPageSize);
		Configure_sheet1->SetNotificationText(pconfig->szNotificationType);
		Configure_sheet1->SetResolutionText(pconfig->szResolution);
		Configure_sheet1->SetMaxDialsText(pconfig->szMaxDials);
		Configure_sheet1->SetMaxTriesText(pconfig->szMaxTries);
		Configure_sheet1->SetInfoDialogTimerText(pconfig->szInfoDialogTimer);

		if (pconfig->dwPassiveIPIgnore == 1)
			Configure_sheet1->SetPasvIPIgnoreMode(BST_CHECKED);
		else
			Configure_sheet1->SetPasvIPIgnoreMode(BST_UNCHECKED);

		if (pconfig->dwFaxNumberFromClipboard == 1)
			Configure_sheet1->SetUseFaxnumberFromClipboard(BST_CHECKED);
		else
			Configure_sheet1->SetUseFaxnumberFromClipboard(BST_UNCHECKED);
		// ************************************************************************************
		// sheet 2 - active mode
		// ************************************************************************************
		if (pconfig->dwUseActiveMode == 1)
			ConfigureActiveMode_sheet2->SetFTPActiveMode(BST_CHECKED);
		else
			ConfigureActiveMode_sheet2->SetFTPActiveMode(BST_UNCHECKED);

		ConfigureActiveMode_sheet2->SetActivePortNumber(pconfig->szActivePortNumber);

		// ************************************************************************************
		// sheet 3 - LDAP
		// ************************************************************************************
		ConfigureLDAP_sheet3->SetUseLDAP(pconfig->dwUseLDAP);
		ConfigureLDAP_sheet3->SetUseLDAPSsl(pconfig->dwLDAP_SSL);
		ConfigureLDAP_sheet3->SetUseLDAPStartTLS(pconfig->dwLDAP_STARTTLS);
		ConfigureLDAP_sheet3->SetLDAPServer(pconfig->szLDAPServer);
		ConfigureLDAP_sheet3->SetLDAPPort(pconfig->szLDAPPort);
		ConfigureLDAP_sheet3->SetLDAPBindDN(pconfig->szLDAPBindDN);
		ConfigureLDAP_sheet3->SetLDAPPassword(pconfig->szLDAPPassword);
		ConfigureLDAP_sheet3->SetLDAPBaseDN(pconfig->szLDAPBaseDN);
		ConfigureLDAP_sheet3->SetLDAPFilter(pconfig->szLDAPFilter);
		if (wcscmp(pconfig->szLDAPAutocompleteAttribute, _T(LDAP_AUTOCOMPLETE_ATT_CN)) == 0)
		{
			ConfigureLDAP_sheet3->SetLDAPAutocompleteAttIndex(0); // cn
			ConfigureLDAP_sheet3->SetLDAPAutocompleteAttribute(_T(LDAP_AUTOCOMPLETE_ATT_CN));
		}
		else
		{
			ConfigureLDAP_sheet3->SetLDAPAutocompleteAttIndex(1); // displayName
			ConfigureLDAP_sheet3->SetLDAPAutocompleteAttribute(_T(LDAP_AUTOCOMPLETE_ATT_DISPLAYNAME));
		}

		// ************************************************************************************
		// call printer port config dialog on the display
		ConfigCallUpDialog((HINSTANCE)hHylaInst, hWnd);
		// ************************************************************************************

		// save default config
		if (Configure_sheet1->GetReturnValue()) // Click OK on config dialog
		{
			cConfigurePortUI_copyvalues(pconfig);
			// write to registry - only admin
			//if (UserConfigure->GetIsAdmin())
			if (!XcvData(hXcv, L"SetConfig", (PBYTE)pconfig, config_len, (PBYTE)(&dwOutput), 0, &dwNeeded, &dwError))
				dwError = GetLastError();

			// save user config
			UserConfigure->SaveXML();
		}

		delete ConfigureLDAP_sheet3;
		ConfigureLDAP_sheet3 = nullptr; // global var - null to safe

		delete ConfigureActiveMode_sheet2;
		ConfigureActiveMode_sheet2 = nullptr; // global var - null to safe

		delete UserConfigure;
		UserConfigure = nullptr; // global var - null to safe

		delete Configure_sheet1;
		Configure_sheet1 = nullptr; // global var - null to safe
	}
	else // if (dwError == ERROR_SUCCESS)
		ShowError(dwError, L"GetConfig Error");

	free(pconfig); // local var, no need null
	return dwError;
}
// **************************************************************
// copy data to the struct
void cConfigurePortUI_copyvalues(h_config* pconfig)
{
	//******************************************************************
	// sheet 1 - tab1 - basic configuration
	//******************************************************************
	// server
	wcsncpy_s(pconfig->szServer, STRVALUE_SERVER, Configure_sheet1->GetServerText(), _TRUNCATE);
	// username
	wcsncpy_s(pconfig->szUsername, STRVALUE_USER, Configure_sheet1->GetUsernameText(), _TRUNCATE);
	// password
	wcsncpy_s(pconfig->szPassword, STRVALUE_PASSWORD, Configure_sheet1->GetPasswordText(), _TRUNCATE);
	// email
	wcsncpy_s(pconfig->szDefaultNotifyEmail, STRVALUE_EMAIL, Configure_sheet1->GetEmailText(), _TRUNCATE);
	// modem
	wcsncpy_s(pconfig->szModem, STRVALUE_MODEM, Configure_sheet1->GetModemText(), _TRUNCATE);
	// address book
	wcsncpy_s(pconfig->szAddressBook, MAX_PATH, Configure_sheet1->GetAddressBookText(), _TRUNCATE);
	// HF port
	wcsncpy_s(pconfig->szHFPortNumber, STRVALUE_PORTNUMBER, Configure_sheet1->GetHFPortNumber(), _TRUNCATE);
	wcsncpy_s(pconfig->szPageSize, STRVALUE_PAGESIZE, Configure_sheet1->GetPageSizeText(), _TRUNCATE);
	// notif
	wcsncpy_s(pconfig->szNotificationType, STRVALUE_NOTIFICATION, Configure_sheet1->GetNotificationText(), _TRUNCATE);
	// resolution
	wcsncpy_s(pconfig->szResolution, STRVALUE_RESOLUTION, Configure_sheet1->GetResolutionText(), _TRUNCATE);
	// maxdials
	wcsncpy_s(pconfig->szMaxDials, STRVALUE_MAXTIMES_NUMBER, Configure_sheet1->GetMaxDialsText(), _TRUNCATE);
	// maxtries
	wcsncpy_s(pconfig->szMaxTries, STRVALUE_MAXTIMES_NUMBER, Configure_sheet1->GetMaxTriesText(), _TRUNCATE);
	// infodialog timer
	wcsncpy_s(pconfig->szInfoDialogTimer, STRVALUE_INFODIALOG_TIMER_NUMBER, Configure_sheet1->GetInfoDialogTimerText(), _TRUNCATE);
	// ignore ip pasv
	if (Configure_sheet1->GetPasvIPIgnoreMode() == BST_CHECKED)
		pconfig->dwPassiveIPIgnore = 1;
	else
		pconfig->dwPassiveIPIgnore = 0;
	// enter FAX number from the clipboard
	if (Configure_sheet1->GetUseFaxnumberFromClipboard() == BST_CHECKED)
		pconfig->dwFaxNumberFromClipboard = 1;
	else
		pconfig->dwFaxNumberFromClipboard = 0;
	//******************************************************************
	// sheet2 - tab 2 - Active mode
	//******************************************************************
	// active port number
	wcsncpy_s(pconfig->szActivePortNumber, STRVALUE_PORTNUMBER, ConfigureActiveMode_sheet2->GetActivePortNumber(), _TRUNCATE);

	// use active mode
	if (ConfigureActiveMode_sheet2->GetFTPActiveMode() == BST_CHECKED)
		pconfig->dwUseActiveMode = 1;
	else
		pconfig->dwUseActiveMode = 0;

	//******************************************************************
	// sheet3 - tab3 - LDAP
	//******************************************************************
	// LDAP - use ldap
	if (ConfigureLDAP_sheet3->GetUseLDAP() == BST_CHECKED)
		pconfig->dwUseLDAP = 1;
	else
		pconfig->dwUseLDAP = 0;
	// use ldap ssl
	if (ConfigureLDAP_sheet3->GetUseLDAPSsl() == BST_CHECKED)
		pconfig->dwLDAP_SSL = 1;
	else
		pconfig->dwLDAP_SSL = 0;
	// use ldap StartTLS
	if (ConfigureLDAP_sheet3->GetUseLDAPStartTLS() == BST_CHECKED)
		pconfig->dwLDAP_STARTTLS = 1;
	else
		pconfig->dwLDAP_STARTTLS = 0;
	// ldap server
	wcsncpy_s(pconfig->szLDAPServer, STRVALUE_SERVER, ConfigureLDAP_sheet3->GetLDAPServer(), _TRUNCATE);
	// ldap port
	wcsncpy_s(pconfig->szLDAPPort, STRVALUE_PORTNUMBER, ConfigureLDAP_sheet3->GetLDAPPort(), _TRUNCATE);
	// ldap bind dn
	wcsncpy_s(pconfig->szLDAPBindDN, STRVALUE_LDAP_BIND_DN, ConfigureLDAP_sheet3->GetLDAPBindDN(), _TRUNCATE);
	// ldap password
	wcsncpy_s(pconfig->szLDAPPassword, STRVALUE_PASSWORD, ConfigureLDAP_sheet3->GetLDAPPassword(), _TRUNCATE);
	// ldap base dn
	wcsncpy_s(pconfig->szLDAPBaseDN, STRVALUE_LDAP_BASE_DN, ConfigureLDAP_sheet3->GetLDAPBaseDN(), _TRUNCATE);
	// filter
	wcsncpy_s(pconfig->szLDAPFilter, STRVALUE_LDAP_FILTER, ConfigureLDAP_sheet3->GetLDAPFilter(), _TRUNCATE);
	// search attribute
	wcsncpy_s(pconfig->szLDAPAutocompleteAttribute, STRVALUE_LDAP_AUTOCOMPLETE_ATT, ConfigureLDAP_sheet3->GetLDAPAutocompleteAttribute(), _TRUNCATE);
}
//***********************************************************
BOOL WINAPI heConfigurePortUI(PCWSTR pszServer, HWND hWnd, PCWSTR pszPortName)
{
	LPWSTR pszServerName = NULL;
	HANDLE hXcv = NULL;
	DWORD dwError, dwServerNameSize;
	PRINTER_DEFAULTS pd = { NULL, NULL, SERVER_ACCESS_ADMINISTER };

#ifdef _DEBUG
	DebugWriteToFile(L"heConfigurePortUI");
#endif

	dwServerNameSize = GetServerNameSize(pszServer, pszPortName, L"XcvPort");
	pszServerName = (WCHAR*)malloc(dwServerNameSize);
	if (!pszServerName)
		return FALSE;
	ConstructXcvName(pszServerName, pszServer, pszPortName, L"XcvPort");

	if (!OpenPrinter(pszServerName, &hXcv, &pd)) {
		free(pszServerName);
		return FALSE;
	}

	dwError = cConfigurePortUI(hXcv, hWnd, pszPortName);

	ClosePrinter(hXcv);
	free(pszServerName);
	return (dwError == ERROR_SUCCESS);
}
//***********************************************************
BOOL WINAPI heDeletePortUI(PCWSTR pszServer, HWND hWnd, PCWSTR pszPortName)
{
	UNREFERENCED_PARAMETER(hWnd);

	PWSTR  pszServerName = NULL;
	HANDLE hXcv = NULL;
	DWORD dwStatus = ERROR_SUCCESS;
	DWORD dwOutput = 0;
	DWORD dwNeeded = 0;
	DWORD dwServerNameSize;
	PRINTER_DEFAULTS pd = { NULL, NULL, SERVER_ACCESS_ADMINISTER };

#ifdef _DEBUG
	DebugWriteToFile(L"heDeletePortUI");
#endif

	dwServerNameSize = GetServerNameSize(pszServer, pszPortName, L"XcvPort");
	pszServerName = (WCHAR*)malloc(dwServerNameSize);
	if (!pszServerName)
		return FALSE;
	ConstructXcvName(pszServerName, pszServer, pszPortName, L"XcvPort");

	if (!OpenPrinter(pszServerName, &hXcv, &pd))
	{
		free(pszServerName);
		return FALSE;
	}

	/* Delete port */
	if (!XcvData(hXcv, L"DeletePort", (PBYTE)pszPortName,
		static_cast<DWORD>((wcslen(pszPortName) + 1) * sizeof(WCHAR)),
		(PBYTE)(&dwOutput), 0, &dwNeeded, &dwStatus)) {
	}

	ClosePrinter(hXcv);
	free(pszServerName);

	return (dwStatus == ERROR_SUCCESS);
}
//***********************************************************
MONITORUI MonitorUI =
{
	sizeof(MONITORUI),
	heAddPortUI,
	heConfigurePortUI,
	heDeletePortUI
};
//***********************************************************
PMONITORUI WINAPI InitializePrintMonitorUI(VOID)
{
#ifdef _DEBUG
	DebugWriteToFile(L"InitializePrintMonitorUI");
#endif
	return &MonitorUI;
}
//***********************************************************
