//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by HylaPrint.rc
//
#define IDD_DIALOG_PORTNAME             101
#define IDD_DIALOG_CONFIG1              102
#define IDD_DIALOG_CONFIG2              112
#define IDD_DIALOG_CONFIG_MAIN          113
#define IDD_DIALOG_CONFIG3              115
#define CONTROL_PANEL_RESOURCE_ID       123
#define IDC_EDIT_PORTNAME               1001
#define ID_PORT_OK                      1002
#define ID_PORT_CANCEL                  1003
#define IDC_EDIT_CONFSERVER_ADDRESS     1004
#define IDC_EDIT_CONFUSERNAME           1005
#define IDC_EDIT_CONFPASSWORD           1006
#define IDC_EDIT_CONFDEF_NOTIFEMAIL     1007
#define IDC_EDIT_CONFMODEM              1008
#define IDC_CHECK_PASVIP                1009
#define IDC_COMBO_CONFNOTIF_TYP         1010
#define IDC_EDIT_CONFBOOK_DIR           1012
#define IDC_COMBO_CONFPAGESIZE          1013
#define IDC_COMBO_CONFRESOLUTION        1014
#define ID_OK_CONFIGDLG                 1016
#define ID_CANCEL_CONFIGDLG             1017
#define IDC_COMBO_ADDRBOOK_FORMAT       1018
#define IDC_CHECK_USEACTIVE             1024
#define IDC_BUTTON_CONFIG_BROWSEDIR     1025
#define IDC_STATIC_VERSION              1026
#define IDC_EDIT_ACTIVEPORT             1032
#define IDC_EDIT_CONFSERVER_HFPORT      1033
#define IDC_BUTTON_CONFIG_CLEAR_PRINTERQUEUE 1036
#define IDC_COMBO_CONFIG_IPADDRESSES    1044
#define IDC_COMBO_SELECT_USER           1045
#define IDC_STATIC_HF_SERVER            1046
#define IDC_STATIC_PORT_NUM             1047
#define IDC_STATIC_SYSTEMWIDE           1048
#define IDC_LANG_STATIC_USERNAME        1049
#define IDC_LANG_STATIC_PASSWORD        1050
#define IDC_LANG_STATIC_EMAIL           1051
#define IDC_LANG_STATIC_NOTIFICATION    1052
#define IDC_LANG_STATIC_MODEM           1053
#define IDC_LANG_STATIC_ADDRBOOK        1054
#define IDC_LANG_STATIC_PAGESIZE        1055
#define IDC_LANG_STATIC_RESOLUTION      1056
#define IDC_STATIC_INCOMING_PORT        1057
#define IDC_SYSLINK1                    1060
#define IDC_STATIC_WPHFXWIN             1061
#define IDC_EDIT_LDAP_SERVER            1062
#define IDC_EDIT_LDAP_BIND_DN           1063
#define IDC_EDIT_LDAP_PORT              1064
#define IDC_EDIT_LDAP_PASSWORD          1065
#define IDC_EDIT_LDAP_BASE_DN           1066
#define IDC_EDIT_LDAP_FILTER            1067
#define IDC_CHECK_LDAP_SSL              1068
#define IDC_CHECK_USE_LDAP              1070
#define IDC_COMBO_LDAP_AUTOCOMPLETE_ATTRIBUTE 1071
#define IDC_STATIC_LDAP_PASSWORD        1072
#define IDC_STATIC_LDAP_PORT            1073
#define IDC_STATIC_LDAP_SERVER          1074
#define IDC_STATIC_LDAP_BIND_DN         1076
#define IDC_STATIC_LDAP_BASE_DN         1077
#define IDC_STATIC_LDAP_FILTER          1078
#define IDC_STATIC_LDAP_ADVANCED        1079
#define IDC_STATIC_LDAP_SETTINGS        1080
#define IDC_STATIC_ACTIVE_MODE          1081
#define IDC_STATIC_ACTIVE_SPECIAL_SETTINGS 1082
#define IDC_STATIC_LDAP_SEARCH_ATTRIBUTE 1083
#define IDC_CHECK_LDAP_STARTTLS         1084
#define IDC_STATIC_UPDATE               1085
#define IDC_BUTTON_UPDATE               1086
#define IDC_LANG_STATIC_MAXDIALS        1087
#define IDC_COMBO_MAXDIALS              1088
#define IDC_LANG_STATIC_MAXTRIES        1089
#define IDC_COMBO_MAXTRIES              1090
#define IDC_STATIC_RELDATE              1091
#define IDC_LANG_STATIC_INFODIALOG_TIMER 1092
#define IDC_COMBO_CONFINFODIALOG_TIMER  1093
#define IDC_CHECK_FAXNUM_CLIPBOARD      1094

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        117
#define _APS_NEXT_COMMAND_VALUE         40003
#define _APS_NEXT_CONTROL_VALUE         1095
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
