#ifndef ipcH
#define ipcH

#include "SendFAX.h"
#include "../Language/language.h"
#include "system_message.h"
#include "info_message.h"

BOOL StartProcess(PROCESS_INFORMATION* pi, STARTUPINFO* si, PHANDLE hPipe);
DWORD WINAPI InstanceThread(LPVOID);
BOOL PipeServer();

#endif
//**************************************************
