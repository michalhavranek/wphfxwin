#include "session_id.h"

// ****************************************************************************************
// get console id
// ****************************************************************************************
DWORD GetSessionID()
{
	DWORD dwSessionId = 0;
	BOOL bResult = FALSE;
	HANDLE hToken = INVALID_HANDLE_VALUE, hTokenDup = INVALID_HANDLE_VALUE;
	DWORD dwRetLen = 0;

	bResult = ImpersonateSelf(SecurityImpersonation);
	if (!bResult) // fail
	{
		dwSessionId = WTSGetActiveConsoleSessionId();
		goto Cleanup;
	}
	// primary token
	bResult = OpenThreadToken(GetCurrentThread(), TOKEN_QUERY | TOKEN_ASSIGN_PRIMARY | TOKEN_DUPLICATE | TOKEN_IMPERSONATE, TRUE, &hToken);
	if (!bResult) // fail
	{
		if (!hToken) // if NULL (bug in ATL)
			hToken = INVALID_HANDLE_VALUE;
		dwSessionId = WTSGetActiveConsoleSessionId();
		goto Cleanup;
	}
	// duplikate token
	bResult = DuplicateTokenEx(hToken, MAXIMUM_ALLOWED, NULL, SecurityImpersonation, TokenPrimary, &hTokenDup);
	if (!bResult) // fail
	{
		if (!hTokenDup) // if NULL (bug in ATL)
			hTokenDup = INVALID_HANDLE_VALUE;
		dwSessionId = WTSGetActiveConsoleSessionId();
		goto Cleanup;
	}
	// console ID
	bResult = GetTokenInformation(hTokenDup, TokenSessionId, &dwSessionId, sizeof(dwSessionId), &dwRetLen);
	if (!bResult)
		dwSessionId = WTSGetActiveConsoleSessionId();
Cleanup:
	RevertToSelf();
	if (hToken != INVALID_HANDLE_VALUE)
		CloseHandle(hToken);
	if (hTokenDup != INVALID_HANDLE_VALUE)
		CloseHandle(hTokenDup);

	return dwSessionId;
}
// ****************************************************************************************
// ****************************************************************************************
