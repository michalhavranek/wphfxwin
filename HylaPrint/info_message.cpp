//***********************************************************
#include "info_message.h"

//***********************************************************
// info message - (message, title, MB_OK|MB_ICONWARNING)
DWORD SendInfoMessage(WCHAR* message, WCHAR* szTitle, UINT type)
{
	DWORD resp;
	DWORD dwTitleLength;
	//_T(LNG_INFO_DIALOG)
	dwTitleLength = static_cast<DWORD>((wcslen(szTitle) + 1) * sizeof(WCHAR));

	WTSSendMessageW(WTS_CURRENT_SERVER_HANDLE, GetSessionID(), szTitle,
		dwTitleLength, message, static_cast<DWORD>((wcslen(message) + 1) * sizeof(WCHAR)),
		type, 6, &resp, FALSE);
	return resp;
}
//***********************************************************
