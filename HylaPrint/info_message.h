#ifndef InfoMessageH
#define InfoMessageH

#include "windows.h"
#include <tchar.h>
#include <Wtsapi32.h>
#include "../Language/language.h"
#include "session_id.h"

#pragma comment (lib, "Wtsapi32.lib")

DWORD SendInfoMessage(WCHAR* message, WCHAR* szTitle, UINT type);

#endif
