#include "convert_string.h"

//***********************************************************
// conv func Unicode-Ansi
void UnicodeToAnsi(const WCHAR* wide, CHAR* multi, size_t iBuflen)
{
	int iLength;
	PSTR pAnsiStr;
	// conv UNICODE - ANSI
	iLength = WideCharToMultiByte(CP_ACP, 0, wide,
		-1, NULL, 0, NULL, NULL);
	if (iLength == 0)
	{
		multi[0] = '\0';
		return;
	}
	pAnsiStr = (PSTR)malloc(iLength);
	if (pAnsiStr)
	{
		WideCharToMultiByte(CP_ACP, 0, wide,
			-1, pAnsiStr, iLength, NULL, NULL);

		strncpy_s(multi, iBuflen, pAnsiStr, _TRUNCATE);
		free(pAnsiStr);
	}
	else
	{
		multi[0] = '\0';
	}
	return;
}
//***********************************************************
void AnsiToUnicode(const CHAR* pszA, WCHAR* pszW, size_t iBuflen)
{
	int iLength;
	iLength = MultiByteToWideChar(CP_ACP, 0, pszA, -1, 0, 0);
	if (iLength == 0)
	{
		pszW[0] = L'\0';
		return;
	}
	WCHAR* buf = new WCHAR[iLength];
	MultiByteToWideChar(CP_ACP, 0, pszA, -1, buf, iLength);
	wcsncpy_s(pszW, iBuflen, buf, _TRUNCATE);
	delete[] buf;
}
//***********************************************************
