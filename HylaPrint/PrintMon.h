//---------------------------------------------------------------------------

#ifndef PrintMonH
#define PrintMonH

#include "HylaPrint.h"
#include "system_message.h"
#include "Addport.h"
#include "config.h"

#define VERSION_NUMBER 0.0

extern HANDLE hHylaInst;
void HylaMonInit(HANDLE hModule);

//---------------------------------------------------------------------------
typedef struct
{
	// tab1
	DWORD dwSize;	/* sizeof this structure */
	DWORD dwVersion;	/* version number */
	WCHAR szPortName[STRVALUE_TEXT_256]; // Winprint Hylafax port name (HFAX1:)
	WCHAR szDescription[STRVALUE_TEXT_256];
	WCHAR szServer[STRVALUE_SERVER]; // hylafax server
	WCHAR szHFPortNumber[STRVALUE_PORTNUMBER]; // hylafax port
	WCHAR szUsername[STRVALUE_USER]; // hylafax user
	WCHAR szPassword[STRVALUE_PASSWORD]; // hylafax password
	WCHAR szDefaultNotifyEmail[STRVALUE_EMAIL]; // notifi email
	WCHAR szModem[STRVALUE_MODEM]; // hylafax - modem group
	WCHAR szAddressBook[MAX_PATH]; // addrbook
	WCHAR szPageSize[STRVALUE_PAGESIZE]; // FAX page size 
	WCHAR szResolution[STRVALUE_RESOLUTION]; // FAX resolution
	WCHAR szNotificationType[STRVALUE_NOTIFICATION]; // notification type
	WCHAR szMaxDials[STRVALUE_MAXTIMES_NUMBER]; // maxdials param
	WCHAR szMaxTries[STRVALUE_MAXTIMES_NUMBER]; // maxtries param
	DWORD dwPassiveIPIgnore; // ignore passive IP
	WCHAR szInfoDialogTimer[STRVALUE_INFODIALOG_TIMER_NUMBER]; // timer for info dialo after fax sending
	DWORD dwFaxNumberFromClipboard; // enter FAX number from the clipboard
	// tab2
	DWORD dwUseActiveMode; // use active mode
	WCHAR szActivePortNumber[STRVALUE_PORTNUMBER]; // FTP active port number
	// tab3
	DWORD dwUseLDAP; // use LDAP server
	WCHAR szLDAPServer[STRVALUE_SERVER]; // LDAP server
	WCHAR szLDAPPort[STRVALUE_PORTNUMBER]; // LDAP port number
	DWORD dwLDAP_SSL; // LDAP SSL
	DWORD dwLDAP_STARTTLS; // LDAP StartTLS
	WCHAR szLDAPBindDN[STRVALUE_LDAP_BIND_DN]; // LDAP Bind DN (username)
	WCHAR szLDAPPassword[STRVALUE_PASSWORD]; // LDAP password
	WCHAR szLDAPBaseDN[STRVALUE_LDAP_BASE_DN]; // LDAP base DN (search)
	WCHAR szLDAPFilter[STRVALUE_LDAP_FILTER]; // LDAP filter
	WCHAR szLDAPAutocompleteAttribute[STRVALUE_LDAP_AUTOCOMPLETE_ATT]; // autocomplete search attribute
} h_config;

void cConfigurePortUI_copyvalues(h_config* pconfig);


typedef struct
{
	HANDLE hPort;		/* handle to this structure */
	HANDLE hMonitor;		/* provided by NT5.0 OpenPort */
	WCHAR portname[STRVALUE_TEXT_256];    /* Name obtained during OpenPort  */

	h_config config;		/* struct, configuration stored in registry */

	HANDLE hPrinter;    /* handle to the printer */
	/* Details obtained during StartDocPort  */
	HANDLE hFile;
	WCHAR pFileName[MAX_PATH];
	WCHAR pPrinterName[STRVALUE_TEXT_256];	/* Printer name for port */
	WCHAR pDocName[MAX_PATH];	/* Document Name (from StartDocPort) */
	DWORD JobId;
	WCHAR pUserName[STRVALUE_USER];	/* User Name (from StartDocPort job info) */
	WCHAR pMachineName[STRVALUE_TEXT_256];	/* Machine Name (from StartDocPort job info) */
} h_portdata;

BOOL hm_get_config(HANDLE hMonitor, LPCWSTR portname, h_config* config);

#endif
