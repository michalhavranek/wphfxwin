#ifndef ConvertStringH
#define ConvertStringH

#pragma warning(disable:4995)
#include "windows.h"
#include <tchar.h>

void UnicodeToAnsi(const WCHAR* wide, CHAR* multi, size_t iBuflen);
void AnsiToUnicode(const CHAR* pszA, WCHAR* pszW, size_t iBuflen);

#endif
