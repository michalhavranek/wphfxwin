#include "stdafx.h"
#include "HFAXInterface.h"

struct TFAXSend* FAXSend;

void reset_portdataHF(h_portdata* prd)
{
	/* do not touch prd->portname, prd->hPort or prd->hMonitor */
	prd->hPrinter = INVALID_HANDLE_VALUE;
	prd->hFile = INVALID_HANDLE_VALUE;
	prd->pFileName[0] = L'\0';
	prd->pPrinterName[0] = L'\0';
	prd->pDocName[0] = L'\0';
	prd->JobId = 0;
	prd->pUserName[0] = L'\0';
	prd->pMachineName[0] = L'\0';
}
//***********************************************************
BOOL get_job_info(h_portdata* prd)
{
#ifdef _DEBUG
	DebugWriteToFile(L"get_job_info");
#endif

	if (OpenPrinter(prd->pPrinterName, &prd->hPrinter, NULL))
	{
		DWORD dwNeeded = 0;
		BOOL bStatus;
		BYTE* pByteArray;
		bStatus = GetJob(prd->hPrinter, prd->JobId, 1, NULL, 0, &dwNeeded);
		if (!bStatus && GetLastError() == ERROR_INSUFFICIENT_BUFFER) // OK - allocate memory to dwNeeded
		{
			pByteArray = new BYTE[dwNeeded];
			bStatus = GetJob(prd->hPrinter, prd->JobId, 1, pByteArray, dwNeeded, &dwNeeded);
			if (bStatus) // OK
			{
				JOB_INFO_1* job_info = reinterpret_cast<JOB_INFO_1*>(pByteArray);
				wcsncpy_s(prd->pMachineName, STRVALUE_TEXT_256, job_info->pMachineName, _TRUNCATE);
				wcsncpy_s(prd->pUserName, STRVALUE_USER, job_info->pUserName, _TRUNCATE);
			}
			else // error
				ClosePrinter(prd->hPrinter);

			delete[] pByteArray;
		}
		else   // error
			ClosePrinter(prd->hPrinter);

		// --------------------------------------------------------------------------
		// if all is OK then - printer handle will be closed in "hfax_end_doc_port()"
		// --------------------------------------------------------------------------
		return bStatus;
	}

	return FALSE;
}
//***********************************************************
BOOL hfax_open_port(HANDLE hMonitor, LPWSTR pName, PHANDLE pHandle)
{
	if (!pName || !pHandle)
		return FALSE;
	// --------------------------------------------------------------------------
	// HeapFree will be called in "hfax_close_port()"
	// --------------------------------------------------------------------------
	h_portdata* prd = (h_portdata*)HeapAlloc(GetProcessHeap(), 0, static_cast<DWORD>(sizeof(h_portdata)));
#ifdef _DEBUG
	DebugWriteToFile(L"hfax_open_port - 1", L" ", pName);
#endif

	if (prd == (h_portdata*)NULL)
	{
		SetLastError((unsigned long)ERROR_NOT_ENOUGH_MEMORY);
		return FALSE;
	}
#ifdef _DEBUG
	DebugWriteToFile(L"hfax_open_port - 2", L" ", pName);
#endif

	FillMemory((PVOID)prd, sizeof(h_portdata), 0);
	reset_portdataHF(prd);
	wcsncpy_s(prd->portname, STRVALUE_TEXT_256, pName, _TRUNCATE);
	prd->hPort = (HANDLE)prd;
	prd->hMonitor = hMonitor;

	*pHandle = (HANDLE)prd; // for HeapFree prd
	return TRUE;
}
//***********************************************************
BOOL hfax_start_doc_port(h_portdata* prd, LPWSTR pPrinterName,
	DWORD JobId, DWORD Level, LPBYTE pDocInfo)
{
	BOOL flag;
	BOOL directory;
#ifdef _DEBUG
	DebugWriteToFile(L"hfax_start_doc_port - 1");
#endif

	if (prd == (h_portdata*)NULL)
	{
		SetLastError((DWORD)ERROR_INVALID_HANDLE);
		return FALSE;
	}
#ifdef _DEBUG
	DebugWriteToFile(L"hfax_start_doc_port - 2");
#endif

	reset_portdataHF(prd);

	wcsncpy_s(prd->pPrinterName, STRVALUE_TEXT_256, pPrinterName, _TRUNCATE);
	prd->JobId = JobId;
	/* remember document name, to be used for output job */
	if ((Level == 1) && pDocInfo)
	{
		DOC_INFO_1* dci1 = (DOC_INFO_1*)pDocInfo;
		wcsncpy_s(prd->pDocName, MAX_PATH, (WCHAR*)dci1->pDocName, MAX_PATH - 30);
	}
	else if ((Level == 2) && pDocInfo)
	{
		DOC_INFO_2* dci2 = (DOC_INFO_2*)pDocInfo;
		wcsncpy_s(prd->pDocName, MAX_PATH, (WCHAR*)dci2->pDocName, MAX_PATH - 30);
	}
	else if ((Level == 3) && pDocInfo)
	{
		DOC_INFO_3* dci3 = (DOC_INFO_3*)pDocInfo;
		wcsncpy_s(prd->pDocName, MAX_PATH, (WCHAR*)dci3->pDocName, MAX_PATH - 30);
	}
	else
		wcscpy_s(prd->pDocName, MAX_PATH, L"HylaFAX");

	/* Get user name and machine name from job info, printer handle */
	if (!get_job_info(prd))
		return FALSE;

	/* get configuration for this port */
	prd->config.dwSize = sizeof(prd->config);
	prd->config.dwVersion = static_cast<DWORD>(VERSION_NUMBER);
	if (!hm_get_config(prd->hMonitor, prd->portname, &prd->config))
	{
		SetLastError((DWORD)REGDB_E_KEYMISSING);
		return FALSE;	/* There are no ports */
	}

	flag = TRUE;	/* all is well */

	WCHAR* p;
	WCHAR szComputerName[256] = L""; // initialize array of '\0'
	DWORD dwNameLen = sizeof(szComputerName) / sizeof(WCHAR) - 1;

	p = prd->pMachineName;
	while (*p && (*p == '\\'))	/* skip leading backslashes */
		p++;

	GetComputerName(szComputerName, &dwNameLen);
	if (lstrcmpi(p, szComputerName) != 0)
	{
		/* The job was submitted from another computer. */
		/* Do not allow this. */
		SetLastError((DWORD)ERROR_ACCESS_DENIED);
		flag = FALSE;
	}

	/* Create file for spooling */
	WCHAR TempFilePath[MAX_PATH];
	WCHAR TempFileName[MAX_PATH];
	// temp path
	wcscpy_s(TempFilePath, _countof(TempFilePath), L"C:\\Temp");
	directory = CreateDirectory(TempFilePath, NULL);
	if (directory == ERROR_PATH_NOT_FOUND)
		return FALSE;

	if (!GetTempFileName(TempFilePath, L"FAX", 0, TempFileName))
	{
		return FALSE;
	}
	// CloseHandle() will be called in the func "hfax_end_doc_port"
	prd->hFile = CreateFile(TempFileName, GENERIC_READ | GENERIC_WRITE,
		0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (prd->hFile == INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}

	wcsncpy_s(prd->pFileName, MAX_PATH, TempFileName, _TRUNCATE);

	return flag;
}
//***********************************************************
BOOL hfax_write_port(h_portdata* prd, LPBYTE pBuffer,
	DWORD cbBuf, LPDWORD pcbWritten)
{
#ifdef _DEBUG
	DebugWriteToFile(L"hfax_write_port");
#endif

	EnterCriticalSection(&LcmSpoolerSection);
	*pcbWritten = 0;

	if (!WriteFile(prd->hFile, pBuffer, cbBuf, pcbWritten, NULL))
	{
		LeaveCriticalSection(&LcmSpoolerSection);
		return FALSE;
	}
	*pcbWritten = cbBuf;
	LeaveCriticalSection(&LcmSpoolerSection);
	return TRUE;
}

//***********************************************************
BOOL hfax_end_doc_port(h_portdata* prd)
{
	FlushFileBuffers(prd->hFile);
	// close handle
	// --------------------------------------------------------------------------
	CloseHandle(prd->hFile); // from "hfax_start_doc_port"
	// --------------------------------------------------------------------------
#ifdef _DEBUG
	DebugWriteToFile(L"hfax_end_doc_port");
#endif

	FAXSend = nullptr;
	FAXSend = (struct TFAXSend*)malloc(sizeof(struct TFAXSend));

	if (FAXSend == NULL) // no memory available
	{
		SetJob(prd->hPrinter, prd->JobId, 0, NULL, JOB_CONTROL_DELETE);
		ClosePrinter(prd->hPrinter); // from "hfax_start_doc_port -> get_job_info"
		DeleteFile(prd->pFileName);
		return FALSE;
	}

	ResetFaxSend();
	TransferSendData(FAXSend, prd); // set transfer only default data - single users data -> load in GUI

	SetJob(prd->hPrinter, prd->JobId, 0, NULL, JOB_CONTROL_SENT_TO_PRINTER);
	//SetJob(prd->hPrinter, prd->JobId, 0, NULL, JOB_CONTROL_DELETE);

	// close handle
	// --------------------------------------------------------------------------
	ClosePrinter(prd->hPrinter); // from "hfax_start_doc_port -> get_job_info"
	// --------------------------------------------------------------------------

	// Dialog FAX send as another process
	if (!PipeServer()) // if fails
		DeleteFile(prd->pFileName);

	free(FAXSend);
	FAXSend = nullptr; // global variable, set to null
	return TRUE;
}
//***********************************************************
BOOL hfax_close_port(HANDLE hPort)
{
	h_portdata* prd = (h_portdata*)hPort;
#ifdef _DEBUG
	if (prd)
		DebugWriteToFile(L"hfax_close_port - prd OK ", prd->portname);
	else
		DebugWriteToFile(L"hfax_close_port - ERROR prd");
#endif
	/* assume files were all closed in EndDocPort() */
	if (prd)
		HeapFree(GetProcessHeap(), 0, prd); //free memory from "hfax_open_port()"

	return TRUE;
}

//***********************************************************
// set data to transfer to GUI
void TransferSendData(struct TFAXSend* fs, h_portdata* prd)
{
	// for all users
	// special
	wcsncpy_s(fs->szDocument, MAX_PATH, prd->pDocName, _TRUNCATE);
	wcsncpy_s(fs->szFileName, MAX_PATH, prd->pFileName, _TRUNCATE);
	// settings (sheet 1)
	wcsncpy_s(fs->szServer, STRVALUE_SERVER, prd->config.szServer, _TRUNCATE);
	fs->dwPassiveIPIgnore = prd->config.dwPassiveIPIgnore;
	wcsncpy_s(fs->szHylafaxPort, STRVALUE_PORTNUMBER, prd->config.szHFPortNumber, _TRUNCATE);
	wcsncpy_s(fs->szUser, STRVALUE_USER, prd->config.szUsername, _TRUNCATE);
	wcsncpy_s(fs->szPasswd, STRVALUE_PASSWORD, prd->config.szPassword, _TRUNCATE);
	wcsncpy_s(fs->szEmail, STRVALUE_EMAIL, prd->config.szDefaultNotifyEmail, _TRUNCATE);
	wcsncpy_s(fs->szAddrBookDir, MAX_PATH, prd->config.szAddressBook, _TRUNCATE);
	wcsncpy_s(fs->szModem, STRVALUE_MODEM, prd->config.szModem, _TRUNCATE);
	wcsncpy_s(fs->szPageSize, STRVALUE_PAGESIZE, prd->config.szPageSize, _TRUNCATE);
	wcsncpy_s(fs->szResolution, STRVALUE_RESOLUTION, prd->config.szResolution, _TRUNCATE);
	wcsncpy_s(fs->szNotification, STRVALUE_NOTIFICATION, prd->config.szNotificationType, _TRUNCATE);
	wcsncpy_s(fs->szMaxDials, STRVALUE_MAXTIMES_NUMBER, prd->config.szMaxDials, _TRUNCATE);
	wcsncpy_s(fs->szMaxTries, STRVALUE_MAXTIMES_NUMBER, prd->config.szMaxTries, _TRUNCATE);
	wcsncpy_s(fs->szInfoDialogTimer, STRVALUE_INFODIALOG_TIMER_NUMBER, prd->config.szInfoDialogTimer, _TRUNCATE);
	fs->dwFaxNumberFromClipboard = prd->config.dwFaxNumberFromClipboard;
	// active mode (sheet 2)
	wcsncpy_s(fs->szActivePortNumber, STRVALUE_PORTNUMBER, prd->config.szActivePortNumber, _TRUNCATE);
	fs->dwUseActiveMode = prd->config.dwUseActiveMode;
	// LDAP (sheet 3)
	fs->dwUseLDAP = prd->config.dwUseLDAP;
	fs->dwLDAP_SSL = prd->config.dwLDAP_SSL;
	fs->dwLDAP_StartTLS = prd->config.dwLDAP_STARTTLS;
	wcsncpy_s(fs->szLDAPServer, STRVALUE_SERVER, prd->config.szLDAPServer, _TRUNCATE);
	wcsncpy_s(fs->szLDAPPort, STRVALUE_PORTNUMBER, prd->config.szLDAPPort, _TRUNCATE);
	wcsncpy_s(fs->szLDAPBindDN, STRVALUE_LDAP_BIND_DN, prd->config.szLDAPBindDN, _TRUNCATE);
	wcsncpy_s(fs->szLDAPPassword, STRVALUE_PASSWORD, prd->config.szLDAPPassword, _TRUNCATE);
	wcsncpy_s(fs->szLDAPBaseDN, STRVALUE_LDAP_BASE_DN, prd->config.szLDAPBaseDN, _TRUNCATE);
	wcsncpy_s(fs->szLDAPFilter, STRVALUE_LDAP_FILTER, prd->config.szLDAPFilter, _TRUNCATE);
	wcsncpy_s(fs->szLDAPAutocompleteAttribute, STRVALUE_LDAP_AUTOCOMPLETE_ATT, prd->config.szLDAPAutocompleteAttribute, _TRUNCATE);

}
//***********************************************************
