//---------------------------------------------------------------------------
#ifndef AddPortH
#define AddPortH
//---------------------------------------------------------------------------
#include "resource.h"
#include "center_window.h"

INT_PTR CALLBACK PortProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
//---------------------------------------------------------------------------
class TAddPortForm
{
private:	// User declarations
	BOOL returnValue;
	WCHAR portName[64];
public:		// User declarations
	TAddPortForm();
	BOOL GetReturnValue() const;
	void OKClick(HWND hdlg);
	void CancelClick();
	void SetReturnValue(BOOL bValue) { returnValue = bValue; }
	void SetPortName(LPCWSTR p_name);
	LPWSTR GetPortName();
	void CallUpDialog(HINSTANCE hInst, HWND hwnd);
};
//---------------------------------------------------------------------------
extern TAddPortForm* AddPortForm;
//---------------------------------------------------------------------------
#endif
