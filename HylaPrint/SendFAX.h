//---------------------------------------------------------------------------
#ifndef SendFAXH
#define SendFAXH

// Windows Header Files:
#include <windows.h>
#include "constants.h"
//---------------------------------------------------------------------------

void ResetFaxSend();

struct TFAXSend
{
	// misc
	WCHAR szFileName[MAX_PATH]; // file name of the ps file
	WCHAR szDocument[MAX_PATH]; // printed document name
	// settings all
	WCHAR szServer[STRVALUE_SERVER];
	WCHAR szHylafaxPort[STRVALUE_PORTNUMBER];
	WCHAR szActivePortNumber[STRVALUE_PORTNUMBER];
	DWORD dwPassiveIPIgnore;
	// default settings or user settings
	WCHAR szUser[STRVALUE_USER];
	WCHAR szPasswd[STRVALUE_PASSWORD];
	WCHAR szEmail[STRVALUE_EMAIL];
	WCHAR szAddrBookDir[MAX_PATH];
	WCHAR szModem[STRVALUE_MODEM];
	WCHAR szPageSize[STRVALUE_PAGESIZE];
	WCHAR szResolution[STRVALUE_RESOLUTION];
	WCHAR szNotification[STRVALUE_NOTIFICATION];
	WCHAR szMaxDials[STRVALUE_MAXTIMES_NUMBER];
	WCHAR szMaxTries[STRVALUE_MAXTIMES_NUMBER];
	WCHAR szInfoDialogTimer[STRVALUE_INFODIALOG_TIMER_NUMBER];
	DWORD dwFaxNumberFromClipboard;
	// active mode
	DWORD dwUseActiveMode;
	// LDAP
	DWORD dwUseLDAP; // use LDAP server
	DWORD dwLDAP_SSL; // LDAP SSL
	DWORD dwLDAP_StartTLS; // LDAP StartTLS
	WCHAR szLDAPServer[STRVALUE_SERVER]; // LDAP server
	WCHAR szLDAPPort[STRVALUE_PORTNUMBER]; // LDAP port number
	WCHAR szLDAPBindDN[STRVALUE_LDAP_BIND_DN]; // LDAP Bind DN (username)
	WCHAR szLDAPPassword[STRVALUE_PASSWORD]; // LDAP password
	WCHAR szLDAPBaseDN[STRVALUE_LDAP_BASE_DN]; // LDAP base DN (search)
	WCHAR szLDAPFilter[STRVALUE_LDAP_FILTER]; // LDAP filter
	WCHAR szLDAPAutocompleteAttribute[STRVALUE_LDAP_AUTOCOMPLETE_ATT]; // search autocomplete attribute
};
//---------------------------------------------------------------------------
extern struct TFAXSend* FAXSend;
//---------------------------------------------------------------------------
#endif
