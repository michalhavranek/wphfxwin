#pragma once
// DEBUG only
// ************************************************************************************************
// DebugWriteToFile(str1), DebugWriteToFile(str1,str2,integer....)
#ifdef _DEBUG
#include <fstream>
#include <iostream>
template <class... Args>
void DebugWriteToFile(Args... args)
{
	wchar_t timeString[200];
	time_t t = time(NULL);
	struct tm* p = localtime(&t);
	wcsftime(timeString, 200, L"%d.%m.%Y %H:%M:%S | ", p);

	std::wofstream  ofs;
	ofs.open("C:\\Temp\\debug_winprint.txt", std::ofstream::out | std::ofstream::app);
	ofs << timeString;
	(ofs << ... << args) << "\n";
	ofs.close();
}
#endif
// ************************************************************************************************
// ************************************************************************************************
