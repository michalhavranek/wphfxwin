#ifndef constantsH
#define constantsH

#define STRVALUE_TEXT_64 64
#define STRVALUE_TEXT_128 128
#define STRVALUE_TEXT_256 256
#define STRVALUE_TEXT_512 512
#define STRVALUE_SERVER 128
#define STRVALUE_EMAIL 128
#define STRVALUE_MEGABUFLEN 1024
#define STRVALUE_USER 128
#define STRVALUE_PASSWORD 64
#define STRVALUE_IPADDRESS 128
#define STRVALUE_PORTNUMBER 16
#define STRVALUE_MAXTIMES_NUMBER 4
#define STRVALUE_INFODIALOG_TIMER_NUMBER 4
#define STRVALUE_MODEM 64
#define STRVALUE_PAGESIZE 16
#define STRVALUE_RESOLUTION 32
#define STRVALUE_NOTIFICATION 32
#define MAXSHORTSTR 64
#define STRVALUE_NUMBER 64
#define STRVALUE_ADDRBOOK_NAME 128
#define STRVALUE_FAXNUMBER STRVALUE_ADDRBOOK_NAME+STRVALUE_NUMBER+10
#define STRVALUE_LDAP_BIND_DN 128
#define STRVALUE_LDAP_BASE_DN 128
#define STRVALUE_LDAP_FILTER 256
#define STRVALUE_LDAP_AUTOCOMPLETE_ATT 16
#define SOCKET_TIMEOUT 60 // socket timeout in seconds
#define STRVALUE_IDENTIFIER 16
// address book = system value MAX_PATH
#endif