// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the HYLAPRINT_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// HYLAPRINT_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef HYLAPRINT_EXPORTS
#define HYLAPRINT_API __declspec(dllexport)
#else
#define HYLAPRINT_API __declspec(dllimport)
#endif

