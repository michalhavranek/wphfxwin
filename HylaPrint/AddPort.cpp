//---------------------------------------------------------------------------
#include "stdafx.h"
#include "AddPort.h"
//---------------------------------------------------------------------------
TAddPortForm* AddPortForm;
//---------------------------------------------------------------------------
TAddPortForm::TAddPortForm()
{
	returnValue = FALSE;
	portName[0] = L'\0';
}

void TAddPortForm::OKClick(HWND hdlg)
{
	returnValue = TRUE;
	GetWindowText(GetDlgItem(hdlg, IDC_EDIT_PORTNAME), portName, 63);
}
//---------------------------------------------------------------------------

void TAddPortForm::CancelClick()
{
	returnValue = FALSE;
}
//---------------------------------------------------------------------------

BOOL TAddPortForm::GetReturnValue()const
{
	return returnValue;
}

void TAddPortForm::SetPortName(LPCWSTR p_name)
{
	wcsncpy_s(portName, _countof(portName), p_name, _TRUNCATE);
}

LPWSTR TAddPortForm::GetPortName()
{
	return portName;
}

void TAddPortForm::CallUpDialog(HINSTANCE hInst, HWND hwnd)
{
	DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG_PORTNAME), hwnd, PortProc);
}
// Message handler for port.
INT_PTR CALLBACK PortProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		SetWindowText(GetDlgItem(hDlg, IDC_EDIT_PORTNAME), AddPortForm->GetPortName());
		CenterDialogWindow(hDlg);
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		switch LOWORD(wParam)
		{
		case ID_PORT_OK:
		{
			AddPortForm->OKClick(hDlg);
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}

		case ID_PORT_CANCEL:
		{
			AddPortForm->CancelClick();
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		case IDCANCEL:
		{
			AddPortForm->CancelClick();
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		}
	}
	return (INT_PTR)FALSE;
}

//********************************************************
//********************************************************
