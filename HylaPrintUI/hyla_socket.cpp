#include "stdafx.h"
#include "hyla_socket.h"
#include "../HylaPrint/info_message.h"
#include "../HylaPrint/convert_string.h"
#include "../HylaPrint/SendFAX.h"
#include "HylaPrintUI.h"
#include "sockets_dialog.h"
#include "../HylaPrint/system_message.h"
#include "../HylaPrint/config_class.h"

// send command to hylaFAX server
int cCommunication::SendCmd(const CHAR* cmd, UINT okstat, SOCKET* Socket)
{
	UINT ftp_status;
	size_t iSendBytes = strlen(cmd);

	// send bytes
	FD_ZERO(&FDs);
	FD_SET(*Socket, &FDs);
	pcStatus->resetProgress();
	iResult = select(0, NULL, &FDs, NULL, &tv); // write timeout, waiting for event - Socket send ready
	if (iResult > 0) // ok
	{
		iResult = send(*Socket, cmd, (int)iSendBytes, 0);
		if (iResult != (int)iSendBytes)
		{
			dwErrorID = WSAGetLastError();
			AppendTextToBox(_T(LNG_CANT_SEND_FAX));
			ShowErrorMessage(dwErrorID);
			return 1; // error
		}
	}
	else // SOCKET_ERROR or time limit expired
	{
		dwErrorID = WSAGetLastError();
		AppendTextToBox(_T(LNG_CANT_SEND_FAX));
		ShowErrorMessage(dwErrorID);
		return 1;
	}

	// receive bytes
	FD_ZERO(&FDs);
	FD_SET(*Socket, &FDs);
	pcStatus->resetProgress();
	iResult = select(0, &FDs, NULL, NULL, &tv); // receive timeout, waiting for event - Socket recv ready
	if (iResult > 0) // ok
	{
		iResult = recv(*Socket, szResponse, STRVALUE_TEXT_256, 0);
		if (iResult == 0 || iResult == SOCKET_ERROR || iResult > (STRVALUE_TEXT_256 - 1)) // error
		{
			dwErrorID = WSAGetLastError();
			AppendTextToBox(_T(LNG_CANT_SEND_FAX));
			ShowErrorMessage(dwErrorID);
			return 1; // error
		}
		else // ok
			szResponse[iResult] = '\0';
	}
	else // SOCKET_ERROR or time limit expired
	{
		dwErrorID = WSAGetLastError();
		AppendTextToBox(_T(LNG_CANT_SEND_FAX));
		ShowErrorMessage(dwErrorID);
		return 1;
	}

	ftp_status = atoi(szResponse);
	AnsiToUnicode(szResponse, szUnicode, _countof(szUnicode));
	AppendTextToBox(szUnicode);

	if (ftp_status == okstat)
		return 0;
	else
		return ftp_status;
}

// ***************************************************************************
BOOL SendToHylafax()
{
	WSADATA wsaData;
	int iResult;
	CHAR szAnsi[STRVALUE_TEXT_256];
	TTcpClient* HFAXSocket = nullptr, * HFAXData = nullptr;
	BOOL bRet = TRUE;
	HANDLE hThread = NULL;
	unsigned dwThreadId;
	DWORD dwErrorID = 0;

	// not FAX number
	if (lstrlen(HylaUI->GetFAXNumberText()) == 0)
	{
		SendInfoMessage(_T(LNG_NO_FAX_NUMBER), _T(LNG_INFO_DIALOG), MB_OK | MB_ICONWARNING);
		return FALSE;
	}

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0)
	{
		// Initialize Winsock - Error
		SendInfoMessage(_T(LNG_INIT_WINSOCK_ERROR), _T(LNG_INFO_DIALOG), MB_OK | MB_ICONWARNING);
		return FALSE;
	}
	// event for create socket dialog
	hEvent = CreateEvent(NULL, //no security attributes
		FALSE, //auto-reset event object
		FALSE, //initial state is nonsignaled
		L"SocketDialogHFX"); //object name
	if (!hEvent) // fail
		return FALSE;

	// Create a thread for this client - status of transmitting document
	hThread = (HANDLE)_beginthreadex(NULL, // security
		0, // default Stack size
		&SocketsDialogThread, // thread proc
		NULL,     // Argument list
		0, // Initial state not suspend
		&dwThreadId);  // thread identifier

	// wait until socket dialog is created
	WaitForSingleObject(hEvent, 10000);
	CloseHandle(hEvent);

	HFAXSocket = new TTcpClient();
	// comunication class
	cCommunication communication;

	// IP address
	UnicodeToAnsi(FAXSend->szServer, szAnsi, _countof(szAnsi));
	HFAXSocket->SetRemoteHost(szAnsi);
	UnicodeToAnsi(FAXSend->szHylafaxPort, szAnsi, _countof(szAnsi));
	HFAXSocket->SetRemotePort(static_cast<u_short>(atoi(szAnsi)));

	// send connect command to Hylafax server
	if (!HFAXSocket->CreateSocket(&dwErrorID))
	{
		// No connection to hylafax server
		//SendInfoMessage(_T(LNG_NO_CONNECTION_HYLAFAX), MB_OK|MB_ICONWARNING, HylaUI->GetSessionID());
		pcStatus->setServerStatus(RED);
		SetWindowText(GetDlgItem(gHwndDialogSock, IDC_EDIT_SOCKET), _T(LNG_NO_CONNECTION_HYLAFAX));
		ShowErrorMessage(dwErrorID);
		bRet = FALSE;
		goto Cleanup;
	}
	// wait to response from Hylafax server
	if (!communication.Connect(HFAXSocket->GetSocketPtr()))
	{
		pcStatus->setServerStatus(RED);
		bRet = FALSE;
		goto Cleanup;
	}

	// LED status - connect OK
	pcStatus->setServerStatus(GREEN);
	pcStatus->setUserStatus(BLINK);
	pcStatus->resetProgress();

	// send user and password
	if (!communication.UserPassword(HFAXSocket->GetSocketPtr()))
	{
		pcStatus->setUserStatus(RED);
		bRet = FALSE;
		goto Cleanup;
	}

	// LED status - username/password OK
	pcStatus->setUserStatus(GREEN);
	pcStatus->setSocketStatus(BLINK);

	// set PS format
	if (!communication.SetPSFormat(HFAXSocket->GetSocketPtr()))
	{
		pcStatus->setSocketStatus(RED); // red
		bRet = FALSE;
		goto Cleanup;
	}
	// TYPE I
	if (!communication.TypeSetImage(HFAXSocket->GetSocketPtr()))
	{
		pcStatus->setSocketStatus(RED); // red
		bRet = FALSE;
		goto Cleanup;
	}
	// MODE S
	if (!communication.ModeStream(HFAXSocket->GetSocketPtr()))
	{
		pcStatus->setSocketStatus(RED); // red
		bRet = FALSE;
		goto Cleanup;
	}
	// Active/Passive data connection
	// socket for data connection
	HFAXData = new TTcpClient();

	// PASSIVE mode
	if (FAXSend->dwUseActiveMode == 0)
		bRet = communication.SetPassiveMode(HFAXSocket, HFAXData);
	// ACTIVE CONNECTION
	else
	{
		HFAXData->SetIPClient(HFAXSocket->GetIPClient()); // set local socket IP  - because of more interfaces
		HFAXData->SetAIFamily(HFAXSocket->GetAIFamily()); // set IP version (IPv4 or IPv6) for sending active port (PORT or EPRT)
		bRet = communication.SetActiveMode(HFAXSocket->GetSocketPtr(), HFAXData);
	}

	if (bRet == FALSE)
	{
		pcStatus->setSocketStatus(RED); // red
		goto Cleanup;
	}
	// LED sockets
	pcStatus->setSocketStatus(GREEN);
	pcStatus->setTransferStatus(BLINK);

	// send postscript file, close socket HFAXData
	if (!communication.LoadFileSendFile(FAXSend->szFileName, HFAXData->GetSocketPtr()))
	{
		pcStatus->setTransferStatus(RED); // red
		bRet = FALSE;
		goto Cleanup;
	}
	// close data socket to get transfer response
	HFAXData->CloseSocket();
	delete HFAXData; // disconnect data connection
	HFAXData = nullptr; // must be null because of goto Cleanup

	// get transfer response from loadfilesendfile, set remote name of ps file
	if (!communication.GetTransferResponse(HFAXSocket->GetSocketPtr()))
	{
		pcStatus->setTransferStatus(RED); // red
		bRet = FALSE;
		goto Cleanup;
	}
	// create new job - JNEW
	if (!communication.NewJob(HFAXSocket->GetSocketPtr()))
	{
		pcStatus->setTransferStatus(RED); // red
		bRet = FALSE;
		goto Cleanup;
	}
	// FROMUSER
	if (!communication.FromUser(HFAXSocket->GetSocketPtr()))
	{
		pcStatus->setTransferStatus(RED); // red
		bRet = FALSE;
		goto Cleanup;
	}
	// LASTTIME 000259
	if (!communication.LastTime(HFAXSocket->GetSocketPtr()))
	{
		pcStatus->setTransferStatus(RED); // red
		bRet = FALSE;
		goto Cleanup;
	}
	// maxdials
	if (!communication.SetMaxDials(HFAXSocket->GetSocketPtr()))
	{
		pcStatus->setTransferStatus(RED); // red
		bRet = FALSE;
		goto Cleanup;
	}
	// maxtries
	if (!communication.SetMaxTries(HFAXSocket->GetSocketPtr()))
	{
		pcStatus->setTransferStatus(RED); // red
		bRet = FALSE;
		goto Cleanup;
	}
	// faxnumber
	if (!communication.DialString(HFAXSocket->GetSocketPtr()))
	{
		pcStatus->setTransferStatus(RED); // red
		bRet = FALSE;
		goto Cleanup;
	}
	// notification type and email - NOTIFYADDR
	if (!communication.SetNotification(HFAXSocket->GetSocketPtr()))
	{
		pcStatus->setTransferStatus(RED); // red
		bRet = FALSE;
		goto Cleanup;
	}
	// jobinfo
	if (!communication.SetJobInfo(HFAXSocket->GetSocketPtr()))
	{
		pcStatus->setTransferStatus(RED); // red
		bRet = FALSE;
		goto Cleanup;
	}
	// fax resolution
	if (!communication.SetResolution(HFAXSocket->GetSocketPtr()))
	{
		pcStatus->setTransferStatus(RED); // red
		bRet = FALSE;
		goto Cleanup;
	}
	// fax pagesize
	if (!communication.SetPageSize(HFAXSocket->GetSocketPtr()))
	{
		pcStatus->setTransferStatus(RED); // red
		bRet = FALSE;
		goto Cleanup;
	}
	// modem group (if not empty string)
	if (!communication.SetModem(HFAXSocket->GetSocketPtr()))
	{
		pcStatus->setTransferStatus(RED); // red
		bRet = FALSE;
		goto Cleanup;
	}
	// set name of ps file
	if (!communication.SetDocument(HFAXSocket->GetSocketPtr()))
	{
		pcStatus->setTransferStatus(RED); // red
		bRet = FALSE;
		goto Cleanup;
	}
	// submit job
	if (!communication.JobSubmit(HFAXSocket->GetSocketPtr()))
	{
		pcStatus->setTransferStatus(RED); // red
		bRet = FALSE;
		goto Cleanup;
	}
	pcStatus->setTransferStatus(GREEN); // green

	HFAXSocket->CloseSocket();
	delete HFAXSocket;
	HFAXSocket = nullptr; // must be null because of goto Cleanup

	// clean data
	//*****************************************************
Cleanup:
	if (HFAXSocket != nullptr)
	{
		HFAXSocket->CloseSocket();
		delete HFAXSocket;
		HFAXSocket = nullptr;
	}
	if (HFAXData != nullptr)
	{
		HFAXData->CloseSocket();
		delete HFAXData;
		HFAXData = nullptr;
	}

	WSACleanup();

	pcStatus->setStatus(bRet); // OK button or Cancel button
	PostMessage(gHwndDialogSock, START_COUNTDOWN, 0, 0);
	if (hThread)
	{
		WaitForSingleObject(hThread, 120000); // 2min
		CloseHandle(hThread);
	}

	return bRet;
}
//***********************************************************
// constructor
TTcpClient::TTcpClient()
{
	ConnectSocket = INVALID_SOCKET;
	clientIP[0] = '\0';
	RemoteHost[0] = '\0';
	RemotePort = 0;
	ai_family = AF_INET;
}
// destructor
TTcpClient::~TTcpClient()
{
}
//***********************************************************
void TTcpClient::SetSocket(SOCKET socket)
{
	ConnectSocket = socket;
}
//***********************************************************
BOOL TTcpClient::CreateSocket(LPDWORD dwErr)
{
	int iResult;
	struct addrinfo hints;
	struct addrinfo* result = NULL, * ptr = NULL;
	struct sockaddr_in6 sin6;
	struct sockaddr_in sin;
	CHAR convBuf[STRVALUE_PORTNUMBER];

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	_itoa_s(RemotePort, convBuf, STRVALUE_PORTNUMBER, 10);

	// Resolve the server address and port
	iResult = getaddrinfo(RemoteHost, convBuf, &hints, &result);
	if (iResult != 0)
	{
		*dwErr = WSAGetLastError();
		return FALSE;
	}
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next)
	{
		// Create a SOCKET for connecting to server
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET)
		{
			*dwErr = WSAGetLastError();
			return FALSE;
		}

		// Connect to server.
		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR)
		{
			*dwErr = WSAGetLastError();
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}
#ifdef _DEBUG
		DebugWriteToFile(L"ai_family: ", ptr->ai_family);
#endif
		ai_family = ptr->ai_family; // IPv4 or IPv6
		break;
	}
	freeaddrinfo(result);
	if (ConnectSocket == INVALID_SOCKET)
		return FALSE;

	// get local IP of connected socket
	if (ai_family == AF_INET6) // Ipv6
	{
		ZeroMemory(&sin6, sizeof(sin6));
		int len = sizeof(sin6);
		getsockname(ConnectSocket, (struct sockaddr*)&sin6, &len);
		inet_ntop(AF_INET6, &sin6.sin6_addr, clientIP, _countof(clientIP));
	}
	else // IPv4
	{
		ZeroMemory(&sin, sizeof(sin));
		int len = sizeof(sin);
		getsockname(ConnectSocket, (struct sockaddr*)&sin, &len);
		inet_ntop(AF_INET, &sin.sin_addr, clientIP, _countof(clientIP));
	}
#ifdef _DEBUG
	DebugWriteToFile(L"local IP: ", clientIP);
#endif

	return TRUE;
}
//***********************************************************
BOOL TTcpClient::ListenSocket(PVOID comm, SOCKET* sock, LPDWORD dwErr)
{
	size_t p;
	int p1, p2;
	UINT a1, a2, a3, a4;
	SOCKADDR_IN sin;
	SOCKADDR_IN6 sin6;
	CHAR cmd[128];
	DWORD inaddr;
	SOCKET ListenSocket = INVALID_SOCKET, ClientSocket = INVALID_SOCKET;
	int Status;
	int iResult;

	// Create a listen SOCKET for 
	// incoming connection requests
	if (ai_family == AF_INET6) // IPv6
	{
		ListenSocket = socket(AF_INET6, SOCK_STREAM, IPPROTO_TCP);
		ZeroMemory(&sin6, sizeof(sin6));
		sin6.sin6_family = AF_INET6;
		sin6.sin6_port = htons(RemotePort);
		inet_pton(AF_INET6, clientIP, (void*)&sin6.sin6_addr.s6_addr);
	}
	else // IPv4
	{
		ListenSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		ZeroMemory(&sin, sizeof(sin));
		sin.sin_family = AF_INET;
		sin.sin_port = htons(RemotePort);
		inet_pton(AF_INET, clientIP, &sin.sin_addr.s_addr);
	}

	if (ListenSocket == INVALID_SOCKET)
	{
		*dwErr = WSAGetLastError();
		return FALSE;
	}

	//----------------------
	// Bind the socket.
	if (ai_family == AF_INET6) // IPv6
		iResult = bind(ListenSocket, (sockaddr*)&sin6, sizeof(sin6));
	else // IPv4
		iResult = bind(ListenSocket, (sockaddr*)&sin, sizeof(sin));

	if (iResult == SOCKET_ERROR)
	{
		*dwErr = WSAGetLastError();
		closesocket(ListenSocket);
		return FALSE;
	}

	iResult = listen(ListenSocket, 2);
	if (iResult == SOCKET_ERROR)
	{
		*dwErr = WSAGetLastError();
		closesocket(ListenSocket);
		return FALSE;
	}

	if (ai_family == AF_INET6) // IPv6 command: EPRT |2|::1|49163|
	{
		sprintf(cmd, "EPRT |2|%s|%d|\n", clientIP, RemotePort);
	}
	else// IPv4 - command: PORT 192,168,201,121,125,1
	{
		p = RemotePort;
		p2 = p & 0xFF;
		p >>= 8;
		p1 = p & 0xFF;

		inaddr = inet_addr(clientIP);
		a1 = inaddr & 0xFF;
		inaddr >>= 8;
		a2 = inaddr & 0xFF;
		inaddr >>= 8;
		a3 = inaddr & 0xFF;
		inaddr >>= 8;
		a4 = inaddr & 0xFF;

		sprintf(cmd, "PORT %u,%u,%u,%u,%d,%d\n", a1, a2, a3, a4, p1, p2);
	}

	// send PORT command
	Status = ((cCommunication*)comm)->SendCmd(cmd, 200, sock);
	if (Status) // error
	{
		AppendTextToBox(L"\r\n");
		closesocket(ListenSocket);
		return FALSE;
	}
	// store a temporary file with a unique name
	Status = ((cCommunication*)comm)->SendCmd("STOT\n", 150, sock);
	if (Status) // error
	{
		AppendTextToBox(L"\r\n");
		closesocket(ListenSocket);
		return FALSE;
	}
	// accept client socket
	ClientSocket = accept(ListenSocket, NULL, NULL);
	if (ClientSocket == INVALID_SOCKET)
	{
		*dwErr = WSAGetLastError();
		closesocket(ListenSocket);
		return FALSE;
	}
	closesocket(ListenSocket);

	ConnectSocket = ClientSocket;

	return TRUE;
}
//***********************************************************
// detail error message
void ShowErrorMessage(DWORD dwID)
{
	LPWSTR szErrorText = NULL;
	WCHAR szErrorNum[32];
	HWND hTextBox;
	if (dwID == 0)
		return;
	if (FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, dwID, 0, (LPWSTR)&szErrorText, 0, NULL) == 0)
		return;
	StringCchPrintf(szErrorNum, 32, L"\r\n- Error %d -\r\n", dwID);
	AppendTextToBox(szErrorNum);
	AppendTextToBox(szErrorText);

	if (szErrorText)
		LocalFree(szErrorText);

	// scroll Editbox up
	hTextBox = GetDlgItem(gHwndDialogSock, IDC_EDIT_SOCKET);
	SendMessage(hTextBox, EM_SETSEL, 0, 0);
	SendMessage(hTextBox, EM_SCROLLCARET, NULL, NULL);
}
//***********************************************************
// communication with Hylafax
cCommunication::cCommunication()
{
	dwErrorID = 0;
	memset(szStatus, 0, 4);
	tv = { 0 };
	FDs = { 0 };
	tv.tv_sec = SOCKET_TIMEOUT; // timeout in seconds
}
cCommunication::~cCommunication()
{
}
//***********************************************************
// connect to server
BOOL cCommunication::Connect(SOCKET* socket)
{
	int LineCode = 130;
	BOOL bRet = TRUE;

	FD_ZERO(&FDs);
	FD_SET(*socket, &FDs);
	bRet = TRUE;
	do
	{
		pcStatus->resetProgress();
		iResult = select(0, &FDs, NULL, NULL, &tv); // receive timeout
		if (iResult > 0) // ok
		{
			iResult = recv(*socket, szResponse, recvbuflen, 0);
			if (iResult > 0) // ok
			{
				szResponse[iResult] = '\0';
				strncpy_s(szStatus, _countof(szStatus), szResponse, _countof(szStatus) - 1);
				LineCode = atoi(szStatus);
				// example: 220 myhost.mycompany.com server (HylaFAX (tm) 4.2.3) ready.
				AnsiToUnicode(szResponse, szUnicode, _countof(szUnicode));
				SetWindowText(GetDlgItem(gHwndDialogSock, IDC_EDIT_SOCKET), szUnicode);
			}
		}
		else // error or timeout
		{
			dwErrorID = WSAGetLastError();
			SetWindowText(GetDlgItem(gHwndDialogSock, IDC_EDIT_SOCKET), _T(LNG_NO_CONNECTION_HYLAFAX));
			ShowErrorMessage(dwErrorID);
			bRet = FALSE;
			break;
		}
	} while (LineCode == 130);

	return bRet;
}
//***********************************************************
// user and password
BOOL cCommunication::UserPassword(SOCKET* socket)
{
	UnicodeToAnsi(FAXSend->szUser, szAnsi, _countof(szAnsi));
	sprintf_s(szCommand, _countof(szCommand), "USER %s\n", szAnsi);
	Status = SendCmd(szCommand, 230, socket); //User logged in
	if (Status == 331) // User name okay, need password
	{
		UnicodeToAnsi(FAXSend->szPasswd, szAnsi, _countof(szAnsi));
		sprintf_s(szCommand, _countof(szCommand), "PASS %s\n", szAnsi);
		Status = SendCmd(szCommand, 230, socket);
		// wrong password
		if (Status)
		{
			// HylaFAX Error - wrong User or Password
			if (Status != 1) // no socket error
				SetWindowText(GetDlgItem(gHwndDialogSock, IDC_EDIT_SOCKET), _T(LNG_HYLAFAX_WRONG_USER_PASS));
			return FALSE;
		} // if (Status)
	}
	else
	{
		if (Status) // != 331 and != 230, then --> wrong user
		{
			// SendInfoMessage(_T(LNG_HYLAFAX_ERROR), MB_OK|MB_ICONWARNING, HylaUI->GetSessionID());
			if (Status != 1) // no socket error
				SetWindowText(GetDlgItem(gHwndDialogSock, IDC_EDIT_SOCKET), _T(LNG_HYLAFAX_WRONG_USER));
			return FALSE;
		}
	}
	return TRUE;
}
//***********************************************************
// set format to postscript 
BOOL cCommunication::SetPSFormat(SOCKET* socket)
{
	Status = SendCmd("FORM PS\n", 200, socket);
	if (Status) // error
	{
		if (Status != 1) // no socket error
			AppendTextToBox(_T(LNG_CANT_SEND_FAX));
		return FALSE;
	}
	return TRUE;
}
//***********************************************************
// set image 
BOOL cCommunication::TypeSetImage(SOCKET* socket)
{
	Status = SendCmd("TYPE I\n", 200, socket);
	if (Status) // error
	{
		if (Status != 1) // no socket error
			AppendTextToBox(_T(LNG_CANT_SEND_FAX));
		return FALSE;
	}
	return TRUE;
}
//***********************************************************
// set transfer mode
BOOL cCommunication::ModeStream(SOCKET* socket)
{
	Status = SendCmd("MODE S\n", 200, socket);
	if (Status) // error
	{
		if (Status != 1) // no socket error
			AppendTextToBox(_T(LNG_CANT_SEND_FAX));
		return FALSE;
	}
	return TRUE;
}
//***********************************************************
// passive mode ftp
BOOL cCommunication::SetPassiveMode(TTcpClient* socket, TTcpClient* data)
{
	int remote_ip_v4[4]{};
	int conect_port_v4[2]{};
	CHAR ip_char_v4[40];
	size_t port_dec;

	if (socket->GetAIFamily() != AF_INET6) // IPv4 passive mode - PASV command
	{
		Status = SendCmd("PASV\n", 227, socket->GetSocketPtr());
		// example: 227 Entering passive mode (10,3,0,127,152,95)
		if (Status) // error
		{
			if (Status != 1) // no socket error
				AppendTextToBox(_T(LNG_ERROR_SOCKET_FOR_PASSIVEDATA));
			return FALSE;
		}
		// ip address and port example: 192,168,1,13,53,245
		str = std::string(szResponse);
		pos1 = str.find('(');
		pos2 = str.find(')');
		// ip + port
		str = str.substr(pos1 + 1, pos2 - pos1 - 1);
		strcpy_s(szResponse, _countof(szResponse), str.c_str());
		sscanf_s(szResponse, "%d,%d,%d,%d,%d,%d",
			&remote_ip_v4[0], &remote_ip_v4[1], &remote_ip_v4[2], &remote_ip_v4[3],
			&conect_port_v4[0], &conect_port_v4[1]);
		sprintf_s(ip_char_v4, _countof(ip_char_v4), "%d.%d.%d.%d",
			remote_ip_v4[0], remote_ip_v4[1], remote_ip_v4[2], remote_ip_v4[3]);
		port_dec = conect_port_v4[0] << 8 | conect_port_v4[1];

		data->SetRemotePort(static_cast<u_short>(port_dec));

		// passive IP ignore
		if (FAXSend->dwPassiveIPIgnore == 1)
			data->SetRemoteHost(socket->GetRemoteHost());
		else
			data->SetRemoteHost(ip_char_v4);
	} // end IPv4
	else // IPv6 passive mode - EPSV command
	{
		Status = SendCmd("EPSV\n", 229, socket->GetSocketPtr());
		// example: 229 Entering Extended Passive Mode(|||40569|)
		if (Status) // error
		{
			if (Status != 1) // no socket error
				AppendTextToBox(_T(LNG_ERROR_SOCKET_FOR_PASSIVEDATA));
			return FALSE;
		}
		data->SetRemoteHost(socket->GetRemoteHost()); // EPSV return only port - so use the same IP as socket connection
		sscanf_s(szResponse, "%*[^'|']|||%zu[^'|']", &port_dec); // example: 40569
		data->SetRemotePort(static_cast<u_short>(port_dec));
	} // end IPv6

	if (!data->CreateSocket(&dwErrorID))
	{
		// Error socket for data connection
		AppendTextToBox(_T(LNG_ERROR_SOCKET_FOR_PASSIVEDATA));
		ShowErrorMessage(dwErrorID);
		return FALSE;
	}
	Status = SendCmd("STOT\n", 150, socket->GetSocketPtr());//File status okay; about to open data connection
	if (Status) // error
	{
		if (Status != 1) // no socket error
			AppendTextToBox(_T(LNG_CANT_SEND_FAX));
		return FALSE;
	}
	return TRUE;
}
//***********************************************************
// active mode ftp
BOOL cCommunication::SetActiveMode(SOCKET* socket, TTcpClient* data)
{
	size_t port_dec;

	port_dec = _wtoi(FAXSend->szActivePortNumber);
	data->SetRemotePort(static_cast<u_short>(port_dec));
	// if error
	if (!data->ListenSocket(this, socket, &dwErrorID))
	{
		// Error socket for data connection
		AppendTextToBox(_T(LNG_ERROR_SOCKET_FOR_ACTIVEDATA));
		ShowErrorMessage(dwErrorID);
		return FALSE;
	}
	return TRUE;
}
//***********************************************************
// get info about file transfer
BOOL cCommunication::GetTransferResponse(SOCKET* socket)
{
	// get info about file transfer
	// command STOT = store a temporary file with a unique name
	// example response from STOT command was: 150 FILE: /tmp/doc1291.ps (Opening new data connection)
	strncpy_s(RemoteFileName, _countof(RemoteFileName), szResponse, _countof(RemoteFileName) - 1);
	FD_ZERO(&FDs);
	FD_SET(*socket, &FDs);
	// control socket response from loadfilesendfile of data socket
	// example: 226 Transfer complete (FILE: /tmp/doc1291.ps). 
	pcStatus->resetProgress();
	iResult = select(0, &FDs, NULL, NULL, &tv); // receive timeout
	if (iResult > 0) // ok
	{
		iResult = recv(*socket, szResponse, recvbuflen, 0);
		if (iResult == 0 || iResult == SOCKET_ERROR) // error
		{
			dwErrorID = WSAGetLastError();
			AppendTextToBox(_T(LNG_CANT_SEND_FAX));
			ShowErrorMessage(dwErrorID);
			return FALSE;
		}
		else // ok
		{
			szResponse[iResult] = '\0';
			AnsiToUnicode(szResponse, szUnicode, _countof(szUnicode));
			AppendTextToBox(szUnicode);
		}
	}
	// timeout or error
	else
	{
		dwErrorID = WSAGetLastError();
		AppendTextToBox(_T(LNG_CANT_SEND_FAX));
		ShowErrorMessage(dwErrorID);
		return FALSE;
	}
	// example
	// 150 FILE: /tmp/doc1291.ps (Opening new data connection). And must be only:
	// /tmp/doc1291.ps
	str = std::string(RemoteFileName);
	pos1 = str.find("FILE: ") + 6;
	pos2 = str.find(" (Opening");
	str = str.substr(pos1, (pos2 - pos1));
	strcpy_s(RemoteFileName, _countof(RemoteFileName), str.c_str());

	return TRUE;
}
//***********************************************************
// create new job
BOOL cCommunication::NewJob(SOCKET* socket)
{
	Status = SendCmd("JNEW\n", 200, socket);
	if (Status) // error
	{
		if (Status != 1) // no socket error
			AppendTextToBox(_T(LNG_CANT_SEND_FAX));
		return FALSE;
	}
	return TRUE;
}
//***********************************************************
// Fromuser
BOOL cCommunication::FromUser(SOCKET* socket)
{
	UnicodeToAnsi(UserConfigure->GetLoggedInUser(), szAnsi, _countof(szAnsi));
	sprintf_s(szCommand, _countof(szCommand), "JPARM FROMUSER \"%s\"\n", szAnsi);
	// send FAX number
	Status = SendCmd(szCommand, 213, socket);
	if (Status) // error
	{
		if (Status != 1) // no socket error
			AppendTextToBox(_T(LNG_CANT_SEND_FAX));
		return FALSE;
	}
	return TRUE;
}
//***********************************************************
// set time before delete job
BOOL cCommunication::LastTime(SOCKET* socket)
{
	Status = SendCmd("JPARM LASTTIME 000259\n", 213, socket);
	if (Status) // error
	{
		if (Status != 1) // no socket error
			AppendTextToBox(_T(LNG_CANT_SEND_FAX));
		return FALSE;
	}
	return TRUE;
}
//***********************************************************
// set dialstring - faxnumber
BOOL cCommunication::DialString(SOCKET* socket)
{
	UnicodeToAnsi(HylaUI->GetFAXNumberText(), szAnsi, _countof(szAnsi));
	sprintf_s(szCommand, _countof(szCommand), "JPARM DIALSTRING \"%s\"\n", szAnsi);
	// send FAX number
	Status = SendCmd(szCommand, 213, socket);
	if (Status) // error
	{
		if (Status != 1) // no socket error
			AppendTextToBox(_T(LNG_CANT_SEND_FAX));
		return FALSE;
	}
	return TRUE;
}
//***********************************************************
// set resolution
BOOL cCommunication::SetResolution(SOCKET* socket)
{
	// STANDARD resolution
	if (wcscmp(FAXSend->szResolution, _T(STAND_RES)) == 0)
	{
		Status = SendCmd("JPARM VRES 98\n", 213, socket);
		if (Status) // error
		{
			if (Status != 1) // no socket error
				AppendTextToBox(_T(LNG_CANT_SEND_FAX));
			return FALSE;
		}
	}
	// FINE resolution
	if (wcscmp(FAXSend->szResolution, _T(FINE_RES)) == 0)
	{
		Status = SendCmd("JPARM VRES 196\n", 213, socket);
		if (Status) // error
		{
			if (Status != 1) // no socket error
				AppendTextToBox(_T(LNG_CANT_SEND_FAX));
			return FALSE;
		}
	}
	// SUPERFINE resolution
	if (wcscmp(FAXSend->szResolution, _T(SUPER_RES)) == 0)
	{
		Status = SendCmd("JPARM USEXVRES YES\n", 213, socket);
		if (Status) // error
		{
			if (Status != 1) // no socket error
				AppendTextToBox(_T(LNG_CANT_SEND_FAX));
			return FALSE;
		}
		Status = SendCmd("JPARM VRES 391\n", 213, socket);
		if (Status) // error
		{
			if (Status != 1) // no socket error
				AppendTextToBox(_T(LNG_CANT_SEND_FAX));
			return FALSE;
		}
	}
	// ULTRAFINE resolution
	if (wcscmp(FAXSend->szResolution, _T(ULTRA_RES)) == 0)
	{
		Status = SendCmd("JPARM USEXVRES YES\n", 213, socket);
		if (Status) // error
		{
			if (Status != 1) // no socket error
				AppendTextToBox(_T(LNG_CANT_SEND_FAX));
			return FALSE;
		}
		Status = SendCmd("JPARM VRES 300\n", 213, socket);
		if (Status) // error
		{
			if (Status != 1) // no socket error
				AppendTextToBox(_T(LNG_CANT_SEND_FAX));
			return FALSE;
		}
	}
	// HYPERFINE resolution
	if (wcscmp(FAXSend->szResolution, _T(HYPER_RES)) == 0)
	{
		Status = SendCmd("JPARM USEXVRES YES\n", 213, socket);
		if (Status) // error
		{
			if (Status != 1) // no socket error
				AppendTextToBox(_T(LNG_CANT_SEND_FAX));
			return FALSE;
		}
		Status = SendCmd("JPARM VRES 400\n", 213, socket);
		if (Status) // error
		{
			if (Status != 1) // no socket error
				AppendTextToBox(_T(LNG_CANT_SEND_FAX));
			return FALSE;
		}
	}
	return TRUE;
}
//***********************************************************
// set pagesize
BOOL cCommunication::SetPageSize(SOCKET* socket)
{
	if (wcscmp(FAXSend->szPageSize, _T(A4_SIZE)) == 0)
	{
		Status = SendCmd("JPARM PAGEWIDTH 209\n", 213, socket);
		if (Status) // error
		{
			if (Status != 1) // no socket error
				AppendTextToBox(_T(LNG_CANT_SEND_FAX));
			return FALSE;
		}
		Status = SendCmd("JPARM PAGELENGTH 296\n", 213, socket);
		if (Status) // error
		{
			if (Status != 1) // no socket error
				AppendTextToBox(_T(LNG_CANT_SEND_FAX));
			return FALSE;
		}
	}
	return TRUE;
}
//***********************************************************
// set notification
BOOL cCommunication::SetNotification(SOCKET* socket)
{
	if (wcslen(HylaUI->GetEmailText()) > 0)
	{
		UnicodeToAnsi(HylaUI->GetEmailText(), szAnsi, _countof(szAnsi));
		sprintf_s(szCommand, _countof(szCommand), "JPARM NOTIFYADDR \"%s\"\n", szAnsi);
		Status = SendCmd(szCommand, 213, socket);
		// email
		if (Status) // error
		{
			if (Status != 1) // no socket error
				AppendTextToBox(_T(LNG_CANT_SEND_FAX));
			return FALSE;
		}
		// success only
		if (wcscmp(FAXSend->szNotification, _T(SUCCESS_ONLY)) == 0)
			Status = SendCmd("JPARM NOTIFY \"done\"\n", 213, socket);
		// failure only
		else if (wcscmp(FAXSend->szNotification, _T(FAILURE_ONLY)) == 0)
			Status = SendCmd("JPARM NOTIFY \"requeue\"\n", 213, socket);
		// none
		else if (wcscmp(FAXSend->szNotification, _T(NONE_NOTIF)) == 0)
			Status = SendCmd("JPARM NOTIFY \"none\"\n", 213, socket);
		// success and failure
		else
			Status = SendCmd("JPARM NOTIFY \"done+requeue\"\n", 213, socket);

		if (Status) // error
		{
			if (Status != 1) // no socket error
				AppendTextToBox(_T(LNG_CANT_SEND_FAX));
			return FALSE;
		}
	} // notification if email text
	else
	{
		Status = SendCmd("JPARM NOTIFY \"none\"\n", 213, socket);
		if (Status) // error
		{
			if (Status != 1) // no socket error
				AppendTextToBox(_T(LNG_CANT_SEND_FAX));
			return FALSE;
		}
	}
	return TRUE;
}
//***********************************************************
// set jobinfo
BOOL cCommunication::SetJobInfo(SOCKET* socket)
{
	if (wcslen(HylaUI->GetIdentifierText()) > 0)
	{
		UnicodeToAnsi(HylaUI->GetIdentifierText(), szAnsi, _countof(szAnsi));
		sprintf_s(szCommand, _countof(szCommand), "JPARM JOBINFO \"%s\"\n", szAnsi);
		Status = SendCmd(szCommand, 213, socket);
		if (Status) // error
		{
			if (Status != 1) // no socket error
				AppendTextToBox(_T(LNG_CANT_SEND_FAX));
			return FALSE;
		}
		return TRUE;
	}
	else
		return TRUE;
}
//***********************************************************
// set maxdials
BOOL cCommunication::SetMaxDials(SOCKET* socket)
{
	if (wcscmp(FAXSend->szMaxDials, _T(MAXDIALS_NONE)) == 0)
		return TRUE;

	if (wcscmp(FAXSend->szMaxDials, _T(MAX_TIMES_1)) == 0)
		Status = SendCmd("JPARM MAXDIALS 1\n", 213, socket);
	else if (wcscmp(FAXSend->szMaxDials, _T(MAX_TIMES_3)) == 0)
		Status = SendCmd("JPARM MAXDIALS 3\n", 213, socket);
	else if (wcscmp(FAXSend->szMaxDials, _T(MAX_TIMES_6)) == 0)
		Status = SendCmd("JPARM MAXDIALS 6\n", 213, socket);
	else if (wcscmp(FAXSend->szMaxDials, _T(MAX_TIMES_9)) == 0)
		Status = SendCmd("JPARM MAXDIALS 9\n", 213, socket);
	else if (wcscmp(FAXSend->szMaxDials, _T(MAX_TIMES_12)) == 0)
		Status = SendCmd("JPARM MAXDIALS 12\n", 213, socket);
	else
		return TRUE;

	if (Status) // error
	{
		if (Status != 1) // no socket error
			AppendTextToBox(_T(LNG_CANT_SEND_FAX));
		return FALSE;
	}

	return TRUE;
}
//***********************************************************
// set maxtries
BOOL cCommunication::SetMaxTries(SOCKET* socket)
{
	if (wcscmp(FAXSend->szMaxTries, _T(MAXTRIES_NONE)) == 0)
		return TRUE;

	if (wcscmp(FAXSend->szMaxTries, _T(MAX_TIMES_1)) == 0)
		Status = SendCmd("JPARM MAXTRIES 1\n", 213, socket);
	else if (wcscmp(FAXSend->szMaxTries, _T(MAX_TIMES_3)) == 0)
		Status = SendCmd("JPARM MAXTRIES 3\n", 213, socket);
	else if (wcscmp(FAXSend->szMaxTries, _T(MAX_TIMES_6)) == 0)
		Status = SendCmd("JPARM MAXTRIES 6\n", 213, socket);
	else
		return TRUE;

	if (Status) // error
	{
		if (Status != 1) // no socket error
			AppendTextToBox(_T(LNG_CANT_SEND_FAX));
		return FALSE;
	}

	return TRUE;
}
//***********************************************************
// set modem group
BOOL cCommunication::SetModem(SOCKET* socket)
{
	if (wcslen(FAXSend->szModem) > 0)
	{
		UnicodeToAnsi(FAXSend->szModem, szAnsi, _countof(szAnsi));
		sprintf_s(szCommand, _countof(szCommand), "JPARM MODEM %s\n", szAnsi);
		Status = SendCmd(szCommand, 213, socket);
		if (Status) // error
		{
			if (Status != 1) // no socket error
				AppendTextToBox(_T(LNG_CANT_SEND_FAX));
			return FALSE;
		}
	}
	return TRUE;
}
//***********************************************************
// set document
BOOL cCommunication::SetDocument(SOCKET* socket)
{
	// document
	sprintf_s(szCommand, _countof(szCommand), "JPARM DOCUMENT %s\n", RemoteFileName);
	Status = SendCmd(szCommand, 200, socket);
	if (Status) // error
	{
		if (Status != 1) // no socket error
			AppendTextToBox(_T(LNG_CANT_SEND_FAX));
		return FALSE;
	}
	return TRUE;
}
//***********************************************************
// submit job
BOOL cCommunication::JobSubmit(SOCKET* socket)
{
	CHAR JobConfirm[128];
	WCHAR szTextMessageOK[STRVALUE_TEXT_256];

	Status = SendCmd("JSUBM\n", 200, socket);
	if (Status) // error
	{
		if (Status != 1) // no socket error
			AppendTextToBox(_T(LNG_CANT_SEND_FAX));
		return FALSE;
	}

	strncpy_s(JobConfirm, _countof(JobConfirm), szResponse, _countof(JobConfirm) - 1);
	// example
	// 200 Job 689 submitted. And must be:
	// Job 689
	str = std::string(JobConfirm);
	pos1 = str.find("Job");
	pos2 = str.find("sub");
	str = str.substr(pos1, pos2 - pos1 - 1);
	strcpy_s(JobConfirm, _countof(JobConfirm), str.c_str());
	// OK Message
	//*****************************************************
	AnsiToUnicode(JobConfirm, szUnicode, _countof(szUnicode));
	swprintf_s(szTextMessageOK, _countof(szTextMessageOK), L"%s\r\n<%s>", _T(LNG_MESSAGE_CONFIRM_OK), szUnicode);
	SetWindowText(GetDlgItem(gHwndDialogSock, IDC_EDIT_SOCKET), szTextMessageOK);

	return TRUE;
}
//***********************************************************
BOOL cCommunication::LoadFileSendFile(LPCWSTR lpFileName, SOCKET* socket)
{
	HANDLE hFile;
	DWORD dwSize;
	DWORD dw;
	LPBYTE lpBuffer = NULL;
	BOOL bRet = TRUE;

	hFile = CreateFile(lpFileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	if (hFile == INVALID_HANDLE_VALUE)
		return FALSE;
	dwSize = GetFileSize(hFile, NULL);
	lpBuffer = (LPBYTE)HeapAlloc(GetProcessHeap(), 0, (DWORD_PTR)dwSize + 1);
	if (!lpBuffer) // fail
		return FALSE;

	if (!ReadFile(hFile, lpBuffer, dwSize, &dw, NULL))
	{
		CloseHandle(hFile);
		HeapFree(GetProcessHeap(), 0, lpBuffer);
		return FALSE;
	}
	CloseHandle(hFile);
	lpBuffer[dwSize] = 0;

	// send file fax
	// send bytes
	FD_ZERO(&FDs);
	FD_SET(*socket, &FDs);
	pcStatus->resetProgress();
	iResult = select(0, NULL, &FDs, NULL, &tv); // write timeout
	if (iResult > 0) // ok
	{
		// send postscript file
		iResult = send(*socket, (const CHAR*)lpBuffer, dwSize, 0);
		if (iResult != (int)dwSize) // error
		{
			dwErrorID = WSAGetLastError();
			AppendTextToBox(_T(LNG_CANT_SEND_FAX));
			ShowErrorMessage(dwErrorID);
			bRet = FALSE;
		}
	}
	else // SOCKET_ERROR or time limit expired
	{
		dwErrorID = WSAGetLastError();
		AppendTextToBox(_T(LNG_CANT_SEND_FAX));
		ShowErrorMessage(dwErrorID);
		bRet = FALSE;
	}
	// free memory
	HeapFree(GetProcessHeap(), 0, lpBuffer);
	if (!bRet)
		return FALSE;

	return TRUE;
}
// ******************************************************************
// ******************************************************************
