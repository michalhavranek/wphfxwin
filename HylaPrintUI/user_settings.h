#ifndef UserSettingsH
#define UserSettingsH

#include "resource.h"

INT_PTR CALLBACK UserSettingsGUI(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK EnumChildProc(HWND hwnd, LPARAM lParam);
void SetXMLUserConfig(HWND hDlg);
void LoadDefaultValues(HWND hDlg);
LRESULT GetTextItemCB(HWND hCombo, LPWSTR str, size_t iBufLen);
void GetTextEBConfigDialog(HWND hEdit, LPWSTR str, size_t iBufLen);

#endif
