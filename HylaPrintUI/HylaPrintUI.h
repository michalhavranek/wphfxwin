#pragma once

#define IDT_TIMER1 8
#define IDT_TIMER2 9
#define IDC_DLG_STATUS 10

#define KEY_IN_CONTROL ( WM_APP + 1 )
#define START_COUNTDOWN (WM_USER + 1)

#include "resource.h"
#include "autocomplete.h"
#include "../HylaPrint/constants.h"

extern HINSTANCE hinst;
extern HWND gHwndDialogSock;
extern HANDLE hEvent;
extern CEnumString* pAuto;
extern int iLDAPLoopReturn;

// Forward declarations of functions included in this code module:
INT_PTR CALLBACK SendFax(HWND, UINT, WPARAM, LPARAM);
void LVSearch(CEnumString* pAuto);
LRESULT CALLBACK WindowProcEdit(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
BOOL WritePipe(PHANDLE hPipe, LPCWSTR lpvMessage);
BOOL ReadPipe(PHANDLE hPipe, WCHAR* buf, DWORD dwSize);
BOOL IPCClient(HANDLE* hPipe);
void WriteTextItemEBSend(HWND hEdit, LPWSTR str, BOOL bRegEx, size_t iBufsize);
void LVMenu(HWND hWnd);
void ReadAddrBook(LPCWSTR path);
void WriteLVToFile();
BOOL InitListViewColumns();
void GetVersionHylaUI(HWND hStatus);
BOOL SetFilePermissions(LPCWSTR filename, LPCWSTR username, int permissions);
void SetForegroundDialog(HWND hwnd);
void CopyUserSettingsData();
INT_PTR CallConfigDialog(HWND hDlg);
void CreateStatusBar(HWND hDlg, HICON icon);
bool CompareNoCase(const std::wstring& s1, const std::wstring& s2);
BOOL DetectIPCClient(WCHAR* chBuf);
void InitAddrbookPath();
void ModifySysmenu(HWND hDlg);
void SaveWindowPosition(HWND hDlg);
void SetDialogPosition(HWND hDlg);
void SetClipBoardFaxNumber(HWND hDlg);

class CHylaUI
{
private:	// User declarations
	WCHAR szEmail[STRVALUE_EMAIL];
	WCHAR szFaxNumber[STRVALUE_FAXNUMBER];
	WCHAR szIdentifier[STRVALUE_IDENTIFIER];
	HWND hDlg;
	LRESULT index;     // index item listview
	BOOL iNewItem;     // true = additem, false = edit item
	RECT rcSocket;
	std::vector <std::wstring> phoneBook;
	LDAP* pldHandle;
	BOOL bLDAPSearchFlag;

public:
	CHylaUI();
	~CHylaUI();

	void SetFAXNumberText(LPCWSTR text);
	void SetEmailText(LPCWSTR text);
	void SetHandleDLG(HWND hwnd);
	void SetItemIndex(LRESULT i);
	void SetNewItem(BOOL b);
	void SetLDAPSearchFlag(BOOL b) { bLDAPSearchFlag = b; }
	void SetFaxDialogRect(RECT* rc);
	void SetIdentifierText(LPCWSTR text);
	void AddPhonebookItem(LPCWSTR text);
	void GetFaxDialogRect(LPRECT rc);
	void SortPBVector() { std::sort(phoneBook.begin(), phoneBook.end(), CompareNoCase); }
	void GetPhoneBookName(int i, LPWSTR text);
	void GetPhoneBookNumber(int i, LPWSTR text);
	void DeletePhoneBookItem(int iPosition);
	void GetPhoneBookItem(int i, LPWSTR text, size_t iBufLen) { StringCchCopy(text, iBufLen, phoneBook.at(i).c_str()); }
	void ResetPBVector() { phoneBook.clear(); };
	void SetPointerLDAPHandle(LDAP* pld);

	LPWSTR GetEmailText();
	LPWSTR GetFAXNumberText();
	LPWSTR GetIdentifierText();
	HWND GetHandleDLG()const;
	LRESULT GetItemIndex()const;
	BOOL GetNewItem();
	BOOL GetLDAPSearchFlag() { return bLDAPSearchFlag; }
	std::vector <std::wstring>::size_type GetPBVectorSize();
	LDAP* GetPointerLDAPHandle();

};
extern CHylaUI* HylaUI;
//***********************************************************
