#include "stdafx.h"
#include "lang_UI.h"
#include "../Language/language.h"
#include "resource.h"

// localize
// ******************************************************************
void LocalDialogSend(HWND hdlg, LPCWSTR szDirPSFile)
{
	WCHAR szText[64];
	int x;
	// OK button
	StringCbPrintf(szText, sizeof(szText), L"  %s", _T(LNG_SEND_FAX));
	SetWindowText(GetDlgItem(hdlg, LANG_SEND_FAX), szText);
	// cancel button
	StringCbPrintf(szText, sizeof(szText), L"  %s", _T(LNG_IDCANCEL));
	SetWindowText(GetDlgItem(hdlg, LANG_IDCANCEL), szText);
	// Static text faxnumber
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_FAXNUMBER), _T(LNG_STATIC_FAXNUMBER));
	// static text email notify
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_EMAILNOTIFY), _T(LNG_STATIC_EMAILNOTIFY));
	// addressbook
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_ADDRESSBOOK), _T(LNG_STATIC_ADDRESSBOOK));
	// identifier
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_IDENTIFIER), _T(LNG_STATIC_IDENTIFIER));
	// dialog title
	x = GetPagesPrinted(szDirPSFile);
	StringCbPrintf(szText, sizeof(szText), L"%s - [%s %d]", _T(LNG_SENDFAX_DIALOG), _T(LNG_PAGES), x);
	SetWindowText(hdlg, szText);
}
// ******************************************************************
void LocalDialogAddEdit(HWND hdlg)
{
	// name
	SetWindowText(GetDlgItem(hdlg, LANG_EDIT_STATIC_NAME), _T(LNG_EDIT_STATIC_NAME));
	// number
	SetWindowText(GetDlgItem(hdlg, LANG_EDIT_STATIC_NUMBER), _T(LNG_EDIT_STATIC_NUMBER));
	// cancel
	SetWindowText(GetDlgItem(hdlg, LANG_EDITADDRBOOK_IDCANCEL), _T(LNG_EDIT_IDCANCEL));
	// ok
	SetWindowText(GetDlgItem(hdlg, IDOK), _T(LNG_EDIT_OK));
	// dialog title
	SetWindowText(hdlg, _T(LNG_EDIT_DIALOG_ADDRBOOK_ITEM));
}
// ******************************************************************
void LocalMenuLB(HMENU menu)
{
	// add new item
	ModifyMenu(menu, LANG_ID_MENULV_ADD, MF_BYCOMMAND | MF_STRING, LANG_ID_MENULV_ADD, _T(LNG_ID_MENULV_ADD));
	// edit item
	ModifyMenu(menu, LANG_ID_MENULV_EDIT, MF_BYCOMMAND | MF_STRING, LANG_ID_MENULV_EDIT, _T(LNG_ID_MENULV_EDIT));
	// delete item
	ModifyMenu(menu, LANG_ID_MENULV_DELETE, MF_BYCOMMAND | MF_STRING, LANG_ID_MENULV_DELETE, _T(LNG_ID_MENULV_DELETE));
}
// ******************************************************************
void LocalDialogSocket(HWND hdlg, BOOL bActiveMode)
{
	// dialog title
	if (bActiveMode)
		SetWindowText(hdlg, _T(LNG_INFO_DIALOG)_T(" - ")_T(LNG_ACTIVEMODE));
	else
		SetWindowText(hdlg, _T(LNG_INFO_DIALOG)_T(" - ")_T(LNG_PASSIVEMODE));
	// edit text
	SetWindowText(GetDlgItem(hdlg, IDC_EDIT_SOCKET), _T(LNG_SENDFAX_WORKING));
	// info
	// username
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_USERNAME), _T(LNG_USERNAME));
	// faxnumber
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_FAXNUMBER), _T(LNG_STATIC_FAXNUMBER));
	// email
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_EMAIL), _T(LNG_EMAIL));
	// nitification
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_NOTIFICATION), _T(LNG_NOTIFICATION));
	// page size
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_PAGESIZE), _T(LNG_PAGESIZE));
	// resolution
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_RESOLUTION), _T(LNG_RESOLUTION));
	// maxdials
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_MAXDIALS), _T(LNG_MAXDIALS));
	// maxtries
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_MAXTRIES), _T(LNG_MAXTRIES));
	// loggedin user
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_LOGGEDINUSER), _T(LNG_LOGGEDINUSER));
	// identifier
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_IDENTIFIER), _T(LNG_STATIC_IDENTIFIER));
}
// ******************************************************************
void LocalDialog_UserSettings(HWND hdlg)
{
	// dialog title
	SetWindowText(hdlg, _T(LNG_USER_SETTINGS_TITLE));
	// checkbox
	SetWindowText(GetDlgItem(hdlg, IDC_CHCK_SETTINGS_USESETTINGS), _T(LNG_USER_SETTINGS_BOX));
	// ok button
	SetWindowText(GetDlgItem(hdlg, IDOK), _T(LNG_SETTINGS_OK));
	// cancel button
	SetWindowText(GetDlgItem(hdlg, IDCANCEL), _T(LNG_SETTINGS_CANCEL));
	// default button
	SetWindowText(GetDlgItem(hdlg, IDC_BTN_SETTINGS_LOAD), _T(LNG_SETTEINGS_DEFAULT));
}
// users items
#include "lang_usersettings.h"

// pages
int GetPagesPrinted(LPCWSTR filename)
{
	HANDLE hFile;
	int x = 0;
	CHAR* chBuffer;
	DWORD dwSize;
	DWORD  dwBytesRead;
	// postscript comment - begin page
	CHAR* needle = "%%Page:";
	BOOL bRet;

	if (INVALID_HANDLE_VALUE ==
		(hFile = CreateFileW(filename, GENERIC_READ, FILE_SHARE_READ,
			NULL, OPEN_EXISTING, 0, NULL)))
		return 0;

	dwSize = GetFileSize(hFile, NULL);
	chBuffer = (CHAR*)HeapAlloc(GetProcessHeap(), 0, (DWORD_PTR)dwSize + 1);

	if (chBuffer == NULL) return 0;

	// copy the file into the buffer:
	bRet = ReadFile(hFile, chBuffer, dwSize, &dwBytesRead, NULL);
	if (!bRet) // fail
		return 0;
	CloseHandle(hFile);
	chBuffer[dwSize] = '\0';

	size_t nlen = strlen(needle);

	while (chBuffer != NULL)
	{
		chBuffer = strstr(chBuffer, needle);
		if (chBuffer != NULL)
		{
			x++;
			chBuffer += nlen;
		}
	}

	HeapFree(GetProcessHeap(), 0, chBuffer);
	return x;
}
// *************************************************************
// *************************************************************
