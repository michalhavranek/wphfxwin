#include "stdafx.h"
#include "user_settings.h"
#include "lang_UI.h"
#include "../HylaPrint/SendFAX.h"
#include "../HylaPrint/system_message.h"
#include "../HylaPrint/config_class.h"
#include "HylaPrintUI.h"

//*********************************************************
// SHARED FUNCTION
#include "../HylaPrint/shared_savedata.h"
//*********************************************************

// Message handler for about box.
INT_PTR CALLBACK UserSettingsGUI(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	static HICON hIconOK, hIconCancel;

	switch (message)
	{
	case WM_INITDIALOG:
		//icon
		hIconOK = (HICON)LoadImage(hinst, MAKEINTRESOURCE(IDI_ICON_CHECK), IMAGE_ICON, 0, 0, LR_DEFAULTCOLOR);
		SendDlgItemMessage(hDlg, IDOK, BM_SETIMAGE, (WPARAM)IMAGE_ICON, (LPARAM)hIconOK);
		hIconCancel = (HICON)LoadImage(hinst, MAKEINTRESOURCE(IDI_ICON_CANCEL), IMAGE_ICON, 0, 0, LR_DEFAULTCOLOR);
		SendDlgItemMessage(hDlg, IDCANCEL, BM_SETIMAGE, (WPARAM)IMAGE_ICON, (LPARAM)hIconCancel);
		// combobox start values - add strings to comboboxes whis are the same for single user/system wide
		AddSharedStringsToCB(hDlg);

		UserConfigure->LoadXML();
		SetXMLUserConfig(hDlg);
		LocalDialog_UserSettings(hDlg);
		LocalDialog_UserSettingsItems(hDlg);
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			// save GUI settings data to class UserConfigure
			SaveUserData<CUserConfigure*>(hDlg, UserConfigure);
			if (SendDlgItemMessage(hDlg, IDC_CHCK_SETTINGS_USESETTINGS, BM_GETCHECK, 0, 0) == BST_CHECKED)
				UserConfigure->SetUseUserConfig(TRUE);
			else
				UserConfigure->SetUseUserConfig(FALSE);
			// save data from class UserConfigure to XML file
			UserConfigure->SaveXML();
			DestroyIcon(hIconOK);
			DestroyIcon(hIconCancel);
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		case IDCANCEL:
			DestroyIcon(hIconOK);
			DestroyIcon(hIconCancel);
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		case IDC_BTN_SETTINGS_BROWSE:
			if (BrowseForFolder(UserConfigure->GetAddressBookText(), MAX_PATH - 1))
				SetDlgItemText(hDlg, IDC_EDIT_CONFBOOK_DIR, UserConfigure->GetAddressBookText());
			break;
		case IDC_BTN_SETTINGS_LOAD:
			LoadDefaultValues(hDlg);
			break;
		case IDC_CHCK_SETTINGS_USESETTINGS:
			// enable - disable GUI windows
			if (SendDlgItemMessage(hDlg, IDC_CHCK_SETTINGS_USESETTINGS, BM_GETCHECK, 0, 0) == BST_CHECKED)
				EnumChildWindows(hDlg, EnumChildProc, 1); // enable windows
			else
				EnumChildWindows(hDlg, EnumChildProc, 0); // disable windows
			break;
		}
	}
	return (INT_PTR)FALSE;
}
//*********************************************************
// enable-disable all child windows 
BOOL CALLBACK EnumChildProc(HWND hwnd, LPARAM lParam)
{
	LPARAM localID;
	localID = GetWindowLong(hwnd, GWL_ID);
	if (localID != IDOK && localID != IDCANCEL && localID != IDC_CHCK_SETTINGS_USESETTINGS)
		EnableWindow(hwnd, (BOOL)lParam);
	return TRUE;
}
//*********************************************************
void SetXMLUserConfig(HWND hDlg)
{
	// editbox
	SetDlgItemText(hDlg, IDC_EDIT_CONFUSERNAME, UserConfigure->GetUsernameText()); // user
	SetDlgItemText(hDlg, IDC_EDIT_CONFPASSWORD, UserConfigure->GetPasswordText()); // passwd
	SetDlgItemText(hDlg, IDC_EDIT_CONFDEF_NOTIFEMAIL, UserConfigure->GetEmailText()); // email
	SetDlgItemText(hDlg, IDC_EDIT_CONFMODEM, UserConfigure->GetModemText()); // modem
	SetDlgItemText(hDlg, IDC_EDIT_CONFBOOK_DIR, UserConfigure->GetAddressBookText()); // addrbook
	// combobox
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFNOTIF_TYP, CB_SELECTSTRING,
		(WPARAM)(-1), (LPARAM)UserConfigure->GetNotificationText());
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFPAGESIZE, CB_SELECTSTRING,
		(WPARAM)(-1), (LPARAM)UserConfigure->GetPageSizeText());
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFRESOLUTION, CB_SELECTSTRING,
		(WPARAM)(-1), (LPARAM)UserConfigure->GetResolutionText());
	SendDlgItemMessage(hDlg, IDC_COMBO_MAXDIALS, CB_SELECTSTRING,
		(WPARAM)(-1), (LPARAM)UserConfigure->GetMaxDialsText());
	SendDlgItemMessage(hDlg, IDC_COMBO_MAXTRIES, CB_SELECTSTRING,
		(WPARAM)(-1), (LPARAM)UserConfigure->GetMaxTriesText());
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFINFODIALOG_TIMER, CB_SELECTSTRING,
		(WPARAM)(-1), (LPARAM)UserConfigure->GetInfoDialogTimerText());
	// faxnumber from clipboard
	if (UserConfigure->GetUseFaxnumberFromClipboard())
		SendDlgItemMessage(hDlg, IDC_CHECK_FAXNUM_CLIPBOARD, BM_SETCHECK, BST_CHECKED, 0);
	else
		SendDlgItemMessage(hDlg, IDC_CHECK_FAXNUM_CLIPBOARD, BM_SETCHECK, BST_UNCHECKED, 0);
	// checkbox use userconfig
	if (UserConfigure->GetUseUserConfig())
		SendDlgItemMessage(hDlg, IDC_CHCK_SETTINGS_USESETTINGS, BM_SETCHECK, BST_CHECKED, 0);
	else
	{
		SendDlgItemMessage(hDlg, IDC_CHCK_SETTINGS_USESETTINGS, BM_SETCHECK, BST_UNCHECKED, 0);
		EnumChildWindows(hDlg, EnumChildProc, 0); // disable windows
	}
}
//*********************************************************
void LoadDefaultValues(HWND hDlg)
{
	// editbox
	SetDlgItemText(hDlg, IDC_EDIT_CONFUSERNAME, FAXSend->szUser); // user
	SetDlgItemText(hDlg, IDC_EDIT_CONFPASSWORD, FAXSend->szPasswd); // passwd
	SetDlgItemText(hDlg, IDC_EDIT_CONFDEF_NOTIFEMAIL, FAXSend->szEmail); // email
	SetDlgItemText(hDlg, IDC_EDIT_CONFMODEM, FAXSend->szModem); // modem
	SetDlgItemText(hDlg, IDC_EDIT_CONFBOOK_DIR, FAXSend->szAddrBookDir); // addrbook
	// combobox
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFNOTIF_TYP, CB_SELECTSTRING,
		(WPARAM)(-1), (LPARAM)FAXSend->szNotification); // notif
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFPAGESIZE, CB_SELECTSTRING,
		(WPARAM)(-1), (LPARAM)FAXSend->szPageSize); // pagesize
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFRESOLUTION, CB_SELECTSTRING,
		(WPARAM)(-1), (LPARAM)FAXSend->szResolution); // resolution
	SendDlgItemMessage(hDlg, IDC_COMBO_MAXDIALS, CB_SELECTSTRING,
		(WPARAM)(-1), (LPARAM)FAXSend->szMaxDials); // maxdials
	SendDlgItemMessage(hDlg, IDC_COMBO_MAXTRIES, CB_SELECTSTRING,
		(WPARAM)(-1), (LPARAM)FAXSend->szMaxTries); // maxtries
	SendDlgItemMessage(hDlg, IDC_COMBO_CONFINFODIALOG_TIMER, CB_SELECTSTRING,
		(WPARAM)(-1), (LPARAM)FAXSend->szInfoDialogTimer); // infodialog timer
}
//*********************************************************************************************
//*********************************************************************************************
