#ifndef HylaSocketH
#define HylaSocketH

#include "../HylaPrint/constants.h"
#include "../HylaPrint/debug_output.h"

enum statusColors { GREY, BLINK, GREEN, RED };
BOOL SendToHylafax();
void ShowErrorMessage(DWORD dwID);

// **************************************************************
class TTcpClient
{
public:
	TTcpClient();
	BOOL CreateSocket(LPDWORD dwErr);
	BOOL ListenSocket(PVOID comm, SOCKET* sock, LPDWORD dwErr);
	~TTcpClient();
	SOCKET GetSocket() { return ConnectSocket; }
	SOCKET* GetSocketPtr() { return &ConnectSocket; }
	void SetRemoteHost(CHAR* host) { strncpy_s(RemoteHost, _countof(RemoteHost), host, _TRUNCATE); }
	void SetRemotePort(u_short port) { RemotePort = port; }
	CHAR* GetRemoteHost() { return RemoteHost; }
	void CloseSocket() { if (ConnectSocket != INVALID_SOCKET) closesocket(ConnectSocket); };
	void SetSocket(SOCKET socket);
	void SetIPClient(const CHAR* ipclient) { strncpy_s(clientIP, _countof(clientIP), ipclient, _TRUNCATE); }
	PCHAR GetIPClient() { return clientIP; } const
	void SetAIFamily(int iFamily) { ai_family = iFamily; }
	int GetAIFamily() { return ai_family; }
private:
	SOCKET ConnectSocket;
	CHAR RemoteHost[STRVALUE_IPADDRESS];
	u_short RemotePort;
	CHAR clientIP[STRVALUE_IPADDRESS];
	int ai_family; // AF_INET (IPv4) or AF_INET6 (IPv6)
};

class cCommunication
{
public:
	cCommunication();
	~cCommunication();
	BOOL Connect(SOCKET* socket);
	BOOL UserPassword(SOCKET* socket);
	BOOL SetPSFormat(SOCKET* socket);
	BOOL TypeSetImage(SOCKET* socket);
	BOOL ModeStream(SOCKET* socket);
	BOOL SetPassiveMode(TTcpClient* socket, TTcpClient* data);
	BOOL SetActiveMode(SOCKET* socket, TTcpClient* data);
	BOOL GetTransferResponse(SOCKET* socket);
	BOOL NewJob(SOCKET* socket);
	BOOL FromUser(SOCKET* socket);
	BOOL LastTime(SOCKET* socket);
	BOOL DialString(SOCKET* socket);
	BOOL SetResolution(SOCKET* socket);
	BOOL SetPageSize(SOCKET* socket);
	BOOL SetMaxDials(SOCKET* socket);
	BOOL SetMaxTries(SOCKET* socket);
	BOOL SetNotification(SOCKET* socket);
	BOOL SetJobInfo(SOCKET* socket);
	BOOL SetModem(SOCKET* socket);
	BOOL SetDocument(SOCKET* socket);
	BOOL JobSubmit(SOCKET* socket);
	BOOL LoadFileSendFile(LPCWSTR lpFileName, SOCKET* socket);
	int SendCmd(const CHAR* cmd, UINT okstat, SOCKET* Socket);
private:
	CHAR szResponse[STRVALUE_TEXT_256];
	CHAR szCommand[STRVALUE_TEXT_256];
	DWORD dwErrorID;
	int iResult;
	int Status;
	CHAR szStatus[4];
	WCHAR szUnicode[STRVALUE_TEXT_256];
	int recvbuflen = STRVALUE_TEXT_256;
	CHAR szAnsi[STRVALUE_TEXT_256];
	CHAR RemoteFileName[MAX_PATH];
	size_t pos1, pos2;
	std::string str;
	TIMEVAL tv;
	fd_set FDs;
};

#endif