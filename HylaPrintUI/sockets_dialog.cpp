#include "stdafx.h"
#include "sockets_dialog.h"
#include "HylaPrintUI.h"
#include "../HylaPrint/SendFAX.h"
#include "lang_UI.h"
#include "hyla_socket.h"
#include "../Language/language.h"
#include "../HylaPrint/config_class.h"

HINSTANCE hinst;
HWND gHwndDialogSock;
HANDLE hEvent;
// *************************************************************
// thread for socket dialog
unsigned __stdcall SocketsDialogThread(LPVOID lpvParam)
{
	UNREFERENCED_PARAMETER(lpvParam);
	DialogBox(hinst, MAKEINTRESOURCE(IDD_DIALOG_SOCKETS), NULL, (DLGPROC)SocketsDialog);
	_endthreadex(0);
	return 0;
}
// *************************************************************
// callback modeless dialog (sockets)
INT_PTR CALLBACK SocketsDialog(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static int b;
	static int iCountdown;
	WCHAR szSec[32];
	static HICON hIconOK, hIconCancel;

	switch (message)
	{
	case WM_INITDIALOG:
		gHwndDialogSock = hwndDlg;
		b = 0;
		hIconOK = NULL;
		hIconCancel = NULL;
		iCountdown = GetICountdown();
		CreateProgressBar(hwndDlg);
		SetTimer(hwndDlg, IDT_TIMER1, 200, (TIMERPROC)NULL);
		// local socket dialog
		if (FAXSend->dwUseActiveMode == 0) // passive mode, dialog text
			LocalDialogSocket(hwndDlg, FALSE);
		else
			LocalDialogSocket(hwndDlg, TRUE); // active mode, dialog text
		// show sending param on socket dialog
		SetInfoSettings(hwndDlg);
		CenterSocketDialog(hwndDlg);
		SetEvent(hEvent);
		return TRUE;

	case WM_TIMER:
		switch (wParam)
		{
			// socket sending dialog 
		case IDT_TIMER1:
			if (b)
				b = 0;
			else
				b = 1;
			if (!IsWindowEnabled(GetDlgItem(hwndDlg, IDC_BUTTON_SOCKET)))
			{
				SendDlgItemMessage(hwndDlg, IDC_PROGRESS1, PBM_SETPOS, pcStatus->getProgress(), 0);
				pcStatus->decProgress(); // decrease progressbar
			}

			InvalidateRect(hwndDlg, NULL, NULL);
			break;
			// countdown timer after fax sending
		case IDT_TIMER2:
			--iCountdown;
			if (pcStatus->getStatus()) // send fax OK
				StringCbPrintf(szSec, sizeof(szSec), L"  %s (%d)", _T(LNG_EDIT_OK), iCountdown);
			else // can't send fax
				StringCbPrintf(szSec, sizeof(szSec), L"  %s (%d)", _T(LNG_IDCANCEL), iCountdown);
			SetDlgItemText(hwndDlg, IDC_BUTTON_SOCKET, szSec);
			if (iCountdown <= 0)
				EndDialog(hwndDlg, LOWORD(wParam));

			break;
		}
		break;
	case WM_DRAWITEM:
		// status LED
		if (wParam == IDC_SIG_SERVER || wParam == IDC_SIG_USER || wParam == IDC_SIG_SOCKET || wParam == IDC_SIG_TRANSFER)
		{
			int iStatus;
			if (wParam == IDC_SIG_SERVER)
				iStatus = pcStatus->getServerStatus();
			else if (wParam == IDC_SIG_USER)
				iStatus = pcStatus->getUserStatus();
			else if (wParam == IDC_SIG_SOCKET)
				iStatus = pcStatus->getSocketStatus();
			else
				iStatus = pcStatus->getTransferStatus();

			LPDRAWITEMSTRUCT Item;
			HGDIOBJ origPen = NULL, origBrush = NULL;
			Item = (LPDRAWITEMSTRUCT)lParam;

			origPen = SelectObject(Item->hDC, GetStockObject(DC_PEN));
			origBrush = SelectObject(Item->hDC, GetStockObject(DC_BRUSH));
			switch (iStatus)
			{
			case BLINK: // blink
			{
				if (b)
					SetDCBrushColor(Item->hDC, RGB(230, 230, 230));
				else
					SetDCBrushColor(Item->hDC, RGB(127, 127, 127));
				SetDCPenColor(Item->hDC, RGB(0, 0, 0));
			}
			break;
			case GREEN: // green
				SetDCBrushColor(Item->hDC, RGB(0, 255, 0));
				SetDCPenColor(Item->hDC, RGB(0, 0, 0));
				break;
			case RED: // red
				SetDCBrushColor(Item->hDC, RGB(255, 0, 0));
				SetDCPenColor(Item->hDC, RGB(0, 0, 0));
				break;
			default: // grey
				SetDCBrushColor(Item->hDC, RGB(127, 127, 127));
				SetDCPenColor(Item->hDC, RGB(0, 0, 0));
				break;
			}
			Rectangle(Item->hDC, Item->rcItem.left, Item->rcItem.top, Item->rcItem.right, Item->rcItem.bottom);
			SelectObject(Item->hDC, origPen);
			SelectObject(Item->hDC, origBrush);
		}
		return (INT_PTR)TRUE;

	case WM_CTLCOLORSTATIC:
		// readonly edit box white background
		if ((HWND)lParam == GetDlgItem(hwndDlg, IDC_EDIT_SOCKET))
		{
			SetBkMode((HDC)wParam, TRANSPARENT);
			SetTextColor((HDC)wParam, 0x000000);
			return (LRESULT)GetStockObject(WHITE_BRUSH);
		}
		break;
		// free memory
	case WM_DESTROY:
		KillTimer(hwndDlg, IDT_TIMER1);
		KillTimer(hwndDlg, IDT_TIMER2);
		DestroyIcon((HICON)hIconOK);
		DestroyIcon((HICON)hIconCancel);
		gHwndDialogSock = NULL;
		return 0;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDCANCEL:
			EndDialog(hwndDlg, LOWORD(wParam));
			return TRUE;
		case IDC_BUTTON_SOCKET:
		case IDOK:
			EndDialog(hwndDlg, LOWORD(wParam));
			return TRUE;
		}
		break;
	case START_COUNTDOWN:
		// enable button
		EnableWindow(GetDlgItem(hwndDlg, IDC_BUTTON_SOCKET), TRUE);
		// send FAX OK
		if (pcStatus->getStatus())
		{
			if (iCountdown == 0)
			{
				MessageBeep(MB_ICONINFORMATION);
				EndDialog(hwndDlg, LOWORD(wParam));
				break;
			}
			StringCbPrintf(szSec, sizeof(szSec), L"  %s (%d)", _T(LNG_EDIT_OK), iCountdown);
			SetDlgItemText(hwndDlg, IDC_BUTTON_SOCKET, szSec);
			hIconOK = (HICON)LoadImage(hinst, MAKEINTRESOURCE(IDI_ICON_CHECK), IMAGE_ICON, 0, 0, LR_DEFAULTCOLOR);
			SendDlgItemMessage(hwndDlg, IDC_BUTTON_SOCKET, BM_SETIMAGE, (WPARAM)IMAGE_ICON, (LPARAM)hIconOK);
			SendDlgItemMessage(hwndDlg, IDC_PROGRESS1, PBM_SETPOS, SOCKET_TIMEOUT * 5, 0);
			MessageBeep(MB_ICONINFORMATION);
		}
		// can't send fax
		else
		{
			iCountdown = 30;
			StringCbPrintf(szSec, sizeof(szSec), L"  %s (%d)", _T(LNG_IDCANCEL), iCountdown);
			SetDlgItemText(hwndDlg, IDC_BUTTON_SOCKET, szSec);
			hIconCancel = (HICON)LoadImage(hinst, MAKEINTRESOURCE(IDI_ICON_CANCEL), IMAGE_ICON, 0, 0, LR_DEFAULTCOLOR);
			SendDlgItemMessage(hwndDlg, IDC_BUTTON_SOCKET, BM_SETIMAGE, (WPARAM)IMAGE_ICON, (LPARAM)hIconCancel);
			SendDlgItemMessage(hwndDlg, IDC_PROGRESS1, PBM_SETPOS, 0, 0);
			MessageBeep(MB_ICONERROR);
		}
		// send WM_TIMER with IDT_TIMER2 every 1000ms
		SetTimer(hwndDlg, IDT_TIMER2, 1000, (TIMERPROC)NULL);
		break;
	}
	return FALSE;
}
//***********************************************************
// status class
cStatus::cStatus()
{
	iServerStatus = BLINK;
	iUserStatus = GREY;
	iSocketStatus = GREY;
	iTransferStatus = GREY;
	bStatus = FALSE;
	iProgress = SOCKET_TIMEOUT * 5;
}
cStatus::~cStatus()
{
}
int cStatus::getServerStatus()
{
	return iServerStatus;
}
int cStatus::getUserStatus()
{
	return iUserStatus;
}
int cStatus::getSocketStatus()
{
	return iSocketStatus;
}
int cStatus::getTransferStatus()
{
	return iTransferStatus;
}
BOOL cStatus::getStatus()
{
	return bStatus;
}
void cStatus::setServerStatus(int s)
{
	iServerStatus = s;
}
void cStatus::setUserStatus(int s)
{
	iUserStatus = s;
}
void cStatus::setSocketStatus(int s)
{
	iSocketStatus = s;
}
void cStatus::setTransferStatus(int s)
{
	iTransferStatus = s;
}
void cStatus::setStatus(BOOL s)
{
	bStatus = s;
}
//***********************************************************
void CenterSocketDialog(HWND hwndDlg)
{
	RECT rc, rcDlg, rcOwner;
	// Get the owner window and dialog box rectangles.
	HylaUI->GetFaxDialogRect(&rcOwner);
	GetWindowRect(hwndDlg, &rcDlg);
	CopyRect(&rc, &rcOwner);

	OffsetRect(&rcDlg, -rcDlg.left, -rcDlg.top);
	OffsetRect(&rc, -rc.left, -rc.top);
	OffsetRect(&rc, -rcDlg.right, -rcDlg.bottom);

	// The new position
	SetWindowPos(hwndDlg,
		HWND_TOP,
		rcOwner.left + (rc.right / 2),
		rcOwner.top + (rc.bottom / 2),
		0, 0,          // Ignores size arguments.
		SWP_NOSIZE);
}
// *************************************************************
// add info text to text box
void AppendTextToBox(LPCWSTR text)
{
	HWND hTextBox;
	hTextBox = GetDlgItem(gHwndDialogSock, IDC_EDIT_SOCKET);

	// text length
	int outLength = GetWindowTextLength(hTextBox);
	SendMessage(hTextBox, EM_SETSEL, (WPARAM)outLength, (LPARAM)outLength);
	// insert
	SendMessage(hTextBox, EM_REPLACESEL, FALSE, (LPARAM)(text));

}
// *************************************************************
// status bar
void CreateProgressBar(HWND hDlg)
{
	SendDlgItemMessage(hDlg, IDC_PROGRESS1, PBM_SETRANGE32, 0, SOCKET_TIMEOUT * 5);
	SendDlgItemMessage(hDlg, IDC_PROGRESS1, PBM_SETPOS, SOCKET_TIMEOUT * 5, 0);
}
// *************************************************************
// show info param
void SetInfoSettings(HWND hwndDlg)
{
	// username
	SetDlgItemText(hwndDlg, IDC_EDITINFO_USERNAME, FAXSend->szUser);
	// faxnumber
	SetDlgItemText(hwndDlg, IDC_EDITINFO_FAXNUMBER, HylaUI->GetFAXNumberText());
	// email
	SetDlgItemText(hwndDlg, IDC_EDITINFO_EMAIL, HylaUI->GetEmailText());
	// notification
	SetDlgItemText(hwndDlg, IDC_EDITINFO_NOTIFICATION, FAXSend->szNotification);
	// page size
	SetDlgItemText(hwndDlg, IDC_EDITINFO_PAGESIZE, FAXSend->szPageSize);
	//resolution
	SetDlgItemText(hwndDlg, IDC_EDITINFO_RESOLUTION, FAXSend->szResolution);
	// maxdials
	SetDlgItemText(hwndDlg, IDC_EDITINFO_MAXDIALS, FAXSend->szMaxDials);
	// maxtries
	SetDlgItemText(hwndDlg, IDC_EDITINFO_MAXTRIES, FAXSend->szMaxTries);
	// logonuser
	SetDlgItemText(hwndDlg, IDC_EDITINFO_LOGGEDINUSER, UserConfigure->GetLoggedInUser());
	// identifier
	SetDlgItemText(hwndDlg, IDC_EDITINFO_IDENTIFIER, HylaUI->GetIdentifierText());
}
// countdown sec. InfoDialog after fax sending 
int GetICountdown()
{
	if (wcscmp(FAXSend->szInfoDialogTimer, L"0") == 0)
		return 0;
	else if (wcscmp(FAXSend->szInfoDialogTimer, L"3") == 0)
		return 3;
	else if (wcscmp(FAXSend->szInfoDialogTimer, L"5") == 0)
		return 5;
	else if (wcscmp(FAXSend->szInfoDialogTimer, L"10") == 0)
		return 10;
	else
		return 3;
}
//***********************************************************
//***********************************************************
