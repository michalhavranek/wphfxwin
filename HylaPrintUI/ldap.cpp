#include "stdafx.h"
#include "ldap.h"
#include "HylaPrintUI.h"

CRITICAL_SECTION HylaUICritSection;
HANDLE ghHylaUISemaphore;
HANDLE hLDAPSearch;

struct S
{
	WCHAR a1[STRVALUE_TEXT_256];
	WCHAR a2[STRVALUE_TEXT_64];
};

// *****************************************************************************
// new thread LDAP search
unsigned __stdcall MyLDAPSearch(LPVOID lpvParam)
{
	LDAPMessage* pEntry = NULL;
	ULONG iCnt = 0;
	BerElement* pBer = nullptr;
	PWSTR pAttribute = NULL;
	PWCHAR* ppValue = nullptr;
	ULONG iValue = 0;
	WCHAR szText[STRVALUE_TEXT_256 + STRVALUE_TEXT_64 + 1];
	ULONG errorCode = LDAP_SUCCESS;
	LDAPMessage* pSearchResult;
	PWSTR pMyAttributes[3];
	ULONG numberOfEntries;
	std::wstring pMyFilter;
	size_t pos = 0;
	UNREFERENCED_PARAMETER(lpvParam);
	LRESULT lrTextLen = 0;
	WCHAR szFaxNumber[STRVALUE_FAXNUMBER];
	BOOL sFlag;
	DWORD dw;

	// Perform a synchronous search 
	pMyAttributes[0] = FAXSend->szLDAPAutocompleteAttribute;
	pMyAttributes[1] = L"facsimileTelephoneNumber";
	pMyAttributes[2] = NULL;

	while (!iLDAPLoopReturn)
	{
		pMyFilter = FAXSend->szLDAPFilter;
		EnterCriticalSection(&HylaUICritSection);
		sFlag = HylaUI->GetLDAPSearchFlag();
		LeaveCriticalSection(&HylaUICritSection);
		if (!sFlag) // nothing to search
			WaitForSingleObject(ghHylaUISemaphore, INFINITE);

		if (iLDAPLoopReturn)
		{
			_endthreadex(0);
			return 0;
		}
		EnterCriticalSection(&HylaUICritSection);
		HylaUI->SetLDAPSearchFlag(FALSE);
		StringCchCopy(szFaxNumber, STRVALUE_FAXNUMBER, HylaUI->GetFAXNumberText());
		LeaveCriticalSection(&HylaUICritSection);

		pos = 0;
		while (true)
		{
			/* Locate the substring to replace. */
			pos = pMyFilter.find(L"%s", pos);
			if (pos == std::string::npos) break;
			/* Make the replacement. */
			pMyFilter.replace(pos, 2, szFaxNumber);
			/* Advance index forward so the next iteration doesn't pick it up as well. */
			pos += lstrlen(szFaxNumber);
		}

		errorCode = ldap_search_s(
			HylaUI->GetPointerLDAPHandle(),        // Session handle
			FAXSend->szLDAPBaseDN,  // DN to start search
			LDAP_SCOPE_SUBTREE,     // Scope
			(PWSTR)pMyFilter.c_str(),  // Filter
			pMyAttributes,      // Retrieve list of attributes
			0,                  // Get both attributes and values
			&pSearchResult);    // [out] Search results

		if (errorCode != LDAP_SUCCESS)
		{
			// failed
			if (pSearchResult != NULL)
				ldap_msgfree(pSearchResult);
			continue;
		}
		// Get the number of entries returned.
		numberOfEntries = ldap_count_entries(
			HylaUI->GetPointerLDAPHandle(),    // Session handle
			pSearchResult);     // Search result

		if (numberOfEntries == NULL)
		{
			// failed
			if (pSearchResult != NULL)
				ldap_msgfree(pSearchResult);
			continue;
		}

		// Loop through the search entries, get, and output the
		// requested list of attributes and values.
		struct S* s = (struct S*) malloc(sizeof(struct S) * numberOfEntries);
		if (s != NULL)
			memset(s, 0, sizeof(struct S) * numberOfEntries);
		else // fail malloc
			numberOfEntries = 0;

		for (iCnt = 0; iCnt < numberOfEntries; iCnt++)
		{
			// Get the first/next entry.
			if (!iCnt)
				pEntry = ldap_first_entry(HylaUI->GetPointerLDAPHandle(), pSearchResult);
			else
				pEntry = ldap_next_entry(HylaUI->GetPointerLDAPHandle(), pEntry);

			if (pEntry == NULL)
			{
				// failed
				ldap_msgfree(pSearchResult);
				continue;
			}

			// Get the first attribute name.
			pAttribute = ldap_first_attribute(
				HylaUI->GetPointerLDAPHandle(),   // Session handle
				pEntry,            // Current entry
				&pBer);            // [out] Current BerElement

			// Output the attribute names for the current object
			// and output values.
			while (pAttribute != NULL)
			{
				// Get the string values.
				ppValue = ldap_get_values(
					HylaUI->GetPointerLDAPHandle(),  // Session Handle
					pEntry,           // Current entry
					pAttribute);      // Current attribute

				if (ppValue != NULL)
				{
					iValue = ldap_count_values(ppValue);
					if (iValue)
					{
						// Output the first attribute value
						if (_wcsicmp(pAttribute, pMyAttributes[0]) == 0)
						{
							wcscpy_s(s[iCnt].a1, STRVALUE_TEXT_256, *ppValue);
						}
						if (_wcsicmp(pAttribute, pMyAttributes[1]) == 0)
						{
							wcscpy_s(s[iCnt].a2, STRVALUE_TEXT_64, *ppValue);
						}
					}
				}
				// Free memory.
				if (ppValue != NULL)
					ldap_value_free(ppValue);
				ppValue = NULL;
				ldap_memfree(pAttribute);

				// Get next attribute name.
				pAttribute = ldap_next_attribute(
					HylaUI->GetPointerLDAPHandle(),   // Session Handle
					pEntry,            // Current entry
					pBer);             // Current BerElement
			}

			if (pBer != NULL)
				ber_free(pBer, 0);
			pBer = NULL;
		}
		EnterCriticalSection(&HylaUICritSection);
		for (size_t i = 0; i < numberOfEntries; i++)
		{

			StringCbPrintf(szText, sizeof(szText), L"%s <%s>", s[i].a1, s[i].a2);
			pAuto->AddString(szText);
		}
		LeaveCriticalSection(&HylaUICritSection);
		if (s != NULL)
			free(s);

		//----------------------------------------------------------
		// Normal cleanup and exit.
		//----------------------------------------------------------
		ldap_msgfree(pSearchResult);
		ldap_value_free(ppValue);

		lrTextLen = SendMessage(GetDlgItem(HylaUI->GetHandleDLG(), IDC_EDIT_FAXNUMBER), WM_GETTEXTLENGTH, 0, 0);
		if (lrTextLen == 0)
		{
			EnterCriticalSection(&HylaUICritSection);
			pAuto->DeleteAllStrings();
			LeaveCriticalSection(&HylaUICritSection);
			continue;
		}
		EnterCriticalSection(&HylaUICritSection);
		sFlag = HylaUI->GetLDAPSearchFlag();
		LeaveCriticalSection(&HylaUICritSection);
		if (!sFlag) // nothing to search - show autocomplete
		{
			SendMessage(GetDlgItem(HylaUI->GetHandleDLG(), IDC_EDIT_FAXNUMBER), EM_GETSEL, (WPARAM)& dw, NULL);
			SendMessage(GetDlgItem(HylaUI->GetHandleDLG(), IDC_EDIT_FAXNUMBER), WM_KEYDOWN, VK_RETURN, 0);
			SendMessage(GetDlgItem(HylaUI->GetHandleDLG(), IDC_EDIT_FAXNUMBER), EM_SETSEL, (WPARAM)dw, (LPARAM)dw);
			SendMessage(GetDlgItem(HylaUI->GetHandleDLG(), IDC_EDIT_FAXNUMBER), WM_KEYDOWN, VK_DOWN, 0);
		}
	} // infinite loop - while (!iLDAPLoopReturn)

	_endthreadex(0);
	return 0;
}
// *****************************************************************************
// new thread LDAP connect function
void HylaLDAPConnect(void* pPar)
{
	LDAP* pLdapConnection = nullptr;
	int iPort;
	ULONG lRtn = 0;
	ULONG version = LDAP_VERSION3;
	ULONG numReturns = 100;
	size_t lv = 0;
	unsigned threadID;
	UNREFERENCED_PARAMETER(pPar);

	// init LDAP
	iPort = _wtoi(FAXSend->szLDAPPort);
	if (iPort > 65535 || iPort < 0)
	{
		if (!FAXSend->dwLDAP_SSL) // no SSL
			iPort = LDAP_PORT;
		else
			iPort = LDAP_SSL_PORT; // SSL
	}

	if (!FAXSend->dwLDAP_SSL) // no SSL
		pLdapConnection = ldap_init(FAXSend->szLDAPServer, iPort);
	else
		pLdapConnection = ldap_sslinit(FAXSend->szLDAPServer, iPort, 1); // SSL

	if (pLdapConnection == NULL)
	{
		//failed
		ldap_unbind(pLdapConnection);
		HylaUI->SetPointerLDAPHandle(NULL);
		_endthread();
		return;
	}

	// Set session options
	// Set the version to 3.0 (default is 2.0)
	lRtn = ldap_set_option(
		pLdapConnection,           // Session handle
		LDAP_OPT_PROTOCOL_VERSION, // Option
		(void*)& version);         // Option value

	if (lRtn != LDAP_SUCCESS)
	{
		// Error
		ldap_unbind(pLdapConnection);
		HylaUI->SetPointerLDAPHandle(NULL);
		_endthread();
		return;
	}
	// Set the limit on the number of entries returned.
	lRtn = ldap_set_option(
		pLdapConnection,       // Session handle
		LDAP_OPT_SIZELIMIT,    // Option
		(void*)& numReturns);  // Option value

	if (lRtn != LDAP_SUCCESS)
	{
		// Error
		ldap_unbind(pLdapConnection);
		HylaUI->SetPointerLDAPHandle(NULL);
		_endthread();
		return;
	}
	// StartTLS
	if (FAXSend->dwLDAP_StartTLS)
	{
		lRtn = ldap_start_tls_s(pLdapConnection, NULL, NULL, NULL, NULL);
		if (lRtn != LDAP_SUCCESS)
		{
			// Error
			ldap_unbind(pLdapConnection);
			HylaUI->SetPointerLDAPHandle(NULL);
			_endthread();
			return;
		}
	}
	// set SSL
	if (FAXSend->dwLDAP_SSL)
	{
		lRtn = ldap_get_option(pLdapConnection, LDAP_OPT_SSL, (void*)& lv);
		if (lRtn != LDAP_SUCCESS)
		{
			// Error
			ldap_unbind(pLdapConnection);
			HylaUI->SetPointerLDAPHandle(NULL);
			_endthread();
			return;
		}
		if ((void*)lv == LDAP_OPT_OFF)
			lRtn = ldap_set_option(pLdapConnection, LDAP_OPT_SSL, LDAP_OPT_ON);
		if (lRtn != LDAP_SUCCESS)
		{
			// Error
			ldap_unbind(pLdapConnection);
			HylaUI->SetPointerLDAPHandle(NULL);
			_endthread();
			return;
		}
	}
	//--------------------------------------------------------
	// Connect to the server.
	//--------------------------------------------------------

	lRtn = ldap_connect(pLdapConnection, NULL);
	if (lRtn != LDAP_SUCCESS)
	{
		// failed
		// printf("ldap_connect failed with 0x%x.\n", lRtn);
		ldap_unbind(pLdapConnection);
		HylaUI->SetPointerLDAPHandle(NULL);
		_endthread();
		return;
	}

	//--------------------------------------------------------
	// Bind with credentials.
	//--------------------------------------------------------

	lRtn = ldap_bind_s(
		pLdapConnection,         // Session Handle
		FAXSend->szLDAPBindDN,   // Domain DN
		FAXSend->szLDAPPassword, // Credential structure (only password)
		LDAP_AUTH_SIMPLE);       // Auth mode
	if (lRtn != LDAP_SUCCESS)
	{
		// failed
		ldap_unbind(pLdapConnection);
		HylaUI->SetPointerLDAPHandle(NULL);
		_endthread();
		return;
	}
	HylaUI->SetPointerLDAPHandle(pLdapConnection);
	// new thread LDAP search
	hLDAPSearch = (HANDLE)_beginthreadex(NULL, 0, &MyLDAPSearch, NULL, 0, &threadID);
	// color edit
	InvalidateRect(GetDlgItem(HylaUI->GetHandleDLG(), IDC_EDIT_FAXNUMBER), NULL, TRUE);
	_endthread();
}
//--------------------------------------------------------
