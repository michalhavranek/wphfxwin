#include "stdafx.h"
#include "edit_add_item.h"
#include "../HylaPrint/constants.h"
#include "HylaPrintUI.h"
#include "lang_UI.h"

// add or edit item listbox
LRESULT CALLBACK EditAddItem(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	WCHAR szAddrBookNumber[STRVALUE_NUMBER];
	WCHAR szAddrBookName[STRVALUE_ADDRBOOK_NAME];
	WCHAR szBuffer[STRVALUE_TEXT_512];
	LRESULT textLength1, textLength2;
	static HICON hIconOK, hIconCancel;

	switch (message)
	{

	case WM_INITDIALOG:
		hIconOK = (HICON)LoadImage(hinst, MAKEINTRESOURCE(IDI_ICON_CHECK), IMAGE_ICON, 0, 0, LR_DEFAULTCOLOR);
		SendDlgItemMessage(hDlg, IDOK, BM_SETIMAGE, (WPARAM)IMAGE_ICON, (LPARAM)hIconOK);
		hIconCancel = (HICON)LoadImage(hinst, MAKEINTRESOURCE(IDI_ICON_CANCEL), IMAGE_ICON, 0, 0, LR_DEFAULTCOLOR);
		SendDlgItemMessage(hDlg, LANG_EDITADDRBOOK_IDCANCEL, BM_SETIMAGE, (WPARAM)IMAGE_ICON, (LPARAM)hIconCancel);
		// only edit
		if (!HylaUI->GetNewItem() && HylaUI->GetItemIndex() >= 0)
		{
			// name
			HylaUI->GetPhoneBookName((int)HylaUI->GetItemIndex(), szAddrBookName);
			SetDlgItemText(hDlg, IDC_EDIT_ADDRBOOK_NAME, szAddrBookName);
			// number
			HylaUI->GetPhoneBookNumber((int)HylaUI->GetItemIndex(), szAddrBookNumber);
			SetDlgItemText(hDlg, IDC_EDIT_ADDRBOOK_NUM, szAddrBookNumber);
		}
		// add new item
		else
		{
			SetDlgItemText(hDlg, IDC_EDIT_ADDRBOOK_NUM, HylaUI->GetFAXNumberText());
		}
		LocalDialogAddEdit(hDlg);
		return TRUE;
		// free memory
	case WM_DESTROY:
		DestroyIcon(hIconOK);
		DestroyIcon(hIconCancel);
		return 0;

	case WM_COMMAND:
		switch LOWORD(wParam)
		{
		case IDOK:
			textLength1 = SendMessage(GetDlgItem(hDlg, IDC_EDIT_ADDRBOOK_NAME), EM_LINELENGTH, 0, 0);
			textLength2 = SendMessage(GetDlgItem(hDlg, IDC_EDIT_ADDRBOOK_NUM), EM_LINELENGTH, 0, 0);
			// missing name or number
			if (textLength1 == 0 || textLength2 == 0)
			{
				EndDialog(hDlg, IDCANCEL);
				return TRUE;
			}

			WriteTextItemEBSend(GetDlgItem(hDlg, IDC_EDIT_ADDRBOOK_NAME), szAddrBookName, FALSE, STRVALUE_ADDRBOOK_NAME); // name
			WriteTextItemEBSend(GetDlgItem(hDlg, IDC_EDIT_ADDRBOOK_NUM), szAddrBookNumber, FALSE, STRVALUE_NUMBER); // number

			if (!HylaUI->GetNewItem()) // edit
			{
				HylaUI->DeletePhoneBookItem((int)HylaUI->GetItemIndex());
				StringCbPrintf(szBuffer, sizeof(szBuffer), L"%s;%s", szAddrBookName, szAddrBookNumber);
				HylaUI->AddPhonebookItem(szBuffer);
			}
			// add new item
			else
			{
				StringCbPrintf(szBuffer, sizeof(szBuffer), L"%s;%s", szAddrBookName, szAddrBookNumber);
				HylaUI->AddPhonebookItem(szBuffer);
				ListView_SetItemCount(GetDlgItem(HylaUI->GetHandleDLG(), IDC_LIST_ADDRESS_BOOK), (int)HylaUI->GetPBVectorSize());
			}
			HylaUI->SortPBVector();
			StringCbPrintf(szBuffer, sizeof(szBuffer), L"%s <%s>", szAddrBookName, szAddrBookNumber);
			SetDlgItemText(HylaUI->GetHandleDLG(), IDC_EDIT_FAXNUMBER, szBuffer); // set faxnumber editbox
			EndDialog(hDlg, LOWORD(wParam));
			return TRUE;

		case LANG_EDITADDRBOOK_IDCANCEL:
			EndDialog(hDlg, LOWORD(wParam));
			return TRUE;

		case IDCANCEL:
			EndDialog(hDlg, LOWORD(wParam));
			return TRUE;
		}
		break;

	}
	return FALSE;

	UNREFERENCED_PARAMETER(lParam);
}
//---------------------------------------------------------------------------
