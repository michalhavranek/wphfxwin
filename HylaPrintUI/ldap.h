#ifndef LdapH
#define LdapH

#include "../HylaPrint/SendFAX.h"

extern CRITICAL_SECTION HylaUICritSection;
extern HANDLE ghHylaUISemaphore;
extern HANDLE hLDAPSearch;

unsigned __stdcall MyLDAPSearch(LPVOID lpvParam);
void HylaLDAPConnect(void* pPar);

#endif
