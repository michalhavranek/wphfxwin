//---------------------------------------------------------------------------
#ifndef AutoCompleteH
#define AutoCompleteH

// autocomplete class
class CEnumString : public IEnumString
{
public:
	CEnumString();
	virtual ~CEnumString();

	//IUnKnow
	virtual HRESULT __stdcall QueryInterface(REFIID riid, void** ppvObject);
	virtual ULONG __stdcall Release();
	virtual ULONG __stdcall AddRef();

	//IEnumString interface 
	virtual HRESULT __stdcall Next(ULONG celt, LPOLESTR* rgelt, ULONG* pceltFetched);
	virtual HRESULT __stdcall Reset();
	virtual HRESULT __stdcall Skip(ULONG celt);
	virtual HRESULT __stdcall Clone(IEnumString** ppEnum);

	int AddString(LPCWSTR szStr);
	void DeleteAllStrings();
	HRESULT Copy(LPOLESTR* p1, LPOLESTR* p2);

protected:
	std::vector<std::wstring> m_vector;
	std::vector<std::wstring>::size_type iCur;

private:
	ULONG iRef;
};

void initAuto(HWND hEdit, CEnumString* pAuto);

#endif
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
