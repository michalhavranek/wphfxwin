#ifndef socketDialogH
#define socketDialogH

#include "resource.h"
#include "../HylaPrint/constants.h"

INT_PTR CALLBACK SocketsDialog(HWND, UINT, WPARAM, LPARAM);
unsigned __stdcall SocketsDialogThread(LPVOID lpvParam);
void CenterSocketDialog(HWND hwndDlg);
void AppendTextToBox(LPCWSTR text);
void CreateProgressBar(HWND hDlg);
void SetInfoSettings(HWND hwndDlg);
int GetICountdown();

class cStatus
{
public:
	cStatus();
	~cStatus();
	int getProgress() { return iProgress; }
	int getServerStatus();
	int getUserStatus();
	int getSocketStatus();
	int getTransferStatus();
	BOOL getStatus();
	void resetProgress() { iProgress = SOCKET_TIMEOUT * 5; }
	void decProgress() { iProgress--; }
	void setServerStatus(int s);
	void setUserStatus(int s);
	void setSocketStatus(int s);
	void setTransferStatus(int s);
	void setStatus(BOOL s);
	//  0 - grey, 1 - blink, 2 - green, 3 - red  
private:
	int iServerStatus;
	int iUserStatus;
	int iSocketStatus;
	int iTransferStatus;
	BOOL bStatus;
	int iProgress;
};
extern cStatus* pcStatus;

#endif