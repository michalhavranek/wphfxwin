#ifndef langUI_H
#define langUI_H

void LocalDialogSend(HWND hdlg, LPCWSTR szDirPSFile);
void LocalDialogAddEdit(HWND hdlg);
void LocalMenuLB(HMENU menu);
void LocalDialogSocket(HWND hdlg, BOOL bActiveMode);
void LocalDialog_UserSettings(HWND hdlg);
void LocalDialog_UserSettingsItems(HWND hdlg);
int GetPagesPrinted(LPCWSTR filename);

#endif
