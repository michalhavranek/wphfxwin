#ifndef LangUserSettingsH
#define LangUserSettingsH

// included function body into cpp file
// ******************************************************************
void LocalDialog_UserSettingsItems(HWND hdlg)
{
	// username
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_USERNAME), _T(LNG_USERNAME));
	// password
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_PASSWORD), _T(LNG_PASSWORD));
	// email
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_EMAIL), _T(LNG_EMAIL));
	// notification
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_NOTIFICATION), _T(LNG_NOTIFICATION));
	// modem
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_MODEM), _T(LNG_MODEM));
	// address book
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_ADDRBOOK), _T(LNG_ADDRBOOK_DIR));
	// page size
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_PAGESIZE), _T(LNG_PAGESIZE));
	// resolution
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_RESOLUTION), _T(LNG_RESOLUTION));
	// maxdials
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_MAXDIALS), _T(LNG_MAXDIALS));
	// maxtries
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_MAXTRIES), _T(LNG_MAXTRIES));
	// infodialog timer
	SetWindowText(GetDlgItem(hdlg, IDC_LANG_STATIC_INFODIALOG_TIMER), _T(LNG_INFODIALOG_TIMER));
	// use faxnumber from the clipboard
	SetWindowText(GetDlgItem(hdlg, IDC_CHECK_FAXNUM_CLIPBOARD), _T(LNG_FAXNUMBER_FROM_CLIPBOARD));

}

// ******************************************************************
#endif
