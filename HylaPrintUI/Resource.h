//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by HylaPrintUI.rc
//
#define IDI_ICON_FAX32_16               2
#define IDD_HYLAPRINTUI_DIALOG          102
#define LANG_SENDFAX_DIALOG             103
#define IDM_ABOUT                       104
#define IDC_HYLAPRINTUI                 109
#define IDR_MAINFRAME                   128
#define IDR_MENU_LV                     132
#define LANG_EDIT_DIALOG_ADDRBOOK_ITEM  133
#define IDI_ICON_FAX16                  135
#define IDI_ICON_CHECK                  137
#define IDI_ICON_CANCEL                 138
#define IDI_ICON_USER                   140
#define IDI_ICON_TRANSFER               141
#define IDD_DIALOG_SOCKETS              142
#define IDI_ICON_SOCKET                 143
#define IDR_SYSMENU_SETTINGS            144
#define IDD_DIALOG_USER_SETTINGS        145
#define IDI_ICON_SERVER                 151
#define IDI_ICON_DOCUMENT               152
#define IDR_SYSMENU_WINDOWPOSITION      153
#define IDC_CANCEL                      1000
#define IDC_EDIT_FAXNUMBER              1001
#define IDC_EDIT_NOTIFEMAIL             1002
#define IDC_EDIT_ADDRBOOK_NUM           1006
#define IDC_EDIT_ADDRBOOK_NAME          1007
#define LANG_SEND_FAX                   1008
#define LANG_IDCANCEL                   1009
#define LANG_EDITADDRBOOK_IDCANCEL      1014
#define LANG_EDIT_STATIC_NAME           1015
#define LANG_EDIT_STATIC_NUMBER         1016
#define IDC_LIST_ADDRESS_BOOK           1017
#define IDC_LANG_STATIC_ADDRESSBOOK     1018
#define IDC_ICON_SERVER                 1020
#define IDC_ICON_USER                   1021
#define IDC_ICON_TRANSFER               1022
#define IDC_EDIT_SOCKET                 1023
#define IDC_BUTTON_SOCKET               1024
#define IDC_SIG_SERVER                  1025
#define IDC_SIG_USER                    1026
#define IDC_SIG_TRANSFER                1027
#define IDC_ICON_SOCKET                 1028
#define IDC_SIG_SOCKET                  1029
#define IDC_CHCK_SETTINGS_USESETTINGS   1030
#define IDC_EDIT_CONFUSERNAME           1031
#define IDC_EDIT_CONFPASSWORD           1032
#define IDC_EDIT_CONFDEF_NOTIFEMAIL     1033
#define IDC_COMBO_CONFNOTIF_TYP         1034
#define IDC_EDIT_CONFMODEM              1035
#define IDC_BTN_SETTINGS_BROWSE         1036
#define IDC_EDIT_CONFBOOK_DIR           1037
#define IDC_COMBO_CONFPAGESIZE          1038
#define IDC_COMBO_CONFRESOLUTION        1039
#define IDC_BTN_SETTINGS_LOAD           1040
#define IDC_LANG_STATIC_PASSWORD        1042
#define IDC_LANG_STATIC_EMAIL           1043
#define IDC_LANG_STATIC_MODEM           1045
#define IDC_LANG_STATIC_ADDRBOOK        1046
#define IDC_LANG_STATIC_PAGESIZE        1047
#define IDC_LANG_STATIC_RESOLUTION      1048
#define IDC_PROGRESS1                   1049
#define IDC_ICON_TIMER                  1050
#define IDC_EDITINFO_SERVER             1051
#define IDC_EDITINFO_USERNAME           1052
#define IDC_EDITINFO_EMAIL              1053
#define IDC_EDITINFO_NOTIFICATION       1054
#define IDC_EDITINFO_PAGESIZE           1055
#define IDC_EDITINFO_RESOLUTION         1056
#define IDC_EDITINFO_FAXNUMBER          1057
#define IDC_LANG_STATIC_USERNAME        1061
#define IDC_LANG_STATIC_NOTIFICATION    1062
#define IDC_LANG_STATIC_FAXNUMBER       1063
#define IDC_LANG_STATIC_EMAILNOTIFY     1064
#define IDC_LANG_STATIC_SERVER          1065
#define IDC_MFCLINK1                    1066
#define IDC_COMBO_MAXDIALS              1067
#define IDC_COMBO_MAXTRIES              1068
#define IDC_LANG_STATIC_MAXDIALS        1069
#define IDC_LANG_STATIC_MAXTRIES        1070
#define IDC_EDITINFO_MAXDIALS           1071
#define IDC_EDITINFO_MAXTRIES           1072
#define IDC_COMBO_CONFINFODIALOG_TIMER  1073
#define IDC_LANG_STATIC_INFODIALOG_TIMER 1074
#define IDC_EDIT_IDENTIFIER             1075
#define IDC_LANG_STATIC_IDENTIFIER      1076
#define IDC_EDITINFO_LOGGEDINUSER       1077
#define IDC_EDITINFO_IDENTIFIER         1078
#define IDC_LANG_STATIC_LOGGEDINUSER    1079
#define IDC_CHECK_FAXNUM_CLIPBOARD      1094
#define LANG_ID_MENULV_EDIT             32781
#define LANG_ID_MENULV_DELETE           32782
#define LANG_ID_MENULV_ADD              32783
#define LANG_ID_SYSMENU_SETTINGS        32785
#define ID_WINDOWPOSITION_CENTER        32790
#define ID_WINDOWPOSITION_REMEMBERPOSITION 32791
#define ID_WINDOWPOSITION_CURSORPOSITION 32792
#define ID_SYSMENU_USER_SETTINGS        32793
#define LANG_ID_WINDOWPOSITION_CENTER   32802
#define LANG_ID_WINDOWPOSITION_REMEMBERPOSITION 32803
#define LANG_ID_WINDOWPOSITION_CURSORPOSITION 32804
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        154
#define _APS_NEXT_COMMAND_VALUE         32805
#define _APS_NEXT_CONTROL_VALUE         1080
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
