// HylaPrintUI.cpp : Defines the entry point for the application.
//
#include "stdafx.h"
#include "HylaPrintUI.h"
#include "edit_add_item.h"
#include "../HylaPrint/SendFAX.h"
#include "hyla_socket.h"
#include "lang_UI.h"
#include "sockets_dialog.h"
#include "user_settings.h"
#include "../HylaPrint/center_window.h"
#include "../HylaPrint/session_id.h"
#include "ldap.h"
#include "../HylaPrint/config_class.h"
#include "../Language/language.h"
#include "../HylaPrint/system_message.h"

// Forward declarations of global variables included in this code module:
cStatus* pcStatus;
struct TFAXSend* FAXSend;
CHylaUI* HylaUI;
WNDPROC oldProc;
CEnumString* pAuto;
int iLDAPLoopReturn;

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR lpCmdLine,
	_In_ int nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);
	UNREFERENCED_PARAMETER(nCmdShow);
	hinst = hInstance;
	INITCOMMONCONTROLSEX icc;
	icc.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icc.dwICC = ICC_WIN95_CLASSES;
	if (!InitCommonControlsEx(&icc))
		return 0;

	HylaUI = nullptr;
	HylaUI = new CHylaUI();
	CoInitialize(NULL);
	DialogBox(hInstance, MAKEINTRESOURCE(LANG_SENDFAX_DIALOG), NULL, (DLGPROC)SendFax);
	CoUninitialize();
	delete HylaUI; // no need check nullptr
	HylaUI = nullptr;
	return FALSE;
}

// Message handler for about box.
INT_PTR CALLBACK SendFax(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	static HWND hEditFaxNum;
	static HICON hIconOK, hIconCancel, hIconDoc;
	static WCHAR* chBuf;
	WCHAR szAddrBookNumber[STRVALUE_NUMBER];
	WCHAR szAddrBookName[STRVALUE_ADDRBOOK_NAME];
	LRESULT curSel;
	HANDLE hFile;
	WCHAR szFaxNumber[STRVALUE_FAXNUMBER];
	DWORD dwUserMaxLength = STRVALUE_USER;

	switch (message)
	{
	case WM_INITDIALOG:
		gHwndDialogSock = NULL;
		FAXSend = nullptr;
		pAuto = nullptr;
		chBuf = nullptr;
		hLDAPSearch = NULL;
		iLDAPLoopReturn = 0;
		ghHylaUISemaphore = NULL;
		UserConfigure = nullptr;
		UserConfigure = new CUserConfigure();
		// ********************************************************
		// detect IPC, allocate chBuf, and allocate FAXSend from chBuf
		if (!DetectIPCClient(chBuf))
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		if (!FAXSend)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		// Sleep(20000);
		// load single user data from XML
		UserConfigure->LoadXML();
		GetUserName(UserConfigure->GetLoggedInUser(), &dwUserMaxLength);
		HylaUI->SetHandleDLG(hDlg);
		// icon application
		SetClassLongPtr(hDlg, GCLP_HICONSM, (LONG_PTR)LoadIcon(hinst, MAKEINTRESOURCE(IDI_ICON_FAX16)));
		SetClassLongPtr(hDlg, GCLP_HICON, (LONG_PTR)LoadIcon(hinst, MAKEINTRESOURCE(IDI_ICON_FAX32_16)));
		// set dialog lang
		LocalDialogSend(hDlg, FAXSend->szFileName);
		// email text
		if (!UserConfigure->GetUseUserConfig()) // default
			SetWindowText(GetDlgItem(hDlg, IDC_EDIT_NOTIFEMAIL), FAXSend->szEmail);
		else // single user
			SetWindowText(GetDlgItem(hDlg, IDC_EDIT_NOTIFEMAIL), UserConfigure->GetEmailText());
		// LV
		InitListViewColumns();
		// statusbar
		hIconDoc = (HICON)LoadImage(hinst, MAKEINTRESOURCE(IDI_ICON_DOCUMENT), IMAGE_ICON, 0, 0, LR_DEFAULTCOLOR);
		CreateStatusBar(hDlg, hIconDoc);
		// address book
		InitAddrbookPath();
		// sysmenu
		ModifySysmenu(hDlg);
		// icon button
		hIconOK = (HICON)LoadImage(hinst, MAKEINTRESOURCE(IDI_ICON_CHECK), IMAGE_ICON, 0, 0, LR_DEFAULTCOLOR);
		SendDlgItemMessage(hDlg, LANG_SEND_FAX, BM_SETIMAGE, (WPARAM)IMAGE_ICON, (LPARAM)hIconOK);
		hIconCancel = (HICON)LoadImage(hinst, MAKEINTRESOURCE(IDI_ICON_CANCEL), IMAGE_ICON, 0, 0, LR_DEFAULTCOLOR);
		SendDlgItemMessage(hDlg, LANG_IDCANCEL, BM_SETIMAGE, (WPARAM)IMAGE_ICON, (LPARAM)hIconCancel);
		// position of the dialog
		SetDialogPosition(hDlg);
		// foreground dialog SendFax 
		SetForegroundDialog(hDlg);
		hEditFaxNum = GetDlgItem(hDlg, IDC_EDIT_FAXNUMBER);
		// autocomplete
		pAuto = new CEnumString; // no delete
		initAuto(hEditFaxNum, pAuto);
		// subclass text box faxnumber
		oldProc = (WNDPROC)SetWindowLongPtr(hEditFaxNum, GWLP_WNDPROC, (LONG_PTR)WindowProcEdit);
		PostMessage(hDlg, WM_NEXTDLGCTL, (WPARAM)hEditFaxNum, TRUE); // set focus
		// LDAP
		ghHylaUISemaphore = CreateSemaphore(NULL, 0, 1, NULL);
		InitializeCriticalSection(&HylaUICritSection); // autocomplete
		if (FAXSend->dwUseLDAP)
			_beginthread(HylaLDAPConnect, 0, NULL);

		SetClipBoardFaxNumber(hDlg); // must be after InitializeCriticalSection
		return (INT_PTR)TRUE;

	case WM_SYSCOMMAND:
		switch (wParam)
		{
		case IDR_SYSMENU_SETTINGS:
			if (CallConfigDialog(hDlg) != 1)
				break;
			// single user
			if (UserConfigure->GetUseUserConfig() && wcslen(UserConfigure->GetAddressBookText()) > 2)
			{
				wcsncat_s(UserConfigure->GetAddressBookText(), MAX_PATH,
					L"\\addrbook.csv", _TRUNCATE);
				ReadAddrBook(UserConfigure->GetAddressBookText());
			}
			// system-wide
			else if (!UserConfigure->GetUseUserConfig() && wcslen(FAXSend->szAddrBookDir) > 2)
				ReadAddrBook(FAXSend->szAddrBookDir);
			else
			{
				HylaUI->ResetPBVector();
				ListView_SetItemCount(GetDlgItem(HylaUI->GetHandleDLG(), IDC_LIST_ADDRESS_BOOK), 0);
			}
			break;
			// systemmenu radioitems 
		case LANG_ID_WINDOWPOSITION_CENTER:
			CheckMenuRadioItem(GetSystemMenu(hDlg, FALSE), LANG_ID_WINDOWPOSITION_CENTER,
				LANG_ID_WINDOWPOSITION_CURSORPOSITION, LANG_ID_WINDOWPOSITION_CENTER, MF_BYCOMMAND);
			break;

		case LANG_ID_WINDOWPOSITION_REMEMBERPOSITION:
			CheckMenuRadioItem(GetSystemMenu(hDlg, FALSE), LANG_ID_WINDOWPOSITION_CENTER,
				LANG_ID_WINDOWPOSITION_CURSORPOSITION, LANG_ID_WINDOWPOSITION_REMEMBERPOSITION, MF_BYCOMMAND);
			break;

		case LANG_ID_WINDOWPOSITION_CURSORPOSITION:
			CheckMenuRadioItem(GetSystemMenu(hDlg, FALSE), LANG_ID_WINDOWPOSITION_CENTER,
				LANG_ID_WINDOWPOSITION_CURSORPOSITION, LANG_ID_WINDOWPOSITION_CURSORPOSITION, MF_BYCOMMAND);
			break;
		}
		break;
	case WM_NOTIFY:
		LPNMHDR	pnmh;
		pnmh = (LPNMHDR)lParam;
		switch (pnmh->code)
		{
		case LVN_GETDISPINFO: // virtual LV, add all items which was set by SetItemCount
		{
			NMLVDISPINFO* plvdi = (NMLVDISPINFO*)lParam;
			if (plvdi->item.iSubItem == 0)
			{
				if (plvdi->item.mask & LVIF_TEXT)
				{
					HylaUI->GetPhoneBookName(plvdi->item.iItem, szAddrBookName);
					wcsncpy_s(plvdi->item.pszText, plvdi->item.cchTextMax,
						szAddrBookName, (size_t)(plvdi->item.cchTextMax) - 1);
				}
			}
		}
		break;
		case NM_RCLICK:
		{
			// right click LV
			if (pnmh->idFrom == IDC_LIST_ADDRESS_BOOK)
			{
				if (UserConfigure->GetUseUserConfig() && wcslen(UserConfigure->GetAddressBookText()) > 2)
					LVMenu(hDlg);
				if (!UserConfigure->GetUseUserConfig() && wcslen(FAXSend->szAddrBookDir) > 2)
					LVMenu(hDlg);
			}
		}
		break;
		case LVN_ITEMCHANGED: // select item
		{
			if (pnmh->idFrom == IDC_LIST_ADDRESS_BOOK)
			{
				curSel = ListView_GetNextItem(GetDlgItem(HylaUI->GetHandleDLG(), IDC_LIST_ADDRESS_BOOK), -1, LVNI_SELECTED);
				if (curSel >= 0)
				{
					HylaUI->GetPhoneBookName((int)curSel, szAddrBookName);
					HylaUI->GetPhoneBookNumber((int)curSel, szAddrBookNumber);
					StringCbPrintf(szFaxNumber, sizeof(szFaxNumber), L"%s <%s>", szAddrBookName, szAddrBookNumber);
					SetWindowText(hEditFaxNum, szFaxNumber);
				}
			}
		}
		break;
		} // switch (pnmh->code)
		break;

		// free memory
	case WM_DESTROY:
		SetWindowLongPtr(hEditFaxNum, GWLP_WNDPROC, (LONG_PTR)oldProc);
		DestroyIcon(hIconCancel);
		DestroyIcon(hIconOK);
		DestroyIcon(hIconDoc);
		if (FAXSend)
			DeleteFile(FAXSend->szFileName);
		if (chBuf) free(chBuf); // free chBuf = free memory of FAXSend
		chBuf = nullptr; // some code after free -> null pointer
		// free LDAP before HylaUI, HylaUI will be deleted in _tWinMain function
		if (HylaUI && HylaUI->GetPointerLDAPHandle())
		{
			// search thread
			iLDAPLoopReturn = 1; // exit thread loop and call endthreadex
			ReleaseSemaphore(ghHylaUISemaphore, 1, NULL);
			WaitForSingleObject(hLDAPSearch, 10000); // handle thread loop
			CloseHandle(hLDAPSearch); // thread
			hLDAPSearch = NULL;
			// unbind ldap
			ldap_unbind(HylaUI->GetPointerLDAPHandle());
			HylaUI->SetPointerLDAPHandle(NULL);
		}
		if (ghHylaUISemaphore)CloseHandle(ghHylaUISemaphore);
		ghHylaUISemaphore = NULL;
		DeleteCriticalSection(&HylaUICritSection);
		delete UserConfigure; // no need check null pointer
		UserConfigure = nullptr; // some code after wm_destroy, do nullptr
		return (INT_PTR)TRUE;

		// change backround color of edit if LDAP connect
	case WM_CTLCOLOREDIT:
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_EDIT_FAXNUMBER) && HylaUI->GetPointerLDAPHandle())
		{
			SetTextColor((HDC)wParam, RGB(0, 0, 0));
			SetBkColor((HDC)wParam, RGB(255, 255, 190));
			return (LRESULT)CreateSolidBrush(RGB(255, 255, 190));
		}
		break;

	case WM_COMMAND:
		INT_PTR iDialog;
		LPCWSTR filename;
		switch LOWORD(wParam)
		{
			// autocomplete text box
		case IDC_EDIT_FAXNUMBER:
			int iLen;
			if (HIWORD(wParam) == EN_CHANGE)
			{
				GetWindowText(hEditFaxNum, szFaxNumber, STRVALUE_FAXNUMBER);
				iLen = lstrlen(szFaxNumber);
				EnterCriticalSection(&HylaUICritSection);
				pAuto->DeleteAllStrings();
				LeaveCriticalSection(&HylaUICritSection);
				if (iLen > 0)
				{
					// add strings to autocomplete
					// phone book list view
					EnterCriticalSection(&HylaUICritSection);
					LVSearch(pAuto);
					LeaveCriticalSection(&HylaUICritSection);
					// ldap phone book
					if (HylaUI->GetPointerLDAPHandle() && szFaxNumber[iLen - 1] != '>')
					{
						StringCchCat(szFaxNumber, STRVALUE_FAXNUMBER, L"*");
						EnterCriticalSection(&HylaUICritSection);
						HylaUI->SetFAXNumberText(szFaxNumber);
						HylaUI->SetLDAPSearchFlag(TRUE);
						LeaveCriticalSection(&HylaUICritSection);
						ReleaseSemaphore(ghHylaUISemaphore, 1, NULL);
					}
				}
				else
				{
					EnterCriticalSection(&HylaUICritSection);
					HylaUI->SetLDAPSearchFlag(FALSE);
					LeaveCriticalSection(&HylaUICritSection);
				}
			} // if (HIWORD(wParam) == EN_CHANGE)
			break;
			// edit addrbook item, edit item from address book
		case LANG_ID_MENULV_EDIT:
		{
			curSel = ListView_GetNextItem(GetDlgItem(HylaUI->GetHandleDLG(), IDC_LIST_ADDRESS_BOOK), -1, LVNI_SELECTED);
			HylaUI->SetItemIndex(curSel);
			HylaUI->SetNewItem(FALSE);
			iDialog = DialogBox(hinst, MAKEINTRESOURCE(LANG_EDIT_DIALOG_ADDRBOOK_ITEM), hDlg, (DLGPROC)EditAddItem);
			if (iDialog != 1) // click cancel
				break;
			if (UserConfigure->GetUseUserConfig())
				filename = UserConfigure->GetAddressBookText();
			else
				filename = FAXSend->szAddrBookDir;
			// redraw items in virtual LV and send WM_PAINT to LV (updatewindow)
			ListView_RedrawItems(GetDlgItem(HylaUI->GetHandleDLG(), IDC_LIST_ADDRESS_BOOK), 0, (int)HylaUI->GetPBVectorSize() - 1);
			UpdateWindow(GetDlgItem(HylaUI->GetHandleDLG(), IDC_LIST_ADDRESS_BOOK));
			// write to file
			if (INVALID_HANDLE_VALUE == (hFile =
				CreateFileW(filename, GENERIC_WRITE, FILE_SHARE_READ,
					NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL)))
				break;
			else
			{
				SetFilePermissions(filename, L"S-1-1-0", GENERIC_READ | GENERIC_WRITE);
				CloseHandle(hFile);
				WriteLVToFile();
			}
		}
		break;
		// menu delete addrbook item
		case LANG_ID_MENULV_DELETE:
		{
			curSel = ListView_GetNextItem(GetDlgItem(HylaUI->GetHandleDLG(), IDC_LIST_ADDRESS_BOOK), -1, LVNI_SELECTED);
			HylaUI->DeletePhoneBookItem((int)curSel);
			HylaUI->SortPBVector();
			// item count for LVN_GETDISPINFO
			ListView_SetItemCount(GetDlgItem(HylaUI->GetHandleDLG(), IDC_LIST_ADDRESS_BOOK), (int)HylaUI->GetPBVectorSize());
			if (UserConfigure->GetUseUserConfig())
				filename = UserConfigure->GetAddressBookText();
			else
				filename = FAXSend->szAddrBookDir;
			if (INVALID_HANDLE_VALUE == (hFile =
				CreateFileW(filename, GENERIC_WRITE, FILE_SHARE_READ,
					NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL)))
				break;
			else
			{
				SetFilePermissions(filename, L"S-1-1-0", GENERIC_READ | GENERIC_WRITE);
				CloseHandle(hFile);
				WriteLVToFile();
			}
			SetWindowText(hEditFaxNum, L"");
		}
		break;
		// menu add addrbook item
		case LANG_ID_MENULV_ADD:
		{
			HylaUI->SetNewItem(TRUE);
			WriteTextItemEBSend(hEditFaxNum, HylaUI->GetFAXNumberText(), TRUE, STRVALUE_FAXNUMBER); // number
			iDialog = DialogBox(hinst, MAKEINTRESOURCE(LANG_EDIT_DIALOG_ADDRBOOK_ITEM), hDlg, (DLGPROC)EditAddItem);
			if (iDialog != 1) // cancel click
				break;
			if (UserConfigure->GetUseUserConfig())
				filename = UserConfigure->GetAddressBookText();
			else
				filename = FAXSend->szAddrBookDir;
			if (INVALID_HANDLE_VALUE == (hFile =
				CreateFileW(filename, GENERIC_WRITE, FILE_SHARE_READ,
					NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL)))
				break;
			else
			{
				SetFilePermissions(filename, L"S-1-1-0", GENERIC_READ | GENERIC_WRITE);
				CloseHandle(hFile);
				WriteLVToFile();
			}
		}
		break;
		// send fax button
		case LANG_SEND_FAX:
		{
			RECT rc;
			WriteTextItemEBSend(hEditFaxNum, HylaUI->GetFAXNumberText(), TRUE, STRVALUE_FAXNUMBER);
			WriteTextItemEBSend(GetDlgItem(hDlg, IDC_EDIT_NOTIFEMAIL), HylaUI->GetEmailText(), FALSE, STRVALUE_EMAIL);
			WriteTextItemEBSend(GetDlgItem(hDlg, IDC_EDIT_IDENTIFIER), HylaUI->GetIdentifierText(), FALSE, STRVALUE_IDENTIFIER);
			// send FAX
			if (UserConfigure->GetUseUserConfig()) // use user config data
				CopyUserSettingsData(); // copy user data to FAXSend class
			pcStatus = new cStatus;
			GetWindowRect(hDlg, &rc);
			HylaUI->SetFaxDialogRect(&rc);
			ShowWindow(hDlg, SW_HIDE);
			SendToHylafax();
			delete pcStatus;
			pcStatus = nullptr; // global variable - set to null
			PostMessage(hDlg, WM_CLOSE, 0, 0);
			//EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		case LANG_IDCANCEL:
		case IDCANCEL:
		{
			// save windows position
			SaveWindowPosition(hDlg);
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		} // case WM_COMMAND:
		break;
	}
	return (INT_PTR)FALSE;
}
//*********************************************************
// fax number edit box
LRESULT CALLBACK WindowProcEdit(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	DWORD dw = 0;
	switch (uMsg)
	{
	case WM_GETDLGCODE:
		return DLGC_WANTALLKEYS;
	case WM_CHAR:
		if (wParam == 0x0D) // Return key
		{
			SendMessage(hWnd, EM_SETSEL, (WPARAM)-1, (LPARAM)-1);
			return CallWindowProc(oldProc, hWnd, uMsg, wParam, lParam);
		}
		if (wParam == 0x08) // backspace
			return CallWindowProc(oldProc, hWnd, uMsg, wParam, lParam);

		SendMessage(hWnd, EM_GETSEL, (WPARAM)&dw, NULL);
		SendMessage(hWnd, WM_KEYDOWN, VK_RETURN, 0);
		SendMessage(hWnd, EM_SETSEL, (WPARAM)dw, (LPARAM)dw);
		SendMessage(hWnd, WM_KEYDOWN, VK_DOWN, 0);
		break;
	case WM_KEYUP:
		if (wParam == VK_DELETE || wParam == VK_BACK)
		{
			SendMessage(hWnd, EM_GETSEL, (WPARAM)&dw, NULL);
			SendMessage(hWnd, WM_KEYDOWN, VK_RETURN, 0);
			SendMessage(hWnd, EM_SETSEL, (WPARAM)dw, (LPARAM)dw);
			SendMessage(hWnd, WM_KEYDOWN, VK_DOWN, 0);
		}
		break;
	}
	return CallWindowProc(oldProc, hWnd, uMsg, wParam, lParam);
}
//*********************************************************
// foreground dialog SendFax
void SetForegroundDialog(HWND hwnd)
{
	HWND hCurrWnd;
	int iMyTID;
	DWORD iCurrTID;

	hCurrWnd = GetForegroundWindow();
	iMyTID = GetCurrentThreadId();
	iCurrTID = GetWindowThreadProcessId(hCurrWnd, 0);

	AttachThreadInput(iMyTID, iCurrTID, TRUE);
	SetForegroundWindow(hCurrWnd);
	SetForegroundWindow(hwnd);
	AttachThreadInput(iMyTID, iCurrTID, FALSE);
}
//*********************************************************
void WriteTextItemEBSend(HWND hEdit, LPWSTR str, BOOL bRegEx, size_t iBufLen)
{
	LPWSTR lpText;
	WCHAR szText2[STRVALUE_TEXT_512] = { 0 };
	int x;
	BOOL bFind = FALSE;

	lpText = (LPWSTR)HeapAlloc(GetProcessHeap(), 0, (STRVALUE_TEXT_512 + 1) * sizeof(WCHAR));
	if (lpText)
	{
		GetWindowText(hEdit, lpText, STRVALUE_TEXT_512);

		if (bRegEx)
		{
			x = lstrlen(lpText) - 1;
			for (x; x > 0; x--)
			{
				if (lpText[x] == '>')
				{
					swscanf_s(lpText, L"%*[^'<']<%[^'>']", szText2, (int)_countof(szText2));
					bFind = TRUE;
					break;
				}
			}
		}

		if (bFind && bRegEx) // fax number in format "%s <number>"
			wcsncpy_s(str, iBufLen, szText2, iBufLen - 1);
		else
			wcsncpy_s(str, iBufLen, lpText, iBufLen - 1);

		HeapFree(GetProcessHeap(), 0, lpText);
	}
}
//*********************************************************
BOOL IPCClient(HANDLE* hPipe)
{
	BOOL   fSuccess = FALSE;
	DWORD  dwMode;
	WCHAR lpszPipename[128];

	// Try to open a named pipe; wait for it, if necessary.
	StringCchPrintf(lpszPipename, _countof(lpszPipename), L"\\\\.\\pipe\\HylaPrintMonUI_%lu", GetSessionID());
	for (;;)
	{
		*hPipe = CreateFile(
			lpszPipename,   // pipe name
			GENERIC_READ |  // read and write access
			GENERIC_WRITE,
			0,              // no sharing
			NULL,           // default security attributes
			OPEN_EXISTING,  // opens existing pipe
			0,              // default attributes
			NULL);          // default attributes 

		// Break if the pipe handle is valid.
		if (*hPipe != INVALID_HANDLE_VALUE)
			break;

		// Exit if an error other than ERROR_PIPE_BUSY occurs.
		if (GetLastError() != ERROR_PIPE_BUSY)
		{
			CloseHandle(*hPipe);
			return FALSE;
		}

		// All pipe instances are busy, so wait for 10 seconds.
		if (!WaitNamedPipe(lpszPipename, 10000))
		{
			CloseHandle(*hPipe);
			return FALSE;
		}
	}
	// The pipe connected; change to bytes mode.
	dwMode = PIPE_READMODE_BYTE;
	fSuccess = SetNamedPipeHandleState(
		*hPipe,    // pipe handle
		&dwMode,  // new pipe mode
		NULL,     // don't set maximum bytes
		NULL);    // don't set maximum time

	if (!fSuccess)
		CloseHandle(*hPipe);

	return fSuccess;
}
// ***********************************************************************
BOOL WritePipe(PHANDLE hPipe, LPCWSTR lpvMessage)
{
	DWORD cbToWrite;
	DWORD cbWritten;
	cbToWrite = (lstrlen(lpvMessage) + 1) * sizeof(WCHAR);
	BOOL ret;

	ret = WriteFile(
		*hPipe,      // pipe handle
		lpvMessage,  // message
		cbToWrite,   // message length
		&cbWritten,  // bytes written
		NULL);       // no overlapped

	return ret;
}
// ***********************************************************************
BOOL ReadPipe(PHANDLE hPipe, WCHAR* buf, DWORD dwSize)
{
	DWORD cbRead;
	BOOL ret = FALSE;

	do
	{	// Read from the pipe.
		ret = ReadFile(
			*hPipe,    // pipe handle
			buf,       // buffer to receive reply
			dwSize,    // size of buffer
			&cbRead,   // number of bytes read
			NULL);     // no overlapped

		if (!ret && GetLastError() != ERROR_MORE_DATA)
			break;
	} while (!ret);  // repeat loop if ERROR_MORE_DATA

	return ret;
}

//---------------------------------------------------------------------------
CHylaUI::CHylaUI()
{
	szEmail[0] = L'\0';
	szFaxNumber[0] = L'\0';
	szIdentifier[0] = L'\0';
	pldHandle = NULL;
	bLDAPSearchFlag = FALSE;
	hDlg = NULL;
	iNewItem = FALSE;
	index = NULL;
	rcSocket = {};
}
CHylaUI::~CHylaUI()
{

}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
LPWSTR CHylaUI::GetEmailText()
{
	return szEmail;
}
void CHylaUI::SetEmailText(LPCWSTR text)
{
	wcsncpy_s(szEmail, _countof(szEmail), text, _TRUNCATE);
}
void CHylaUI::SetFAXNumberText(LPCWSTR text)
{
	wcsncpy_s(szFaxNumber, _countof(szFaxNumber), text, _TRUNCATE);
}
void CHylaUI::SetIdentifierText(LPCWSTR text)
{
	wcsncpy_s(szIdentifier, _countof(szIdentifier), text, _TRUNCATE);
}
LPWSTR CHylaUI::GetFAXNumberText()
{
	return szFaxNumber;
}
LPWSTR CHylaUI::GetIdentifierText()
{
	return szIdentifier;
}

HWND CHylaUI::GetHandleDLG()const
{
	return hDlg;
}
void CHylaUI::SetHandleDLG(HWND hwnd)
{
	hDlg = hwnd;
}
LRESULT CHylaUI::GetItemIndex()const
{
	return index;
}
void CHylaUI::SetItemIndex(LRESULT i)
{
	index = i;
}
void CHylaUI::SetNewItem(BOOL b)
{
	iNewItem = b;
}
BOOL CHylaUI::GetNewItem()
{
	return iNewItem;
}
void CHylaUI::SetFaxDialogRect(RECT* rc)
{
	rcSocket.left = rc->left;
	rcSocket.right = rc->right;
	rcSocket.top = rc->top;
	rcSocket.bottom = rc->bottom;
}
void CHylaUI::GetFaxDialogRect(LPRECT rc)
{
	rc->left = rcSocket.left;
	rc->right = rcSocket.right;
	rc->top = rcSocket.top;
	rc->bottom = rcSocket.bottom;
}
void CHylaUI::AddPhonebookItem(LPCWSTR text)
{
	phoneBook.push_back(text);
}
std::vector <std::wstring>::size_type CHylaUI::GetPBVectorSize()
{
	return phoneBook.size();
}
void CHylaUI::GetPhoneBookName(int i, LPWSTR text)
{
	WCHAR szName[STRVALUE_USER] = { 0 }, szNumber[STRVALUE_NUMBER] = { 0 };
	swscanf_s(phoneBook.at(i).c_str(), L"%255[^;];%255[^\n]", szName, STRVALUE_USER, szNumber, STRVALUE_NUMBER);
	wcsncpy_s(text, STRVALUE_USER, szName, _TRUNCATE);
}
void CHylaUI::GetPhoneBookNumber(int i, LPWSTR text)
{
	WCHAR szName[STRVALUE_USER] = { 0 }, szNumber[STRVALUE_NUMBER] = { 0 };
	swscanf_s(phoneBook.at(i).c_str(), L"%255[^;];%255[^\n]", szName, STRVALUE_USER, szNumber, STRVALUE_NUMBER);
	wcsncpy_s(text, STRVALUE_NUMBER, szNumber, _TRUNCATE);
}
void CHylaUI::DeletePhoneBookItem(int iPosition)
{
	phoneBook.erase(phoneBook.begin() + iPosition);
}
LDAP* CHylaUI::GetPointerLDAPHandle()
{
	return pldHandle;
}
void CHylaUI::SetPointerLDAPHandle(LDAP* pld)
{
	pldHandle = pld;
}
//***********************************************************
void LVMenu(HWND hWnd)
{
	HMENU hMenu = LoadMenu(hinst, MAKEINTRESOURCE(IDR_MENU_LV));
	HMENU hPopupMenu = GetSubMenu(hMenu, 0);
	POINT pt;
	SetMenuDefaultItem(hPopupMenu, (UINT)-1, TRUE);
	GetCursorPos(&pt);
	SetForegroundWindow(hWnd);
	// language
	LocalMenuLB(hPopupMenu);
	if (ListView_GetItemCount(GetDlgItem(HylaUI->GetHandleDLG(), IDC_LIST_ADDRESS_BOOK)) == 0 ||
		ListView_GetNextItem(GetDlgItem(HylaUI->GetHandleDLG(), IDC_LIST_ADDRESS_BOOK), -1, LVNI_SELECTED) == -1)
	{
		ModifyMenu(hPopupMenu, LANG_ID_MENULV_EDIT, MF_BYCOMMAND | MF_GRAYED, LANG_ID_MENULV_EDIT, _T(LNG_ID_MENULV_EDIT));
		ModifyMenu(hPopupMenu, LANG_ID_MENULV_DELETE, MF_BYCOMMAND | MF_GRAYED, LANG_ID_MENULV_DELETE, _T(LNG_ID_MENULV_DELETE));
	}
	TrackPopupMenu(hPopupMenu, TPM_LEFTALIGN, pt.x, pt.y, 0, hWnd, NULL);
	SetForegroundWindow(hWnd);

	DestroyMenu(hPopupMenu);
	DestroyMenu(hMenu);
}
//***********************************************************
void ReadAddrBook(LPCWSTR path)
{
	HANDLE hFile;
	DWORD iFileSize;
	LPBYTE lpBuffer, pConv, pText;
	int iUniTest, i;
	BYTE  bySwap;
	WCHAR* next_token = NULL;
	WCHAR* tokenRow = NULL;
	const WCHAR sepsRow[] = L"\x0D\x0A";
	std::vector<std::wstring> file;
	DWORD dw;

	HylaUI->ResetPBVector();
	ListView_SetItemCount(GetDlgItem(HylaUI->GetHandleDLG(), IDC_LIST_ADDRESS_BOOK), 0);

	hFile = CreateFile(path, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);
	if (hFile == INVALID_HANDLE_VALUE)
		return;

	iFileSize = GetFileSize(hFile, NULL);
	// Add an extra two bytes for zero termination.
	lpBuffer = (LPBYTE)HeapAlloc(GetProcessHeap(),
		0, (DWORD_PTR)iFileSize + 2);
	if (lpBuffer == NULL) // cannot allocate memory
	{
		CloseHandle(hFile);
		return;
	}

	if (!ReadFile(hFile, lpBuffer, iFileSize, &dw, NULL))
	{
		CloseHandle(hFile);
		HeapFree(GetProcessHeap(), 0, lpBuffer);
		return;
	}
	CloseHandle(hFile);
	lpBuffer[iFileSize] = '\0';
	lpBuffer[iFileSize + 1] = '\0';
	// Test Unicode
	iUniTest = IS_TEXT_UNICODE_SIGNATURE | IS_TEXT_UNICODE_REVERSE_SIGNATURE;

	if (IsTextUnicode(lpBuffer, iFileSize, &iUniTest))
	{
		pText = lpBuffer + 2;
		iFileSize -= 2;

		if (iUniTest & IS_TEXT_UNICODE_REVERSE_SIGNATURE)
		{
			for (i = 0; i < (int)(iFileSize / 2); i++)
			{
				bySwap = ((BYTE*)pText)[2 * i];
				((BYTE*)pText)[2 * i] = ((BYTE*)pText)[2 * i + 1];
				((BYTE*)pText)[2 * i + 1] = bySwap;
			}
		}

		// allocate memory
		pConv = (LPBYTE)HeapAlloc(GetProcessHeap(),
			0, (DWORD_PTR)iFileSize + 2);
		if (pConv == NULL) // fail - allocate memory
		{
			HeapFree(GetProcessHeap(), 0, lpBuffer);
			return;
		}

		// convert unicode to multibyte
#ifndef UNICODE
		WideCharToMultiByte(CP_ACP, 0, (PWSTR)pText, -1, (LPSTR)pConv,
			iFileSize + 2, NULL, NULL);

		// if Unicode - only copy string
#else
		lstrcpy((PTSTR)pConv, (PTSTR)pText);
#endif
	}

	else      // no Unicode file
	{
		pText = lpBuffer;

		// allocate memory
		pConv = (LPBYTE)HeapAlloc(GetProcessHeap(), 0, (2 * (DWORD_PTR)iFileSize + 2));
		if (pConv == NULL) // fail - allocate memory
		{
			HeapFree(GetProcessHeap(), 0, lpBuffer);
			return;
		}

		// convert ASCII to unicode
#ifdef UNICODE
		MultiByteToWideChar(CP_ACP, 0, (LPCSTR)pText, -1, (PTSTR)pConv,
			iFileSize + 1);

		// only copy
#else
		lstrcpy((PTSTR)pConv, (PTSTR)pText);
#endif
	}
	// insert strings into vector
	tokenRow = wcstok_s((PTSTR)pConv, sepsRow, &next_token);
	for (; tokenRow;)
	{
		HylaUI->AddPhonebookItem(tokenRow);
		tokenRow = _tcstok_s(NULL, sepsRow, &next_token);
	}
	ListView_SetItemCount(GetDlgItem(HylaUI->GetHandleDLG(), IDC_LIST_ADDRESS_BOOK), (int)HylaUI->GetPBVectorSize());
	HylaUI->SortPBVector();

	HeapFree(GetProcessHeap(), 0, lpBuffer);
	HeapFree(GetProcessHeap(), 0, pConv);
}
//***********************************************************

// *******************************************************************************
// write Listbox to file
void WriteLVToFile()
{
	WCHAR* szBuffer;
	size_t iLength;
	DWORD  dwBytesWritten;
	HANDLE hFile;
	WORD   wByteOrderMark = 0xFEFF;
	DWORD iSize;
	LPCWSTR filename;


	if (UserConfigure->GetUseUserConfig())
		filename = UserConfigure->GetAddressBookText();
	else
		filename = FAXSend->szAddrBookDir;
	// allocate memory
	szBuffer = (WCHAR*)HeapAlloc(GetProcessHeap(),
		0, STRVALUE_TEXT_512 * sizeof(WCHAR));
	if (!szBuffer) // fail
		return;
	szBuffer[0] = L'\0';

	if (INVALID_HANDLE_VALUE ==
		(hFile = CreateFileW(filename, GENERIC_WRITE, FILE_SHARE_READ,
			NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL)))
	{
		// free memory
		HeapFree(GetProcessHeap(), 0, szBuffer);
		return;
	}
	iSize = GetFileSize(hFile, NULL);

	// if text is Unicode- write byte order mark
	if (iSize == 0)
		WriteFile(hFile, &wByteOrderMark, 2, &dwBytesWritten, NULL);

	// end of file
	SetFilePointer(hFile, 0, NULL, FILE_END);

	for (int i = 0; i < (int)HylaUI->GetPBVectorSize(); i++)
	{
		HylaUI->GetPhoneBookItem(i, szBuffer, STRVALUE_TEXT_512);
		iLength = wcslen(szBuffer);
		WriteFile(hFile, szBuffer, static_cast<DWORD>(iLength * sizeof(WCHAR)), &dwBytesWritten, NULL);
		// end row
		WriteFile(hFile, L"\x0D\x0A", 4, &dwBytesWritten, NULL);
	}

	CloseHandle(hFile);
	// free memory
	HeapFree(GetProcessHeap(), 0, szBuffer);
}
// *******************************************************************************
// add column
BOOL InitListViewColumns()
{
	LVCOLUMN lvc;
	RECT rc;
	HWND hList;

	hList = GetDlgItem(HylaUI->GetHandleDLG(), IDC_LIST_ADDRESS_BOOK);

	GetWindowRect(hList, &rc);
	// Initialize the LVCOLUMN structure.
	// The mask specifies that the format, width, text, and subitem members
	// of the structure are valid. 
	lvc.mask = LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;

	// Add the columns.
	lvc.iSubItem = 0;
	lvc.pszText = L"Name";
	lvc.cx = rc.right - rc.left - 5;     // width of column in pixels

	// Insert the columns into the list view.
	if (ListView_InsertColumn(hList, 1, &lvc) == -1)
		return FALSE;

	SendMessage(hList, LVM_SETEXTENDEDLISTVIEWSTYLE, 0, LVS_EX_FULLROWSELECT);
	return TRUE;
}

//*********************************************************
// version
void GetVersionHylaUI(HWND hStatus)
{
	DWORD dwVerInfoSize;
	DWORD dwHandle = 0;
	LPWSTR szDir;
	int iBufSize = MAX_PATH;
	WCHAR szFileVer[64];
	WCHAR* pTemp;
	VS_FIXEDFILEINFO* pBuff = nullptr;
	UINT nInfoLen;

	szDir = (LPWSTR)HeapAlloc(GetProcessHeap(), 0,
		iBufSize * sizeof(WCHAR));
	if (!szDir) // fail
		return;
	szDir[0] = L'\0';
	if (GetModuleFileName(hinst, szDir, iBufSize) > 0)
	{
		dwVerInfoSize = GetFileVersionInfoSize(szDir, &dwHandle);
		pTemp = (WCHAR*)malloc(dwVerInfoSize);
		if (pTemp)
		{
			GetFileVersionInfo(szDir, (DWORD)NULL, dwVerInfoSize, pTemp);
			VerQueryValue(pTemp, L"\\", (LPVOID*)&pBuff, &nInfoLen);

			StringCbPrintf(szFileVer, sizeof(szFileVer), L"%d.%d.%d.%d",
				HIWORD(pBuff->dwFileVersionMS),
				LOWORD(pBuff->dwFileVersionMS),
				HIWORD(pBuff->dwFileVersionLS),
				LOWORD(pBuff->dwFileVersionLS));

			SendMessage(hStatus, SB_SETTEXT, 0, (LPARAM)szFileVer);
			free(pTemp);
		}
	}
	// free memory
	HeapFree(GetProcessHeap(), 0, szDir);
}
// *************************************************************
BOOL SetFilePermissions(LPCWSTR filename, LPCWSTR username, int permissions)
{
	EXPLICIT_ACCESS eAcc;

	PSID pSid = NULL;
	PACL dacl = NULL;
	int lRes = ERROR_SUCCESS;

	eAcc.grfAccessMode = GRANT_ACCESS;
	eAcc.grfAccessPermissions = permissions;
	eAcc.grfInheritance = OBJECT_INHERIT_ACE | CONTAINER_INHERIT_ACE;
	eAcc.Trustee.MultipleTrusteeOperation = NO_MULTIPLE_TRUSTEE;
	eAcc.Trustee.pMultipleTrustee = NULL;
	eAcc.Trustee.TrusteeType = TRUSTEE_IS_WELL_KNOWN_GROUP;

	// NOTE: In some cases, you will want to use a "well-known security identifiers"
	//       stead of a username or group
	//       since SIDs remain the same from one operating system language to another.
	if (ConvertStringSidToSid(username, &pSid))
	{
		eAcc.Trustee.TrusteeForm = TRUSTEE_IS_SID;
		eAcc.Trustee.ptstrName = (LPWSTR)pSid;
	}
	else
	{
		// Reset lasterror since ConvertSidToStringSid() is also used
		// to determine if a username is a SID or not.
		SetLastError(0);
		eAcc.Trustee.TrusteeForm = TRUSTEE_IS_NAME;
		eAcc.Trustee.ptstrName = (LPWSTR)username;
	}

	// Create a DACL
	lRes = SetEntriesInAcl(1, &eAcc, NULL, &dacl);
	if (lRes == ERROR_SUCCESS)
	{
		// Set DACL
		lRes = SetNamedSecurityInfo((LPWSTR)filename, SE_FILE_OBJECT,
			DACL_SECURITY_INFORMATION, NULL, NULL, dacl, NULL);
	}

	if (pSid != NULL)
		LocalFree((HLOCAL)pSid);

	if (dacl != NULL)
		LocalFree((HLOCAL)dacl);

	return lRes == ERROR_SUCCESS;
}
// *************************************************************
// search listview
void LVSearch(CEnumString* plAuto)
{
	WCHAR szName[STRVALUE_ADDRBOOK_NAME];
	WCHAR szNumber[STRVALUE_NUMBER];
	WCHAR szText[STRVALUE_TEXT_512];

	for (int i = 0; i < (int)HylaUI->GetPBVectorSize(); i++)
	{
		HylaUI->GetPhoneBookName(i, szName);
		HylaUI->GetPhoneBookNumber(i, szNumber);
		StringCbPrintf(szText, sizeof(szText), L"%s <%s>", szName, szNumber);
		plAuto->AddString(szText);
	}
}
// *************************************************************
// config dialog
INT_PTR CallConfigDialog(HWND hDlg)
{
	INT_PTR iDialog;
	iDialog = DialogBox(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_DIALOG_USER_SETTINGS), hDlg, UserSettingsGUI);
	if (iDialog == 1 && UserConfigure->GetUseUserConfig()) // OK & single user
		SetDlgItemText(hDlg, IDC_EDIT_NOTIFEMAIL, UserConfigure->GetEmailText());
	if (!UserConfigure->GetUseUserConfig())
		SetDlgItemText(hDlg, IDC_EDIT_NOTIFEMAIL, FAXSend->szEmail);
	return iDialog;
}
// *************************************************************
// copy data from user settings dialog to FAXSend class (only data needed send to hylafax)
void CopyUserSettingsData()
{
	//  edit box
	wcsncpy_s(FAXSend->szUser, _countof(FAXSend->szUser), UserConfigure->GetUsernameText(), _TRUNCATE);
	wcsncpy_s(FAXSend->szPasswd, _countof(FAXSend->szPasswd), UserConfigure->GetPasswordText(), _TRUNCATE);
	wcsncpy_s(FAXSend->szEmail, _countof(FAXSend->szEmail), UserConfigure->GetEmailText(), _TRUNCATE);
	wcsncpy_s(FAXSend->szAddrBookDir, _countof(FAXSend->szAddrBookDir), UserConfigure->GetAddressBookText(), _TRUNCATE);
	wcsncpy_s(FAXSend->szModem, _countof(FAXSend->szModem), UserConfigure->GetModemText(), _TRUNCATE);
	// combo box
	wcsncpy_s(FAXSend->szPageSize, _countof(FAXSend->szPageSize), UserConfigure->GetPageSizeText(), _TRUNCATE);
	wcsncpy_s(FAXSend->szResolution, _countof(FAXSend->szResolution), UserConfigure->GetResolutionText(), _TRUNCATE);
	wcsncpy_s(FAXSend->szNotification, _countof(FAXSend->szNotification), UserConfigure->GetNotificationText(), _TRUNCATE);
	wcsncpy_s(FAXSend->szMaxDials, _countof(FAXSend->szMaxDials), UserConfigure->GetMaxDialsText(), _TRUNCATE);
	wcsncpy_s(FAXSend->szMaxTries, _countof(FAXSend->szMaxTries), UserConfigure->GetMaxTriesText(), _TRUNCATE);
	wcsncpy_s(FAXSend->szInfoDialogTimer, _countof(FAXSend->szInfoDialogTimer), UserConfigure->GetInfoDialogTimerText(), _TRUNCATE);
}
// *************************************************************
// status bar
void CreateStatusBar(HWND hDlg, HICON icon)
{
	HWND hStatus;
	RECT rc;
	hStatus = CreateWindowEx(0, STATUSCLASSNAME,
		NULL, WS_CHILD | WS_VISIBLE,
		0, 0, 0, 0, hDlg, (HMENU)IDC_DLG_STATUS, GetModuleHandle(NULL), NULL);
	GetClientRect(hDlg, &rc);
	int status_parts[2] = { rc.right - rc.left - 350,
		rc.right - rc.left };
	SendMessage(hStatus, SB_SETPARTS, 2, (LPARAM)&status_parts);
	// version
	GetVersionHylaUI(hStatus);
	SendMessage(hStatus, SB_SETICON, 1, (LPARAM)icon);
	SendMessage(hStatus, SB_SETTEXT, 1, (LPARAM)FAXSend->szDocument);
}
// *************************************************************
// case in-sensitive sort
bool ciCharLess(wchar_t c1, wchar_t c2)
{
	return (std::tolower(static_cast<wchar_t>(c1)) < std::tolower(static_cast<wchar_t>(c2)));
}

bool CompareNoCase(const std::wstring& s1, const std::wstring& s2)
{
	return std::lexicographical_compare(s1.begin(), s1.end(), s2.begin(), s2.end(), ciCharLess);
}
// *************************************************************
// detect IPC
BOOL DetectIPCClient(WCHAR* chBuf)
{
	BOOL ret, bSuc;
	HANDLE hPipe;

	ret = IPCClient(&hPipe);
	if (ret) // OK
	{
		chBuf = (WCHAR*)malloc(sizeof(struct TFAXSend)); // free will be called in WM_DESTROY
		bSuc = WritePipe(&hPipe, L"CONNECT"); // send first message to other process
		bSuc = ReadPipe(&hPipe, chBuf, sizeof(struct TFAXSend));
		CloseHandle(hPipe);

		if (bSuc)
			FAXSend = reinterpret_cast<struct TFAXSend*>(chBuf);
		else
			return FALSE;
	}
	// no pipe,no handle pipe - end dialog
	else
		return FALSE;

	return TRUE;
}
// *************************************************************
// initialize address book path
void InitAddrbookPath()
{
	// address book
	if (wcslen(FAXSend->szAddrBookDir) > 2)
		wcsncat_s(FAXSend->szAddrBookDir, _countof(FAXSend->szAddrBookDir),
			L"\\addrbook.csv", _TRUNCATE);
	// addrbook system-wide
	if (!UserConfigure->GetUseUserConfig() && wcslen(FAXSend->szAddrBookDir) > 2)
		ReadAddrBook(FAXSend->szAddrBookDir);
	// addrbook single user
	if (UserConfigure->GetUseUserConfig() && wcslen(UserConfigure->GetAddressBookText()) > 2)
	{
		wcsncat_s(UserConfigure->GetAddressBookText(), MAX_PATH,
			L"\\addrbook.csv", _TRUNCATE);
		ReadAddrBook(UserConfigure->GetAddressBookText());
	}
}
// *************************************************************
// set dialog position
void SetDialogPosition(HWND hDlg)
{
	POINT ptCur;
	RECT rect;

	if (UserConfigure->GetIntWinCenter())
	{
		CenterDialogWindow(hDlg);
		return;
	}
	else if (UserConfigure->GetIntWinRememberPos())
	{
		SetWindowPos(hDlg,
			HWND_TOP,
			UserConfigure->GetXWinPos(),
			UserConfigure->GetYWinPos(),
			0, 0,          // Ignores size arguments.
			SWP_NOSIZE);
		return;
	}
	else if (UserConfigure->GetIntWinCursorPos())
	{
		GetCursorPos(&ptCur);
		GetWindowRect(hDlg, &rect);
		SetWindowPos(hDlg,
			HWND_TOP,
			ptCur.x - ((rect.right - rect.left) / 2),
			ptCur.y - (rect.bottom - rect.top),
			0, 0,          // Ignores size arguments.
			SWP_NOSIZE);
		return;
	}
	else
		CenterDialogWindow(hDlg);

}
// *************************************************************
// modify systemmenu
void ModifySysmenu(HWND hDlg)
{
	// menu will be destroyed automatically - is assigned to a window
	HMENU hMenu = LoadMenu(hinst, MAKEINTRESOURCE(IDR_SYSMENU_WINDOWPOSITION));
	hMenu = GetSubMenu(hMenu, 0);
	// local items
	ModifyMenu(hMenu, LANG_ID_WINDOWPOSITION_CENTER, MF_BYCOMMAND | MF_STRING, LANG_ID_WINDOWPOSITION_CENTER, _T(LNG_WINDOW_CENTER));
	ModifyMenu(hMenu, LANG_ID_WINDOWPOSITION_REMEMBERPOSITION, MF_BYCOMMAND | MF_STRING, LANG_ID_WINDOWPOSITION_REMEMBERPOSITION, _T(LNG_WINDOW_REMEMBER_POS));
	ModifyMenu(hMenu, LANG_ID_WINDOWPOSITION_CURSORPOSITION, MF_BYCOMMAND | MF_STRING, LANG_ID_WINDOWPOSITION_CURSORPOSITION, _T(LNG_WINDOW_CURSOR_POS));
	// append menu to systemmenu
	AppendMenu(GetSystemMenu(hDlg, FALSE), MF_SEPARATOR, 0, NULL);
	AppendMenu(GetSystemMenu(hDlg, FALSE), MF_STRING, IDR_SYSMENU_SETTINGS, _T(LNG_ID_SYSMENU_USER_SETTINGS));
	AppendMenu(GetSystemMenu(hDlg, FALSE), MF_SEPARATOR, 0, NULL);
	AppendMenu(GetSystemMenu(hDlg, FALSE), MF_STRING | MF_POPUP, (UINT_PTR)hMenu, _T(LNG_WINDOW_POSITION));

	if (UserConfigure->GetIntWinCenter())
		CheckMenuRadioItem(GetSystemMenu(hDlg, FALSE), LANG_ID_WINDOWPOSITION_CENTER,
			LANG_ID_WINDOWPOSITION_CURSORPOSITION, LANG_ID_WINDOWPOSITION_CENTER, MF_BYCOMMAND);
	else if (UserConfigure->GetIntWinRememberPos())
		CheckMenuRadioItem(GetSystemMenu(hDlg, FALSE), LANG_ID_WINDOWPOSITION_CENTER,
			LANG_ID_WINDOWPOSITION_CURSORPOSITION, LANG_ID_WINDOWPOSITION_REMEMBERPOSITION, MF_BYCOMMAND);
	else if (UserConfigure->GetIntWinCursorPos())
		CheckMenuRadioItem(GetSystemMenu(hDlg, FALSE), LANG_ID_WINDOWPOSITION_CENTER,
			LANG_ID_WINDOWPOSITION_CURSORPOSITION, LANG_ID_WINDOWPOSITION_CURSORPOSITION, MF_BYCOMMAND);
	else
		CheckMenuRadioItem(GetSystemMenu(hDlg, FALSE), LANG_ID_WINDOWPOSITION_CENTER,
			LANG_ID_WINDOWPOSITION_CURSORPOSITION, LANG_ID_WINDOWPOSITION_CENTER, MF_BYCOMMAND);
}
// *************************************************************
// save window position
void SaveWindowPosition(HWND hDlg)
{
	tinyxml2::XMLElement* windowposition, * elem, * root;
	tinyxml2::XMLDocument doc;
	tinyxml2::XMLDeclaration* decl;
	WCHAR szWidecharString[MAX_PATH];
	CHAR szMultibyteString[STRVALUE_TEXT_512];
	RECT rect;
	MENUITEMINFO mii;
	HMENU hmenu;

	// user folder
	GetUserFolder(szWidecharString);
	UnicodeToAnsi(szWidecharString, szMultibyteString, _countof(szMultibyteString));

	if (doc.LoadFile(szMultibyteString) != tinyxml2::XML_SUCCESS)
	{
		decl = doc.NewDeclaration();
		doc.InsertFirstChild(decl);
		root = doc.NewElement(APP_NAME);
	}
	else
		root = doc.FirstChildElement(APP_NAME);
	if (!root) return;

	windowposition = root->FirstChildElement(HYLAFAX_WINDOW_POSITION);
	if (!windowposition)
		windowposition = doc.NewElement(HYLAFAX_WINDOW_POSITION);
	if (!windowposition)
		return;

	// radio button menu
	ZeroMemory(&mii, sizeof(mii));
	mii.cbSize = sizeof(MENUITEMINFO);
	mii.fMask = MIIM_STATE;
	hmenu = GetSystemMenu(hDlg, FALSE);
	// center window
	elem = windowposition->FirstChildElement(HYLAFAX_WINDOW_CENTER);
	if (!elem)
		elem = doc.NewElement(HYLAFAX_WINDOW_CENTER);
	if (!elem)
		return;
	GetMenuItemInfo(hmenu, LANG_ID_WINDOWPOSITION_CENTER,
		FALSE, &mii);
	mii.fState& MFS_CHECKED ? elem->SetText(1) : elem->SetText(0);
	windowposition->InsertEndChild(elem);
	// remember position
	elem = windowposition->FirstChildElement(HYLAFAX_WINDOW_REMEMBERPOSITION);
	if (!elem)
		elem = doc.NewElement(HYLAFAX_WINDOW_REMEMBERPOSITION);
	if (!elem)
		return;
	GetMenuItemInfo(hmenu, LANG_ID_WINDOWPOSITION_REMEMBERPOSITION,
		FALSE, &mii);
	mii.fState& MFS_CHECKED ? elem->SetText(1) : elem->SetText(0);
	windowposition->InsertEndChild(elem);
	// cursor position
	elem = windowposition->FirstChildElement(HYLAFAX_WINDOW_CURSORPOSITION);
	if (!elem)
		elem = doc.NewElement(HYLAFAX_WINDOW_CURSORPOSITION);
	if (!elem)
		return;
	GetMenuItemInfo(hmenu, LANG_ID_WINDOWPOSITION_CURSORPOSITION,
		FALSE, &mii);
	mii.fState& MFS_CHECKED ? elem->SetText(1) : elem->SetText(0);
	windowposition->InsertEndChild(elem);
	GetWindowRect(hDlg, &rect);
	// x window position
	elem = windowposition->FirstChildElement(HYLAFAX_WINDOW_XPOS);
	if (!elem)
		elem = doc.NewElement(HYLAFAX_WINDOW_XPOS);
	if (!elem)
		return;
	elem->SetText(rect.left);
	windowposition->InsertEndChild(elem);
	// y window position
	elem = windowposition->FirstChildElement(HYLAFAX_WINDOW_YPOS);
	if (!elem)
		elem = doc.NewElement(HYLAFAX_WINDOW_YPOS);
	if (!elem)
		return;
	elem->SetText(rect.top);
	windowposition->InsertEndChild(elem);

	root->InsertEndChild(windowposition);
	doc.InsertEndChild(root);
	doc.SetBOM(true);
	// save xml
	doc.SaveFile(szMultibyteString);
}
// *************************************************************
// set faxnumber from the clipboard
void SetClipBoardFaxNumber(HWND hDlg)
{
	DWORD dwFaxNumberFromClipboard;
	int iCaret;

	if (FAXSend == NULL || UserConfigure == NULL)
		return;

	if (!UserConfigure->GetUseUserConfig()) // default
		dwFaxNumberFromClipboard = FAXSend->dwFaxNumberFromClipboard;
	else
		dwFaxNumberFromClipboard = UserConfigure->GetUseFaxnumberFromClipboard();

	if (!dwFaxNumberFromClipboard) // faxnumber from the clipboard is not checked, return
		return;

	if (!IsClipboardFormatAvailable(CF_UNICODETEXT))
		return;

	if (!OpenClipboard(hDlg))
		return;

	HGLOBAL hglb;
	LPWSTR lpClipText;
	hglb = GetClipboardData(CF_UNICODETEXT);
	if (hglb == NULL)
	{
		CloseClipboard();
		return;
	}

	lpClipText = (LPWSTR)GlobalLock(hglb);
	if (lpClipText == NULL)
	{
		CloseClipboard();
		return;
	}

	// check null or endline characters at 0 index of clipboard text
	if (lpClipText[0] == 0 || lpClipText[0] == '\n')
	{
		GlobalUnlock(hglb);
		CloseClipboard();
		return;
	}
	iCaret = lstrlen(lpClipText);
	SetDlgItemText(hDlg, IDC_EDIT_FAXNUMBER, lpClipText);
	GlobalUnlock(hglb);
	CloseClipboard();
	// set caret to the last possition of the editbox faxnumber
	SendDlgItemMessage(hDlg, IDC_EDIT_FAXNUMBER, EM_SETSEL, iCaret, iCaret);
	return;
}
// *************************************************************
// *************************************************************
