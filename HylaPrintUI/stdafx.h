// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN  // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#include <stdio.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

// TODO: reference additional headers your program requires here
#include <CommCtrl.h>
#include <string>
#include <vector> 
#include <algorithm>    // std::sort
#include <Winver.h>
#include <AccCtrl.h>
#include <Sddl.h>
#include <Aclapi.h>
#include <Wtsapi32.h>
#include <Shlobj.h>
#include <Shlwapi.h>
#include <atlstr.h>
#include <iphlpapi.h>
#include <Strsafe.h>
#include <Winldap.h>
#include <Winber.h>

// additional lib
#pragma comment (lib, "ComCtl32.lib")
#pragma comment (lib, "Version.lib")
#pragma comment (lib, "Wtsapi32.lib")
#pragma comment (lib, "Ws2_32.lib")
#pragma comment(lib, "IPHLPAPI.lib")
#pragma comment(lib, "Wldap32.lib")
