//---------------------------------------------------------------------------
// autocomplete fax number box
#include "stdafx.h"
#include "autocomplete.h"

void initAuto(HWND hEdit, CEnumString* pAuto)
{
	IAutoComplete2* m_pac1;
	IAutoComplete2* pac2;
	IEnumString* m_pes;
	HRESULT hr;

	hr = CoCreateInstance(CLSID_AutoComplete, NULL,
		CLSCTX_INPROC_SERVER, IID_IAutoComplete2,
		(LPVOID*)&m_pac1);
	if (hr != S_OK) // fail
		return;

	pAuto->QueryInterface(IID_IEnumString, (LPVOID*)&m_pes);

	m_pac1->Init(hEdit, m_pes, NULL, NULL);
	m_pes->Release();
	m_pac1->QueryInterface(IID_IAutoComplete2, (LPVOID*)&pac2);
	m_pac1->Release();

	pac2->SetOptions(ACO_AUTOSUGGEST | ACO_UPDOWNKEYDROPSLIST | ACO_WORD_FILTER);
	pac2->Release();
}
//---------------------------------------------------------------------------
CEnumString::CEnumString()
{
	iCur = 0;
	iRef = 0;
}
//---------------------------------------------------------------------------
CEnumString::~CEnumString()
{
}
//---------------------------------------------------------------------------
HRESULT CEnumString::QueryInterface(REFIID riid, void** ppvObject)
{
	if (ppvObject == NULL)
		return E_POINTER;

	if (riid == IID_IEnumString)
	{
		*ppvObject = (IEnumString*)this;
		((IEnumString*)(*ppvObject))->AddRef();
		return S_OK;
	}
	else if (riid == IID_IUnknown)
	{
		*ppvObject = (IUnknown*)this;
		((IEnumString*)(*ppvObject))->AddRef();
		return S_OK;
	}

	else
		return E_NOINTERFACE;
}
//---------------------------------------------------------------------------
ULONG CEnumString::AddRef()
{
	iRef++;
	return iRef;
}
//---------------------------------------------------------------------------
ULONG CEnumString::Release()
{
	if (iRef > 0)
		iRef--;
	if (iRef == 0)
	{
		delete this;
		return 0;
	}
	return iRef;
}
//---------------------------------------------------------------------------
HRESULT CEnumString::Reset()
{
	iCur = 0;
	return S_OK;
}
//---------------------------------------------------------------------------
HRESULT CEnumString::Skip(ULONG celt)
{
	std::vector<std::wstring>::size_type iSize;
	iSize = m_vector.size();

	if ((ULONG)iSize <= (celt + iCur))
	{
		iCur = iSize - 1;
		return S_FALSE;
	}

	iCur = iCur + celt;
	return S_OK;
}
//---------------------------------------------------------------------------
HRESULT CEnumString::Next(ULONG celt, LPOLESTR* rgelt, ULONG* pceltFetched)
{
	std::vector<std::wstring>::size_type iSize;

	iSize = m_vector.size();

	if (rgelt == NULL || (celt > 1 && pceltFetched == NULL))
		return S_FALSE;
	if (iCur >= iSize)
		return S_FALSE;

	if (celt > static_cast<ULONG>(iSize - iCur))
		celt = static_cast<ULONG>(iSize - iCur);

	if (pceltFetched != NULL)
		*pceltFetched = celt;

	for (ULONG i = 0; i < celt; i++)
	{
		BSTR bstr = ::SysAllocString(m_vector[iCur++].c_str());
		Copy(rgelt++, &bstr);
		::SysFreeString(bstr);
	}
	return S_OK;
}
//---------------------------------------------------------------------------
HRESULT CEnumString::Clone(IEnumString** ppEnum)
{
	std::vector<std::wstring>::size_type iSize, iFor;
	HRESULT hRes;
	iSize = m_vector.size();

	if (ppEnum != NULL)
	{
		CEnumString* p = NULL;
		p = new CEnumString;
		if (p == NULL)
		{
			*ppEnum = NULL;
			return E_OUTOFMEMORY;
		}
		else
		{
			for (iFor = 0; iFor < iSize; iFor++)
				p->AddString((LPCWSTR)m_vector[iFor].c_str());
			hRes = p->QueryInterface(IID_IEnumString, (void**)ppEnum);
			if (FAILED(hRes))
			{
				delete p;
				return E_UNEXPECTED;
			}
			delete p;
		}
	}
	else
		return E_INVALIDARG;

	return S_OK;
}
//---------------------------------------------------------------------------
int CEnumString::AddString(LPCWSTR szStr)
{
	m_vector.push_back(szStr);
	return S_OK;
}
//---------------------------------------------------------------------------
HRESULT CEnumString::Copy(LPOLESTR* p1, LPOLESTR* p2)
{
	HRESULT hr = S_OK;
	int iSize;
	int iLen;

	iLen = ocslen(*p2) + 1;
	iSize = sizeof(OLECHAR) * (iLen);

	(*p1) = (LPOLESTR)CoTaskMemAlloc(iSize);
	if (*p1 == NULL)
		hr = E_OUTOFMEMORY;
	else
		ocscpy_s(*p1, iLen, *p2);
	return hr;
}
//---------------------------------------------------------------------------
void CEnumString::DeleteAllStrings()
{
	m_vector.clear();
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
